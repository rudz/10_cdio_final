CREATE DATABASE  IF NOT EXISTS `grp10` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `grp10`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 62.79.16.16    Database: grp10
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `entry` varchar(50) NOT NULL,
  `level` varchar(15) NOT NULL,
  `msg` varchar(1000) NOT NULL,
  `opr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `opr_id` FOREIGN KEY (`id`) REFERENCES `operatoer` (`opr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 COMMENT='Simple test log table for use with log4j';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operatoer`
--

DROP TABLE IF EXISTS `operatoer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operatoer` (
  `opr_id` int(11) NOT NULL AUTO_INCREMENT,
  `opr_navn` text,
  `ini` text,
  `cpr` text,
  `password` text,
  `salt` text,
  `lvl` int(11) NOT NULL,
  PRIMARY KEY (`opr_id`),
  KEY `lvl` (`lvl`),
  CONSTRAINT `operatoer_ibfk_1` FOREIGN KEY (`lvl`) REFERENCES `roller` (`lvl`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operatoer`
--

LOCK TABLES `operatoer` WRITE;
/*!40000 ALTER TABLE `operatoer` DISABLE KEYS */;
INSERT INTO `operatoer` VALUES (1,'rudzen','rud','241102-9959','f7255cce0fdfa5fac3f0dcd920d308960ebf24f9f1611d2ff6ce18ee0245d6a5f34f27f82529f4c2016789565cafcdb7f6373fdbf3d4422341aea84ec67f4127','VW80VD9vV04tQTRiS2xwQj1RVzVJNjY9K3M4K21B',9),(2,'Antonella B','AB','241102-9967','bde842f2d3d1a9656dff7e6e22dc6d165ea2b8ce70c9a18270c24ef6a2f900008aa850c6574c2af3ac8ad01e510fea057d13e7865fba5567353bb6bad65d1d57','Yy1nOGoyRCFfP3I9ODU9UzdnLVBiYTdNPTZxNmM0',1),(3,'Kei B','AB','241102-9967','f7255cce0fdfa5fac3f0dcd920d308960ebf24f9f1611d2ff6ce18ee0245d6a5f34f27f82529f4c2016789565cafcdb7f6373fdbf3d4422341aea84ec67f4127','VW80VD9vV04tQTRiS2xwQj1RVzVJNjY9K3M4K21B',2),(4,'Angelo A','AA','241102-9959','2c2593c0e270cd949d84ef48ce9a01c6703db46a9993110a00f8d3e6592edc42356eb29646b6ba447bba763084be325e421c8998fdedefd056e7fd04d1003951','QT15NlY9YzdLSU0tK1U1QlE2ZzVFTi13eC5kYT9N',3);
/*!40000 ALTER TABLE `operatoer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produktbatch`
--

DROP TABLE IF EXISTS `produktbatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produktbatch` (
  `pb_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '0',
  `recept_id` int(11) DEFAULT NULL,
  `oprettet` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prod_start` datetime DEFAULT NULL,
  `prod_slut` datetime DEFAULT NULL,
  PRIMARY KEY (`pb_id`),
  KEY `recept_id` (`recept_id`),
  CONSTRAINT `produktbatch_ibfk_1` FOREIGN KEY (`recept_id`) REFERENCES `recept` (`recept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produktbatch`
--

LOCK TABLES `produktbatch` WRITE;
/*!40000 ALTER TABLE `produktbatch` DISABLE KEYS */;
INSERT INTO `produktbatch` VALUES (1,0,1,'2015-06-18 11:12:29',NULL,NULL),(2,0,1,'2015-06-18 11:12:29',NULL,NULL),(3,0,2,'2015-06-18 11:12:29',NULL,NULL),(4,0,3,'2015-06-18 11:12:29',NULL,NULL),(5,0,3,'2015-06-18 11:12:29',NULL,NULL);
/*!40000 ALTER TABLE `produktbatch` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`grp10`@`%`*/ /*!50003 TRIGGER prodBatch_upd_chck BEFORE UPDATE ON produktbatch	
				FOR EACH ROW
					BEGIN
						IF (NEW.status = 1 or NEW.status = 2) then
							IF (NEW.status = 1 AND OLD.status = 0) then SET new.prod_start = CURRENT_TIMESTAMP;
								
							
                            ELSEIF (NEW.status = 2 AND OLD.status = 1) then SET new.prod_slut = CURRENT_TIMESTAMP;
								else SET NEW.status = OLD.status;
                            END IF;
                            
                         else SET NEW.status = OLD.status;   
						
						END IF; 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `produktbatchkomponent`
--

DROP TABLE IF EXISTS `produktbatchkomponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produktbatchkomponent` (
  `pb_id` int(11) NOT NULL DEFAULT '0',
  `rb_id` int(11) NOT NULL DEFAULT '0',
  `tara` decimal(20,3) DEFAULT NULL,
  `netto` decimal(20,3) DEFAULT NULL,
  `opr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pb_id`,`rb_id`),
  KEY `rb_id` (`rb_id`),
  KEY `opr_id` (`opr_id`),
  CONSTRAINT `produktbatchkomponent_ibfk_1` FOREIGN KEY (`pb_id`) REFERENCES `produktbatch` (`pb_id`),
  CONSTRAINT `produktbatchkomponent_ibfk_2` FOREIGN KEY (`rb_id`) REFERENCES `raavarebatch` (`rb_id`),
  CONSTRAINT `produktbatchkomponent_ibfk_3` FOREIGN KEY (`opr_id`) REFERENCES `operatoer` (`opr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produktbatchkomponent`
--

LOCK TABLES `produktbatchkomponent` WRITE;
/*!40000 ALTER TABLE `produktbatchkomponent` DISABLE KEYS */;
INSERT INTO `produktbatchkomponent` VALUES (1,1,0.500,10.050,1),(1,2,0.500,2.030,1),(1,4,0.500,1.980,1),(2,1,0.500,10.010,2),(2,2,0.500,1.990,2),(2,5,0.500,1.470,2),(3,1,0.500,10.070,1),(3,3,0.500,2.060,2),(3,4,0.500,1.550,1),(3,6,0.500,1.530,2),(4,1,0.500,10.020,3),(4,5,0.500,1.570,3),(4,6,0.500,1.030,3),(4,7,0.500,0.990,3);
/*!40000 ALTER TABLE `produktbatchkomponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raavare`
--

DROP TABLE IF EXISTS `raavare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raavare` (
  `raavare_id` int(11) NOT NULL,
  `raavare_navn` text,
  `leverandoer` text,
  PRIMARY KEY (`raavare_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raavare`
--

LOCK TABLES `raavare` WRITE;
/*!40000 ALTER TABLE `raavare` DISABLE KEYS */;
INSERT INTO `raavare` VALUES (1,'dej','Wawelka'),(2,'tomat','Knoor'),(3,'tomat','Veaubais'),(4,'tomat','Franz'),(5,'blå ost','Ost og Skinke A/S'),(6,'rød ost','Ost og Skinke A/S'),(7,'hvid ost','Ost og Skinke A/S'),(8,'gul ost','Ost og Skinke A/S'),(9,'sort ost','Ost og Skinke A/S'),(10,'ond ost','Ost og Skinke A/S'),(11,'vred ost','Ost og Skinke A/S'),(12,'gammel ost','Ost og Skinke A/S'),(13,'gaaaamel ost','Ost og Skinke A/S'),(14,'led ost','Ost og Skinke A/S'),(15,'oldinge ost','Ost og Skinke A/S'),(16,'rejer','Ost og Skinke A/S'),(17,'oksekød','Ost og Skinke A/S'),(18,'champignon','Ost og Skinke A/S'),(19,'hestekød','Ost og Skinke A/S'),(20,'pepperoni','Ost og Skinke A/S'),(21,'salat','Ost og Skinke A/S'),(22,'rotte strimler','Ost og Skinke A/S'),(23,'terminal lemming','Ost og Skinke A/S'),(24,'grøn the','Ost og Skinke A/S'),(25,'rødvin','Igloo Frostvarer');
/*!40000 ALTER TABLE `raavare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raavarebatch`
--

DROP TABLE IF EXISTS `raavarebatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raavarebatch` (
  `rb_id` int(11) NOT NULL,
  `raavare_id` int(11) DEFAULT NULL,
  `maengde` decimal(20,3) DEFAULT NULL,
  PRIMARY KEY (`rb_id`),
  KEY `raavare_id` (`raavare_id`),
  CONSTRAINT `raavarebatch_ibfk_1` FOREIGN KEY (`raavare_id`) REFERENCES `raavare` (`raavare_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raavarebatch`
--

LOCK TABLES `raavarebatch` WRITE;
/*!40000 ALTER TABLE `raavarebatch` DISABLE KEYS */;
INSERT INTO `raavarebatch` VALUES (1,1,200.123),(2,2,300.000),(3,3,300.000),(4,5,100.000),(5,5,100.000),(6,6,100.000),(7,7,100.000);
/*!40000 ALTER TABLE `raavarebatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recept`
--

DROP TABLE IF EXISTS `recept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recept` (
  `recept_id` int(11) NOT NULL,
  `recept_navn` text,
  PRIMARY KEY (`recept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recept`
--

LOCK TABLES `recept` WRITE;
/*!40000 ALTER TABLE `recept` DISABLE KEYS */;
INSERT INTO `recept` VALUES (1,'margherita'),(2,'prosciutto'),(3,'capricciosa'),(4,'Ooond'),(5,'ost'),(6,'yy'),(7,'yessusss');
/*!40000 ALTER TABLE `recept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptkomponent`
--

DROP TABLE IF EXISTS `receptkomponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receptkomponent` (
  `recept_id` int(11) NOT NULL DEFAULT '0',
  `raavare_id` int(11) NOT NULL DEFAULT '0',
  `nom_netto` decimal(20,3) DEFAULT NULL,
  `tolerance` decimal(20,3) DEFAULT NULL,
  `sekvens` int(11) DEFAULT '0',
  PRIMARY KEY (`recept_id`,`raavare_id`),
  KEY `raavare_id` (`raavare_id`),
  CONSTRAINT `receptkomponent_ibfk_1` FOREIGN KEY (`recept_id`) REFERENCES `recept` (`recept_id`),
  CONSTRAINT `receptkomponent_ibfk_2` FOREIGN KEY (`raavare_id`) REFERENCES `raavare` (`raavare_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptkomponent`
--

LOCK TABLES `receptkomponent` WRITE;
/*!40000 ALTER TABLE `receptkomponent` DISABLE KEYS */;
INSERT INTO `receptkomponent` VALUES (1,1,2.012,0.100,1),(1,2,2.012,0.100,2),(1,5,2.012,0.100,3),(2,1,10.123,0.100,1),(2,3,2.000,0.100,2),(2,5,1.500,0.100,3),(2,6,1.500,0.100,4),(3,1,10.000,0.100,1),(3,4,1.500,0.100,2),(3,5,1.500,0.100,3),(3,6,1.000,0.100,4),(3,7,1.000,0.100,5),(4,9,1.000,1.000,0),(4,10,1.000,1.000,0),(4,11,1.000,1.000,0),(5,6,1.000,1.000,0),(5,7,1.000,1.000,0),(5,8,1.000,1.000,0),(6,12,1.000,1.000,1),(6,13,1.000,1.000,2),(6,14,1.000,1.000,3),(7,13,2.000,1.000,1),(7,14,2.000,1.000,3),(7,16,2.000,1.000,2);
/*!40000 ALTER TABLE `receptkomponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roller`
--

DROP TABLE IF EXISTS `roller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roller` (
  `lvl` int(11) NOT NULL DEFAULT '0',
  `rolle_navn` text,
  PRIMARY KEY (`lvl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roller`
--

LOCK TABLES `roller` WRITE;
/*!40000 ALTER TABLE `roller` DISABLE KEYS */;
INSERT INTO `roller` VALUES (0,'Inaktiv'),(1,'Operatoer'),(2,'Værkfører'),(3,'Farmaceut'),(9,'Admin');
/*!40000 ALTER TABLE `roller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaegtforbindelse`
--

DROP TABLE IF EXISTS `vaegtforbindelse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaegtforbindelse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` text NOT NULL,
  `port` int(11) DEFAULT '8000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaegtforbindelse`
--

LOCK TABLES `vaegtforbindelse` WRITE;
/*!40000 ALTER TABLE `vaegtforbindelse` DISABLE KEYS */;
INSERT INTO `vaegtforbindelse` VALUES (1,'127.0.0.1',8000),(2,'168.192.1',8000);
/*!40000 ALTER TABLE `vaegtforbindelse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'grp10'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-18 15:25:50
