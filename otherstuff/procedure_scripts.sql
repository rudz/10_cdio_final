-- procedure returns the raavare based on the input rb_id (raavarebatch id)
	delimiter //
	create procedure hentRaavare_ved_rb_id(in rb_id_in integer(10))
		begin
			select raavare.raavare_id, raavare_navn, leverandoer 
				from raavare join raavarebatch 
				on raavare.raavare_id = raavarebatch.raavare_id where rb_id = rb_id_in;		
		end ; //
		delimiter ; //
	
	--procedure that deletes the operatoer with the given opr_id. It firsts deletes any produktbatchkomponents that references the operatoer. 
	--Should be uses with extreme caution
	delimiter //
	create procedure deleteOperatoer (in opr_id_in integer(10))
		begin start transaction;
		set @oldOprCount = (select count(*) from operatoer);		
				
		delete from produktbatchkomponent where opr_id = opr_id_in;						
		
		delete from operatoer where operatoer.opr_id = opr_id_in;		
		
		set @OprDel = (select exists (select * from operatoer where opr_id = opr_id_in));
		if(@OprDel = 0) then commit;
		
		else rollback;
		end if;
		end; //
		delimiter ; //
		
		--procedure that updates the amount of raw material. It is implemented as a transaction
		delimiter //
		create procedure updateRaavareBatch (in amount integer(100), rb_id integer(10))
			begin start transaction;
			set @oldAmount = (select maengde from raavareBatch where raavareBatch.rb_id = rb_id);
			
			update raavareBatch set maengde = amount where raavarebatch.rb_id = rb_id;
			
			if( (select maengde from raavareBatch where raavarebatch.rb_id = rb_id) = amount) then commit;
			else rollback;
			end if;
			end; //
			delimiter ; //
			
			DROP TRIGGER IF EXISTS prodBatch_upd_chck;
			
			delimiter //			
			CREATE TRIGGER prodBatch_upd_chck BEFORE UPDATE ON produktbatch	
				FOR EACH ROW
					BEGIN				
						IF NEW.status = 1 then SET new.prod_start = CURRENT_TIMESTAMP;
						ElSEIF NEW.status = 2 then SET new.prod_slut = CURRENT_TIMESTAMP;
						END IF; 
					END; //
			delimiter ; //
		
		--redundent code
		call deleteOperatoer(1);
		
		delete from produktbatch where produktbatch.pb_id = (select pb_id from pb_idtable);
		
		create temporary table if not exists pb_idtable as (select distinct pb_id from produktbatchkomponent 
			join operatoer on produktbatchkomponent.opr_id = operatoer.opr_id 
				where operatoer.opr_id = opr_id_in);	
		
		