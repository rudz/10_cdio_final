										/*DDL*/

/* must be dropped in this order to avoid constraint violations */
DROP TABLE IF EXISTS produktbatchkomponent;
DROP TABLE IF EXISTS produktbatch;
DROP TABLE IF EXISTS receptkomponent;
DROP TABLE IF EXISTS recept;
DROP TABLE IF EXISTS raavarebatch;
DROP TABLE IF EXISTS log; /* delete log so the script works */
DROP TABLE IF EXISTS raavare;
DROP TABLE IF EXISTS operatoer;
DROP TABLE IF EXISTS roller;
DROP TABLE IF EXISTS vaegtforbindelse;

CREATE TABLE roller (
    lvl INT,
    rolle_navn TEXT,
    PRIMARY KEY (lvl)
)  ENGINE=INNODB;

	CREATE TABLE operatoer (
    opr_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    opr_navn TEXT,
    ini TEXT,
    cpr TEXT,
    password TEXT,
    salt TEXT,
    lvl INT NOT NULL,
    FOREIGN KEY (lvl)
        REFERENCES roller (lvl)
)  ENGINE=INNODB;
	
CREATE TABLE raavare (
    raavare_id INT PRIMARY KEY,
    raavare_navn TEXT,
    leverandoer TEXT
)  ENGINE=INNODB;

CREATE TABLE raavarebatch (
    rb_id INT PRIMARY KEY,
    raavare_id INT,
    maengde DECIMAL(20 , 3 ),
    FOREIGN KEY (raavare_id)
        REFERENCES raavare (raavare_id)
)  ENGINE=INNODB;
	
CREATE TABLE recept (
    recept_id INT PRIMARY KEY,
    recept_navn TEXT
)  ENGINE=INNODB;

CREATE TABLE receptkomponent (
    recept_id INT,
    raavare_id INT,
    nom_netto DECIMAL(20 , 3 ),
    tolerance DECIMAL(20 , 3 ),
    sekvens INT default 0,
    PRIMARY KEY (recept_id , raavare_id),
    FOREIGN KEY (recept_id)
        REFERENCES recept (recept_id),
    FOREIGN KEY (raavare_id)
        REFERENCES raavare (raavare_id)
)  ENGINE=INNODB;

CREATE TABLE produktbatch (
    pb_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    status INT DEFAULT 0,
    recept_id INT,
    oprettet TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    prod_start DATETIME,
    prod_slut DATETIME,
    FOREIGN KEY (recept_id)
        REFERENCES recept (recept_id)
)  ENGINE=INNODB;

CREATE TABLE produktbatchkomponent (
    pb_id INT,
    rb_id INT,
    tara DECIMAL(20 , 3 ),
    netto DECIMAL(20 , 3 ),
    opr_id INT,
    PRIMARY KEY (pb_id , rb_id),
    FOREIGN KEY (pb_id)
        REFERENCES produktbatch (pb_id),
    FOREIGN KEY (rb_id)
        REFERENCES raavarebatch (rb_id),
    FOREIGN KEY (opr_id)
        REFERENCES operatoer (opr_id)
)  ENGINE=INNODB;

CREATE TABLE vaegtforbindelse (
    id INT AUTO_INCREMENT,
    ip TEXT NOT NULL,
    port INT DEFAULT 8000,
    PRIMARY KEY (id)
)  ENGINE=INNODB;
-- -----------------------------------------------------
-- Table log
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS log (
    id INT(11) NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    entry VARCHAR(50) NOT NULL,
    level VARCHAR(15) NOT NULL,
    msg VARCHAR(1000) NOT NULL,
    opr_id INT(11) NULL DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT opr_id FOREIGN KEY (id)
        REFERENCES operatoer (opr_id)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARACTER SET=LATIN1 CHECKSUM=1 COMMENT='Simple test log table for use with log4j';

DROP TRIGGER IF EXISTS prodBatch_upd_chck;	
delimiter //			
			CREATE TRIGGER prodBatch_upd_chck BEFORE UPDATE ON produktbatch	
				FOR EACH ROW
					BEGIN
						IF (NEW.status = 1 or NEW.status = 2) then
							IF (NEW.status = 1 AND OLD.status = 0) then SET new.prod_start = CURRENT_TIMESTAMP;
								
							
                            ELSEIF (NEW.status = 2 AND OLD.status = 1) then SET new.prod_slut = CURRENT_TIMESTAMP;
								else SET NEW.status = OLD.status;
                            END IF;
                            
                         else SET NEW.status = OLD.status;   
						
						END IF; 
					END; //
delimiter ; //		
	
	
	
	
/* the salt values are stored as BASE64 encoded entries
-> Basic strings is as follows (base64 followed by DECIMAL(20,3) salt, then password as string) :
YTJfcTFDLStNbENfMjcuOD9RKy5LazdYc2c2VUE0NjE3KyF5UjFPK19oRXYzLS5GK2lvbi5IVTNoNzU9bV81ZERMMDVVY3M/UnBRV2N0bzE3N3g3aGIuWS49MllLPT0zcmQhRQ==
a2_q1C-+MlC_27.8?Q+.Kk7Xsg6UA4617+!yR1O+_hEv3-.F+ion.HU3h75=m_5dDL05Ucs?RpQWcto177x7hb.Y.=2YK==3rd!E
1234
*/
	
		
									/*DML*/	
INSERT INTO roller(lvl, rolle_navn) VAlUES 
(0, 'Inaktiv'),
(1, 'Operatoer'),
(2, 'Værkfører'),
(3, 'Farmaceut'),
(9, 'Admin');
	
INSERT INTO operatoer(opr_id, opr_navn, ini, cpr, password, salt, lvl) VALUES
(1, 'rudzen', 'rud', '241102-9959', 'f7255cce0fdfa5fac3f0dcd920d308960ebf24f9f1611d2ff6ce18ee0245d6a5f34f27f82529f4c2016789565cafcdb7f6373fdbf3d4422341aea84ec67f4127','VW80VD9vV04tQTRiS2xwQj1RVzVJNjY9K3M4K21B', 9),
(2, 'Antonella B', 'AB', '241102-9967', 'bde842f2d3d1a9656dff7e6e22dc6d165ea2b8ce70c9a18270c24ef6a2f900008aa850c6574c2af3ac8ad01e510fea057d13e7865fba5567353bb6bad65d1d57','Yy1nOGoyRCFfP3I9ODU9UzdnLVBiYTdNPTZxNmM0', 1),
(3, 'Kei B', 'AB', '241102-9967', 'f7255cce0fdfa5fac3f0dcd920d308960ebf24f9f1611d2ff6ce18ee0245d6a5f34f27f82529f4c2016789565cafcdb7f6373fdbf3d4422341aea84ec67f4127','VW80VD9vV04tQTRiS2xwQj1RVzVJNjY9K3M4K21B', 2),
(4, 'Angelo A', 'AA', '241102-9959', '2c2593c0e270cd949d84ef48ce9a01c6703db46a9993110a00f8d3e6592edc42356eb29646b6ba447bba763084be325e421c8998fdedefd056e7fd04d1003951','QT15NlY9YzdLSU0tK1U1QlE2ZzVFTi13eC5kYT9N', 3);

INSERT INTO raavare(raavare_id, raavare_navn, leverandoer) VALUES
(1, 'dej', 'Wawelka'),
(2, 'tomat', 'Knoor'),
(3, 'tomat', 'Veaubais'),
(4, 'tomat', 'Franz'),
(5, 'blå ost', 'Ost og Skinke A/S'),
(6, 'rød ost', 'Ost og Skinke A/S'),
(7, 'hvid ost', 'Ost og Skinke A/S'),
(8, 'gul ost', 'Ost og Skinke A/S'),
(9, 'sort ost', 'Ost og Skinke A/S'),
(10, 'ond ost', 'Ost og Skinke A/S'),
(11, 'vred ost', 'Ost og Skinke A/S'),
(12, 'gammel ost', 'Ost og Skinke A/S'),
(13, 'gaaaamel ost', 'Ost og Skinke A/S'),
(14, 'led ost', 'Ost og Skinke A/S'),
(15, 'oldinge ost', 'Ost og Skinke A/S'),
(16, 'rejer', 'Ost og Skinke A/S'),
(17, 'oksekød', 'Ost og Skinke A/S'),
(18, 'champignon', 'Ost og Skinke A/S'),
(19, 'hestekød', 'Ost og Skinke A/S'),
(20, 'pepperoni', 'Ost og Skinke A/S'),
(21, 'salat', 'Ost og Skinke A/S'),
(22, 'rotte strimler', 'Ost og Skinke A/S'),
(23, 'terminal lemming', 'Ost og Skinke A/S'),
(24, 'grøn the', 'Ost og Skinke A/S'),
(25, 'rødvin', 'Igloo Frostvarer');

INSERT INTO raavarebatch(rb_id, raavare_id, maengde) VALUES
(1, 1, 1000),
(2, 2, 300),
(3, 3, 300),
(4, 4, 100),
(5, 5, 100), 
(6, 6, 100),
(7, 7, 100);

INSERT INTO recept(recept_id, recept_navn) VALUES
(1, 'margherita'),
(2, 'prosciutto'),
(3, 'capricciosa');

INSERT INTO receptkomponent(recept_id, raavare_id, nom_netto, tolerance, sekvens) VALUES
(1, 1, 2.0123456789, 3 ,1),
(1, 2, 2.0123456789, 3 ,2),
(1, 5, 2.0123456789, 3 ,3),

(2, 1, 10.123456789, 3,1),
(2, 3, 2.0, 3, 2),  
(2, 5, 1.5, 3, 3),
(2, 6, 1.5, 3, 4),

(3, 1, 10.0, 2, 1),
(3, 4, 1.5, 2, 2),
(3, 5, 1.5, 2, 3),
(3, 6, 1.0, 2, 4),
(3, 7, 1.0, 2, 5);

INSERT INTO produktbatch(pb_id, recept_id, status) VALUES
(1, 1, 0), 
(2, 1, 0),
(3, 2, 0),
(4, 3, 0),
(5, 3, 0);

INSERT INTO vaegtforbindelse(ip, port) VALUES
	('127.0.0.1', 8000),
	('169.254.2.3', 8000),
    ('169.254.2.2', 8000);
	
/*INSERT INTO produktbatchkomponent(pb_id, rb_id, tara, netto, opr_id) VALUES
(1, 1, 0.5, 10.05, 1),
(1, 2, 0.5, 2.03, 1),
(1, 4, 0.5, 1.98, 1),

(2, 1, 0.5, 10.01, 2),
(2, 2, 0.5, 1.99, 2),
(2, 5, 0.5, 1.47, 2),

(3, 1, 0.5, 10.07, 1),
(3, 3, 0.5, 2.06, 2),
(3, 4, 0.5, 1.55, 1),
(3, 6, 0.5, 1.53, 2),

(4, 1, 0.5, 10.02, 3),
(4, 5, 0.5, 1.57, 3),
(4, 6, 0.5, 1.03, 3),
(4, 7, 0.5, 0.99, 3);*/

