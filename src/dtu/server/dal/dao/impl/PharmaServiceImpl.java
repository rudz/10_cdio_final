
package dtu.server.dal.dao.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sun.xml.internal.messaging.saaj.util.Base64;
import dtu.client.service.IAdminService;
import dtu.server.dal.dao.MySQLOperatoerDAO;
import dtu.server.dal.dao.MySQLProduktBatchDAO;
import dtu.server.dal.dao.MySQLProduktBatchKompDAO;
import dtu.server.dal.dao.MySQLRaavareBatchDAO;
import dtu.server.dal.dao.MySQLRaavareDAO;
import dtu.server.dal.dao.MySQLReceptDAO;
import dtu.server.dal.dao.MySQLReceptKompDAO;
import dtu.server.dal.dao.MySQLVaegtForbindelseDAO;
import dtu.server.local.PasswordGenerator;
import dtu.server.token.TokenHandler;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;
import dtu.shared.hashing.SlowEquals;
//import dtu.server.dal.dao.MySQLRaavareDAO.preparedStatements;
import dtu.shared.misc.StringToolLib;


public class PharmaServiceImpl extends RemoteServiceServlet implements IAdminService {

	private static final long		serialVersionUID	= 9171221492805067765L;

	private static TokenHandler th = TokenHandler.getInstance();
	
	@Override
	public String getToken(int id) throws DALException {
		return th.createToken(Integer.toString(id));
	}

	
	@Override
	public OperatoerDTO getOperatoer(int ID) throws DALException {
		return MySQLOperatoerDAO.getOperatoer(ID);
	}

	@Override
	public boolean createOperatoer(OperatoerDTO opr) throws DALException {
		return MySQLOperatoerDAO.createOperatoer(opr);
	}

	@Override
	public boolean updateOperatoer(OperatoerDTO opr) throws DALException {
		return MySQLOperatoerDAO.updateOperatoer(opr);
	}

	@Override
	public ArrayList<OperatoerDTO> getOperatoerList() throws DALException {
		return MySQLOperatoerDAO.getOperatoerList();
	}

	@Override
	public ArrayList<OperatoerDTO> getOperatoerList(int opr_id) throws DALException {
		return MySQLOperatoerDAO.getOperatoerList(opr_id);
	}

	@Override
	public boolean sackOperatoer(int opr) throws DALException {
		return MySQLOperatoerDAO.sackOperatoer(opr);
	}

	@Override
	public int getSize() throws DALException {
		return MySQLOperatoerDAO.countOperatoer();
	}

	@Override
	public HashSet<String> getInitials() throws DALException {
		return MySQLOperatoerDAO.getInitials();
	}

	@Override
	public String getOperatorName(int oprId) throws DALException {
		return MySQLOperatoerDAO.getOperatorName(oprId);
	}

	@Override
	public boolean passwordValidation(OperatoerDTO opr) throws DALException {
		/* get stored salt as base64 and convert it */
		String rawSalt = Base64.base64Decode(MySQLOperatoerDAO.getSalt(opr.getId()));

		/* get stored password hash */
		String pwdb = MySQLOperatoerDAO.getOperatorPassword(opr.getId());

		/* get parsed raw password */
		String pw = opr.getPassword();

	
		/* generate new hash from parsed password */
		try {
			MessageDigest mda = MessageDigest.getInstance("SHA-512");
			mda.update(rawSalt.getBytes());
			mda.update(pw.getBytes());
			mda.update(rawSalt.getBytes());
			final String result = StringToolLib.byteArrToHexString(mda.digest());
			mda = null;
			
			/* wait till last minute to actually check if user is inactive! */
			boolean isValid = MySQLOperatoerDAO.isOperatoerActive(opr.getId());
			boolean isOkayPassword = SlowEquals.slowEquals(result.getBytes(), pwdb.getBytes());
			return isValid & isOkayPassword;
		} catch (NoSuchAlgorithmException e) {
			throw new DALException("Algoritme fejl, kontakt systemadministratoren!");
		} finally {
			/* reset object */
			opr = null;
		}
	}

	@Override
	public boolean updatePW(OperatoerDTO opr) throws DALException {
		return MySQLOperatoerDAO.updatePW(opr);
	}

	@Override
	public String genPassword(int operatorID) throws DALException {
		return PasswordGenerator.generatePassword(6, true, operatorID);
	}

	@Override
	public boolean createRaavare(RaavareDTO raavare) throws DALException {
		return MySQLRaavareDAO.createRaavare(raavare);
	}

	@Override
	public boolean raavareExists(int raavareId) throws DALException {
		return MySQLRaavareDAO.raavareIdExists(raavareId);
	}

	@Override
	public boolean isRaavare(int raavareId) throws DALException {
		return MySQLRaavareDAO.isRaavare(raavareId);
	}

	@Override
	public boolean deleteRaavare(int raavareId) throws DALException {
		return MySQLRaavareDAO.deleteRaavare(raavareId);
	}

	@Override
	public ArrayList<RaavareDTO> getRaavareList() throws DALException {
		return MySQLRaavareDAO.getRaavareList();
	}

	@Override
	public boolean updateRaavare(RaavareDTO raavare) throws DALException {
		return MySQLRaavareDAO.updateRaavare(raavare);
	}

	@Override
	public boolean createRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException {
		return MySQLRaavareBatchDAO.createRaavareBatch(raavarebatch);
	}

	@Override
	public ArrayList<RaavareBatchDTO> browseRaavareBatch() throws DALException {
		return MySQLRaavareBatchDAO.getRaavareBatchList();
	}

	@Override
	public boolean deleteRaavareBatch(int rb_id) throws DALException {
		return MySQLRaavareBatchDAO.deleteRaavareBatch(rb_id);
	}

	@Override
	public boolean updateRaavareBatch(RaavareBatchDTO raavareBatchDTO) throws DALException {
		return MySQLRaavareBatchDAO.updateRaavareBatch(raavareBatchDTO);
	}
	
	@Override
	public boolean raavareBatchIdExists(int rb_id) throws DALException{
		return MySQLRaavareBatchDAO.RavareBatchIdExists(rb_id);
	}
	
	@Override
	public boolean createRecept(ReceptDTO receptDTO) throws DALException {
		return MySQLReceptDAO.createRecept(receptDTO);
	}

	@Override
	public ArrayList<ReceptDTO> getReceptList() throws DALException {
		return MySQLReceptDAO.getReceptList();
	}

	@Override
	public ReceptDTO getRecept(int receptId) throws DALException {
		return MySQLReceptDAO.getRecept(receptId);
	}

	@Override
	public boolean updateRecept(ReceptDTO receptDTO) throws DALException {
		return MySQLReceptDAO.updateRecept(receptDTO);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean deleteRecept(int receptId) throws DALException {
		return MySQLReceptDAO.deleteRecept(receptId);
	}

	@Override
	public ArrayList<ReceptKomponenterDTO> getReceptKomponenter(int receptId) throws DALException {
		return MySQLReceptDAO.getReceptKomponenter(receptId);
	}

	@Override
	public boolean createReceptKomponent(ReceptKompDTO receptKompDTO) throws DALException {
		return MySQLReceptKompDAO.createReceptKomp(receptKompDTO);
	}

	@Override
	public boolean createReceptKomponentList(ArrayList<ReceptKompDTO> list) throws DALException {
		return MySQLReceptKompDAO.createReceptList(list);
	}

	@Override
	public boolean isRecept(int receptId) throws DALException {
		return MySQLReceptDAO.isRecept(receptId);
	}

	@Override
	public RaavareDTO getRaavare(int id) throws DALException {
		return MySQLRaavareDAO.getRaavare(id);
	}

	@Override
	public RaavareBatchDTO getRaavareBatch(int id) throws DALException {
		return MySQLRaavareBatchDAO.getRaavareBatch(id);
	}

	@Override
	public boolean updateProduktBatch(ProduktBatchDTO produktBatchDTO) throws DALException {
		return MySQLProduktBatchDAO.updateProduktBatch(produktBatchDTO);
	}

	@Override
	public boolean createProduktBatch(ProduktBatchDTO produktBatchDTO) throws DALException {
		return MySQLProduktBatchDAO.createProduktBatch(produktBatchDTO);
	}

	@Override
	public boolean deleteProduktBatch(int produktBatchId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean createProduktBatchList(ArrayList<ProduktBatchDTO> list) throws DALException {
		return MySQLProduktBatchDAO.createProduktBatchList(list);
	}

	@Override
	public ProduktBatchDTO getProduktBatch(int pbid) throws DALException {
		return MySQLProduktBatchDAO.getProduktBatch(pbid);
	}

	@Override
	public ArrayList<ReceptKompDTO> getReceptKomp(int i) throws DALException {
		return MySQLReceptKompDAO.getReceptKompList(i);
	}
	
	@Override
	public void createProduktBatchKomp(ProduktBatchKompDTO dto) throws DALException {
		MySQLProduktBatchKompDAO.createProduktBatchKomp(dto);
	}
	@Override
	public ArrayList<ProduktBatchDTO> getProduktBatchList(int start, int end, int s0, int s1, int s2) throws DALException {
		return MySQLProduktBatchDAO.getProduktBatchListWithStatus(start, end, s0, s1, s2);
	}
	@Override
	public ArrayList<ProduktBatchKompDTO> getProduktBatchKompList(int produktBatchId) throws DALException {
		return MySQLProduktBatchKompDAO.getProduktBatchKompList(produktBatchId);
	}
	@Override
	public ArrayList<ProduktBatchDTO> getProduktBatchListWithStatus(int start, int end, int s0, int s1, int s2) throws DALException {
		return MySQLProduktBatchDAO.getProduktBatchListWithStatus(start, end, s0, s1, s2);
	}
	@Override
	public void updateReceptKomponenter(ArrayList<ReceptKompDTO> list) throws DALException{
		MySQLReceptKompDAO.updateReceptKomponenter(list);
	}
	
	@Override
	public void updateReceptKomponent(ReceptKompDTO DTO) throws DALException{
		MySQLReceptKompDAO.updateReceptKomp(DTO);
	}
	
	//Weigt Connection DAO calls
	@Override
	public ArrayList<VaegtForbindelseDTO> getForbindelseList() throws DALException{
		return MySQLVaegtForbindelseDAO.getForbindelseList();
		
	}
	@Override
	public void OpretForbindelsesData(VaegtForbindelseDTO vfDTO) throws DALException{
		MySQLVaegtForbindelseDAO.createForbindelse(vfDTO);
	}
	@Override
	public boolean sletForbindelse(int id) throws DALException{
		return MySQLVaegtForbindelseDAO.sletForbindelse(id);
	}


	@Override
	public ArrayList<ReceptKompDTO> getReceptKompListWCU(int receptId, int produktBatchId) throws DALException {
		// TODO Auto-generated method stub
		return MySQLReceptKompDAO.getReceptKompListWCU(receptId, produktBatchId);
	}

}
