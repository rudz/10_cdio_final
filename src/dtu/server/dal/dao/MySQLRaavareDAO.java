
package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.exceptions.DALException;

/**
 * Raavare DAO.<br>
 * Using PreparedStatements.
 * @author Theis Friis Strøming
 */
public abstract class MySQLRaavareDAO {

	static class preparedStatements {
		static PreparedStatement	getraavare;
		static PreparedStatement	update;
		static PreparedStatement	create;
		static PreparedStatement	delete;
		static PreparedStatement	exist;
		static PreparedStatement	list;
	}

	static preparedStatements	preStatement;
	static ResultSet			rs;


	private static DataSource	conPool;
	@SuppressWarnings("unused")
	private static int	isUsed;
	
	static {
		try {
			conPool = DataSource.getInstance();
			//preStatement = new preparedStatements();
			
				try {
					Connection connection = conPool.getConnection();
					preparedStatements.list = connection.prepareStatement("SELECT * FROM raavare");
					preparedStatements.getraavare = connection.prepareStatement("SELECT * FROM raavare WHERE raavare_id = ?");
					preparedStatements.update = connection.prepareStatement("UPDATE raavare SET raavare_navn = ?, leverandoer = ? WHERE raavare_id = ?");
					preparedStatements.create = connection.prepareStatement("INSERT INTO raavare(raavare_id, raavare_navn, leverandoer) VALUES (? ,? ,?)");
					preparedStatements.delete = connection.prepareStatement("DELETE FROM raavare WHERE raavare_id = ?");
					preparedStatements.exist = connection.prepareStatement("SELECT count(raavare_id) FROM raavare WHERE raavare_id = ?");
				} catch (SQLException e) {

					e.printStackTrace();
				}
			
			} catch (PropertyVetoException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		
		}

	
	
//	public MySQLRaavareDAO() throws DALException {
//		preStatement = new preparedStatements();
//		try(Connection connection = conPool.getConnection()){
//		try {
//			preStatement.list = connection.prepareStatement("SELECT * FROM raavare");
//			preStatement.getraavare = connection.prepareStatement("SELECT * FROM raavare WHERE raavare_id = ?");
//			preStatement.update = connection.prepareStatement("UPDATE raavare SET raavare_navn = ?, leverandoer = ? WHERE raavare_id = ?");
//			preStatement.create = connection.prepareStatement("INSERT INTO raavare(raavare_id, raavare_navn, leverandoer) VALUES (? ,? ,?)");
//			preStatement.delete = connection.prepareStatement("DELETE FROM raavare WHERE raavare_id = ?");
//			preStatement.exist = connection.prepareStatement("SELECT count(raavare_id) FROM raavare WHERE raavare_id = ?");
//		} catch (SQLException e) {
//			throw new DALException("Fejl ved dbase forbindelse.");
//		}
//		} catch (SQLException e1) {
//			throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
//		}
//	}

	private static RaavareDTO rsToDTO(ResultSet rs1) throws DALException {
		try {
			return new RaavareDTO(rs1.getInt("raavare_id"), rs1.getString("raavare_navn"), rs1.getString("leverandoer"));
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("shiiit");
			throw new DALException(e);
			
		}
	}

	
	public static RaavareDTO getRaavare(int raavareId) throws DALException {
		try {
			preparedStatements.getraavare.setInt(1, raavareId);
			rs = preparedStatements.getraavare.executeQuery();
			if (rs.first()) return rsToDTO(rs);
			throw new DALException("Raavare id: " + raavareId + " eksistere ikke");
		} catch (SQLException e) {
			throw new DALException(e);
		} finally {
			try {
				preparedStatements.getraavare.clearParameters();
				rs.close();
			} catch (SQLException e) {
				throw new DALException(e);
			}
		}
	}

	
	public static ArrayList<RaavareDTO> getRaavareList_old() throws DALException {
		ArrayList<RaavareDTO> list = new ArrayList<>();
		try {
			rs = preparedStatements.list.executeQuery();
			while (rs.next()) {
				list.add(rsToDTO(rs));
			}
			if (list.isEmpty()) throw new DALException("Raavare is empty");
			return list;
		} catch (Exception e1) {
			throw new DALException("ejejej " + e1.getMessage());
		}
	}
	
	public static ArrayList<RaavareDTO> getRaavareList() throws DALException {
		ArrayList<RaavareDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptListStatement = con.prepareStatement("SELECT * FROM raavare")) {
				getReceptListStatement.closeOnCompletion();
				try (ResultSet rs1 = getReceptListStatement.executeQuery()) {
					if (!rs1.first()) {
						rs1.close();
						throw new DALException("Kunne ikke finde noget indhold i raavare.");
					}
					do {
						list.add(new RaavareDTO(rs1.getInt("raavare_id"), rs1.getString("raavare_navn"), rs1.getString("leverandoer")));
					} while (rs1.next());
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få liste fra raavare.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}
		return list;
	}

	
	public static boolean createRaavare(RaavareDTO raavare) throws DALException {
		try {
			preparedStatements.create.setInt(1, raavare.getRaavareId());
			preparedStatements.create.setString(2, raavare.getRaavareNavn());
			preparedStatements.create.setString(3, raavare.getLeverandoer());
			preparedStatements.create.executeUpdate();
			return true;
		} catch (SQLException e) {
			throw new DALException(e);
		} finally {
			try {
				preparedStatements.create.clearParameters();
			} catch (SQLException e) {
				throw new DALException(e);
			}
		}
	}

	
	public static boolean updateRaavare(RaavareDTO raavare) throws DALException {
		try {
			preparedStatements.update.setString(1, raavare.getRaavareNavn());
			preparedStatements.update.setString(2, raavare.getLeverandoer());
			preparedStatements.update.setInt(3, raavare.getRaavareId());
			preparedStatements.update.executeUpdate();
			return true;
		} catch (SQLException e) {
			throw new DALException(e);
		} finally {
			try {
				preparedStatements.update.clearParameters();
			} catch (SQLException e) {
				throw new DALException(e);
			}
		}
	}
	
	public static boolean deleteRaavare(int raavare_id) throws DALException {
		try {
			preparedStatements.delete.setInt(1, raavare_id);
			preparedStatements.delete.executeUpdate();
			return true;
		} catch (SQLException e) {
			throw new DALException(e);
		} finally {
			try {
				preparedStatements.delete.clearParameters();
			} catch (SQLException e) {
				throw new DALException(e);
			}
		}
	}

	
	public static boolean raavareIdExists(int raavareId) throws DALException {	
		try {
			preparedStatements.exist.setInt(1, raavareId);
			rs = preparedStatements.exist.executeQuery();
			if (rs.first()) return rs.getInt(1) == 1;
			throw new DALException("Invalid handling.");
		} catch (SQLException e) {
			throw new DALException(e);
		}
	}
	
	/**
	 * Simplified detection if a specific raavare exists in db.
	 * @param raavareId : The raavareId to check for.
	 * @return true if exists, otherwise false.
	 * @throws DALException
	 */
	public static boolean isRaavare(int raavareId) throws DALException  {
		boolean returnValue = false;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptStatement = con.prepareStatement("SELECT raavare_id FROM raavare WHERE raavare_id=?")) {
				getReceptStatement.closeOnCompletion();
				getReceptStatement.setInt(1, raavareId);
				try (ResultSet rs1 = getReceptStatement.executeQuery()) {
					returnValue = rs1.first();
					rs1.close();
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få råvare med id " + Integer.toString(raavareId));
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}
		return returnValue;
	}

	
	
}
