package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.exceptions.DALException;

/**
 * Raavare batch DAO.<br>
 * Using PreparedStatements and Java 7 resource handling.
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class MySQLRaavareBatchDAO {
	
//	private Connection	connection;
//
//	public MySQLRaavareBatchDAO(Connection connection) {
//		this.connection = connection;
//	}

	
	private static DataSource	conPool;
	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			System.out.println("FATAL FEJL!");
			e.printStackTrace();
		}
	}
	
	
	public static RaavareBatchDTO getRaavareBatch(int rbId) throws DALException {
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement getRaavareBatchStatement = connection.prepareStatement("SELECT * FROM raavarebatch WHERE (rb_id = ?)")) {
			getRaavareBatchStatement.closeOnCompletion();
			getRaavareBatchStatement.setInt(1, rbId);
			try (ResultSet rs = getRaavareBatchStatement.executeQuery()) {
				if (!rs.first()) throw new DALException("raavarebatch " + rbId + " findes ikke");
				return new RaavareBatchDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3));
			}
		} catch (SQLException e) {
			throw new DALException("Fejl: Kunne ikke finde raavarebatch med id : " + Integer.toString(rbId));
		}
		} catch (SQLException e1) {			
			throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
		}
	}

	
	public static ArrayList<RaavareBatchDTO> getRaavareBatchList() throws DALException {
		ArrayList<RaavareBatchDTO> list = new ArrayList<>();
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement getRaavareBatchListStatement = connection.prepareStatement("SELECT * FROM raavarebatch")) {
			getRaavareBatchListStatement.closeOnCompletion();
			try (ResultSet rs = getRaavareBatchListStatement.executeQuery()) {
				while (rs.next()) list.add(new RaavareBatchDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3)));
			}
		} catch (SQLException e) {
			throw new DALException("Tilgang til dbasen.");
		}
		return list;
	} catch (SQLException e1) {
		throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
	}
	}
	
	
	public static ArrayList<RaavareBatchDTO> getRaavareBatchList(int raavareId) throws DALException {
		ArrayList<RaavareBatchDTO> list = new ArrayList<>();
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement getRaavareBatchListStatement = connection.prepareStatement("SELECT * FROM raavarebatch WHERE raavare_id = ?")) {
			getRaavareBatchListStatement.closeOnCompletion();
			getRaavareBatchListStatement.setInt(1, raavareId);
			try (ResultSet rs = getRaavareBatchListStatement.executeQuery()) {
				if(!rs.first()) throw new DALException("Kunne ikke finde  raavareID " + raavareId + " findes ikke");	
				while (rs.next()) list.add(new RaavareBatchDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3)));
			}
		} catch (SQLException e) {
			throw new DALException("Tilgang til dbasen.");
		}
		return list;
	} catch (SQLException e1) {
		throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
	}
	}
	
	
	public static boolean createRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException {
		try (Connection connection = conPool.getConnection()) {
			try (PreparedStatement createRaavareBatchStatement = connection
					.prepareStatement("INSERT INTO raavarebatch(rb_id, raavare_id, maengde) VALUES (?,?,?)")) {
				createRaavareBatchStatement.setInt(1, raavarebatch.getRbId());
				createRaavareBatchStatement.setInt(2, raavarebatch.getRaavareId());
				createRaavareBatchStatement.setDouble(3, raavarebatch.getMaengde());
				createRaavareBatchStatement.executeUpdate();
				connection.close();
				return true;
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
		}
	}

	
	public static boolean updateRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException {
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement updateRaavareBatchStatement = connection.prepareStatement("UPDATE raavarebatch SET raavare_id = ?, maengde = ? WHERE rb_id = ?")) {
			updateRaavareBatchStatement.setInt(1, raavarebatch.getRaavareId());
			updateRaavareBatchStatement.setDouble(2, raavarebatch.getMaengde());
			updateRaavareBatchStatement.setInt(3, raavarebatch.getRbId());
			updateRaavareBatchStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DALException("Tilgang til dbasen.");
		}
		} catch (SQLException e1) {
			throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
		}
		return true;
	}
	
	public static boolean deleteRaavareBatch(int rb_id) throws DALException{
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement deleteRaavareBatchStatement = connection.prepareStatement("DELETE FROM raavarebatch WHERE rb_id = ?")) {
			deleteRaavareBatchStatement.setInt(1, rb_id);
			deleteRaavareBatchStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DALException("Kunne ikke slette raavarebatch med id : " + Integer.toString(rb_id));
		}
		} catch (SQLException e1) {
			throw new DALException("Fejl: Kunne ikke oprette forbindelse til databasen");
		}
		return true;
	}
	
	public static boolean RavareBatchIdExists(int rb_id) throws DALException{
		try(Connection connection = conPool.getConnection()){
			try (PreparedStatement isRaavareBatchIdStatement = connection.prepareStatement("SELECT COUNT(rb_id) FROM raavarebatch WHERE rb_id = ?")) {
				isRaavareBatchIdStatement.setInt(1, rb_id);
				isRaavareBatchIdStatement.closeOnCompletion();
				try (ResultSet rs = isRaavareBatchIdStatement.executeQuery()) {
					if(rs.first()) {
						return rs.getInt(1) == 1;
					}
				}
				throw new DALException("Fejl i database tingang");	
			}
		} catch (SQLException e) {
			throw new DALException(e);
		}
	}
}
