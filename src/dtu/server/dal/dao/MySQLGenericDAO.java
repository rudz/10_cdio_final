package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.exceptions.DALException;


public abstract class MySQLGenericDAO {
	private static DataSource	conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}
	
	public static int getLowestUnUsed(String table, String id) throws DALException {
		int returnValue = 0;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getUnusedId = con.prepareStatement("SELECT min(unused) AS unused FROM (SELECT MIN(t1.?)+1 as unused FROM ? AS t1 WHERE NOT EXISTS (SELECT * FROM ? AS t2 WHERE t2.? = t1.?+1) UNION SELECT 1 FROM DUAL WHERE NOT EXISTS (SELECT * FROM ? WHERE ? = 1) ) AS subquery")) {
				getUnusedId.closeOnCompletion();
				getUnusedId.setString(1, id);
				getUnusedId.setString(2, table);
				getUnusedId.setString(3, table);
				getUnusedId.setString(4, id);
				getUnusedId.setString(5, table);
				getUnusedId.setString(6, id);
				getUnusedId.setString(7, id);
				try (ResultSet rs = getUnusedId.executeQuery()) {
					if (rs.first()) {
						returnValue = rs.getInt(1);
					}
				}
			} catch (SQLException e) {
				throw new DALException("Fejl ved tjek af entiteten : " + table + " og id som : " + id);
			}
		return returnValue;
	} catch (SQLException e1) {
		throw new DALException("Tilgang til db.");
	}
	}
}
