
package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.exceptions.DALException;


public interface ProduktBatchKompDAO {

	ProduktBatchKompDTO getProduktBatchKomp(int pbId, int rbId) throws DALException;
	List<ProduktBatchKompDTO> getProduktBatchKompList(int pbId) throws DALException;
	List<ProduktBatchKompDTO> getProduktBatchKompList() throws DALException;
	void createProduktBatchKomp(ProduktBatchKompDTO produktbatchkomponent) throws DALException;
	void updateProduktBatchKomp(ProduktBatchKompDTO produktbatchkomponent) throws DALException;
	void deleteProduktBatchKomp(int pbId, int rbId) throws DALException;
}
