
package dtu.server.dal.dao.interfaces;

import java.util.ArrayList;
import java.util.List;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.exceptions.DALException;


public interface ReceptKompDAO {

	ReceptKompDTO getReceptKomp(int receptId, int raavareId) throws DALException;
	List<ReceptKompDTO> getReceptKompList(int receptId) throws DALException;
	List<ReceptKompDTO> getReceptKompListWCU(int receptId, int pb_id) throws DALException;
	
	List<ReceptKompDTO> getReceptKompList() throws DALException;
	void createReceptKomp(ReceptKompDTO receptkomponent) throws DALException;
	void updateReceptKomp(ReceptKompDTO receptkomponent) throws DALException;
	void deleteReceptKomp(int receptId, int raavareId) throws DALException;
	void createReceptKompList(ArrayList<ReceptKompDTO> list) throws DALException;
}
