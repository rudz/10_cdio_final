
package dtu.server.dal.dao.interfaces;

import java.util.ArrayList;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.exceptions.DALException;


public interface RaavareDAO {

	RaavareDTO getRaavare(int raavareId) throws DALException;
	ArrayList<RaavareDTO> getRaavareList() throws DALException;
	void createRaavare(RaavareDTO raavare) throws DALException;
	void updateRaavare(RaavareDTO raavare) throws DALException;
	boolean raavareIdExists(int raavareId) throws DALException;
	
	
}
