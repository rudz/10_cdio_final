
package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.exceptions.DALException;


public interface ReceptDAO {

	ReceptDTO getRecept(int receptId) throws DALException;
	List<ReceptDTO> getReceptList() throws DALException;
	void createRecept(ReceptDTO recept) throws DALException;
	void updateRecept(ReceptDTO recept) throws DALException;
	void deleteRecept(int receptId) throws DALException;
}
