
package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.exceptions.DALException;


public interface OperatoerDAO {

	OperatoerDTO getOperatoer(int oprId) throws DALException;

	List<OperatoerDTO> getOperatoerList() throws DALException;

	boolean createOperatoer(OperatoerDTO opr) throws DALException;

	boolean updateOperatoer(OperatoerDTO opr) throws DALException;
}
