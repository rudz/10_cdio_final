
package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.exceptions.DALException;


public interface ProduktBatchDAO {

	ProduktBatchDTO getProduktBatch(int pbId) throws DALException;
	List<ProduktBatchDTO> getProduktBatchList() throws DALException;
	void createProduktBatch(ProduktBatchDTO produktbatch) throws DALException;
	void updateProduktBatch(ProduktBatchDTO produktbatch) throws DALException;
	void deleteProduktBatch(int pb_id) throws DALException;
}
