
package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.exceptions.DALException;


public interface RaavareBatchDAO {

	RaavareBatchDTO getRaavareBatch(int rbId) throws DALException;

	List<RaavareBatchDTO> getRaavareBatchList() throws DALException;

	List<RaavareBatchDTO> getRaavareBatchList(int raavareId) throws DALException;

	void createRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException;

	boolean updateRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException;
}
