package dtu.server.dal.dao.interfaces;

import java.util.List;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;


public interface VaegtForbindelserDAO {

	OperatoerDTO getForbindelse(int id) throws DALException;

	List<VaegtForbindelseDTO> getForbindelseList() throws DALException;

	boolean createForbindelse(VaegtForbindelseDTO vfDTO) throws DALException;

	boolean updateForbindelse(VaegtForbindelseDTO vfDTO) throws DALException;
	
	
}
