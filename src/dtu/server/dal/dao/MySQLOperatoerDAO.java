
package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import dtu.server.dal.dao.pool.DataSource;
import dtu.server.hashing.Hasher;
import dtu.server.local.PasswordGenerator;
import dtu.server.token.Base64;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.exceptions.DALException;
import dtu.shared.validate.CPRValidering;

/** Operatoer DAO.<br>
 * Using PreparedStatements and Java 7 <i>ARM</i>.
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class MySQLOperatoerDAO {

	private static DataSource	conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	public static OperatoerDTO getOperatoer(int ID) throws DALException {
		OperatoerDTO oprDTO = null;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorPasswordStatement = con.prepareStatement("SELECT * FROM operatoer WHERE opr_id = ?")) {
				getOperatorPasswordStatement.closeOnCompletion();
				getOperatorPasswordStatement.setInt(1, ID);
				try (ResultSet rs = getOperatorPasswordStatement.executeQuery()) {
					if (rs.first()) {
						if (isValidCPR(rs.getString("cpr"))) {
							oprDTO = new OperatoerDTO(rs.getInt("opr_id"), rs.getString("opr_navn"), rs.getString("ini"), rs.getString("cpr"),
									rs.getInt("lvl"));
							rs.close();
						}
					}
				}
			} catch (SQLException e) {
				throw new DALException("Operatoeren " + ID + " findes ikke.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
		return oprDTO;
	}
	
	public static boolean isOperatoerActive(int ID) throws DALException {
		boolean returnValue = false;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorPasswordStatement = con.prepareStatement("SELECT lvl FROM operatoer WHERE opr_id = ?")) {
				getOperatorPasswordStatement.closeOnCompletion();
				getOperatorPasswordStatement.setInt(1, ID);
				try (ResultSet rs = getOperatorPasswordStatement.executeQuery()) {
					if (rs.first()) {
						int level = rs.getInt("lvl");
						rs.close();
						returnValue = level > 0 && level < 10;
						level = -1;
					}
				}
			} catch (SQLException e) {
				throw new DALException("Operatoeren " + ID + " findes ikke.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
		return returnValue;
	}


	public static boolean createOperatoer(OperatoerDTO opr) throws DALException {
		if (!isValidCPR(opr.getCpr())) return false;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement saveOperatoerState = con
					.prepareStatement("INSERT INTO operatoer(opr_navn, ini, cpr, password, salt, lvl) VALUES (?,?,?,?,?,?)")) {
				saveOperatoerState.setString(1, opr.getName());
				saveOperatoerState.setString(2, opr.getIni());
				saveOperatoerState.setString(3, opr.getCpr());
				String salt = genPassword(opr.getId());
				saveOperatoerState.setString(4, Hasher.genHash(salt, new String(Base64.getDecoder().decode(opr.getPassword()))));
				saveOperatoerState.setString(5, Base64.getEncoder().encodeToString(salt.getBytes())); // Base64Coder.encodeString(salt));
				saveOperatoerState.setInt(6, opr.getLevel());
				saveOperatoerState.executeUpdate();
				saveOperatoerState.close();
				return true;
			} catch (SQLException e) {
				throw new DALException(e);
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
	}

	public static boolean updateOperatoer(OperatoerDTO opr) throws DALException {
		if (!isValidCPR(opr.getCpr())) return false;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement updateOperatorState = con
					.prepareStatement("UPDATE operatoer SET opr_navn = ?, ini = ?, cpr = ?, lvl = ? WHERE opr_id = ?")) {
				updateOperatorState.setString(1, opr.getName());
				updateOperatorState.setString(2, opr.getIni());
				updateOperatorState.setString(3, opr.getCpr());
				updateOperatorState.setInt(4, opr.getLevel());
				updateOperatorState.setInt(5, opr.getId());
				updateOperatorState.executeUpdate();
				updateOperatorState.close();
				return true;
			} catch (SQLException e) {
				throw new DALException(e);
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
	}

	public static ArrayList<OperatoerDTO> getOperatoerList() throws DALException {
		ArrayList<OperatoerDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorListState = con.prepareStatement("SELECT * FROM operatoer")) {
				getOperatorListState.closeOnCompletion();
				try (ResultSet rs = getOperatorListState.executeQuery()) {
					while (rs.next()) {
						if (isValidCPR(rs.getString("cpr")))
							list.add(new OperatoerDTO(rs.getInt("opr_id"), rs.getString("opr_navn"), rs.getString("ini"), rs.getString("cpr"), rs.getInt("lvl")));
					}
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Fejl opstået ved hentning af operatørliste.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
		return list;
	}

	/**
	 * Twerk function to retrieve list of specific types of user.
	 * @param opr_id : The userlevel to retrieve.
	 * @return ArrayList of OperatoerDTO containing the operators, only contains ID and NAME
	 * @throws DALException
	 */
	public static ArrayList<OperatoerDTO> getOperatoerList(int opr_id) throws DALException {
		ArrayList<OperatoerDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorListState = con.prepareStatement("SELECT * FROM operatoer WHERE opr_id = ?")) {
				getOperatorListState.closeOnCompletion();
				getOperatorListState.setInt(1, opr_id);
				try (ResultSet rs = getOperatorListState.executeQuery()) {
					while (rs.next()) {
						if (isValidCPR(rs.getString("cpr")))
							list.add(new OperatoerDTO(rs.getInt("opr_id"), rs.getString("opr_navn"), null, null, -1));
					}
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Fejl opstået ved hentning af operatørliste.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til databasen.");
		}
		return list;
	}
	

	public static boolean sackOperatoer(int opr) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement sackOperatorState = con.prepareStatement("UPDATE operatoer SET lvl = 0 WHERE opr_id = ?")) {
				sackOperatorState.setInt(1, opr);
				sackOperatorState.executeUpdate();
				sackOperatorState.close();
				return true;
			} catch (SQLException e) {
				throw new DALException("Kunne ikke fyre operatøren.");
			}
		} catch (SQLException sqle) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
	}

	public static int countOperatoer() throws DALException {
		int result = 0;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getSizeState = con.prepareStatement("SELECT COUNT(*) FROM operatoer")) {
				getSizeState.closeOnCompletion();
				try (ResultSet rs = getSizeState.executeQuery()) {
					rs.next();
					result = rs.getInt(1);
					rs.close();
				} catch (SQLException e) {
					throw new DALException("Kunne ikke læse antal operatører.");
				}
			}
		} catch (SQLException e1) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
		return result;
	}

	public static HashSet<String> getInitials() throws DALException {
		HashSet<String> returnSet = new HashSet<>();
		try (Connection con = conPool.getConnection()) {
			final String col = "ini";
			try (PreparedStatement getInitialsState = con.prepareStatement("SELECT " + col + " FROM operatoer")) {
				getInitialsState.closeOnCompletion();
				try (ResultSet rs = getInitialsState.executeQuery()) {
					while (rs.next()) if (!rs.wasNull()) returnSet.add(rs.getString(col));
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Fejl ved hentning af initialiser. " + e.getMessage());
			}
		} catch (SQLException e1) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
		return returnSet;
	}
	
	public static String getOperatorName(int oprId) throws DALException {
		String name = null;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorNameState = con.prepareStatement("SELECT opr_navn FROM operatoer where opr_id = ?")) {
				getOperatorNameState.closeOnCompletion();
				getOperatorNameState.setInt(1, oprId);
				try (ResultSet rs = getOperatorNameState.executeQuery()) {
					rs.first();
					name = rs.getString(1);
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke hente operatørnavn " + e);
			}
		} catch (SQLException e1) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
		return name;
	}
	
	public static String getSalt(int oprId) throws DALException {
		String salt = null;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getSaltState = con.prepareStatement("SELECT salt FROM operatoer WHERE opr_id = ?")) {
				getSaltState.setInt(1, oprId);
				try (ResultSet rs = getSaltState.executeQuery()) {
					rs.first();
					salt = rs.getString(1);
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException(e.getMessage());
			}
		} catch (SQLException e1) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
		return salt;
	}
	
	public static boolean updatePW(OperatoerDTO opr) throws DALException {
		String salt = PasswordGenerator.generatePassword((int) (Math.random() * 50) + 1, true, Integer.MAX_VALUE);
		String hash = Hasher.genHash(salt, opr.getPassword());
		//String hash = new SHA1().doHash(salt + opr.getPassword() + salt);
		if (!"".equals(salt) || !"".equals(hash)) { /* bingo! */
			try (Connection con = conPool.getConnection()) {
				try (PreparedStatement updatePassword = con.prepareStatement("UPDATE operatoer SET password = ? , salt = ? WHERE opr_id = ?")) {
					updatePassword.setString(1, hash);
					updatePassword.setString(2, Base64.getEncoder().encodeToString(salt.getBytes())); //  Base64Coder.encodeString(salt));
					updatePassword.setInt(3, opr.getId());
					updatePassword.executeUpdate();
					updatePassword.close();
					return true;
				} catch (SQLException e) {
					throw new DALException("Database tilgangsfejl, kontakt administrator.");
				}
			} catch (SQLException e1) {
				throw new DALException("Manglende forbindelse til databasen.");
			}
		}
		throw new DALException("Fatal fejl ved opdatering af kodeord, kontakt venligst en administrator eller den nærmeste bager.");
	}
	
	public static String getOperatorPassword(int ID) throws DALException {
		String returnString = null;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getOperatorPasswordStatement = con.prepareStatement("SELECT password FROM operatoer WHERE opr_id = ?")) {
				getOperatorPasswordStatement.closeOnCompletion();
				getOperatorPasswordStatement.setInt(1, ID);
				try (ResultSet rs = getOperatorPasswordStatement.executeQuery()) {
					if (rs.first()) returnString = rs.getString("password");
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Operatoeren " + ID + " findes ikke.");
			}
		} catch (SQLException sqle) {
			throw new DALException("Manglende forbindelse til databasen.");
		}
		return returnString;
	}
	
	private static boolean isValidCPR(String cpr) {
		return (CPRValidering.validateCPR(cpr) > 0);
	}
	private static String genPassword(int operatorID) {
		return PasswordGenerator.generatePassword(60, true, operatorID);
	}
	

	/** Aggresive attempt at generating a "random" number based partialy on ID.<br>
	 * <strong>Homebrewed random!</strong>
	 * 
	 * @param ID
	 *            : The operator ID
	 * @return : Magic double number for session awareness. */
	@SuppressWarnings("unused")
	private static double doTheMagic(int ID) {
		SecureRandom rnd = new SecureRandom();
		double ret = 0, loop = rnd.nextDouble() * (rnd.nextDouble() + ID);
		for (int i = 0; i < (int) loop + ID; i++)
			ret = rnd.nextDouble() * 31.3333333333;
		Date d = new Date(Double.doubleToLongBits(ret));
		return Double.parseDouble(Double.toString((loop + ID)) + d.toString().replaceAll("-", "").substring(1));
	}
}
