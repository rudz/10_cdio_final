package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.exceptions.DALException;

/**
 * ProduktBatchKompDAO<br>
 * Using PreparedStatements and Java 7 resource handling.
 * @author Rudy Alex Kohn [s133235]
 */
public class MySQLProduktBatchKompDAO {

	
	
	private static DataSource conPool;
	
	static{
		try{
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			System.out.println("FATAL FEJL!");
			e.printStackTrace();
		}
	}
	
	public static ProduktBatchKompDTO getProduktBatchKomp(int pbId, int rbId) throws DALException {
		ProduktBatchKompDTO dto = null;
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement getProduktBatchKompStatement = connection.prepareStatement("SELECT * FROM produktbatchkomponent WHERE (rb_id=? AND pb_id=?)")) {
			getProduktBatchKompStatement.closeOnCompletion();
			getProduktBatchKompStatement.setInt(1, rbId);
			getProduktBatchKompStatement.setInt(2, pbId);
			try (ResultSet rs = getProduktBatchKompStatement.executeQuery()) {
				if (rs.first()) {
					dto = new ProduktBatchKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),rs.getInt(5));
					rs.close();
					return dto;
				}
				rs.close();
				throw new DALException("ProduktBatchKomp: " + Integer.toString(rbId) + " med pb id: " + Integer.toString(pbId) + " findes ikke.");
			}
		} catch (SQLException sqle) {
			throw new DALException("DBase tilgang.");
		}
		
	} catch (SQLException e) {
		throw new DALException("DBase tilgang.");
	}
	}
	
	public static ArrayList<ProduktBatchKompDTO> getProduktBatchKompList(int pbId) throws DALException {		
		ArrayList<ProduktBatchKompDTO> list = new ArrayList<>();
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement ProduktBatchKompList = connection.prepareStatement("SELECT produktbatchkomponent.pb_id, produktbatchkomponent.rb_id, produktbatchkomponent.tara, produktbatchkomponent.netto,"
				+ "produktbatchkomponent.opr_id,"
				+ "produktbatch.oprettet,"
				+ "produktbatch.prod_start,"
				+ "produktbatch.prod_slut,"
				+ "raavarebatch.raavare_id FROM "
				+ "produktbatchkomponent JOIN produktbatch "
				+ "ON produktbatch.pb_id=produktbatchkomponent.pb_id JOIN raavarebatch on produktbatchkomponent.rb_id = raavarebatch.rb_id WHERE produktbatch.pb_id=?")) {
//			ProduktBatchKompList.closeOnCompletion();
			ProduktBatchKompList.setInt(1, pbId);
			try (ResultSet rs = ProduktBatchKompList.executeQuery()) {
				while (rs.next()) list.add(new ProduktBatchKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),rs.getInt(5),rs.getDate(6),rs.getDate(7),rs.getDate(8), rs.getInt(9)));
				rs.close();
			}
		} catch (SQLException sqle) {
			throw new DALException(sqle.getMessage());
		}
		return list;
	} catch (SQLException e) {
		throw new DALException("DBase tilgang.");
	}
	}

	
	public static ArrayList<ProduktBatchKompDTO> getProduktBatchKompList() throws DALException {
		ArrayList<ProduktBatchKompDTO> list = new ArrayList<>();
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement ProduktBatchKompList = connection.prepareStatement("SELECT * FROM produktBatchKomponent")) {
			ProduktBatchKompList.closeOnCompletion();
			try (ResultSet rs = ProduktBatchKompList.executeQuery()) {
				while (rs.next()) list.add(new ProduktBatchKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4),rs.getInt(5)));
				rs.close();
			}
		} catch (SQLException sqle) {
			throw new DALException("DBase tilgang.");
		}
		return list;
		} catch (SQLException e) {
			throw new DALException("DBase tilgang.");
		}
	}

	public static void createProduktBatchKomp(ProduktBatchKompDTO pb_komp) throws DALException {
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement createProduktBatchKomp = connection.prepareStatement("INSERT INTO produktbatchkomponent (pb_id, rb_id, tara, netto, opr_id) VALUES(?,?,?,?,?)")) {
			createProduktBatchKomp.setInt(1, pb_komp.getPbId());
			createProduktBatchKomp.setInt(2, pb_komp.getRbId());
			createProduktBatchKomp.setDouble(3, pb_komp.getTara());
			createProduktBatchKomp.setDouble(4, pb_komp.getNetto());
			createProduktBatchKomp.setInt(5, pb_komp.getOprId());
			createProduktBatchKomp.executeUpdate();
		} catch (SQLException sqle) {
			throw new DALException("DBase tilgang.");
		}
		} catch (SQLException e) {
			throw new DALException("DBase tilgang.");
		}
		
	}

	
	public static void updateProduktBatchKomp(ProduktBatchKompDTO pb_komp) throws DALException {
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement createProduktBatchKomp = connection.prepareStatement("UPDATE produktbatchkomponent SET pb_id=?, rb_id=?, tara=?, netto=?, opr_id=? WHERE (pb_id=? AND rb_id=?)")) {
			createProduktBatchKomp.setInt(1, pb_komp.getPbId());
			createProduktBatchKomp.setInt(2, pb_komp.getRbId());
			createProduktBatchKomp.setDouble(3, pb_komp.getTara());
			createProduktBatchKomp.setDouble(4, pb_komp.getNetto());
			createProduktBatchKomp.setInt(5, pb_komp.getOprId());
			createProduktBatchKomp.setInt(6, pb_komp.getPbId());
			createProduktBatchKomp.setInt(7, pb_komp.getRbId());
			createProduktBatchKomp.executeUpdate();
		} catch (SQLException sqle) {
			throw new DALException("DBase tilgang.");
		}
		} catch (SQLException e) {
			throw new DALException("DBase tilgang.");
		}
	}

	
	public static void deleteProduktBatchKomp(int pbId, int rbId) throws DALException {
		try(Connection connection = conPool.getConnection()){
		try (PreparedStatement deleteProduktBatchKomp = connection.prepareStatement("DELETE FROM produktbatchkomponent WHERE (pb_id=? AND rb_id=?")) {
			deleteProduktBatchKomp.setInt(1, pbId);
			deleteProduktBatchKomp.setInt(2, rbId);
			deleteProduktBatchKomp.executeUpdate();
		} catch (SQLException sqle) {
			throw new DALException("DBase tilgang.");
		}
		} catch (SQLException e) {
			throw new DALException("DBase tilgang.");
		}
	}
}
