
package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.exceptions.DALException;


/** Produkt batch DAO.<br>
 * Using PreparedStatements and Java 7 resource handling. <<<<<<< HEAD
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class MySQLProduktBatchDAO {

	private static DataSource	conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			System.out.println("FATAL FEJL!");
			e.printStackTrace();
		}
	}

	public static ProduktBatchDTO getProduktBatch(int pbId) throws DALException {
		ProduktBatchDTO dto = null;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getProduktBatchStatement = 
						con.prepareStatement("SELECT pb_id, status, produktbatch.recept_id, recept_navn FROM produktbatch join recept "
						+ "on produktbatch.recept_id = recept.recept_id WHERE pb_id= ?")) {
				getProduktBatchStatement.closeOnCompletion();
				getProduktBatchStatement.setInt(1, pbId);
				try (ResultSet rs = getProduktBatchStatement.executeQuery()) {
					if (rs.first()) {
						dto = new ProduktBatchDTO(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4));
						rs.close();
					}
				}
				con.close();
			} catch (SQLException e) {
				throw new DALException("Kunne ikke indhente produktbatch");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til MySQL");
		}
		return dto;

	}

	public static ArrayList<ProduktBatchDTO> getProduktBatchList() throws DALException {
		ArrayList<ProduktBatchDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getProduktBatchListStatement = con.prepareStatement("SELECT pb_id, recept_id, status FROM produktbatch")) {
				getProduktBatchListStatement.closeOnCompletion();
				try (ResultSet rs = getProduktBatchListStatement.executeQuery()) {
					while (rs.next())
						list.add(new ProduktBatchDTO(rs.getInt("pb_id"), rs.getInt("status"), rs.getInt("recept_id")));
					rs.close();
				}
			}
		} catch (SQLException e) {
			throw new DALException("Kunne ikke indhente produktbatchliste");
		}
		return list;
	}
	public static ArrayList<ProduktBatchDTO> getProduktBatchList(int start, int end) throws DALException {
		ArrayList<ProduktBatchDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getProduktBatchListStatement = con
					.prepareStatement("SELECT pb_id, recept_id, status FROM produktbatch WHERE pb_id BETWEEN ? and ? ")) {
				getProduktBatchListStatement.closeOnCompletion();
				getProduktBatchListStatement.setInt(1, start);
				getProduktBatchListStatement.setInt(2, end);
				try (ResultSet rs = getProduktBatchListStatement.executeQuery()) {
					while (rs.next())
						list.add(new ProduktBatchDTO(rs.getInt("pb_id"), rs.getInt("status"), rs.getInt("recept_id")));
					rs.close();
				}
			}
		} catch (SQLException e) {
			throw new DALException("Kunne ikke indhente produktbatchliste");
		}
		return list;
	}

	public static boolean createProduktBatch(ProduktBatchDTO produktbatch) throws DALException {

		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement createProduktBatchStatement = con
					.prepareStatement("INSERT INTO produktbatch(pb_id, recept_id, status) VALUES (?,?,?)")) {
				createProduktBatchStatement.setInt(1, produktbatch.getPbId());
				createProduktBatchStatement.setInt(2, produktbatch.getReceptId());
				createProduktBatchStatement.setInt(3, produktbatch.getStatus());
				createProduktBatchStatement.executeUpdate();
				createProduktBatchStatement.close();
				con.close();
				return true;
			} catch (SQLException e) {
				throw new DALException("Kunne ikke oprette produktbatch");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse");
		}
	}

	public static boolean updateProduktBatch(ProduktBatchDTO produktbatch) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement updateProduktBatchStatement = con
					.prepareStatement("UPDATE produktbatch SET status = ?, recept_id = ? WHERE pb_id = ?")) {
				updateProduktBatchStatement.setInt(1, produktbatch.getStatus());
				updateProduktBatchStatement.setInt(2, produktbatch.getReceptId());
				updateProduktBatchStatement.setInt(3, produktbatch.getPbId());
				updateProduktBatchStatement.executeUpdate();
				updateProduktBatchStatement.close();
				con.close();
				return true;
			} catch (SQLException e) {
				throw new DALException("Kunne ikke ændre i produktbatch");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse");
		}

	}

	public static boolean deleteProduktBatch(int pb_id) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement deleteProduktBatchStatement = con.prepareStatement("DELETE FROM produktbatch WHERE pb_id = ?")) {
				deleteProduktBatchStatement.setInt(1, pb_id);
				deleteProduktBatchStatement.executeUpdate();
				deleteProduktBatchStatement.close();
				con.close();
				return true;
			} catch (SQLException e) {
				throw new DALException("Kunne ikke slette produktbatch");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse");
		}
	}

	public static boolean createProduktBatchList(ArrayList<ProduktBatchDTO> list) throws DALException {
		try (Connection con = conPool.getConnection()) {
			for (ProduktBatchDTO produktbatch : list) {
				try (PreparedStatement createProduktBatchStatement = con.prepareStatement("INSERT INTO produktbatch(recept_id) VALUES (?)")) {
					createProduktBatchStatement.setInt(1, produktbatch.getReceptId());
					createProduktBatchStatement.executeUpdate();
					createProduktBatchStatement.close();
				}
			}
			return true;
		} catch (SQLException e) {
			throw new DALException(e.getMessage());
		}
	}

	public static ArrayList<ProduktBatchDTO> getProduktBatchListWithStatus(int start, int end, int s0, int s1, int s2) throws DALException {

		ArrayList<ProduktBatchDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getProduktBatchListStatement = con
					.prepareStatement("SELECT produktbatch.pb_id, produktbatch.recept_id, produktbatch.status, recept.recept_navn , produktbatch.oprettet, produktbatch.prod_start, produktbatch.prod_slut FROM produktbatch INNER JOIN recept ON recept.recept_id = produktbatch.recept_id"
							+ " WHERE produktbatch.pb_id BETWEEN ? and ? AND (produktbatch.status = ? OR produktbatch.status = ? OR produktbatch.status = ?)")) {
				getProduktBatchListStatement.closeOnCompletion();
				getProduktBatchListStatement.setInt(1, start);
				getProduktBatchListStatement.setInt(2, end);
				getProduktBatchListStatement.setInt(3, s0);
				getProduktBatchListStatement.setInt(4, s1);
				getProduktBatchListStatement.setInt(5, s2);
				try (ResultSet rs = getProduktBatchListStatement.executeQuery()) {
					while (rs.next())
						list.add(new ProduktBatchDTO(rs.getInt("pb_id"), rs.getInt("status"), rs.getInt("recept_id"), rs.getString("recept_navn"),rs.getString("oprettet"),rs.getString("prod_start"),rs.getString("prod_slut")));
					rs.close();
				}
			}
		} catch (SQLException e) {
			throw new DALException("Kunne ikke indhente produktbatchliste");
		}
		return list;
	}

}
