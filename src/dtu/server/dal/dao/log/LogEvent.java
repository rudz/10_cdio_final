package dtu.server.dal.dao.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import dtu.server.local.log.LogObject;
import dtu.server.local.log.LogObject.type;

public abstract class LogEvent {
	
	public static void logEvent(LogObject log, String event, Connection con) {
		/* generate prepared statement */
		try (PreparedStatement logStatement = con.prepareStatement("INSERT INTO log_?(date, time, operator, action) VALUES (?,?,?,?)")) {
			logStatement.setString(1, event);
			logStatement.setString(2, log.getDate());
			logStatement.setString(3, log.getTime());
			logStatement.setInt(4, log.getOprID());
			if (log.getType() == type.OPERATOR_ACTION)
				logStatement.setDouble(5, Double.valueOf(Integer.toString(log.getAction())));
			else
				logStatement.setDouble(5, log.getWeightedAmount());
			logStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
