package dtu.server.dal.dao.log;

import java.beans.PropertyVetoException;
import dtu.server.dal.dao.pool.DataSource;

/**
 * Generic log DAO.
 * @author Rudy Alex Kohn [s133235]
 */
public class LogDAO {
	@SuppressWarnings("unused")
	private static DataSource	conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
