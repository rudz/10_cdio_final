package dtu.server.dal.dao.pool;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import com.mchange.v2.c3p0.ComboPooledDataSource;

/** Simple data source instance class for connection pools.<br>
 * Usage:<br>
 * getInstance -> getConnection -> Use statement off that.<br>
 * @author Rudy Alex Kohn [s133235] */
public class DataSource {

	private static DataSource		datasource;
	private ComboPooledDataSource	cpds;

	private DataSource() throws PropertyVetoException {
		cpds = new ComboPooledDataSource();
		cpds.setDriverClass("com.mysql.jdbc.Driver"); // loads the jdbc driver

//		cpds.setJdbcUrl("jdbc:mysql://62.79.16.16/grp10");
//		cpds.setUser("grp10");
//		cpds.setPassword("sjXCvqWc");

		
		cpds.setJdbcUrl("jdbc:mysql://127.0.0.1/g10final");
		cpds.setUser("root");

		
		/* configure the pool */
		cpds.setInitialPoolSize(2);
		cpds.setAcquireIncrement(1);
		cpds.setMinPoolSize(2);
		cpds.setMaxPoolSize(3);
		cpds.setMaxConnectionAge(30);
		//cpds.setMaxStatements(200);
	}

	public static DataSource getInstance() throws PropertyVetoException {
		if (datasource == null) datasource = new DataSource();
		return datasource;
	}

	public Connection getConnection() throws SQLException {
		return this.cpds.getConnection();
	}

}
