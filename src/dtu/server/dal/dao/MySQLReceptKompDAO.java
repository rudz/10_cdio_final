
package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.exceptions.DALException;


/** ReceptKomp DAO.<br>
 * Using PreparedStatements and <i>ARM</i>.
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class MySQLReceptKompDAO {

	private static DataSource	conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	public static boolean createReceptList(ArrayList<ReceptKompDTO> list) throws DALException {
		try (Connection con = conPool.getConnection()) {			
			for (ReceptKompDTO DTO : list) {				
				try (PreparedStatement createReceptKompStatement = con
						.prepareStatement("INSERT INTO receptkomponent (recept_id,raavare_id,nom_netto,tolerance, sekvens) VALUES (?,?,?,?,?)")) {
					createReceptKompStatement.setInt(1, DTO.getReceptId());
					createReceptKompStatement.setInt(2, DTO.getRaavareId());
					createReceptKompStatement.setDouble(3, DTO.getNomNetto());
					createReceptKompStatement.setDouble(4, DTO.getTolerance());
					createReceptKompStatement.setInt(5, DTO.getSequence());
					createReceptKompStatement.executeUpdate();

				} catch (SQLException e) {
					throw new DALException("Tilgang til dbasen.");
				}
			}
			return true;
		} catch (SQLException e1) {
			throw new DALException("Ingen adgang til databasen");
		}

	}
	public static ReceptKompDTO getReceptKomp(int receptId, int raavareId) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptKompStatement = con
					.prepareStatement("SELECT * FROM receptkomponent WHERE (recept_id = ? AND raavare_id = ?)")) {
				getReceptKompStatement.closeOnCompletion();
				getReceptKompStatement.setInt(1, receptId);
				getReceptKompStatement.setInt(2, raavareId);
				try (ResultSet rs = getReceptKompStatement.executeQuery()) {
					if (!rs.first()) throw new DALException("Recept Komponent: " + receptId + " med raavare id: " + raavareId + " findes ikke.");
					return new ReceptKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4), rs.getInt(5));
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få receptkomponent med receptId : '" + Integer.toString(receptId) + "' og raavareId : '"
						+ Integer.toString(raavareId) + "'.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}

	}
	public static ArrayList<ReceptKompDTO> getReceptKompListWCU(int receptId, int pb_id) throws DALException {
		ArrayList<ReceptKompDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptKompListStatement = con.prepareStatement("select * from receptkomponent"
						+" where receptkomponent.recept_id = ? " + 
						"and receptkomponent.raavare_id not in " +
						"(SELECT distinct raavare.raavare_id FROM receptkomponent " +
						"join recept on recept.recept_id=receptkomponent.recept_id "+
						"join produktbatch on recept.recept_id=produktbatch.recept_id "+
						"join produktbatchkomponent on produktbatch.pb_id=produktbatchkomponent.pb_id "+
						"join raavarebatch on produktbatchkomponent.rb_id=raavarebatch.rb_id "+
						"join raavare on raavarebatch.raavare_id= raavare.raavare_id "+
						"WHERE receptkomponent.recept_id = ? and produktbatch.pb_id = ?)"
						+ " GROUP BY sekvens;")) {
				getReceptKompListStatement.closeOnCompletion();
				getReceptKompListStatement.setInt(1, receptId);
				getReceptKompListStatement.setInt(2, receptId);
				getReceptKompListStatement.setInt(3, pb_id);
				try (ResultSet rs = getReceptKompListStatement.executeQuery()) {
					while (rs.next())
						list.add(new ReceptKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4), rs.getInt(5)));
					if (list.isEmpty())
						throw new DALException("Der er ingen receptKomp med den recept id");
				}
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
		return list;
	}


	public static ArrayList<ReceptKompDTO> getReceptKompList(int receptId) throws DALException {
		ArrayList<ReceptKompDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptKompListStatement = con.prepareStatement("SELECT * FROM receptkomponent WHERE recept_id = ? GROUP BY sekvens")) {
				getReceptKompListStatement.closeOnCompletion();
				getReceptKompListStatement.setInt(1, receptId);
				try (ResultSet rs = getReceptKompListStatement.executeQuery()) {
					while (rs.next())
						list.add(new ReceptKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4), rs.getInt(5)));
					if (list.isEmpty())
						throw new DALException("Der er ingen receptKomp med den recept id");
				}
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
		return list;
	}

	public static ArrayList<ReceptKompDTO> getReceptKompList() throws DALException {
		ArrayList<ReceptKompDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptKompListStatement = con.prepareStatement("SELECT * FROM receptkomponent")) {
				getReceptKompListStatement.closeOnCompletion();
				try (ResultSet rs = getReceptKompListStatement.executeQuery()) {
					if (!rs.first()) throw new DALException("Der findes ikke nogen receptkomp.");
					while (rs.next())
						list.add(new ReceptKompDTO(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getDouble(4), rs.getInt(5)));
				}
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
		if (!list.isEmpty()) Collections.sort(list, receptKompDTOComparator);
		return list;
	}

	public static boolean createReceptKomp(ReceptKompDTO receptkomponent) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement createReceptKompStatement = con
					.prepareStatement("INSERT INTO receptkomponent (recept_id,raavare_id,nom_netto,tolerance) VALUES (?,?,?,?)")) {
				createReceptKompStatement.setInt(1, receptkomponent.getReceptId());
				createReceptKompStatement.setInt(2, receptkomponent.getRaavareId());
				createReceptKompStatement.setDouble(3, receptkomponent.getNomNetto());
				createReceptKompStatement.setDouble(4, receptkomponent.getTolerance());
				createReceptKompStatement.executeUpdate();
				return true;
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
	}

	public static void updateReceptKomp(ReceptKompDTO receptkomponent) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement updateReceptKompStatement = con
					.prepareStatement("UPDATE receptkomponent SET recept_id=? ,raavare_id=?,nom_netto=?,tolerance=?,sekvens=? WHERE (recept_id=? AND raavare_id=?)")) {
				updateReceptKompStatement.setInt(1, receptkomponent.getReceptId());
				updateReceptKompStatement.setInt(2, receptkomponent.getRaavareId());
				updateReceptKompStatement.setDouble(3, receptkomponent.getNomNetto());
				updateReceptKompStatement.setDouble(4, receptkomponent.getTolerance());
				updateReceptKompStatement.setInt(5, receptkomponent.getSequence());
				updateReceptKompStatement.setInt(6, receptkomponent.getReceptId());
				updateReceptKompStatement.setInt(7, receptkomponent.getRaavareId());
				updateReceptKompStatement.executeUpdate();
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
	}

	public static void deleteReceptKomp(int receptId, int raavareId) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement deleteReceptKompStatement = con
					.prepareStatement("DELETE FROM receptkomponent WHERE (recept_id=? AND raavare_id=?)")) {
				deleteReceptKompStatement.setInt(1, receptId);
				deleteReceptKompStatement.setInt(2, raavareId);
				deleteReceptKompStatement.executeUpdate();
			} catch (SQLException e) {
				throw new DALException("Tilgang til dbasen.");
			}
		} catch (SQLException e1) {
			throw new DALException("Kunne ikke oprette forbindelse til databasen.");
		}
	}
	
	public static void updateReceptKomponenter(ArrayList<ReceptKompDTO> list) throws DALException{		
			for(int i = 0; i < list.size(); i++){				
				updateReceptKomp(list.get(i));			
		}			
	}
	
	/** Comparator for ReceptKompDTO, used to sort the list! */
	static Comparator<ReceptKompDTO> receptKompDTOComparator = new Comparator<ReceptKompDTO>() {
		@Override
		public int compare(ReceptKompDTO currentItem, ReceptKompDTO key) {
			if (currentItem.getSequence() < key.getSequence()) return -1;
			if (currentItem.getSequence() > key.getSequence()) return 1;
			return 0;
		}
	};

}
