package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;


public abstract class MySQLVaegtForbindelseDAO {

	private static DataSource conPool;
	
	static{

		try{			
			conPool = DataSource.getInstance();	
			
		} catch(PropertyVetoException e){
			e.printStackTrace();		
		}
	}

	//@Override
	public static OperatoerDTO getForbindelse(int id) throws DALException {
		// TODO Auto-generated method stub
		return null;
	}

	//@Override
	public static ArrayList<VaegtForbindelseDTO> getForbindelseList() throws DALException {		

		ArrayList<VaegtForbindelseDTO> list = new ArrayList<>();		
		try(Connection con = conPool.getConnection()){
			try(PreparedStatement getForbindelseListe = con.prepareStatement(" SELECT * FROM vaegtforbindelse")){
				getForbindelseListe.closeOnCompletion();
				try(ResultSet rs = getForbindelseListe.executeQuery()){
					while(rs.next()){
						list.add(new VaegtForbindelseDTO(rs.getInt("id"), rs.getString("ip"), rs.getInt("port")));
					}
				}
			}
		} catch (SQLException e) {
			throw new DALException(e);
		}	
	
		return list;
	}

	//@Override
	public static boolean createForbindelse(VaegtForbindelseDTO vfDTO) throws DALException {
		
		try(Connection con = conPool.getConnection()){
			try(PreparedStatement opretForbindelsesData = con.prepareStatement("INSERT INTO vaegtforbindelse (ip, port) VALUES (?, ?)")){
				opretForbindelsesData.closeOnCompletion();
				opretForbindelsesData.setString(1, vfDTO.getIp());
				opretForbindelsesData.setInt(2, vfDTO.getPort());				
				opretForbindelsesData.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DALException(e);
		}
		
		return false;
	}

	public static boolean sletForbindelse(int id) throws DALException{
		try(Connection con = conPool.getConnection()){
			try(PreparedStatement opretForbindelsesData = con.prepareStatement("DELETE FROM vaegtforbindelse WHERE id = ?")){
				opretForbindelsesData.closeOnCompletion();
				opretForbindelsesData.setInt(1, id);				
				opretForbindelsesData.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			throw new DALException(e);
		}	
	}

	//@Override
	public static boolean updateForbindelse(VaegtForbindelseDTO vfDTO) throws DALException {
		// TODO Auto-generated method stub
		return false;
	}	
}
