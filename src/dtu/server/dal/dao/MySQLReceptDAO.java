package dtu.server.dal.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import dtu.server.dal.dao.pool.DataSource;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.exceptions.DALException;

/**
 * Recept DAO.<br>
 * Using PreparedStatements and Java 7 <i>ARM</i>.
 * @author Rudy Alex Kohn [s133235]
 */
public class MySQLReceptDAO {
	
	private static DataSource conPool;

	static {
		try {
			conPool = DataSource.getInstance();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}
	
	public static ReceptDTO getRecept(int receptId) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptStatement = con.prepareStatement("SELECT recept_id, recept_navn FROM recept WHERE recept_id=?")) {
				getReceptStatement.closeOnCompletion();
				getReceptStatement.setInt(1, receptId);
				try (ResultSet rs = getReceptStatement.executeQuery()) {
					if (!rs.first()) throw new DALException("Recept: " + receptId + " findes ikke i databasen.");
					return new ReceptDTO(rs.getInt("recept_id"), rs.getString("recept_navn"));
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få recept med id " + receptId);
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}
	}
	
	/**
	 * Simplified detection if a specific recept exists in db.
	 * @param receptId : The receptId to check for.
	 * @return true if exists, otherwise false.
	 * @throws DALException
	 */
	public static boolean isRecept(int receptId) throws DALException  {
		boolean returnValue = false;
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptStatement = con.prepareStatement("SELECT recept_id FROM recept WHERE recept_id=?")) {
				getReceptStatement.closeOnCompletion();
				getReceptStatement.setInt(1, receptId);
				try (ResultSet rs = getReceptStatement.executeQuery()) {
					returnValue = rs.first();
					rs.close();
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få recept med id " + receptId);
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}
		return returnValue;
	}

	public static ArrayList<ReceptDTO> getReceptList() throws DALException {
		ArrayList<ReceptDTO> list = new ArrayList<>();
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement getReceptListStatement = con.prepareStatement("SELECT recept_id, recept_navn FROM recept")) {
				getReceptListStatement.closeOnCompletion();
				try (ResultSet rs = getReceptListStatement.executeQuery()) {
					if (!rs.first()) throw new DALException("Kunne ikke finde noget indhold i recepter.");
					do {
						list.add(new ReceptDTO(rs.getInt("recept_id"),rs.getString("recept_navn")));
					}while (rs.next());
				}
			} catch (SQLException e) {
				throw new DALException("Kunne ikke få receptlisten.");
			}
		} catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}
		if (!list.isEmpty()) Collections.sort(list, receptDTOComparator);
		return list;
	}
	
	public static boolean createRecept(ReceptDTO recept) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement createReceptStatement = con.prepareStatement("INSERT INTO recept(recept_id, recept_navn) VALUES (?,?)")) {
				createReceptStatement.setInt(1, recept.getReceptId());
				createReceptStatement.setString(2, recept.getReceptNavn());
				createReceptStatement.executeUpdate();
				return true;
			} catch (SQLException sqle) {
				throw new DALException("Kunne ikke oprette recept.");
			}
		} catch (SQLException e) {
			throw new DALException("Tilgang til db.");
		}
	}
	
	public static boolean updateRecept(ReceptDTO recept) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement updateReceptStatement = con.prepareStatement("UPDATE recept SET recept_navn = ? WHERE recept_id = ?)")) {
				updateReceptStatement.setString(1, recept.getReceptNavn());
				updateReceptStatement.setInt(2, recept.getReceptId());
				updateReceptStatement.executeUpdate();
				return true;
			} catch (SQLException sqle) {
				throw new DALException("Kunne ikke opdatere recept med recept id " + recept.getReceptId());
			}
		} catch (SQLException e) {
			throw new DALException("Tilgang til db.");
		}
	}
	
	@Deprecated
	public static boolean deleteRecept(int receptId) throws DALException {
		try (Connection con = conPool.getConnection()) {
			try (PreparedStatement updateReceptStatement = con.prepareStatement("DELETE FROM recept WHERE recept_id = ?")) {
				updateReceptStatement.setInt(1, receptId);
				updateReceptStatement.executeUpdate();
				return true;
			} catch (SQLException sqle) {
				throw new DALException("Kunne ikke slette recept med id " + receptId);
			}
		} catch (SQLException e) {
			throw new DALException("Tilgang til db.");
		}
	}
	
	public static ArrayList<ReceptKomponenterDTO> getReceptKomponenter(int receptId) throws DALException{
		ArrayList<ReceptKomponenterDTO> List = new ArrayList<>();
		try(Connection con = conPool.getConnection()){
			//System.out.println("connection good");
			try (PreparedStatement getKomponenterStatement = con.prepareStatement(
					"SELECT raavare_navn, raavare_id, recept_id, nom_netto, tolerance, sekvens "
							+ "FROM recept natural join "
							+ "receptkomponent natural join "
							+ "raavare "
							+ "where recept_id = ? GROUP BY sekvens")){
				//System.out.println("statement run");
//				getKomponenterStatement.closeOnCompletion();
				getKomponenterStatement.setInt(1, receptId);
				try(ResultSet rs = getKomponenterStatement.executeQuery()){
					//System.out.println("executed");
					if(!rs.first()) throw new DALException("ingen recept fundet");
					do{
						List.add(new ReceptKomponenterDTO(rs.getInt("raavare_id"), 
								rs.getInt("recept_id"), 
								rs.getString("raavare_navn"), 
								rs.getDouble("nom_Netto"), 
								rs.getDouble("tolerance"),
								rs.getInt("sekvens")));
					}while (rs.next());
					//System.out.println("returning");
				}

			}catch (SQLException sqle){
				throw new DALException("Ingen recept med id "+ receptId +" fundet" + sqle.getMessage());

			}
		}catch (SQLException e1) {
			throw new DALException("Tilgang til db.");
		}return List;
	}
	
	/** Comparator for ReceptKompDTO, used to sort the list! */
	static Comparator<ReceptDTO> receptDTOComparator = new Comparator<ReceptDTO>() {
		@Override
		public int compare(ReceptDTO currentItem, ReceptDTO key) {
			if (currentItem.getReceptId() < key.getReceptId()) return -1;
			if (currentItem.getReceptId() > key.getReceptId()) return 1;
			return 0;
		}
	};
}
