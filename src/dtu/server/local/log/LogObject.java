package dtu.server.local.log;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogObject implements Serializable{
	/* type of log */
	public enum type {
		
		/* action :
		 *  0 = sack
		 *  1 = create
		 *  2 = update
		 */
		OPERATOR_ACTION,
		

		WEIGHING;
	}
	
	private static final long serialVersionUID = -8254238293515459124L;
	
	private String currentDate, currentTime, event;
	private int oprID, action;
	private double weightedAmount;
	private type logType;
	
	
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	private final String CSV_SPLIT = ",";

	public LogObject() { }

	public LogObject(int ID, String event, double weightedAmount) {
		Date date = new Date();
		currentDate = dateFormat.format(date);
		currentTime = timeFormat.format(date);
		this.event = event;
		this.oprID = ID;
		this.weightedAmount = weightedAmount;
		logType = type.WEIGHING;
	}

	public LogObject(int ID, String event, int action) {
		Date date = new Date();
		currentDate = dateFormat.format(date);
		currentTime = timeFormat.format(date);
		this.event = event;
		this.oprID = ID;
		this.action = action;
		logType = type.OPERATOR_ACTION;
	}

	public int getAction() {
		return action;
	}
	public String getDate() {
		return currentDate;
	}
	public String getEvent() {
		return event;
	}
	public int getOprID() {
		return oprID;
	}
	public String getTime() {
		return currentTime;
	}
	public type getType() {
		return this.logType;
	}
	public double getWeightedAmount() {
		return weightedAmount;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public void setDate(String date) {
		this.currentDate = date;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public void setOprID(int oprID) {
		this.oprID = oprID;
	}
	public void setTime(String time) {
		this.currentTime = time;
	}
	public void setType(type logType) {
		this.logType = logType;
	}
	public void setWeightedAmount(double weightedAmount) {
		this.weightedAmount = weightedAmount;
	}
	@Override
	public String toString() {
		return getDate() + CSV_SPLIT + getTime() + CSV_SPLIT + getOprID() + CSV_SPLIT + getEvent() + CSV_SPLIT + getWeightedAmount();
	}
	
}
