package dtu.server.local;

import java.security.SecureRandom;
import dtu.shared.validate.PasswordValidator;

/**
 * Simple class for generating a valid password.<br>
 * Uses SecureRandom.
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class PasswordGenerator extends PasswordValidator {
	
	private final static char[] charsSpecial		= new char[] {'.', '-', '_', '+', '!', '?', '='};
	private final static char[] upperCharBounds		= new char[] {'9', 'z', 'Z'};
	private final static char[] lowerCharBounds		= new char[] {'0', 'a', 'A'};

	/**
	 * Generates a password with user defined length,
	 * <br>which adheres to the current rules for DTU password adversity.
	 * <p>It utilises SecureRandom to generate it's randomness.
	 * @param len : The length of the password to generate
	 * @param includeSpecialChars : if true, special character are included in the generating process.
	 * @return The password generated as string.
	 */
	private static String genPassword(int len, boolean includeSpecialChars) {
		SecureRandom sr = new SecureRandom();
		StringBuilder ret = new StringBuilder(len+1);
		int i, adj = (includeSpecialChars ? 4 : 3);
		len -= adj;
		for (int j = 1; j <= len; j++) {
			i = (int) (sr.nextDouble() * (adj));
			if (i < 3)
				ret.append((char) (sr.nextDouble() * (upperCharBounds[i] - lowerCharBounds[i]) + lowerCharBounds[i]));
			else
				ret.append(charsSpecial[(int) (sr.nextDouble() * charsSpecial.length)]);
		}
		int rnGeezuz = new Double(sr.nextDouble() * adj).intValue() + 1;
		/* insert at least one of each char type @ random location (not each others!) */
		ret.insert(rnGeezuz,   (char) (sr.nextDouble() * (upperCharBounds[1] - lowerCharBounds[1]) + lowerCharBounds[1]));
		ret.insert(++rnGeezuz, (char) (sr.nextDouble() * (upperCharBounds[0] - lowerCharBounds[0]) + lowerCharBounds[0]));
		ret.insert(++rnGeezuz, (char) (sr.nextDouble() * (upperCharBounds[2] - lowerCharBounds[2]) + lowerCharBounds[2]));
		if (includeSpecialChars)
			ret.insert(++rnGeezuz, charsSpecial[(int) (sr.nextDouble() * charsSpecial.length)]);
		return ret.toString();
	}

	/**
	 * Generate a password with specific lenght, no less than min. req,  which doesn't contain a specified value.
	 * @param includeSpecialChars : if true, special characters are included in the generated password.
	 * @param avoidance : Number to be excluded from the password.
	 * @return : The generated password as string.
	 */
	public static String generatePassword(int lenght, boolean includeSpecialChars, int avoidance) {
		return generatePassword(lenght, includeSpecialChars, Integer.toString(avoidance));
	}
	public static String generatePassword(int lenght, boolean includeSpecialChars, String avoidance) {
		if (lenght < PW_LENGTH_REQ) lenght = PW_LENGTH_REQ;
		String s;
		do s = genPassword(lenght, includeSpecialChars);
		while (s.contains(avoidance));
		return s;
	}
	
	public static void main(String[] args) {
		long start = System.nanoTime();
		final int RUNS = 100000;
		String pw;
		for (int i = 0; i < RUNS; i++) {
			do {
				pw = generatePassword(200, true, i);
			} while (!isPasswordValidStrict(i, pw));
			//System.out.println(Base64Coder.encodeString("øå"+pw+"øå"));
//			System.out.println(pw + " valid=" + isPasswordValidStrict(i, pw) + " for " + i);
		}
		System.out.println(((System.nanoTime() - start) / RUNS) + " ns = 1 pw gen");
	}
	
}
