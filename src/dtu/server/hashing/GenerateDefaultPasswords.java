package dtu.server.hashing;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;
import dtu.server.local.PasswordGenerator;
import dtu.shared.misc.StringToolLib;


public class GenerateDefaultPasswords {

	private final static String[] foreNames = new String[] { "Antonio", "Arnold", "Bruce", "Carl", "Jaimy", "Eden", "Ben" };
	private final static String[] lastNames = new String[] { "Lannister", "Nyx", "Frost", "Lee", "Pikachu", "Stevenson", "Andersen" };
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		String pw = "1234";
		
		MessageDigest mda = MessageDigest.getInstance("SHA-512");
		final int numUsers = foreNames.length;
		String salt[] = new String[numUsers];
		String saltB64[] = new String[numUsers];
		String pwHashes[] = new String[numUsers];
		System.out.print("Generating salts.. ");
		for (int i = 0; i < numUsers; i++) salt[i] = PasswordGenerator.generatePassword(30, true, i);
		System.out.println("done...");
		System.out.print("Generating hashes.. ");
		for (int i = 0; i < numUsers; i++) {
			mda.update(salt[i].getBytes());
			mda.update(pw.getBytes());
			mda.update(salt[i].getBytes());
			pwHashes[i] = Hex.encodeHexString(mda.digest());
		}
		System.out.println(" done...");
		System.out.print("Base64 enconding.. ");
		for (int i = 0; i < numUsers; i++) saltB64[i] = dtu.server.token.Base64.getEncoder().encodeToString(salt[i].getBytes());
		System.out.println(" done...");
		
		System.out.println();
		System.out.println("Bruger data:");
		for (int i = 0; i < foreNames.length; i++) {
			System.out.println(StringToolLib.replicate('-', 40));
			System.out.println("navn    : " + foreNames[i] + " " + lastNames[i]);
			System.out.println("kodeord : " + pw);
			System.out.println("pwhash  : " + pwHashes[i]);
			System.out.println("saltB64 : " + saltB64[i]);
			System.out.println(StringToolLib.replicate('-', 40));
		}
	}
}
