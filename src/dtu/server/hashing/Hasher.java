package dtu.server.hashing;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;


public abstract class Hasher {

	public static String genHash(String salt, String pw) {
		try {
			MessageDigest mda = MessageDigest.getInstance("SHA-512");
			mda.update(salt.getBytes());
			mda.update(pw.getBytes());
			mda.update(salt.getBytes());
			return Hex.encodeHexString(mda.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
}
