package dtu.server.token;

import java.util.HashMap;

public abstract class SessionValidator {
	/** session where the key is user id login, and data is the DTO interface */
	private static HashMap<Integer, String>	session;
	
	/** tokenhandler instance */
	private static TokenHandler th;
	
	static {
		th = TokenHandler.getInstance();
		session = new HashMap<>();
	}
	
	/**
	 * Update the current token for the user.<br>
	 * @param id : The user ID
	 * @return true if Token was updated, otherwise false - in case it's invalid.
	 */
	public static boolean updateSessionToken(int id) {
		if (session.containsKey(id)) {
			session.put(id, th.validateToken(session.get(id)));
			return session.get(id) != null;
		}
		return false;
	}

	/**
	 * Create a new SessionToken for the current user.
	 * @param id : The user to create the token for.
	 */
	public static void createSessionToken(int id) {
		session.put(id, th.createToken(Integer.toString(id)));
	}

	/**
	 * Removed a token from the map. 
	 * @param id : The user ID for the token to remove.
	 */
	public static void killSessionToken(int id) {
		if (session.containsKey(id)) session.remove(id);
	}
	
	
	
}
