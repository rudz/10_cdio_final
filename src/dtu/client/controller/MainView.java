package dtu.client.controller;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import dtu.client.service.GenericClientImpl;
import dtu.client.ui.generic.ContentView;
import dtu.client.ui.navBar.NavBar;
import dtu.shared.LoginStatus;

public class MainView {

	// reference to remote data layer
	private GenericClientImpl clientImpl;

	// reference to ContentView
	private ContentView contentView;
	
	public MainView() {
		clientImpl = new GenericClientImpl(GWT.getModuleBaseURL() + "ipharmaservice");

//		// wrap menuView
//		MenuView m = new MenuView(this);
//		RootPanel.get("nav").add(m);

		NavBar navBar = new NavBar(this);
		RootPanel.get("nav").add(navBar);
		
		// wrap contentView
		contentView = new ContentView(clientImpl);
		RootPanel.get("section").add(contentView);

	}

	public void run() {
		if (LoginStatus.loggedIn)
			welcome();
		else
			login();
	}

	// Call back handlers
	public void login() {
		contentView.openLoginView();
	}
	
	public void logout() {
		contentView.openLogoutView();
	}

	public void welcome() {
		contentView.openWelcomeView();
	}

	public void addPerson() {
		contentView.openAddView();
	}

	public void showPersons() {
		contentView.openBrowseView();
	}

	public void editPersons() {
		contentView.openEditView();
	}

	public void deletePersons() {
		contentView.openDeleteView();
	}

	public void ChangePassword() {
		contentView.openChangePwView();
	}
	
	public void addRaavare(){
		contentView.openRaavareAddView();
	}
	
	public void browseRaavare(){
		contentView.openRaavareBrowse();
	}
	
	public void addCommodityBatch() {
		contentView.openCommodityBatchAdd();
	}

	public void editRaavare() {
		contentView.openRaavareEdit();
	}

	public void browseCommodityBatch() {
		contentView.openCommodityBatchBrowse();		
	}
	
	public void deleteCommodityBatch() {
		contentView.openCommodityBatchDelete();		
	}
	
	public void editCommodityBatch() {
		contentView.openCommodityBatchEdit();		
	}

	public void browseRecept() {
		contentView.openReceptBrowser();
	}
	public void addRecept() {
		contentView.openReceptAdd();
	}
	public void addProduktBatch(){
		contentView.openProduktBatchAdd();
	}
	public void browseProduktBatch(){
		contentView.openProduktBatchBrowse();
	}
	
	public void openForbindelseView() {
		contentView.openVaegtForbindlseView();
	}
	
	public void openForbindelseAdd(){
		contentView.openVaegtForbindelseAdd();
	}
}
