package dtu.client.service;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.RaavareDTO;


public interface IRaavareService {
	
	void createRaavare(RaavareDTO raavare, AsyncCallback<Boolean> callback);

	void raavareExists(int raavareId, AsyncCallback<Boolean> callback);

	void getRaavareList(AsyncCallback<ArrayList<RaavareDTO>> callback);
	
	void updateRaavare(RaavareDTO raavare, AsyncCallback<Boolean> callback);
	
	void isRaavare(int raavareId, AsyncCallback<Boolean> callback);

	void getRaavare(int id, AsyncCallback<RaavareDTO> callback);
	
	void deleteRaavare( int raavareId, AsyncCallback<Boolean> callback);
}
