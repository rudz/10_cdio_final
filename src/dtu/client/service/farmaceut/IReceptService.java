package dtu.client.service.farmaceut;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.exceptions.DALException;

public interface IReceptService {
	void getReceptList(AsyncCallback<ArrayList<ReceptDTO>> callback);
	void getRecept(int receptId, AsyncCallback<ReceptDTO> callback);
	void updateRecept(ReceptDTO receptDTO, AsyncCallback<Boolean> callback);
	void createRecept(ReceptDTO receptDTO, AsyncCallback<Boolean> callback);
	void deleteRecept(int receptId, AsyncCallback<Boolean> callback);
	void getReceptKomponenter(int receptId, AsyncCallback<ArrayList<ReceptKomponenterDTO>> callback);
	void getRaavareList(AsyncCallback<ArrayList<RaavareDTO>> callback);
	void createReceptKomponent(ReceptKompDTO receptKompDTO, AsyncCallback<Boolean> callback);
	void createReceptKomponentList(ArrayList<ReceptKompDTO> list, AsyncCallback<Boolean> callback);
	void isRecept(int receptId, AsyncCallback<Boolean> callback);
	void updateReceptKomponenter(ArrayList<ReceptKompDTO> list, AsyncCallback<Void> callback);	
	void updateReceptKomponent(ReceptKompDTO DTO, AsyncCallback<Void> callback) throws DALException;
}
