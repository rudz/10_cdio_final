package dtu.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.client.service.admin.IPasswordManager;
import dtu.shared.dto.db.OperatoerDTO;

public interface ILoginViewService extends IPasswordManager {
	@Override
	void passwordValidation(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	void getOperatoer(int oprId, AsyncCallback<OperatoerDTO> callback);
	void getToken(int id, AsyncCallback<String> callback);
	
}
