package dtu.client.service.admin;

import java.util.HashSet;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;

/**
 * Interface for OperatoerAdd
 * 
 */
public interface IAddViewService {
	void createOperatoer(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	void getInitials(AsyncCallback<HashSet<String>> callback);
	void genPassword(int ID, AsyncCallback<String> callback);
	void getSize(AsyncCallback<Integer> callback);
}
