package dtu.client.service.admin;

import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;


public interface IChangePasswordView {
	void updatePW(OperatoerDTO opr, AsyncCallback<Boolean> callback);
}
