package dtu.client.service.admin;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;

/**
 * Interfaces used by EditService
 */
public interface IEditService {
	void getOperatoerList(AsyncCallback<ArrayList<OperatoerDTO>> callback);
	void updateOperatoer(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	void updatePW(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	void genPassword(int ID, AsyncCallback<String> callback);
}
