package dtu.client.service.admin;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;

/**
 * Interface used by OperatoerDelete
 */
public interface IDeleteService {
	void getOperatoer(int oprId, AsyncCallback<OperatoerDTO> callback);
	void getOperatoerList(AsyncCallback<ArrayList<OperatoerDTO>> callback);
	void sackOperatoer(int oprId, AsyncCallback<Boolean> callback);
}
