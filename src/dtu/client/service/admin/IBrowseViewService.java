package dtu.client.service.admin;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;

/**
 * Interface for Browseview
 * 
 */
public interface IBrowseViewService {
	void getOperatoerList(AsyncCallback<ArrayList<OperatoerDTO>> callback);
}
