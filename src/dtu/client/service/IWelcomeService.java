package dtu.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Interface used only by WelcomeView
 * 
 */

public interface IWelcomeService {
	void getSize(AsyncCallback<Integer> callback);
}
