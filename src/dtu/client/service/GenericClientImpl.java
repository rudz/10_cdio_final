


package dtu.client.service;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public class GenericClientImpl {
	//gobla reference to service endpoint
	public IAdminServiceAsync service;
	
	public GenericClientImpl(String url){
		//set RPC service end point
		this.service = GWT.create(IAdminService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
	}
}
