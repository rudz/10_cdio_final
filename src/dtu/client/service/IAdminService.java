package dtu.client.service;

import java.util.ArrayList;
import java.util.HashSet;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;

@RemoteServiceRelativePath("ipharmaservice")
public interface IAdminService extends RemoteService {
	/* token stuff */
	String getToken(int id) throws DALException;
	
	
	
	OperatoerDTO getOperatoer(int oprId) throws DALException;
	String getOperatorName(int oprId) throws DALException;
	ArrayList<OperatoerDTO> getOperatoerList() throws DALException;
	ArrayList<OperatoerDTO> getOperatoerList(int opr_id) throws DALException;
	boolean createOperatoer(OperatoerDTO opr) throws DALException;
	boolean updateOperatoer(OperatoerDTO opr) throws DALException;
	int getSize()throws Exception;
	boolean sackOperatoer(int oprId) throws DALException;
	HashSet<String> getInitials() throws DALException;
	boolean updatePW(OperatoerDTO opr) throws DALException;
	boolean passwordValidation(OperatoerDTO opr) throws DALException;
	String genPassword(int operatorID) throws DALException;
	
	boolean createRaavare(RaavareDTO raavare) throws DALException;
	boolean raavareExists(int raavareId)throws DALException;
	boolean updateRaavare(RaavareDTO raavare) throws DALException;
	ArrayList<RaavareDTO> getRaavareList() throws DALException;
	RaavareDTO getRaavare(int id) throws DALException;
	boolean isRaavare(int raavareId) throws DALException;
	boolean deleteRaavare(int raavareId) throws DALException;
	
	RaavareBatchDTO getRaavareBatch(int id) throws DALException;
	boolean createRaavareBatch(RaavareBatchDTO raavarebatch) throws DALException;
	ArrayList<RaavareBatchDTO> browseRaavareBatch() throws DALException;
	boolean updateRaavareBatch(RaavareBatchDTO raavareBatchDTO) throws DALException;
	boolean deleteRaavareBatch(int rb_id) throws DALException;

	ReceptDTO getRecept(int receptId) throws DALException;
	boolean createRecept(ReceptDTO receptDTO) throws DALException;
	boolean updateRecept(ReceptDTO receptDTO) throws DALException;
	boolean deleteRecept(int receptId) throws DALException;
	ArrayList<ReceptDTO> getReceptList() throws DALException;
	ArrayList<ReceptKomponenterDTO> getReceptKomponenter(int receptId) throws DALException;
	boolean isRecept(int receptId) throws DALException;
	boolean createReceptKomponent(ReceptKompDTO receptKompDTO) throws DALException;
	boolean createReceptKomponentList(ArrayList<ReceptKompDTO> list) throws DALException;
	void updateReceptKomponent(ReceptKompDTO DTO) throws DALException;

	boolean updateProduktBatch(ProduktBatchDTO produktBatchDTO) throws DALException;
	boolean createProduktBatch(ProduktBatchDTO produktBatchDTO) throws DALException;
	boolean deleteProduktBatch(int produktBatchId) throws DALException;
	boolean createProduktBatchList(ArrayList<ProduktBatchDTO> list) throws DALException;
	

	ProduktBatchDTO getProduktBatch(int rbid) throws DALException;
	ArrayList<ReceptKompDTO> getReceptKomp(int i) throws DALException;
	void createProduktBatchKomp(ProduktBatchKompDTO dto) throws DALException;
	ArrayList<ProduktBatchKompDTO> getProduktBatchKompList(int pbId) throws DALException;
	ArrayList<ProduktBatchDTO> getProduktBatchList(int start, int end, int s0, int s1, int s2) throws DALException;
	ArrayList<ProduktBatchDTO> getProduktBatchListWithStatus(int start, int end, int s0, int s1, int s2) throws DALException;
	void updateReceptKomponenter(ArrayList<ReceptKompDTO> list) throws DALException;
	boolean raavareBatchIdExists(int rb_id) throws DALException;

	ArrayList<VaegtForbindelseDTO> getForbindelseList() throws DALException;
	void OpretForbindelsesData(VaegtForbindelseDTO vfDTO) throws DALException;



	boolean sletForbindelse(int id) throws DALException;



	ArrayList<ReceptKompDTO> getReceptKompListWCU(int receptId, int produktBatchId) throws DALException;






	



	
}
