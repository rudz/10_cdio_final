package dtu.client.service.foreman;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.client.service.IRaavareService;
import dtu.shared.dto.db.RaavareBatchDTO;

public interface IAddRaavareBatchService extends IRaavareService {
	void createRaavareBatch(RaavareBatchDTO raavarebatch, AsyncCallback<Boolean> callback);

	void raavareBatchIdExists(int rb_id, AsyncCallback<Boolean> callback);
	
	void browseRaavareBatch(AsyncCallback<ArrayList<RaavareBatchDTO>> callback);
}
