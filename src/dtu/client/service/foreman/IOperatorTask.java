package dtu.client.service.foreman;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;

/**
 * For delegating task to operator
 * @author Rudy Alex Kohn [s133235]
 */
public interface IOperatorTask {
	void getOperatoerList(int opr_id, AsyncCallback<ArrayList<OperatoerDTO>> callback);
	void getProduktBatchList(int start, int end, int status0, int status1, int status2, AsyncCallback<ArrayList<ProduktBatchDTO>> callback);
	void getProduktBatchKompList(int produktBatchId, AsyncCallback<ArrayList<ProduktBatchKompDTO>> asyncCallback);
	
}
