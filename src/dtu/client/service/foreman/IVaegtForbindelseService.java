package dtu.client.service.foreman;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.VaegtForbindelseDTO;


public interface IVaegtForbindelseService {
	void getForbindelseList(AsyncCallback<ArrayList<VaegtForbindelseDTO>> callback);

	void OpretForbindelsesData(VaegtForbindelseDTO vfDTO, AsyncCallback<Void> callback);	

	void sletForbindelse(int id, AsyncCallback<Boolean> callback);
}
