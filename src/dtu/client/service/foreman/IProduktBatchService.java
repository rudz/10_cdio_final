package dtu.client.service.foreman;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;



public interface IProduktBatchService {
	void getReceptList(AsyncCallback<ArrayList<ReceptDTO>> callback);
	void getRecept(int receptId, AsyncCallback<ReceptDTO> callback);
	void updateProduktBatch(ProduktBatchDTO produktBatchDTO, AsyncCallback<Boolean> callback);
	void createProduktBatch(ProduktBatchDTO produktBatchDTO, AsyncCallback<Boolean> callback);
	void deleteProduktBatch(int produktBatchId, AsyncCallback<Boolean> callback);
	void createProduktBatchList(ArrayList<ProduktBatchDTO> list, AsyncCallback<Boolean> callback);
	void getProduktBatchList(int start, int end, int status0, int status1, int status2, AsyncCallback<ArrayList<ProduktBatchDTO>> callback);
	void getProduktBatchListWithStatus(int startInterval, int endInterval, int status0, int status1, int status2,
			AsyncCallback<ArrayList<ProduktBatchDTO>> asyncCallback);
	void getProduktBatchKompList(int produktBatchId, AsyncCallback<ArrayList<ProduktBatchKompDTO>> asyncCallback);
	
	void getReceptKomponenter(int receptId, AsyncCallback<ArrayList<ReceptKomponenterDTO>> callback);

}
