package dtu.client.service.foreman;

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.dto.db.RaavareBatchDTO;

public interface IBrowseRaavareBatchService{
	void browseRaavareBatch(AsyncCallback<ArrayList<RaavareBatchDTO>> callback);
	void deleteRaavareBatch(int rb_id, AsyncCallback<Boolean> callback);
	void updateRaavareBatch(RaavareBatchDTO raavareBatchDTO, AsyncCallback<Boolean> callback);
}
