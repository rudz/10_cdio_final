package dtu.client.service;

import java.util.ArrayList;
import java.util.HashSet;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.client.service.admin.IAddViewService;
import dtu.client.service.admin.IBrowseViewService;
import dtu.client.service.admin.IChangePasswordView;
import dtu.client.service.admin.IDeleteService;
import dtu.client.service.admin.IEditService;
import dtu.client.service.farmaceut.IReceptService;
import dtu.client.service.foreman.IAddRaavareBatchService;
import dtu.client.service.foreman.IBrowseRaavareBatchService;
import dtu.client.service.foreman.IOperatorTask;
import dtu.client.service.foreman.IProduktBatchService;
import dtu.client.service.foreman.IVaegtForbindelseService;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;

public interface IAdminServiceAsync extends 
IWelcomeService, ILoginViewService,

IDeleteService, IAddViewService, IBrowseViewService, IEditService, IChangePasswordView,

IAddRaavareBatchService, IBrowseRaavareBatchService, IReceptService, IProduktBatchService, IVaegtForbindelseService,
IOperatorTask
{
	/*******************
	/* token begin --> *
	 *******************/
	@Override
	void getToken(int id, AsyncCallback<String> callback);
	
	
	/*******************
	/* <-- token end   *
	 *******************/
	
	/*******************
	/* user begin -->  *
	 *******************/
	@Override
	void getOperatoer(int oprId, AsyncCallback<OperatoerDTO> callback);
	@Override
	void createOperatoer(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	@Override
	void updateOperatoer(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	@Override
	void getSize(AsyncCallback<Integer> callback);
	@Override
	void getInitials(AsyncCallback<HashSet<String>> callback);
	@Override
	void updatePW(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	@Override
	void passwordValidation(OperatoerDTO opr, AsyncCallback<Boolean> callback);
	@Override
	void genPassword(int ID, AsyncCallback<String> callback);
	@Override
	void getOperatoerList(AsyncCallback<ArrayList<OperatoerDTO>> callback);
	@Override
	void getOperatoerList(int opr_id, AsyncCallback<ArrayList<OperatoerDTO>> callback);

	void getOperatorName(int oprId, AsyncCallback<String> callback);

	/*******************
	/* <-- user end    *
	 *******************/

	/*******************
	/* raavare begin -->*
	 *******************/
	@Override
	void createRaavare(RaavareDTO raavare, AsyncCallback<Boolean> callback);
	@Override
	void raavareExists(int raavareId, AsyncCallback<Boolean> callback);
	@Override
	void getRaavareList(AsyncCallback<ArrayList<RaavareDTO>> callback);
	@Override
	void updateRaavare(RaavareDTO raavare, AsyncCallback<Boolean> callback);
	@Override
	void deleteRaavare(int raavareId, AsyncCallback<Boolean> callback);
	/*******************
	/* <-- raavare end *
	 *******************/

	/*************************
	/* raavareBatch begin -->*
	 *************************/
	void getRaavareBatch(int id, AsyncCallback<RaavareBatchDTO> callback);
	@Override
	void createRaavareBatch(RaavareBatchDTO raavareBatchDTO, AsyncCallback<Boolean> callback);
	@Override
	void browseRaavareBatch(AsyncCallback<ArrayList<RaavareBatchDTO>> callback);
	@Override
	void updateRaavareBatch(RaavareBatchDTO raavareBatchDTO, AsyncCallback<Boolean> callback);
	@Override
	void raavareBatchIdExists(int rb_id, AsyncCallback<Boolean> callback);
	
	/***********************
	/* raavareBatch end -->*
	 ***********************/
	
	/*******************
	/* recept begin -->*
	 *******************/
	@Override
	void getReceptList(AsyncCallback<ArrayList<ReceptDTO>> callback);
	@Override
	void getRecept(int receptId, AsyncCallback<ReceptDTO> callback);
	@Override
	void updateRecept(ReceptDTO receptDTO, AsyncCallback<Boolean> callback);
	@Override
	void createRecept(ReceptDTO receptDTO, AsyncCallback<Boolean> callback);
	@Override
	void deleteRecept(int receptId, AsyncCallback<Boolean> callback);
	@Override
	void getReceptKomponenter(int receptId, AsyncCallback<ArrayList<ReceptKomponenterDTO>> callback);
	@Override
	void createReceptKomponent(ReceptKompDTO receptKompDTO, AsyncCallback<Boolean> callback);
	@Override
	void createReceptKomponentList(ArrayList<ReceptKompDTO> list , AsyncCallback<Boolean> callback);
	@Override
	void isRecept(int receptId, AsyncCallback<Boolean> callback);
	@Override
	void updateReceptKomponenter(ArrayList<ReceptKompDTO> list, AsyncCallback<Void> callback);
	
	@Override
	void updateReceptKomponent(ReceptKompDTO DTO, AsyncCallback<Void> callback) throws DALException;
	/*******************
	/* <-- recept end  *
	 *******************/
	
	/***************************
	/* <-- ProduktBacth start  *
	 **************************/
	@Override
	void updateProduktBatch(ProduktBatchDTO produktBatchDTO, AsyncCallback<Boolean> callback);
	@Override
	void createProduktBatch(ProduktBatchDTO produktBatchDTO, AsyncCallback<Boolean> callback);
	@Override
	void deleteProduktBatch(int produktBatchId, AsyncCallback<Boolean> callback);
	@Override
	void createProduktBatchList(ArrayList<ProduktBatchDTO> list, AsyncCallback<Boolean> callback);
	/*************************
	/* <-- ProduktBatch end  *
	 *************************/

	/*******************************
	/* <-- ProduktBatchKomp start  *
	 *******************************/
	@Override
	void getProduktBatchList(int start, int end,int s0,int s1, int s2, AsyncCallback<ArrayList<ProduktBatchDTO>> callback);	
	@Override
	void getProduktBatchKompList(int pbId, AsyncCallback<ArrayList<ProduktBatchKompDTO>> callback);

	void createProduktBatchKomp(ProduktBatchKompDTO dto, AsyncCallback<Void> callback) throws DALException;
	/*****************************
	/* <-- ProduktBatchKomp end  *
	 *****************************/

	/******************************
	Weigt Connection start*/
	
	@Override
	void getForbindelseList(AsyncCallback<ArrayList<VaegtForbindelseDTO>> callback);
	
	@Override
	void OpretForbindelsesData(VaegtForbindelseDTO vfDTO, AsyncCallback<Void> callback);
	
	@Override
	void sletForbindelse(int id, AsyncCallback<Boolean> callback);
	
	/******************
	 * NOT GWT
	 ******************/
	void getProduktBatch(int rbid, AsyncCallback<ProduktBatchDTO> callback);
	void getReceptKomp(int i, AsyncCallback<ArrayList<ReceptKompDTO>> callback);

	@Override
	void getProduktBatchListWithStatus(int start, int end, int s0, int s1, int s2, AsyncCallback<ArrayList<ProduktBatchDTO>> callback);

	void getReceptKompListWCU(int receptId, int produktBatchId, AsyncCallback<ArrayList<ReceptKompDTO>> callback);
	
}
