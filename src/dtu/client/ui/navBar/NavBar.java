
package dtu.client.ui.navBar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.controller.MainView;
import dtu.client.ui.generic.MenuViewAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.misc.OperatorLevel;


public class NavBar extends MenuViewAbstract {

	@UiField
	StackLayoutPanel	p;

	public NavBar(final MainView main) {
		super(main);
		this.main = main;
		initWidget(onInitialize());
	}

	public Widget onInitialize() {
		/* Create a new stack layout panel. */
		StackLayoutPanel stackPanel = new StackLayoutPanel(Unit.EM);
		stackPanel.setVisible(false);
		if (LoginStatus.loggedIn) {
			/* Get the images. */
			Images images = (Images) GWT.create(Images.class);
			stackPanel.setPixelSize(130, 490);

			generic(stackPanel, images);
			switch (LoginStatus.level) {
			case 9:
				admin(stackPanel, images);
				break;
			case 3:
				pharmacist(stackPanel, images);
				break;
			case 2:
				foreman(stackPanel, images);
				break;
			case 1:
			default:
				operator(stackPanel, images);
				break;
			}
			/* Return the stack panel. */
			stackPanel.ensureDebugId("cwStackLayoutPanel");
			stackPanel.setVisible(true);
		}
		return stackPanel;
	}

	public interface Images extends Tree.Resources {

		@Source("tumb.png")
		ImageResource tumb();

		@Source("search.png")
		ImageResource vis();

		@Source("sign_add.png")
		ImageResource add();

		@Source("sign_info.png")
		ImageResource info();

		@Source("form_edit.png")
		ImageResource edit();

		@Source("sign_remove.png")
		ImageResource delete();

		@Source("lock.png")
		ImageResource password();

		@Source("logout.png")
		ImageResource logout();

		@Source("home.png")
		ImageResource home();

		@Source("form.png")
		ImageResource recept();

		@Source("pizza.png")
		ImageResource raavare();

		@Source("rocket.png")
		ImageResource rocket();

		@Source("user.png")
		ImageResource user();

		@Source("bookshelf.png")
		ImageResource produktBatch();

		@Source("raavareBatch.png")
		ImageResource raavareBatch();

		@Source("skull.png")
		ImageResource forbindelse();
		
		@Source("ASE.png")
		ImageResource ASE();
		
		@Source("tubeIcon.png")
		ImageResource tube();

		/** Use noimage.png, which is a blank 1x1 image. */
		@Override
		@Source("tumb.png")
		ImageResource treeLeaf();
	}

	interface Resources extends ClientBundle {

		@Source("tumb.png")
		ImageResource tumb();

	}

	private Widget createOperatoerItem(Images images) {
		Tree operatorPanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Vis alle brugere i systemet.", operatorPanel, images.vis(), new UserBrowseClickHandler());
		createItemWidgetAnchor("Tilføj", "Tilføj bruger til systemet", operatorPanel, images.add(), new UserAddClickHandler());
		createItemWidgetAnchor("Ret", "Tilføj bruger til systemet", operatorPanel, images.edit(), new UserEditClickHandler());
		createItemWidgetAnchor("Slet", "Sæt brugere til inaktiv status.", operatorPanel, images.delete(), new UserDeleteClickHandler());
		return operatorPanel;
	}

	private Widget createRaavareBatchItem(Images images) {
		Tree raavareBatchPanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Vis alle råvarer i systemet", raavareBatchPanel, images.vis(), new RaavareBatchBrowseClickHandler());
		createItemWidgetAnchor("Tilføj", "Tilføj nye råvarebatches til systemet.", raavareBatchPanel, images.add(), new RaavareBatchAddClickHandler());
		createItemWidgetAnchor("Ret", "Ret eksisterende råvarebatches i systemet", raavareBatchPanel, images.edit(),
				new RaavareBatchEditClickHandler());
		createItemWidgetAnchor("Slet", "Slet eksisterende råvarebatch(es) i systemet", raavareBatchPanel, images.delete(),
				new RaavareBatchDeleteClickHandler());
		return raavareBatchPanel;
	}

	private Widget createProduktbatchItem(Images images) {
		Tree produktBatchPanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Vis alle produktbatches i systemet", produktBatchPanel, images.vis(), new ProduktBatchViewClickHandler());
		createItemWidgetAnchor("Tilføj", "Tilføj nye produktbatch(es) til systemet.", produktBatchPanel, images.add(),
				new ProduktBatchAddClickHandler());
		return produktBatchPanel;
	}

	private Widget createVaegtForbindelseItem(Images images) {
		Tree connectionPanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Se vægtforbindelser som ASEN forbinder til", connectionPanel, images.vis(), new ForbindelseViewClickHandler());
		createItemWidgetAnchor("Opret", "Opret Forbindelses Data", connectionPanel, images.add(), new ForbindelseAddClickHandler());
		return connectionPanel;
	}

	private Widget createRaavareItem(Images images) {
		Tree raavarePanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Vis alle råvarer i systemet.", raavarePanel, images.vis(), new RaavareBrowseAddClickHandler());
		createItemWidgetAnchor("Tilføj", "Tilføj råvare i systemet.", raavarePanel, images.add(), new RaavareAddClickHandler());
		createItemWidgetAnchor("Ret", "Ret eksisterende råvarer i systemet.", raavarePanel, images.edit(), new RaavareEditClickHandler());
		return raavarePanel;
	}

	private Widget createReceptItem(Images images) {
		Tree receptPanel = new Tree(images);
		createItemWidgetAnchor("Vis", "Vis eksisterende recepter i systemet.", receptPanel, images.vis(), new ReceptBrowseClickHandler());
		createItemWidgetAnchor("Tilføj", "Tilføj recept(er) til systemet.", receptPanel, images.add(), new ReceptAddClickHandler());
		return receptPanel;
	}

	private Widget createGeneralItem(Images images) {
		Tree generalPanel = new Tree(images);
		createItemWidgetAnchor("Log ud [" + Integer.toString(LoginStatus.userID) + "]",
				LoginStatus.userName + "\n" + OperatorLevel.olText(LoginStatus.level), generalPanel, images.logout(), new LogoutClickHandler());
		createItemWidgetAnchor("Skift kode", "Skift dit kodeord. (husk at gøre dette hver måned)", generalPanel, images.password(),
				new ChangePasswordClickHandler());
		createItemWidgetAnchor("Information", "Simplistisk information omkring systemet.", generalPanel, images.info(), new InformationClickHandler());
		return generalPanel;
	}

	/** Add a {@link TreeItem} to a root item.
	 * 
	 * @param panel
	 *            the root {@link TreeItem}
	 * @param image
	 *            the icon for the new child item
	 * @param label
	 *            the label for the child icon */
	@SuppressWarnings("unused")
	private void addItem(String label, String toolTip, Tree panel, ImageResource image, ClickHandler handler) {
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.append(SafeHtmlUtils.fromTrustedString(AbstractImagePrototype.create(image).getHTML()));
		sb.appendEscaped(" ").appendEscaped(label);

		// root.addItem(sb.toSafeHtml());
		Anchor an = new Anchor(sb.toSafeHtml());
		an.addClickHandler(handler);
		an.setTitle(toolTip);
		panel.addItem(an);
	}

	private void createItemWidgetAnchor(String text, String toolTip, Tree panel, ImageResource image, ClickHandler handler) {
		// Add the image and text to a horizontal panel
		HorizontalPanel itemWidget = new HorizontalPanel();
		itemWidget.setHeight("100%");
		itemWidget.setSpacing(0);
		itemWidget.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		itemWidget.add(new Image(image));
		HTML widgetText = new HTML(text);
		widgetText.setStyleName("cw-StackPanelItem");
		itemWidget.add(widgetText);
		itemWidget.addDomHandler(handler, ClickEvent.getType());
		itemWidget.setTitle(toolTip);
		panel.add(itemWidget);
	}

	/** Create a widget to display in the header that includes an image and some
	 * text.
	 * 
	 * @param text
	 *            the header text
	 * @param image
	 *            the {@link ImageResource} to add next to the header
	 * @return the header widget */
	private Widget createHeaderWidget(String text, ImageResource image) {
		// Add the image and text to a horizontal panel
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setHeight("100%");
		hPanel.setSpacing(0);
		hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		hPanel.add(new Image(image));
		HTML headerText = new HTML(text);
		headerText.setStyleName("cw-StackPanelHeader");
		hPanel.add(headerText);
		return new SimplePanel(hPanel);
	}

	private void generic(StackLayoutPanel stackPanel, Images images) {
		Widget generalHeader = createHeaderWidget("Generelt", images.rocket());
		stackPanel.add(createGeneralItem(images), generalHeader, 4);
	}

	private void admin(StackLayoutPanel stackPanel, Images images) {
		Widget oprHeader = createHeaderWidget("Brugere", images.user());
		stackPanel.add(createOperatoerItem(images), oprHeader, 4);

		pharmacist(stackPanel, images);
	}

	private void foreman(StackLayoutPanel stackPanel, Images images) {
		Widget raavareBatchMenuHeader = createHeaderWidget("RåvareBatch", images.raavareBatch());
		stackPanel.add(createRaavareBatchItem(images), raavareBatchMenuHeader, 4);

		Widget produktBatchHeader = createHeaderWidget("ProduktBatch", images.produktBatch());
		stackPanel.add(createProduktbatchItem(images), produktBatchHeader, 4);

		Widget vaegForbindelseHeader = createHeaderWidget("ASE", images.ASE());
		stackPanel.add(createVaegtForbindelseItem(images), vaegForbindelseHeader, 4);

		operator(stackPanel, images);
	}

	private void pharmacist(StackLayoutPanel stackPanel, Images images) {
		Widget raavareHeader = createHeaderWidget("Råvarer", images.tube());
		stackPanel.add(createRaavareItem(images), raavareHeader, 4);

		Widget recepterHeader = createHeaderWidget("Recepter", images.recept());
		stackPanel.add(createReceptItem(images), recepterHeader, 4);

		foreman(stackPanel, images);
	}

	/** operator level = 1 */
	@SuppressWarnings("unused")
	private void operator(StackLayoutPanel stackPanel, Images images) {
		// TODO : Add some stuff here!
	}

}
