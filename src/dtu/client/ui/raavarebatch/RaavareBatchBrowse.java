
package dtu.client.ui.raavarebatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IBrowseRaavareBatchService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.misc.DK;


public class RaavareBatchBrowse extends RaavareBatchAbstract {

	private static RaavareBatchBrowseUiBinder	uiBinder	= GWT.create(RaavareBatchBrowseUiBinder.class);
	@UiField
	FlexTable									t;
	@UiField
	Label										lblTitle;
	@UiField
	Button										btnShowEmpty;
	@UiField
	Button										btnShowLowest;
	@UiField
	Button										btnShowAsRaavare;
	@UiField
	Button										btnShowAll;
	@UiField
	ScrollPanel									parentScrollPanel;
	@UiField
	Button										btnSearch;
	@UiField
	IntegerBox									intSearchField;

	interface RaavareBatchBrowseUiBinder extends UiBinder<Widget, RaavareBatchBrowse> {/**/}

	public RaavareBatchBrowse(IBrowseRaavareBatchService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Vis R" + DK.aa + "varebatch");
		btnShowAsRaavare.setText("Vis samlet");
		btnSearch.setText(DK.toDk(btnSearch.getText()));
		btnShowAsRaavare.setText(btnShowAsRaavare.getText());
		serv.browseRaavareBatch(new browseRaavareBatch());
		setTips();
	}
	
	private void setTips(){
		intSearchField.setTitle("Indtast raavare id for at finde batches der matcher");
		btnSearch.setTitle("Find og vis alle raavare batches med det indtastede raavare id");
		btnShowEmpty.setTitle("Vis kun raavare batches med 0 eller mindre maengde");
		btnShowLowest.setTitle("Vis den raavare batch med laveste maengde over nul for hver raavare");
		btnShowAsRaavare.setTitle("Vis samlet maengde for hver raavare");
		btnShowAll.setTitle("Vis alle raavare batches");
	}

	private class browseRaavareBatch extends AsyncCallbackAbstract<ArrayList<RaavareBatchDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareBatchDTO> result) {
			list = result;
			createTable();
			int i = 1;
			for (RaavareBatchDTO o : result) {
				insertElement(i++, o);
			}
		}
	}
	@Override
	protected void createTable() {
		t.removeAllRows();
		t.getFlexCellFormatter().setWidth(0, 0, "50px");
		t.getFlexCellFormatter().setWidth(0, 1, "100px");
		t.getFlexCellFormatter().setWidth(0, 2, "50px");
		UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "vareBatch ID", "R" + DK.aa + "vare ID", "M" + DK.ae + "ngde");
	}

	@Override
	protected void insertElement(int i, RaavareBatchDTO o) {
		t.setText(i, 0, Integer.toString(o.getRbId()));
		t.setText(i, 1, Integer.toString(o.getRaavareId()));
		t.setText(i, 2, Double.toString(o.getMaengde()));
		UIAbstract.setRowStyle(i, t, true);
	}

	@UiHandler("btnShowEmpty")
	void onBtnShowEmptyClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showEmpty();
	}
	@UiHandler("btnShowLowest")
	void onBtnShowLowestClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showLowest();
	}
	@UiHandler("btnShowAll")
	void onBtnShowAllClick(@SuppressWarnings("unused") ClickEvent event) {
		serv.browseRaavareBatch(new browseRaavareBatch());
	}
	@UiHandler("btnSearch")
	void onBtnSearchClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showRaavare(Integer.parseInt(intSearchField.getText()));
	}
	@UiHandler("btnShowAsRaavare")
	void onBtnShowAsRaavareClick(@SuppressWarnings("unused") ClickEvent event) {
		t.removeAllRows();
		t.getFlexCellFormatter().setWidth(0, 0, "100px");
		t.getFlexCellFormatter().setWidth(0, 1, "100px");
		UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "vare ID", "M" + DK.ae + "ngde");
		HashMap<Integer, Double> m = new HashMap<>();
		for (RaavareBatchDTO o : list) {
			if (m.containsKey(o.getRaavareId())) m.put(o.getRaavareId(), m.get(o.getRaavareId()) + o.getMaengde());
			else m.put(o.getRaavareId(), o.getMaengde());
		}
		int i = 1;
		Set<Integer> k = m.keySet();
		for (Integer d : k) {
			t.setText(i, 0, Integer.toString(d));
			t.setText(i, 1, Double.toString(m.get(d)));
			UIAbstract.setRowStyle(i++, t, true);
		}
	}
}
