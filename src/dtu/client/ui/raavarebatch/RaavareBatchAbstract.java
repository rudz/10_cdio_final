
package dtu.client.ui.raavarebatch;

import java.util.ArrayList;
import java.util.LinkedList;
import com.google.gwt.user.client.ui.Composite;
import dtu.client.service.foreman.IBrowseRaavareBatchService;
import dtu.shared.dto.db.RaavareBatchDTO;


public abstract class RaavareBatchAbstract extends Composite {

	/* fields */
	protected ArrayList<RaavareBatchDTO>	list;
	protected IBrowseRaavareBatchService	serv;

	/* interface */
	protected abstract void createTable();
	protected abstract void insertElement(int i, RaavareBatchDTO o);

	/* methods */
	protected void showEmpty() {
		createTable();
		int i = 1;
		for (RaavareBatchDTO o : list) {
			if (o.getMaengde() <= 0d) insertElement(i++, o);
		}
	}

	protected void showLowest() {
		createTable();
		LinkedList<RaavareBatchDTO> ll = new LinkedList<>();
		for (RaavareBatchDTO o : list) {
			if (o.getMaengde() > 0) {
				if (ll.isEmpty()) ll.add(o);
				for (RaavareBatchDTO l : ll) {
					if (o.getRaavareId() == l.getRaavareId()) {
						if (o.getMaengde() < l.getMaengde()) {
							ll.remove(l);
							ll.add(o);
							break;
						}
						break;
					} else if (l.equals(ll.getLast())) ll.add(o);
				}
			}
		}
		int i = 1;
		for (RaavareBatchDTO l : ll)
			insertElement(i++, l);
	}

	protected void showRaavare(int i) {
		int j = 1;
		createTable();
		for (RaavareBatchDTO o : list) {
			if (o.getRaavareId() == i) insertElement(j++, o);
		}
	}

}
