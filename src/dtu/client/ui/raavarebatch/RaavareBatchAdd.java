
package dtu.client.ui.raavarebatch;

import java.text.ParseException;
import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IAddRaavareBatchService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.misc.DK;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.CaptionPanel;


public class RaavareBatchAdd extends Composite {

	IAddRaavareBatchService					service;

	private static RaavareBatchAddUiBinder	uiBinder	= GWT.create(RaavareBatchAddUiBinder.class);
	@UiField
	Button									SubmitButton;
	@UiField
	DoubleBox								ValueInput;
	@UiField
	IntegerBox								CommodityBatchId;
	@UiField
	Label									lblAmount;
	@UiField
	Label									lblRaavareBatchId;
	@UiField
	Label									lblRaavareId;
	@UiField
	Label									lblTitle;
	@UiField
	ListBox									CommodityId;
	@UiField
	CaptionPanel							capRaavareBatch;

	interface RaavareBatchAddUiBinder extends UiBinder<Widget, RaavareBatchAdd> { /**/}

	private boolean	raavareBatchIdOk;
	private boolean	maengdeOk;

	public RaavareBatchAdd(final IAddRaavareBatchService service) {
		this.service = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Tilf" + DK.oe + "j R" + DK.aa + "vareBatch");
		lblRaavareId.setText("R" + DK.aa + "vare ID");
		lblRaavareBatchId.setText("R" + DK.aa + "vareBatch ID");
		lblAmount.setText("M" + DK.ae + "ngde i kilogram");
		clearFields();
	}

	void clearFields() {
		ValueInput.setValue(0.0);
		ValueInput.fireEvent(new KeyUpEvent() { /**/});
		CommodityBatchId.setText("");
		lowstBatchId();
		raavareList();
		isOk();
	}

	private void lowstBatchId() {
		service.browseRaavareBatch(new getLowestBatchId());
	}

	private class getLowestBatchId implements AsyncCallback<ArrayList<RaavareBatchDTO>> {

		@Override
		public void onFailure(Throwable arg0) {
			Window.alert("Failed to retrive RaavareBatchList, check db connection");
			onCommodityBatchIdKeyUp(null);
		}

		@Override
		public void onSuccess(ArrayList<RaavareBatchDTO> list) {
			int i;
			for (i = 0; i < list.size(); i++)
				if (list.get(i).getRbId() == i + 1) ; // void
				else {
					CommodityBatchId.setText(Integer.toString(i + 1));
					onCommodityBatchIdKeyUp(null);
					return;
				}
			// if list is complete
			CommodityBatchId.setText(Integer.toString(i + 2));
			onCommodityBatchIdKeyUp(null);
		}

	}

	private void raavareList() {
		service.getRaavareList(new getRaavareList());
	}

	private class getRaavareList implements AsyncCallback<ArrayList<RaavareDTO>> {

		@Override
		public void onFailure(Throwable arg0) {
			Window.alert("Failed to retrive RaavareList, check db connection");
		}

		@Override
		public void onSuccess(ArrayList<RaavareDTO> list) {
			CommodityId.clear();
			for (RaavareDTO r : list)
				CommodityId.addItem(Integer.toString(r.getRaavareId()) + "  " + r.getRaavareNavn(), Integer.toString(r.getRaavareId()));
		}

	}

	@UiHandler("SubmitButton")
	void onSubmitButtonClick(@SuppressWarnings("unused") ClickEvent event) {
		if (!isOk()) return;
		RaavareBatchDTO r = new RaavareBatchDTO(Integer.valueOf(CommodityBatchId.getText()), Integer.valueOf(CommodityId.getValue(CommodityId
				.getSelectedIndex())), Double.valueOf(ValueInput.getText()));
		service.createRaavareBatch(r, new AsyncCallback<Boolean>() {

			@Override
			public void onSuccess(Boolean result) {
				if (result) {
					Window.alert("Raavarebatch gemt i databasen!");
					clearFields();
				} else onFailure(new Exception("Fejl ved oprettelse af raavarebatch, prøv venligst igen."));
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(DK.toDk("Serverfejl ved opretning af Raavarebatch i databasen."));
			}
		});
	}

	@UiHandler("CommodityBatchId")
	void onCommodityBatchIdKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		try {
			CommodityBatchId.getValueOrThrow();
			service.raavareBatchIdExists(CommodityBatchId.getValue(), new isRaavareBatchId());
		} catch (ParseException e) {
			UIAbstract.setValidity(CommodityBatchId, false);
			raavareBatchIdOk = false;
		}
		isOk();

	}

	@UiHandler("ValueInput")
	void onValueInputKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {

		try {
			double amount = ValueInput.getValueOrThrow();
			if (amount < 1000d && amount >= 0d) {
				UIAbstract.setValidity(ValueInput, true);
				maengdeOk = true;
			} else {
				UIAbstract.setValidity(ValueInput, false);
				maengdeOk = false;
			}

		} catch (ParseException e) {
			UIAbstract.setValidity(ValueInput, false);
			maengdeOk = false;
		}
		isOk();
	}

	private abstract class AbstractFailCallBack<T> implements AsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
		}
	}

	private class isRaavareBatchId extends AbstractFailCallBack<Boolean> {

		@Override
		public void onSuccess(Boolean arg0) {
			if (arg0) {
				UIAbstract.setValidity(CommodityBatchId, false);
				raavareBatchIdOk = false;
			} else {
				UIAbstract.setValidity(CommodityBatchId, true);
				raavareBatchIdOk = true;
			}
			isOk();
		}

	}

	private boolean isOk() {
		if (raavareBatchIdOk && maengdeOk) {
			SubmitButton.setEnabled(true);
			return true;
		}
		SubmitButton.setEnabled(false);
		return false;
	}
}
