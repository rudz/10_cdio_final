
package dtu.client.ui.raavarebatch;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IBrowseRaavareBatchService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.misc.DK;


public class RaavareBatchDelete extends RaavareBatchAbstract {

	private static RaavareBatchDeleteUiBinder	uiBinder		= GWT.create(RaavareBatchDeleteUiBinder.class);

	/* data layer reference */
	private IBrowseRaavareBatchService			service;

	/* widgets */
	@UiField
	FlexTable									t;
	@UiField
	Label										lblTitle;
	@UiField
	Button										btnShowEmpty;
	@UiField
	Button										btnShowSmallest;
	@UiField
	IntegerBox									IntRaavareId;
	@UiField
	Button										btnSearch;
	@UiField
	Button										btnShowAll;

	/* anchors */
	private Anchor								previousCancel	= null;
	private Anchor								deleteAnchor	= null;
	private Anchor								okAnchor		= null;
	private Anchor								cancelAnchor	= null;

	/* variables */
	private int									eventRowIndex;

	interface RaavareBatchDeleteUiBinder extends UiBinder<Widget, RaavareBatchDelete> { /**/}

	public RaavareBatchDelete(IBrowseRaavareBatchService service) {
		this.service = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Slet R" + DK.aa + "varebatch");
		buildTable();
		setTips();
	}
	
	private void setTips(){
		IntRaavareId.setTitle("Indtast raavare id for at finde batches der matcher");
		btnSearch.setTitle("Find og vis alle raavare batches med det indtastede raavare id");
		btnShowEmpty.setTitle("Vis kun raavare batches med 0 eller mindre maengde");
		btnShowSmallest.setTitle("Vis den raavare batch med laveste maengde over nul for hver raavare");
		btnShowAll.setTitle("Vis alle raavare batches");
	}

	private void buildTable() {
		createTable();
		service.browseRaavareBatch(new GetCommodityBatch());
	}

	@Override
	protected void createTable() {
		t.removeAllRows();
		t.getFlexCellFormatter().setWidth(0, 0, "25px"); // rb id
		t.getFlexCellFormatter().setWidth(0, 1, "200px"); // råvare id
		t.getFlexCellFormatter().setWidth(0, 2, "25x"); // mængde
		t.getFlexCellFormatter().setWidth(0, 3, "50px"); // delete anchor
		t.getFlexCellFormatter().setWidth(0, 4, "50px"); // cancel anchor
		UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "vareBatch ID", "R" + DK.aa + "vare ID", "M" + DK.ae + "ngde");
	}

	/* *****************************************************
	 * /* ************** HANDLER CLASSES ********************** /*
	 * ****************************************************
	 */

	private class DeleteHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			/* if previous cancel open - force cancel operation */
			if (previousCancel != null) previousCancel.fireEvent(new ClickEvent() { /**/});

			/* get rowindex where event happened */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			/* get delete anchor ref for cancel operation */
			deleteAnchor = (Anchor) event.getSource();

			/* configure "ok" / "fortryd" anchors */
			okAnchor = UIAbstract.generateAnchor("ok", "Verificer sletning af RåvareBatch med ID = " + t.getText(eventRowIndex, 0),
					new OkClickHandler());
			cancelAnchor = UIAbstract.generateAnchor("fortryd", "Afbryd sletning af RåvareBatch.", new CancelClickHandler());
			previousCancel = cancelAnchor;

			/* show ok and cancel widgets */
			t.setWidget(eventRowIndex, 3, okAnchor);
			t.setWidget(eventRowIndex, 4, cancelAnchor);
		}
	}

	private class CancelClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			t.clearCell(eventRowIndex, 3);
			t.clearCell(eventRowIndex, 4);
			t.setWidget(eventRowIndex, 3, deleteAnchor);
		}
	}

	private class OkClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			service.deleteRaavareBatch(Integer.parseInt(t.getText(eventRowIndex, 0)), new DeleteCommodity());
			previousCancel = null;
		}
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	private class DeleteCommodity extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean result) {
			Window.alert(DK.toDk("RåvareBatch slettet."));
			buildTable();
		}
	}

	private class GetCommodityBatch extends AsyncCallbackAbstract<ArrayList<RaavareBatchDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareBatchDTO> result) {
			int i = 0;
			list = result;
			for (RaavareBatchDTO o : result) {
				insertElement(++i, o);
			}
		}
	}

	@Override
	protected void insertElement(int i, RaavareBatchDTO o) {
		t.setText(i, 0, Integer.toString(o.getRbId()));
		t.setText(i, 1, Integer.toString(o.getRaavareId()));
		t.setText(i, 2, Double.toString(o.getMaengde()));
		Anchor delete = UIAbstract.generateAnchor("slet", "Slet RåvareBatch med ID = " + t.getText(i, 0), new DeleteHandler());
		t.setWidget(i, 3, delete);
		t.setText(i, 4, " ");
		UIAbstract.setRowStyle(i, t, true);
	}
	@UiHandler("btnSearch")
	void onBtnSearchClick(@SuppressWarnings("unused") ClickEvent event) {
		try {
			super.showRaavare(Integer.parseInt(IntRaavareId.getText()));
		} catch (NumberFormatException e) {
			Window.alert("Indtast kun integers");
		}
	}
	@UiHandler("btnShowEmpty")
	void onBtnShowEmptyClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showEmpty();
	}
	@UiHandler("btnShowSmallest")
	void onBtnShowSmallestClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showLowest();
	}
	@UiHandler("btnShowAll")
	void onBtnShowAllClick(@SuppressWarnings("unused") ClickEvent event) {
		buildTable();
	}
}
