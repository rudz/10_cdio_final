
package dtu.client.ui.raavarebatch;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IBrowseRaavareBatchService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.misc.DK;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.uibinder.client.UiHandler;


public class RaavareBatchEdit extends RaavareBatchAbstract {

	private static RaavareBatchEditUiBinder	uiBinder		= GWT.create(RaavareBatchEditUiBinder.class);

	@UiField
	FlexTable								t;
	@UiField
	Label									lblTitle;
	@UiField
	Button									btnSearch;
	@UiField
	IntegerBox								IntegerBox;
	@UiField
	Button									btnShowUsed;
	@UiField
	Button									btnShowLowest;
	@UiField
	Button									btnShowAll;

	/* row index for current action being performed */
	private int								eventRowIndex;

	/* id currently being worked on */
	private int								currentID, currentRaavareID;

	private final int						HEADER_ROW		= 0;

	private final int						EDIT_INDEX		= 3;
	private final int						OK_INDEX		= 3;
	private final int						CANCEL_INDEX	= 4;

	/* maps for editing. */
	//private HashMap<Integer, TextBox>		txtBoxes		= new HashMap<>(3, 1.0f);
	//private HashMap<Integer, String>		tmpStrings		= new HashMap<>(3, 1.0f);
	
	private DoubleBox 						dublBox;

	/* map key values */
	@SuppressWarnings("unused")
	private int								rbid			= 1;
	private int								raavareId		= 2;
	private int								maengde			= 3;

	/* Anchors */
	private Anchor							ok;
	private Anchor							cancel;
	private Anchor							edit;
	private Anchor							previousCancel	= null;

	/* not-visible raavarebatch ids, and  */
	private int[]							ID, raavareID;

	interface RaavareBatchEditUiBinder extends UiBinder<Widget, RaavareBatchEdit> { /* */}

	public RaavareBatchEdit(IBrowseRaavareBatchService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		btnSearch.setText("S" + DK.oe + "g");
		lblTitle.setText("Ret R" + DK.aa + "vareBatch");
		serv.browseRaavareBatch(new CommodityGetListCallback());
		setTips();
	}
	
	private void setTips(){
		IntegerBox.setTitle("Indtast raavare id for at finde batches der matcher");
		btnSearch.setTitle("Find og vis alle raavare batches med det indtastede raavare id");
		btnShowUsed.setTitle("Vis kun raavare batches med 0 eller mindre maengde");
		btnShowLowest.setTitle("Vis den raavare batch med laveste maengde over nul for hver raavare");
		btnShowAll.setTitle("Vis alle raavare batches");
	}

	@Override
	protected void createTable() {
		t.removeAllRows();
		t.getFlexCellFormatter().setWidth(HEADER_ROW, 0, "150px"); // rb_id
		t.getFlexCellFormatter().setWidth(HEADER_ROW, 1, "150px"); // raavare id
		t.getFlexCellFormatter().setWidth(HEADER_ROW, 2, "150px"); // mængde
		UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "varebatch ID", "R" + DK.aa + "vare ID", "M" + DK.ae + "ngde");
//		for (int i = 2; i < 6; i++)
//			txtBoxes.put(i, new TextBox());
//		txtBoxes.get(raavareId).setWidth("150px");
//		txtBoxes.get(maengde).setWidth("150px");
		
		
		
	}

	private class EditHandler extends UIAbstract implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			/*
			 * detect if a previous edit action is in progress, if so, force
			 * cancel click handler to fire
			 */
			if (previousCancel != null) previousCancel.fireEvent(new ClickEvent() { /**/});

			/* mark current row of pressed anchor */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			/* configure text boxes */
//			for (int i = 2; i < 3; i++) {
//				txtBoxes.get(i + 1).setText(t.getText(eventRowIndex, i));
//				t.setWidget(eventRowIndex, i, txtBoxes.get(i + 1));
//			}
			String maengde = String.valueOf(t.getText(eventRowIndex, 2));	
			dublBox = new DoubleBox();
			dublBox.setText(maengde);
			dublBox.addKeyUpHandler(new MaengdeKeyUpHandler());
			t.setWidget(eventRowIndex, 2, dublBox);
			// start editing here
			//txtBoxes.get(raavareId).setFocus(true);

			// get edit anchor ref for cancel operation
			edit = (Anchor) event.getSource();

			// get textbox contents for cancel operation
			/* store the values in the case cancel will be pressed */
//			for (Integer i : txtBoxes.keySet())
//				tmpStrings.put(i, txtBoxes.get(i).getText());

			/* set up anchors and their respective click handlers */
			ok = UIAbstract.generateAnchor("ok", "Foretag ændringer RåvareBatch [" + t.getText(eventRowIndex, 0) + "]", new OkClickHandler());
			cancel = UIAbstract.generateAnchor("fortryd", "Fortryd eventuelle ændringer foretaget.", new CancelClickHandler());
			previousCancel = cancel;

			/* assign event handler classes to the text boxes */

			// txtBoxes.get(raavareId).addKeyUpHandler(new NameKeyUpHandler());

			/* force KeyUpEvent to make sure they are validated */
//			for (Integer i : txtBoxes.keySet())
//				txtBoxes.get(i).fireEvent(new KeyUpEvent() { /**/});

			/* drop those anchors onto the table */
			t.setWidget(eventRowIndex, OK_INDEX, ok);
			t.setWidget(eventRowIndex, CANCEL_INDEX, cancel);
		}

		private class OkClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {

				currentID = ID[eventRowIndex - 1];
				currentRaavareID = raavareID[eventRowIndex-1];

				try {
					//if (Integer.valueOf(txtBoxes.get(raavareId).getText()) < 0) throw new NumberFormatException();

					serv.updateRaavareBatch(
							new RaavareBatchDTO(currentID, currentRaavareID, dublBox.getValue()), new CommodityUpdateCallback());

					/*
					 * since ok was clicked, modify the table to show the new
					 * values
					 */
//					for (Integer i : txtBoxes.keySet())
//						t.setText(eventRowIndex, i - 1, txtBoxes.get(i).getText());

					t.setText(eventRowIndex, 2, String.valueOf(dublBox.getValue()));
					
					t.clearCell(eventRowIndex, OK_INDEX);
					t.clearCell(eventRowIndex, CANCEL_INDEX);
					previousCancel = null;
					cancel = null;
					t.setWidget(eventRowIndex, EDIT_INDEX, edit);
				} catch (NumberFormatException e) {
					Window.alert("Indtast kun heltal i id feltet og decimal tal i maengde feltet");
					return;
				}

				// restore new state

			}
		}

		private class CancelClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				// restore original content of textboxes and rerun input
				// validation
//				txtBoxes.get(raavareId).fireEvent(new KeyUpEvent() { /**/});
				// TODO : FIX ALIGNMENT!
//				for (Integer i : tmpStrings.keySet())
//					t.setText(eventRowIndex, i - 1, tmpStrings.get(i));
				t.setText(eventRowIndex, 2, dublBox.getText());
				// restore edit link

				t.clearCell(eventRowIndex, CANCEL_INDEX);
				previousCancel = null;
				t.setWidget(eventRowIndex, EDIT_INDEX, edit);
			}
		}
	}

	private class CommodityUpdateCallback extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean bool) {
			if (bool) {
//				for (Integer i : txtBoxes.keySet())
//					tmpStrings.put(i, txtBoxes.get(i).getText());
				t.setText(eventRowIndex, 2, dublBox.getText());
				Window.alert(DK.toDk("RåvareBatch med ID : " + Integer.toString(currentID) + " opdateret i databasen."));
			} else Window.alert(DK.toDk("Fejl ved opdatering af RåvareBatch, kontakt sysadmin."));
		}
	}

	private class CommodityGetListCallback extends AsyncCallbackAbstract<ArrayList<RaavareBatchDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareBatchDTO> result) {
			createTable();
			list = result;
			if (!result.isEmpty()) {
				ID = new int[result.size()];
				raavareID = new int[result.size()];
				for (int i = 0; i < result.size();) {
					ID[i] = result.get(i).getRbId();
					raavareID[i] = result.get(i).getRaavareId();
					insertElement(i + 1, result.get(i++));
				}
			}
		}
	}

	@Override
	protected void insertElement(int i, RaavareBatchDTO o) {
		t.setText(i, 0, String.valueOf(o.getRbId()));
		t.setText(i, 1, String.valueOf(o.getRaavareId()));
		t.setText(i, 2, String.valueOf(o.getMaengde()));
		edit = UIAbstract.generateAnchor("ret", "Rediger information for RåvareBatch med ID = " + t.getText(i, 0), new EditHandler());
		t.setWidget(i, EDIT_INDEX, edit);
		t.setText(i, EDIT_INDEX + 1, " ");
		UIAbstract.setRowStyle(i, t, true);
	}

	@UiHandler("btnSearch")
	void onBtnSearchClick(@SuppressWarnings("unused") ClickEvent event) {
		try {
			int i = Integer.parseInt(IntegerBox.getText());
			super.showRaavare(i);
		} catch (NumberFormatException e) {
			Window.alert("Indtast kun heltal");
		}
	}
	
	private class MaengdeKeyUpHandler implements KeyUpHandler{

		@Override
		public void onKeyUp(KeyUpEvent arg0) {
			try {
				dublBox.getValueOrThrow();
				UIAbstract.setValidity(dublBox, true);
				ok.setEnabled(true);
			} catch (ParseException e) {
				UIAbstract.setValidity(dublBox, false);
				ok.setEnabled(false);
			}
			
		}
		
	}
	
	@UiHandler("btnShowUsed")
	void onBtnShowUsedClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showEmpty();
	}
	@UiHandler("btnShowLowest")
	void onBtnShowLowestClick(@SuppressWarnings("unused") ClickEvent event) {
		super.showLowest();
	}
	@UiHandler("btnShowAll")
	void onBtnShowAllClick(@SuppressWarnings("unused") ClickEvent event) {
		serv.browseRaavareBatch(new CommodityGetListCallback());
	}
}
