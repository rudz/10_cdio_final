
package dtu.client.ui.vaegtForbindelse;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IVaegtForbindelseService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.misc.DK;


public class ForbindelseView extends Composite {

	private static ForbindelseViewUiBinder	uiBinder		= GWT.create(ForbindelseViewUiBinder.class);
	@UiField
	Label									title;
	@UiField
	FlexTable								t;
	@UiField
	VerticalPanel							panel;

	/* anchors */
	private Anchor							previousCancel	= null;
	private Anchor							deleteAnchor	= null;
	private Anchor							okAnchor		= null;
	private Anchor							cancelAnchor	= null;

	/* variables */
	private int								eventRowIndex, prevCancelRowIndex;

	interface ForbindelseViewUiBinder extends UiBinder<Widget, ForbindelseView> { /**/}

	private IVaegtForbindelseService	service;

	public ForbindelseView(IVaegtForbindelseService service) {

		initWidget(uiBinder.createAndBindUi(this));
		this.service = service;
		title.setText(DK.toDk("Vægtforbindelser"));
		buildForbindelseFlexTable(t);
	}

	private void buildForbindelseFlexTable(FlexTable ft) {
		ft.removeAllRows();
		final FlexTable sft = ft;
		service.getForbindelseList(new AsyncCallback<ArrayList<VaegtForbindelseDTO>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(DK.toDk(caught.toString()));
			}

			@Override
			public void onSuccess(ArrayList<VaegtForbindelseDTO> arg0) {
				final ArrayList<VaegtForbindelseDTO> list = arg0;
				int connections = list.size();

				UIAbstract.setFlexTableHeaders(sft, "id", "IP Adresse", "Port Adresse");

				for (int i = 1; i <= connections; i++) {
					sft.setText(i, 0, String.valueOf(list.get(i - 1).getId()));
					sft.setText(i, 1, list.get(i - 1).getIp());
					sft.setText(i, 2, String.valueOf(list.get(i - 1).getPort()));
					deleteAnchor = UIAbstract.generateAnchor("Slet", "Slet forbindelse " + list.get(i - 1).getIp(), new deleteClickHandler());
					sft.setWidget(i, 3, deleteAnchor);
					UIAbstract.setRowStyle(i, sft, true);
				}
			}
		});
	}

	private class deleteClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			if (previousCancel != null) {
				previousCancel.fireEvent(new ClickEvent() { /**/});
			}

			/* get rowindex where event happened */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			/* get delete anchor ref for cancel operation */
			deleteAnchor = (Anchor) event.getSource();

			/* configure "ok" / "fortryd" anchors */
			okAnchor = UIAbstract.generateAnchor("ok", "Verificer sletning af forbindelse med ID = " + t.getText(eventRowIndex, 0),
					new OkClickHandler());
			cancelAnchor = UIAbstract.generateAnchor("fortryd", "Afbryd sletning af forbindelse.", new CancelClickHandler());
			prevCancelRowIndex = eventRowIndex;
			previousCancel = cancelAnchor;

			/* show ok and cancel widgets */
			t.setWidget(eventRowIndex, 3, okAnchor);
			t.setWidget(eventRowIndex, 4, cancelAnchor);
		}
	}

	private class OkClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			previousCancel = null;

			eventRowIndex = t.getCellForEvent(event).getRowIndex();
			int id = Integer.parseInt(t.getText(eventRowIndex, 0));

			service.sletForbindelse(id, new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert(DK.toDk(caught.toString()));
				}

				@Override
				public void onSuccess(Boolean arg0) {
					buildForbindelseFlexTable(t);
				}
			});
		}
	}

	private class CancelClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			previousCancel = null;
			t.removeCell(prevCancelRowIndex, 4);
			t.removeCell(prevCancelRowIndex, 3);
			deleteAnchor = UIAbstract.generateAnchor("Slet", "Slet forbindelse", new deleteClickHandler());
			t.setWidget(prevCancelRowIndex, 3, deleteAnchor);
		}
	}
}
