
package dtu.client.ui.vaegtForbindelse;

import java.text.ParseException;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IVaegtForbindelseService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.validate.FieldVerifier;


public class ForbindelseAdd extends Composite {

	private static ForbindelseAddUiBinder	uiBinder	= GWT.create(ForbindelseAddUiBinder.class);
	@UiField
	Button									ok;
	@UiField
	Label									portLabel;
	@UiField
	Label									ipLabel;
	@UiField
	CaptionPanel							cPanel;
	@UiField
	Label									ttlLabel;
	@UiField
	AbsolutePanel							xyPanel;
	@UiField
	HTMLPanel								mainPanel;
	@UiField
	HorizontalPanel							ipPanel;
	@UiField
	HorizontalPanel							portPanel;
	@UiField
	TextBox									textBox;
	@UiField
	IntegerBox								integerBox;
	@UiField
	Label									statusLabel;

	private IVaegtForbindelseService		service;

	private boolean							ipOk, portOk;

	interface ForbindelseAddUiBinder extends UiBinder<Widget, ForbindelseAdd> { /**/ }

	public ForbindelseAdd(IVaegtForbindelseService service) {
		this.service = service;
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("textBox")
	void onTextBoxKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		try {
			String ip = textBox.getValueOrThrow();
			if (FieldVerifier.isValidIp(ip)) {
				UIAbstract.setValidity(textBox, true);
				ipOk = true;
			} else {
				ipOk = false;
				UIAbstract.setValidity(textBox, false);
			}

		} catch (ParseException e) {
			ipOk = false;
			UIAbstract.setValidity(textBox, false);
		}
		isGood();
	}

	@UiHandler("integerBox")
	void onIntegerBoxKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		try {
			int port = integerBox.getValueOrThrow();
			if (FieldVerifier.isValidPort(port)) {
				UIAbstract.setValidity(integerBox, true);
				portOk = true;
			} else {
				UIAbstract.setValidity(integerBox, false);
				portOk = false;
			}

		} catch (ParseException e) {
			UIAbstract.setValidity(integerBox, false);
			portOk = false;
		}
		isGood();
	}

	@UiHandler("ok")
	void onOkClick(@SuppressWarnings("unused") ClickEvent event) {

		VaegtForbindelseDTO vfDTO = new VaegtForbindelseDTO(textBox.getText(), integerBox.getValue());

		service.OpretForbindelsesData(vfDTO, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void arg0) {
				statusLabel.setText("Forbindelsesdata oprettet i databasen");

			}

			@Override
			public void onFailure(Throwable arg0) {
				statusLabel.setText("Fejl: " + arg0.getMessage());

			}
		});
	}

	private void isGood() {
		if (portOk && ipOk) ok.setEnabled(true);
		else ok.setEnabled(false);

	}
}
