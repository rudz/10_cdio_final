
package dtu.client.ui.generic;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import dtu.client.service.GenericClientImpl;
import dtu.client.ui.login.Login;
import dtu.client.ui.login.Logout;
import dtu.client.ui.operatoer.OperatoerAdd;
import dtu.client.ui.operatoer.OperatoerBrowse;
import dtu.client.ui.operatoer.OperatoerDelete;
import dtu.client.ui.operatoer.OperatoerEdit;
import dtu.client.ui.produktbatch.ProduktBatchAdd;
import dtu.client.ui.produktbatch.ProduktbatchView;
import dtu.client.ui.raavare.RaavareAdd;
import dtu.client.ui.raavare.RaavareBrowse;
import dtu.client.ui.raavare.RaavareEdit;
import dtu.client.ui.raavarebatch.RaavareBatchAdd;
import dtu.client.ui.raavarebatch.RaavareBatchBrowse;
import dtu.client.ui.raavarebatch.RaavareBatchDelete;
import dtu.client.ui.raavarebatch.RaavareBatchEdit;
import dtu.client.ui.recept.ReceptAdd;
import dtu.client.ui.recept.ReceptView;
import dtu.client.ui.vaegtForbindelse.ForbindelseAdd;
import dtu.client.ui.vaegtForbindelse.ForbindelseView;
import dtu.shared.LoginStatus;


public class ContentView extends Composite {

	private GenericClientImpl	clientImpl;
	private VerticalPanel		contentPanel;

	public ContentView() {
	}

	public ContentView(GenericClientImpl clientImpl) {
		this.clientImpl = clientImpl;
		contentPanel = new VerticalPanel();
		initWidget(this.contentPanel);
	}

	/* *************************************
	 * ********* NAVIGATION MENU ************
	 * ************************************
	 */

	public void openWelcomeView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			WelcomeView welcomeView = new WelcomeView(clientImpl.service);
			contentPanel.add(welcomeView);
		} else openLoginView();
	}

	public void openLoginView() {
		contentPanel.clear();
		final Login loginView = new Login(clientImpl.service);
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {

			@Override
			public void execute() {
				loginView.getIdBox().setFocus(true);
			}
		});
		contentPanel.add(loginView);
	}

	public void openLogoutView() {
		LoginStatus.clear();
		RootPanel.get("nav").clear();
		// RootPanel.get("section").clear();
		Logout lo = new Logout();
		contentPanel.add(lo);
	}

	public void openAddView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			OperatoerAdd addView = new OperatoerAdd(clientImpl.service);
			contentPanel.add(addView);
		} else openLoginView();
	}

	public void openBrowseView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			OperatoerBrowse browseView = new OperatoerBrowse(clientImpl.service);
			contentPanel.add(browseView);
		} else openLoginView();
	}

	public void openDeleteView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			OperatoerDelete deleteView = new OperatoerDelete(clientImpl.service);
			contentPanel.add(deleteView);
		} else openLoginView();
	}

	public void openEditView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			OperatoerEdit editView = new OperatoerEdit(clientImpl.service);
			contentPanel.add(editView);
		} else openLoginView();
	}

	public void openChangePwView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ChangePasswordView pwView = new ChangePasswordView(clientImpl.service);
			contentPanel.add(pwView);
		} else openLoginView();
	}

	public void openRaavareAddView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareAdd raavareView = new RaavareAdd(clientImpl.service);
			contentPanel.add(raavareView);
		} else openLoginView();
	}

	public void openRaavareBrowse() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareBrowse raavareB = new RaavareBrowse(clientImpl.service);
			contentPanel.add(raavareB);
		} else openLoginView();
	}

	public void openCommodityBatchAdd() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareBatchAdd addCommodityView = new RaavareBatchAdd(clientImpl.service);
			contentPanel.add(addCommodityView);
		} else openLoginView();
	}

	public void openCommodityBatchBrowse() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareBatchBrowse browseCommodityView = new RaavareBatchBrowse(clientImpl.service);
			contentPanel.add(browseCommodityView);
		}
	}

	public void openCommodityBatchDelete() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareBatchDelete deleteCommodityView = new RaavareBatchDelete(clientImpl.service);
			contentPanel.add(deleteCommodityView);
		}
	}

	public void openCommodityBatchEdit() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareBatchEdit editCommodityView = new RaavareBatchEdit(clientImpl.service);
			contentPanel.add(editCommodityView);
		}
	}

	public void openRaavareEdit() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			RaavareEdit re = new RaavareEdit(clientImpl.service);
			contentPanel.add(re);

		} else openLoginView();
	}

	public void openReceptBrowser() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ReceptView rw = new ReceptView(clientImpl.service);
			contentPanel.add(rw);
		} else openLoginView();
	}
	public void openReceptAdd() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ReceptAdd ra = new ReceptAdd(clientImpl.service);
			contentPanel.add(ra);
		} else openLoginView();
	}

	public void openProduktBatchAdd() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ProduktBatchAdd pb = new ProduktBatchAdd(clientImpl.service);
			contentPanel.add(pb);
		} else openLoginView();
	}

	public void openProduktBatchBrowse() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ProduktbatchView pb = new ProduktbatchView(clientImpl.service);
			contentPanel.add(pb);
		} else openLoginView();

	}

	public void openVaegtForbindlseView() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ForbindelseView fw = new ForbindelseView(clientImpl.service);
			contentPanel.add(fw);
		} else openLoginView();
	}

	public void openVaegtForbindelseAdd() {
		if (LoginStatus.loggedIn) {
			contentPanel.clear();
			ForbindelseAdd fa = new ForbindelseAdd(clientImpl.service);
			contentPanel.add(fa);
		} else openLoginView();
	}
}
