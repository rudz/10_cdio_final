
package dtu.client.ui.generic;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import dtu.shared.misc.DK;


/** Helper class with generic helper methods for UI
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class UIAbstract {

	/** Set the style according to validity.
	 * 
	 * @param tb
	 *            : The TextBox widget to alter.
	 * @param valid
	 *            : The validity state. */
	public static void setValidity(TextBox tb, boolean valid) {
		if (!valid) tb.setStyleName("gwt-TextBox-invalidEntry");
		else tb.removeStyleName("gwt-TextBox-invalidEntry");
	}
	/** Set the style according to validity.<br>
	 * Overload of {@link #setValidity(TextBox, boolean) setValidity} to support
	 * LongBox.
	 * 
	 * @param tb
	 *            : The LongBox widget to alter.
	 * @param valid
	 *            : The validity state. */
	public static void setValidity(LongBox tb, boolean valid) {
		if (!valid) tb.setStyleName("gwt-TextBox-invalidEntry");
		else tb.removeStyleName("gwt-TextBox-invalidEntry");
	}
	/** Set the style according to validity.<br>
	 * Overload of {@link #setValidity(TextBox, boolean) setValidity} to support
	 * IntegerBox.
	 * 
	 * @param tb
	 *            : The IntegerBox widget to alter.
	 * @param valid
	 *            : The validity state. */
	public static void setValidity(IntegerBox tb, boolean valid) {
		if (!valid) tb.setStyleName("gwt-TextBox-invalidEntry");
		else tb.removeStyleName("gwt-TextBox-invalidEntry");
	}
	/** Set the style according to validity.<br>
	 * Overload of {@link #setValidity(TextBox, boolean) setValidity} to support
	 * DoubleBox.
	 * 
	 * @param tb
	 *            : The DoubleBox widget to alter.
	 * @param valid
	 *            : The validity state. */
	public static void setValidity(DoubleBox tb, boolean valid) {
		if (!valid) tb.setStyleName("gwt-TextBox-invalidEntry");
		else tb.removeStyleName("gwt-TextBox-invalidEntry");
	}
	/** Set the style according to validity.<br>
	 * Overload of {@link #setValidity(TextBox, boolean) setValidity} to support
	 * SuggestionBox.
	 * 
	 * @param tb
	 *            : The SuggestionBox widget to alter.
	 * @param valid
	 *            : The validity state. */
	public static void setValidity(SuggestBox supplierName, boolean valid) {
		if (!valid) supplierName.setStyleName("gwt-TextBox-invalidEntry");
		else supplierName.removeStyleName("gwt-TextBox-invalidEntry");
	}

	/** Set a sleak rowstyle, oscilating between dark and light
	 * 
	 * @param row
	 *            : The row
	 * @param t
	 *            : The flextable to alter */
	public static void setRowStyle(int row, FlexTable t, boolean includeFirst) {
		if (row % 2 != 0) t.getRowFormatter().setStyleName(row, "detail-cell1");
		else t.getRowFormatter().setStyleName(row, "detail-cell2");
		if (!includeFirst) t.getCellFormatter().addStyleName(row, 0, "empty");
	}

	/** Populates the flextable header slots and set the style.
	 * 
	 * @param t
	 *            : The FlexTable
	 * @param headerNames
	 *            : The headernames as string(s) */
	public static void setFlexTableHeaders(FlexTable t, String... headerNames) {
		t.getRowFormatter().addStyleName(0, "FlexTable-Header");
		for (int i = 0; i < headerNames.length; i++)
			t.setText(0, i, headerNames[i]);
	}

	/** Simplified functionality to populate a flextable row.
	 * 
	 * @param t
	 *            : The flextable
	 * @param row
	 *            : The row number
	 * @param text
	 *            : The text, both single and array accepted. */
	public static void addFlexTableRow(FlexTable t, int row, String... text) {
		for (int i = 0; i < text.length; i++)
			t.setText(row, i, text[i]);
	}

	/** Generate a "complete package" for anchors.
	 * 
	 * @param name
	 *            : The name of the anchor.
	 * @param toolTip
	 *            : The tooltip for the anchor.
	 * @param handler
	 *            : The clickhandler
	 * @return The Anchor object */
	public static Anchor generateAnchor(String name, String toolTip, ClickHandler handler) {
		Anchor a = new Anchor(DK.toDk(name));
		a.setTitle(DK.toDk(toolTip));
		a.addClickHandler(handler);
		return a;
	}

}
