
package dtu.client.ui.generic;

import dtu.client.controller.MainView;
import dtu.shared.LoginStatus;
import dtu.shared.misc.OperatorLevel;


public class MenuView extends MenuViewAbstract {

	public MenuView(final MainView main) {
		super(main);
		initWidget(vPanel);
		this.main = main;

		/* just return if not logged in, else the basic will always show up */
		if (!LoginStatus.loggedIn) return;

		generic();
		switch (LoginStatus.level) {
		case 9:
			admin();
			break;
		case 3:
			pharmacist();
			break;
		case 2:
			foreman();
			break;
		case 1:
		default:
			operator();
			break;
		}

	}

	/** for everyone */
	private void generic() {
		vPanel.add(UIAbstract.generateAnchor("Log ud [" + Integer.toString(LoginStatus.userID) + "]",
				LoginStatus.userName + "\n" + OperatorLevel.olText(LoginStatus.level), new LogoutClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Skift kodeord", "Skift dit kodeord. (husk at gøre dette hver måned)", new ChangePasswordClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Information", "Simplistisk information omkring systemet.", new InformationClickHandler()));
	}

	/** operator level = 1 */
	private void operator() {
		// TODO : Add some stuff here!
	}

	/** foreman (værkfører) level = 2 */
	private void foreman() {
		vPanel.add(headerLabel("RåvareBatch"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Vis alle råvarer i systemet", new RaavareBatchBrowseClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Tilføj", "Tilføj nye råvarebatches til systemet.", new RaavareBatchAddClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Ret", "Ret eksisterende råvarebatches i systemet", new RaavareBatchEditClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Slet", "Slet eksisterende råvarebatch(es) i systemet", new RaavareBatchDeleteClickHandler()));

		vPanel.add(headerLabel("ProduktBatch"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Vis alle produktbatches i systemet", new ProduktBatchViewClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Tilføj", "Tilføj nye produktbatch(es) til systemet.", new ProduktBatchAddClickHandler()));

		vPanel.add(headerLabel("ASE forbindelser"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Se vægtforbindelser som ASEN forbinder til", new ForbindelseViewClickHandler()));
	}

	/** pharmacist (farmaceut) level = 3 */
	private void pharmacist() {
		vPanel.add(headerLabel("Råvarer"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Vis alle råvarer i systemet.", new RaavareBrowseAddClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Tilføj", "Tilføj råvare i systemet.", new RaavareAddClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Ret", "Ret eksisterende råvarer i systemet.", new RaavareEditClickHandler()));
		vPanel.add(headerLabel("Recepter"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Vis eksisterende recepter i systemet.", new ReceptBrowseClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Tilføj", "Tilføj recept(er) til systemet.", new ReceptAddClickHandler()));
	}

	/** admin level = 9 */
	private void admin() {
		vPanel.add(headerLabel("Brugere"));
		vPanel.add(UIAbstract.generateAnchor("Vis", "Vis alle brugere i systemet.", new UserBrowseClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Tilføj", "Tilføj bruger til systemet.", new UserAddClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Ret", "Rediger nuværende bruger.", new UserEditClickHandler()));
		vPanel.add(UIAbstract.generateAnchor("Slet", "Sæt brugere til inaktiv status.", new UserDeleteClickHandler()));
		foreman();
		pharmacist();
	}

}
