
package dtu.client.ui.generic;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;


/** This class is intended as a generic popup class for tooltips. The constructor
 * takes the message to be displayed in the label, atm 200px wide. Its screen
 * position is 0,0 by default, so that has to be set manually fx with
 * setPosition(int x, int y) in the event handler (getScreenX() and getScreenY()
 * are great with offset +100 - 200). call show() on the instance of this class
 * to show it one the screen. <br>
 * To be expanded with setter methods and fairy dust sprinkles <br>
 * <br>
 * 
 * @author DarudeSandstorm (Did he dieded?) */

public class MsgPopup extends PopupPanel {
	Label	Message;
	// String width;
	public MsgPopup(String msg) {
		super(true);
		Message = new Label(msg);
		// PasswordManager.getRequirements();
		Message.setWidth("200px");
		setWidget(Message);
	}

	/** @param width
	 *            can be both pix or percentage */
	public void setMsgWidt(String width) {
		Message.setWidth(width);
	}

}
