
package dtu.client.ui.generic;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.admin.IChangePasswordView;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;
import dtu.shared.validate.FieldVerifier;
import dtu.shared.validate.PasswordValidator;


public class ChangePasswordView extends Composite {

	private IChangePasswordView					serv;
	private boolean								newPwValid;
	private static ChangePasswordViewUiBinder	uiBinder	= GWT.create(ChangePasswordViewUiBinder.class);
	@UiField
	PasswordTextBox								pwNew;
	@UiField
	PasswordTextBox								pwNewRep;
	@UiField
	Button										btnUpdate;
	@UiField
	DisclosurePanel								dpRules;
	@UiField
	TextArea									txtRules;
	@UiField
	Label										lblTitle;
	@UiField
	Label										lblInformation;

	interface ChangePasswordViewUiBinder extends UiBinder<Widget, ChangePasswordView> { /* */}

	public ChangePasswordView(IChangePasswordView service) {
		this.serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblInformation.setText(DK.toDk("Indtast ens kodeord foroven."));
		btnUpdate.setEnabled(false);
		txtRules.setReadOnly(true);
		txtRules.setText(PasswordValidator.getRequirements());
	}

	/* *********************************
	 * ******* HELPER METHODS *******************************************
	 */

	private void checkFormValid() {
		btnUpdate.setEnabled(newPwValid);
		if (!btnUpdate.isEnabled()) lblInformation.setText("Kodeordet er ikke gyldigt, eller ikke ens.");
	}

	/* *********************************
	 * ********* UI HANDLERS ********************************************
	 */

	@UiHandler("pwNew")
	void onPwNewKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		newPwValid = FieldVerifier.isValidPW(LoginStatus.userID, pwNew.getText(), pwNewRep.getText());
		UIAbstract.setValidity(pwNew, newPwValid);
		UIAbstract.setValidity(pwNewRep, newPwValid);
		checkFormValid();
	}
	@UiHandler("pwNew")
	void onPwNewKeyDown(KeyDownEvent event) {
		if (newPwValid && event.getNativeKeyCode() == 13) btnUpdate.click();
	}
	@UiHandler("pwNewRep")
	void onPwNewRepKeyUp(KeyUpEvent event) {
		onPwNewKeyUp(event);
	}
	@UiHandler("pwNewRep")
	void onPwNewRepKeyDown(KeyDownEvent event) {
		onPwNewKeyDown(event);
	}
	@UiHandler("btnUpdate")
	void onBtnUpdateClick(@SuppressWarnings("unused") ClickEvent event) {
		serv.updatePW(new OperatoerDTO(LoginStatus.userID, pwNew.getText()), new changePwCallBack());
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	private class changePwCallBack implements AsyncCallback<Boolean> {

		@Override
		public void onFailure(Throwable caught) {
			lblInformation.setText("Fejl: " + caught.getMessage());
		}
		@Override
		public void onSuccess(Boolean result) {
			if (result) {
				lblInformation.setText("Kodeord updateret.");
				pwNew.setText(null);
				pwNewRep.setText(null);
				pwNew.fireEvent(new KeyUpEvent() { /**/});
			}
		}
	}

}
