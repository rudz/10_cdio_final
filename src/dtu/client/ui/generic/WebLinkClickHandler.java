
package dtu.client.ui.generic;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;


/** <p>
 * Simple generic click handler for external links.
 * 
 * @author Rudy Alex Kohn [s133235] */
public class WebLinkClickHandler implements ClickHandler {

	private final String	BASE_URL	= "https://www.campusnet.dtu.dk/cnnet/participants/showperson.aspx?id=";
	private String			link;
	private boolean			tab;
	private boolean			DTU;

	public WebLinkClickHandler(String link, boolean inTab, boolean DTUCampusUserLink) {
		if (link != null || !"".equals(link)) {
			this.link = link;
			tab = inTab;
			DTU = DTUCampusUserLink;
		} else {
			this.link = null;
			tab = false;
			DTU = false;
		}
	}
	@Override
	public void onClick(ClickEvent arg0) {
		if (link != null) Window.open(DTU ? BASE_URL + link : link, tab ? "_blank" : "", "");
	}

}
