package dtu.client.ui.generic;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dtu.shared.misc.DK;


/**
 * General abstraction of onFailure().
 * @author Rudy Alex Kohn [s133235]
 * @param <T>
 */
public abstract class AsyncCallbackAbstract<T> implements AsyncCallback<T> {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert(DK.toDk(caught.toString()));
		}
}
