
package dtu.client.ui.generic;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.IWelcomeService;
import dtu.shared.LoginStatus;
import dtu.shared.misc.DK;
import dtu.shared.misc.OperatorLevel;


public class WelcomeView extends Composite {

	private IWelcomeService				service;

	private String[]					devs		= new String[] {
			"s123725:Emil Hvid Hansen:160538", "s130063:Theis Friis Strømming:166300", "s133235:Rudy Alex Kohn:180706",
			"s144867:Thor Adrian Dahlstrøm:202446", "s144868:Adam Hammer Palmar:202448", "s144872:Stefan Hyltoft:202456"
													};

	private static WelcomeViewUiBinder	uiBinder	= GWT.create(WelcomeViewUiBinder.class);
	@UiField
	VerticalPanel						vpTop;
	@UiField
	Label								lblTitle;
	@UiField
	FlexTable							t;
	@UiField
	VerticalPanel						vp;

	interface WelcomeViewUiBinder extends UiBinder<Widget, WelcomeView> { /**/}

	public WelcomeView(IWelcomeService service) {
		this.service = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("_-¨-_-¨ G10 " + GWT.getModuleName() + " - GWT v" + GWT.getVersion() + " ¨-_-¨-_");
		UIAbstract.setFlexTableHeaders(t, "Studienummer", "Navn");
		t.getFlexCellFormatter().setWidth(0, 0, "180px");
		t.getFlexCellFormatter().setWidth(0, 1, "300px");
		String s[];
		for (int i = 0; i < devs.length; i++) {
			s = devs[i].split(":");
			t.setWidget(i + 1, 0, UIAbstract.generateAnchor(s[0], DK.toDk("Gå til siden for " + s[1] + " på CampusNet \nKræver login!."),
					new WebLinkClickHandler(s[2], true, true)));
			t.setText(i + 1, 1, DK.toDk(s[1]));
			UIAbstract.setRowStyle(i + 1, t, true);
		}
		vp.add(new Label(DK.toDk("Velkommen, " + LoginStatus.userName + ".")));
		vp.add(new Label("Du er logged ind med ID : " + Integer.toString(LoginStatus.userID)));
		vp.add(new Label("Dine bruger rettigheder er : " + DK.toDk(OperatorLevel.olText(LoginStatus.level))));
		this.service.getSize(new welcomeCallBack());
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	/** Callback for retrieving user count from server
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class welcomeCallBack extends AsyncCallbackAbstract<Integer> {

		@Override
		public void onSuccess(Integer result) {
			if (result > 0) {
				LoginStatus.userCount = result.intValue();
				vp.add(new Label(DK.toDk("Antal operatører i kartotek = " + LoginStatus.userCount)));
			} else onFailure(new Exception("0 Bruger fejl, kontakt system administrator."));
		}
	}

}
