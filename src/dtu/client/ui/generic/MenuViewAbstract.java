
package dtu.client.ui.generic;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import dtu.client.controller.MainView;
import dtu.shared.misc.DK;


/** Helper class for MenuView.
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class MenuViewAbstract extends Composite {

	protected VerticalPanel	vPanel	= new VerticalPanel();

	// reference to MainView controller for call backs
	protected MainView		main;

	public MenuViewAbstract(final MainView main) {
		this.main = main;
	}

	protected static HorizontalPanel headerLabel(String name) {
		HorizontalPanel hp = new HorizontalPanel();
		Label l = new Label(DK.toDk(name));
		l.setTitle(l.getText());
		l.setStyleName("FlexTable-Header");
		hp.add(l);
		hp.setWidth("130px");
		hp.setBorderWidth(1);
		return hp;
	}

	/* ************************************* */
	/* Click handler classes for this widget */
	/* ************************************* */

	// class loginClickHandler implements ClickHandler {
	//
	// @Override
	// public void onClick(ClickEvent event) {
	// main.login();
	// }
	// }

	public class LogoutClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.logout();
		}
	}

	public class InformationClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.welcome();
		}
	}

	public class UserBrowseClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.showPersons();
		}
	}

	public class UserAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.addPerson();
		}
	}

	public class UserEditClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.editPersons();
		}
	}

	public class UserDeleteClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.deletePersons();
		}
	}

	public class RaavareAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.addRaavare();
		}
	}

	public class RaavareBrowseAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.browseRaavare();;
		}
	}

	public class RaavareEditClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.editRaavare();;
		}
	}

	public class ReceptBrowseClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.browseRecept();
		}
	}

	public class ReceptAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.addRecept();
		}
	}

	public class RaavareBatchAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.addCommodityBatch();;
		}
	}

	public class RaavareBatchBrowseClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.browseCommodityBatch();;
		}
	}

	public class RaavareBatchDeleteClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.deleteCommodityBatch();;
		}
	}

	public class RaavareBatchEditClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.editCommodityBatch();;
		}
	}

	public class ChangePasswordClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.ChangePassword();
		}
	}

	public class ProduktBatchAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			main.addProduktBatch();
		}
	}

	public class ProduktBatchViewClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent arg0) {
			main.browseProduktBatch();
		}

	}

	public class ForbindelseViewClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent arg0) {
			main.openForbindelseView();

		}

	}

	public class ForbindelseAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent arg0) {
			main.openForbindelseAdd();

		}

	}

}
