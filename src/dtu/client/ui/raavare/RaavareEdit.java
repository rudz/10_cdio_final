
package dtu.client.ui.raavare;

import java.util.ArrayList;
import java.util.HashMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.IRaavareService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.misc.DK;
import dtu.shared.validate.FieldVerifier;


public class RaavareEdit extends RaavareAbstract {

	/* remote procedure call service interface reference */
	private IRaavareService				serv;

	private static RaavareEditUiBinder	uiBinder		= GWT.create(RaavareEditUiBinder.class);

	@UiField
	FlexTable							t;
	@UiField
	Label								lblTitle;

	/* row index for current action being performed */
	private int							eventRowIndex;

	/* id currently being worked on */
	private int							currentID;

	private final int					EDIT_INDEX		= 3;
	private final int					OK_INDEX		= 3;
	private final int					CANCEL_INDEX	= 4;

	/* maps for editing. */
	private HashMap<Integer, TextBox>	txtBoxes		= new HashMap<>(3, 1.0f);
	private HashMap<Integer, String>	tmpStrings		= new HashMap<>(3, 1.0f);

	/* validation fields to keep track of in-/valid inputs */
	private boolean						nameValid		= true;

	/* map key values */
	private int							raavareName		= 2;
	private int							leverandoer		= 3;

	/* Anchors */
	private Anchor						ok;
	private Anchor						cancel;
	private Anchor						edit;
	private Anchor						previousCancel	= null;

	/* not-visible operator ids */
	private int[]						ID;

	interface RaavareEditUiBinder extends UiBinder<Widget, RaavareEdit> { /**/}

	public RaavareEdit(IRaavareService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Ret R" + DK.aa + "varer");
		setFlexTableHeaderSizes(t);
		setTableHeaders();
		serv.getRaavareList(new RaavareGetListCallback());
	}

	private void setTableHeaders() {
		UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "vare ID", "R" + DK.aa + "vare Navn", "Leverand" + DK.oe + "r");
	}

	private void validateForm() {
		if (nameValid) t.setWidget(eventRowIndex, OK_INDEX, ok);
		else t.setText(eventRowIndex, OK_INDEX, "ok");
	}

	private class EditHandler extends UIAbstract implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			/*
			 * detect if a previous edit action is in progress, if so, force
			 * cancel click handler to fire
			 */
			if (previousCancel != null) previousCancel.fireEvent(new ClickEvent() { /**/});

			/* mark current row of pressed anchor */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			/* configure text boxes */
			for (int i = 1; i < 3; i++) {
				txtBoxes.get(i + 1).setText(t.getText(eventRowIndex, i));
				setTextBoxWidth(txtBoxes.get(i + 1), i);
				t.setWidget(eventRowIndex, i, txtBoxes.get(i + 1));
			}

			// start editing here
			txtBoxes.get(raavareName).setFocus(true);

			// get edit anchor ref for cancel operation
			edit = (Anchor) event.getSource();

			// get textbox contents for cancel operation
			/* store the values in the case cancel will be pressed */
			for (Integer i : txtBoxes.keySet())
				tmpStrings.put(i, txtBoxes.get(i).getText());

			/* set up anchors and their respective click handlers */
			ok = UIAbstract.generateAnchor("ok", "Gem ændringer.", new OkClickHandler());
			cancel = UIAbstract.generateAnchor("afbryd", "Afbryd og forkast ændringer foretaget.", new CancelClickHandler());
			previousCancel = cancel;

			/* assign event handler classes to the text boxes */

			txtBoxes.get(raavareName).addKeyUpHandler(new NameKeyUpHandler());

			/* force KeyUpEvent to make sure they are validated */
			for (Integer i : txtBoxes.keySet())
				txtBoxes.get(i).fireEvent(new KeyUpEvent() { /**/});

			/* drop those anchors onto the table */
			t.setWidget(eventRowIndex, OK_INDEX, ok);
			t.setWidget(eventRowIndex, CANCEL_INDEX, cancel);
			// t.setWidget(eventRowIndex, RESET_INDEX, reset);
			UIAbstract.setRowStyle(eventRowIndex, t, true);
			setFlexTableHeaderSizes(t);

			txtBoxes.get(raavareName).setSelectionRange(0, txtBoxes.get(raavareName).getText().length());
		}

		/** <p>
		 * ClickHandler for 'ok' Anchor.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class OkClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				currentID = ID[eventRowIndex - 1];
				/* since ok was clicked, modify the table to show the new values */
				// TODO : FIX ALIGNMENT!
				for (Integer i : txtBoxes.keySet())
					t.setText(eventRowIndex, i - 1, txtBoxes.get(i).getText());
				serv.updateRaavare(new RaavareDTO(currentID, txtBoxes.get(raavareName).getText(), txtBoxes.get(leverandoer).getText()),
						new RaavareUpdateCallback());

				// restore new state

				// t.clearCell(eventRowIndex, PW_INDEX);
				t.clearCell(eventRowIndex, OK_INDEX);
				t.clearCell(eventRowIndex, CANCEL_INDEX);
				previousCancel = null;
				cancel = null;
				t.setWidget(eventRowIndex, EDIT_INDEX, edit);
				UIAbstract.setRowStyle(eventRowIndex, t, true);
			}
		}

		/** <p>
		 * ClickHandler for 'cancel' anchor.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class CancelClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				// restore original content of textboxes and rerun input
				// validation
				txtBoxes.get(raavareName).fireEvent(new KeyUpEvent() { /**/});
				// TODO : FIX ALIGNMENT!
				for (Integer i : tmpStrings.keySet())
					t.setText(eventRowIndex, i - 1, tmpStrings.get(i));
				// restore edit link

				t.clearCell(eventRowIndex, CANCEL_INDEX);
				// t.clearCell(eventRowIndex, RESET_INDEX);
				previousCancel = null;
				t.setWidget(eventRowIndex, EDIT_INDEX, edit);
				setFlexTableHeaderSizes(t);
				UIAbstract.setRowStyle(eventRowIndex, t, true);
			}
		}

		/** <p>
		 * KeyUpHandler class for 'name' input field.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class NameKeyUpHandler implements KeyUpHandler {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				nameValid = FieldVerifier.isValidName(txtBoxes.get(raavareName).getText());
				setValidity(txtBoxes.get(raavareName), nameValid);
				validateForm();
			}
		}
	}

	private class RaavareUpdateCallback extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean bool) {
			if (bool) {
				// TODO :FIX ALIGNMENT!
				for (Integer i : txtBoxes.keySet())
					tmpStrings.put(i, txtBoxes.get(i).getText());
				Window.alert("R" + DK.aa + "vare med ID : " + Integer.toString(currentID) + " opdateret i databasen.");
			} else Window.alert("Fejl ved opdatering af r" + DK.aa + "vare, kontakt sysadmin.");
		}
	}

	private class RaavareGetListCallback extends AsyncCallbackAbstract<ArrayList<RaavareDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareDTO> result) {
			if (!result.isEmpty()) {
				int rowIndex = 0;
				ID = new int[result.size()];
				for (int i = 0; i < result.size(); i++) {
					ID[rowIndex] = result.get(i).getRaavareId();
					UIAbstract.addFlexTableRow(t, rowIndex + 1, Integer.toString(ID[rowIndex]), DK.toDk(result.get(i).getRaavareNavn()),
							DK.toDk(result.get(i).getLeverandoer()), " ", " ");
					UIAbstract.setRowStyle(rowIndex + 1, t, true);
					edit = UIAbstract.generateAnchor(
							"ret",
							"Foretag ændringer på \n[" + Integer.toString(result.get(i).getRaavareId()) + "] "
									+ DK.toDk(result.get(i).getRaavareNavn()) + " \n" + DK.toDk(result.get(i).getLeverandoer()), new EditHandler());
					t.setWidget(rowIndex++ + 1, EDIT_INDEX, edit);
				}
			}
			// editPanel.add(t);

			/*
			 * text boxes, doing these through map allows instant access based
			 * on string
			 */
			for (int i = 2; i < 6; i++)
				txtBoxes.put(i, new TextBox());

			txtBoxes.get(raavareName).setWidth("200px");
			txtBoxes.get(leverandoer).setWidth("200px");
		}
	}

}
