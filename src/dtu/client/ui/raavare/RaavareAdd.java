
package dtu.client.ui.raavare;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.IRaavareService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.misc.DK;
import dtu.shared.validate.FieldVerifier;
import com.google.gwt.event.dom.client.KeyPressEvent;


public class RaavareAdd extends Composite {

	private static RaavareAddUiBinder		uiBinder	= GWT.create(RaavareAddUiBinder.class);
	@UiField
	Button									button;
	@UiField
	IntegerBox								raavareId;
	@UiField
	TextBox									raavareName;
	@UiField(provided = true)
	SuggestBox								supplierName;
	@UiField
	Label									lblTitle;
	@UiField
	CaptionPanel							capRaavareAdd;
	@UiField
	Label									lblSupplier;
	@UiField
	Label									lblName;
	@UiField
	Label									lblID;

	private IRaavareService					service;
	private int								lowId;
	private final MultiWordSuggestOracle	oracle		= new MultiWordSuggestOracle();

	private volatile boolean				validID;
	private boolean							validName, validSupplier;

	interface RaavareAddUiBinder extends UiBinder<Widget, RaavareAdd> {/* void */}

	public RaavareAdd(IRaavareService service) {
		supplierName = new SuggestBox(oracle);
		initWidget(uiBinder.createAndBindUi(this));
		this.service = service;
		lblTitle.setText("Tilf" + DK.oe + "j R" + DK.aa + "vare");
		lblSupplier.setText(DK.toDk(lblSupplier.getText()));
		capRaavareAdd.setCaptionText(lblTitle.getText());
		button.setEnabled(false);
		service.getRaavareList(new browseRaavare());
	}

	@UiHandler("button")
	void onButtonClick(@SuppressWarnings("unused") ClickEvent event) {
		if (isValid()) service.createRaavare(new RaavareDTO(raavareId.getValue(), raavareName.getText(), supplierName.getText()), new addRaavare());
	}

	@UiHandler("raavareName")
	void onRaavareNavnKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) button.click();
	}
	@UiHandler("raavareId")
	void onRaavareIdKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) button.click();
	}
	@UiHandler("supplierName")
	void onSupplierNameKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) button.click();
	}

	private void clearBoxes() {
		raavareName.setText(null);
		raavareId.setText(null);
		supplierName.setText(null);
		raavareName.fireEvent(new KeyUpEvent() { /**/});
		raavareId.fireEvent(new KeyUpEvent() { /**/});
		supplierName.fireEvent(new KeyUpEvent() { /**/});
		service.getRaavareList(new browseRaavare());
	}

	private class browseRaavare extends AsyncCallbackAbstract<ArrayList<RaavareDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareDTO> result) {
			for (RaavareDTO r : result) oracle.add(r.getLeverandoer());

			int listSize = result.size(), pos;
			if (listSize >= result.get(listSize - 1).getRaavareId()) lowId = result.get(listSize - 1).getRaavareId() + 1;
			else {
				int low = 0, high = listSize;
				while (true) {
					pos = (low + high) << 1;
					if (low + 1 == high) {
						lowId = result.get(pos).getRaavareId() + 1;
						break;
					}
					if (pos + 1 < result.get(pos).getRaavareId()) high = pos;
					else if (pos + 1 == result.get(pos).getRaavareId()) low = pos;
					else {
						lowId = result.get(pos).getRaavareId() + 1;
						break;
					}
				}
			}
			raavareId.setValue(lowId);
			if (raavareId.getValue() > 0 && raavareId.getValue() <= Integer.MAX_VALUE) service.isRaavare(raavareId.getValue(), new isRaavare());
			else UIAbstract.setValidity(raavareId, false);
			isValid();
		}
	}

	private boolean isValid() {
		boolean valid = validID & validName & validSupplier;
		button.setEnabled(valid);
		return valid;
	}

	private class addRaavare extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean result) {
			if (result) {
				Window.alert("R" + DK.aa + "vare med id = " + raavareId.getValue() + " gemt i databasen.");
				clearBoxes();
				service.getRaavareList(new browseRaavare());
			} else onFailure(new Exception("Fejl ved oprettelse af raavare, prøv venligst igen."));
		}
	}

	private class isRaavare extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean res) {
			validID = !res;
			UIAbstract.setValidity(raavareId, validID);
			isValid();
		}
	}

	@UiHandler("raavareId")
	void onRaavareIdKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		if (raavareId.getValue() > 0 && raavareId.getValue() < 1E8) service.raavareExists(raavareId.getValue(), new isRaavare());
		else {
			validID = false;
			UIAbstract.setValidity(raavareId, false);
		}
		
		isValid();
	}
	@UiHandler("raavareName")
	void onRaavareNameKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		validName = FieldVerifier.isValidName(raavareName.getText(), 2, 20);
		UIAbstract.setValidity(raavareName, validName);
		isValid();
	}

	@UiHandler("supplierName")
	void onSupplierNameKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		validSupplier = FieldVerifier.isValidName(supplierName.getText(), 2, 50);
		UIAbstract.setValidity(supplierName, validSupplier);
		isValid();
	}
	@UiHandler("supplierName")
	void onSupplierNameKeyPress(@SuppressWarnings("unused") KeyPressEvent event) {
		supplierName.fireEvent(new KeyUpEvent() { /**/});
	}
}
