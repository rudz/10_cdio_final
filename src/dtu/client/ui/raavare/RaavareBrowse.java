
package dtu.client.ui.raavare;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.IRaavareService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.misc.DK;


public class RaavareBrowse extends Composite {

	private IRaavareService					serv;

	private static RaavareBrowseUiBinder	uiBinder	= GWT.create(RaavareBrowseUiBinder.class);
	@UiField
	Label									lblTitle;
	@UiField
	VerticalPanel							panel;
	@UiField
	FlexTable								t;

	interface RaavareBrowseUiBinder extends UiBinder<Widget, RaavareBrowse> { /**/}

	public RaavareBrowse(IRaavareService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Vis R" + DK.aa + "vare");
		serv.getRaavareList(new browseRaavare());
	}

	private class browseRaavare extends AsyncCallbackAbstract<ArrayList<RaavareDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareDTO> result) {
			t.getFlexCellFormatter().setWidth(0, 0, "50px");
			t.getFlexCellFormatter().setWidth(0, 1, "100px");
			t.getFlexCellFormatter().setWidth(0, 2, "100px");
			UIAbstract.setFlexTableHeaders(t, "R" + DK.aa + "vare ID", "R" + DK.aa + "vare Navn", "Leverand" + DK.oe + "r");
			int i = 0;
			for (RaavareDTO o : result) {
				UIAbstract.addFlexTableRow(t, i + 1, Integer.toString(o.getRaavareId()), DK.toDk(o.getRaavareNavn()), DK.toDk(o.getLeverandoer()));
				UIAbstract.setRowStyle(i++ + 1, t, true);
			}
		}
	}

}
