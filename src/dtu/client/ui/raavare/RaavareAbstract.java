
package dtu.client.ui.raavare;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;


public abstract class RaavareAbstract extends Composite {

	private int[]	headerSize	= new int[] {
			70, 100, 130, 70, 40, 50
								};
	protected void setFlexTableHeaderSizes(FlexTable t) {
		for (int i = 0; i < headerSize.length; i++)
			t.getFlexCellFormatter().setWidth(0, i, Integer.toString(headerSize[i]) + "px");
	}
	protected void setTextBoxWidth(TextBox tb, int col) {
		tb.setWidth(Integer.toString(headerSize[col]) + "px");
	}
	
}
