
package dtu.client.ui.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.controller.MainView;
import dtu.client.service.ILoginViewService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.StringToolLib;


/** Login class, for the particular offset minded people!<br>
 * 
 * @author Rudy Alex Kohn [s133235] */
public class Login extends Composite {

	/* constants */
	private final String			FEJL		= "Fejl ved validering.";
	private final String			INT			= "Indtast venligst ID og kodeord.";

	/* UIBinding stuff */
	private static LoginUiBinder	uiBinder	= GWT.create(LoginUiBinder.class);
	@UiField
	VerticalPanel					vp;
	@UiField
	Label							id;
	@UiField
	TextBox							idBox;
	@UiField
	Label							pw;
	@UiField
	Label							info;
	@UiField
	PasswordTextBox					passwordTextBox;
	@UiField
	Button							b;
	@UiField
	HorizontalPanel					hpID;
	@UiField
	HorizontalPanel					hpko;
	@UiField
	Image							patient;
	@UiField
	CaptionPanel					capPanel;

	/* RPC callback service object */
	private ILoginViewService		serv;

	/* validity booleans for input boxes */
	private boolean					nameValid	= false;
	private boolean					passValid	= false;

	/* the values from the boxes */
	private int						boxId;

	interface LoginUiBinder extends UiBinder<Widget, Login> { /**/}

	public Login(ILoginViewService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		patient.getElement().getStyle().setVisibility(Visibility.HIDDEN);
		capPanel.setCaptionText(DK.toDk(StringToolLib.toCamelCase(GWT.getModuleName())) + " login.");
		info.setText(INT);
		idBox.setTitle("Login ID.");
		idBox.setTabIndex(1);
		passwordTextBox.setTitle("Kodeord.");
		passwordTextBox.setTabIndex(2);
		b.setTabIndex(3);
		idBox.setFocus(true);
		b.setEnabled(false);
	}

	/* ******************************************************
	 * /* **************** UI Binding ************************** /*
	 * ****************************************************
	 */

	public TextBox getIdBox() {
		return idBox;
	}

	@SuppressWarnings("unused")
	@UiHandler("b")
	void onBClick(ClickEvent event) {
		patient.getElement().getStyle().setVisibility(Visibility.VISIBLE);
		info.setText("Kontakter server.. Venter p" + DK.aa + " svar");
		boxId = Integer.parseInt(idBox.getText());
		serv.passwordValidation(new OperatoerDTO(Integer.parseInt(idBox.getText()), passwordTextBox.getText()), new loginCallBack());
	}
	@SuppressWarnings("unused")
	@UiHandler("idBox")
	void onIdboxKeyUp(KeyUpEvent event) {
		/* for alpha numeric numbers */
		if (idBox.getText().trim().length() > 0) nameValid = idBox.getText().matches("^[0-9]+$");
		else nameValid = false;
		UIAbstract.setValidity(idBox, nameValid);
		checkFormValid();
	}
	@UiHandler("idBox")
	void onIdboxKeyDown(KeyDownEvent event) {
		if (passValid && nameValid && event.getNativeKeyCode() == 13) b.click();
	}
	@SuppressWarnings("unused")
	@UiHandler("passwordTextBox")
	void onPasswordTextBoxKeyUp(KeyUpEvent event) {
		passValid = checkPassValid();
		UIAbstract.setValidity(passwordTextBox, passValid);
		idBox.fireEvent(new KeyUpEvent() { /**/});
		checkFormValid();
	}
	@UiHandler("passwordTextBox")
	void onPasswordTextBoxKeyDown(KeyDownEvent event) {
		onIdboxKeyDown(event);
	}

	/* ******************************************************
	 * /* ************** HELPER METHODS ************************ /*
	 * ****************************************************
	 */

	/** Set if the login button should be enabled or not.<br>
	 * This is based on the validity of the ID Box and PasswordBox. */
	private void checkFormValid() {
		b.setEnabled(nameValid && passValid);
	}

	/** Verify the PasswordBox validity for this widget.
	 * 
	 * @return true if the input is "valid", otherwise false. */
	private boolean checkPassValid() {
		return passwordTextBox.getText().trim().length() > 0;
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	/** General abstraction of onFailure().
	 * 
	 * @author Rudy Alex Kohn [s133235]
	 * @param <T> */
	private abstract class AbstractCallBack<T> implements AsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			if (caught.getMessage().equals("0")) info.setText("404");
			else info.setText(DK.toDk(caught.getMessage()));
			UIAbstract.setValidity(getIdBox(), false);
			UIAbstract.setValidity(passwordTextBox, false);
			passwordTextBox.setText(null);
			patient.getElement().getStyle().setVisibility(Visibility.HIDDEN);
		}
	}

	/** Callback class for retrieving password hash
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class operatorCallback extends AbstractCallBack<OperatoerDTO> {

		@Override
		public void onSuccess(OperatoerDTO object) {
			LoginStatus.attemptingLogin = object;
			LoginStatus.level = object.getLevel();
			LoginStatus.userID = boxId;
			LoginStatus.userName = object.getName();
			LoginStatus.loggedIn = true;
			RootPanel.get("nav").clear();
			RootPanel.get("section").clear();
			patient.getElement().getStyle().setVisibility(Visibility.HIDDEN);
			new MainView().run();
			return;
		}
	}

	/** Callback for retrieving OperatorDTO from server
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class loginCallBack extends AbstractCallBack<Boolean> {

		@Override
		public void onSuccess(Boolean bool) {
			if (bool) {
				/*
				 * looks like it was fine! let's fetch the operator dto for the
				 * user
				 */
				serv.getOperatoer(Integer.parseInt(getIdBox().getText()), new operatorCallback());
			} else onFailure(new Exception(FEJL));
		}
	}

}
