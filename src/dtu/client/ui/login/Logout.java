
package dtu.client.ui.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.controller.MainView;
import dtu.shared.LoginStatus;


/** Simple logout message widget.<br>
 * Does not utilise any interfaces!
 * 
 * @author Rudy Alex Kohn [s133235] */
public class Logout extends Composite {

	private static LogoutUiBinder	uiBinder	= GWT.create(LogoutUiBinder.class);

	interface LogoutUiBinder extends UiBinder<Widget, Logout> { /* */}

	public Logout() {
		initWidget(uiBinder.createAndBindUi(this));
		RootPanel.get("nav").clear();
		RootPanel.get("section").clear();
		LoginStatus.clear();
		Window.alert("Du er nu logget af.");
		new MainView().run();
	}
}
