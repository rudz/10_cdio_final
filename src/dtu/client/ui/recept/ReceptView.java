
package dtu.client.ui.recept;

import java.util.ArrayList;
import java.util.HashMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.farmaceut.IReceptService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.misc.DK;
import dtu.shared.validate.FieldVerifier;


public class ReceptView extends Composite {

	private static ReceptViewUiBinder	uiBinder			= GWT.create(ReceptViewUiBinder.class);
	@UiField
	FlexTable							t;															// NO_UCD
	@UiField
	FlexTable							v;
	@UiField
	Label								inf;
	@UiField
	Button								changeBtn;
	@UiField
	Button								okBtn;
	@UiField
	Button								cancelBtn;
	@UiField
	Label								status;
	@UiField
	Label								lblTitle;
	// (use
	// private)

	private IReceptService				serv;
	private Anchor						details;
	@SuppressWarnings("unused")
	private Anchor						detailsEnd;												// used
	@SuppressWarnings("unused")
	private Anchor						previousDetailsEnd	= null;								// for
	@SuppressWarnings("unused")
	private FlexTable					ts;														// closing
	// details

	private IntegerBox					sequenceBox;
	private DoubleBox					tolrBox, nomNetBox;

	@SuppressWarnings("unused")
	private HashMap<Integer, IntegerBox>	idBoxes, sequenceBoxes;
	private HashMap<Integer, DoubleBox>		tolrBoxes, nomNetBoxes;

	private ArrayList<ReceptDTO>			ReceptList;
	private ArrayList<ReceptKomponenterDTO>	CurrentKomponenterDTO;

	private int								detailEventRowIndex;
	@SuppressWarnings("unused")
	private int								adjustEventRowIndex;

	private int								currentReceptId;
	private String							currentReceptNavn;

	private boolean							sekvOk	= true, tolrOk = true, nomNetOk = true;

	interface ReceptViewUiBinder extends UiBinder<Widget, ReceptView> { /* */}

	public ReceptView(IReceptService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));

		// initail setup
		okBtn.setVisible(false);
		okBtn.setEnabled(false);

		cancelBtn.setVisible(false);
		cancelBtn.setEnabled(false);

		changeBtn.setVisible(false);

		t.getFlexCellFormatter().setWidth(0, 0, "20px");
		t.getFlexCellFormatter().setWidth(0, 1, "50px");

		UIAbstract.setFlexTableHeaders(t, "Recept ID", "Recept Navn");

		lblTitle.setText("H" + DK.aa + "ndtér Recepter");

		serv.getReceptList(new AsyncCallback<ArrayList<ReceptDTO>>() {

			@Override
			public void onSuccess(ArrayList<ReceptDTO> result) {
				ReceptList = result;

				for (int i = 0; i < ReceptList.size(); i++) {
					t.setText(i + 1, 0, String.valueOf((ReceptList.get(i).getReceptId())));
					details = UIAbstract.generateAnchor(ReceptList.get(i).getReceptNavn(), "Vis receptinformation for "
							+ ReceptList.get(i).getReceptNavn(), new detaljerClickHandler());
					t.setWidget(i + 1, 1, details);
					UIAbstract.setRowStyle(i + 1, t, true);
				}
			}
			@Override
			public void onFailure(Throwable arg0) {
				status.setText(arg0.getMessage());
			}
		});

	}

	private class detaljerClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {

			v.removeAllRows();
			resetButtons();

			detailEventRowIndex = t.getCellForEvent(event).getRowIndex();

			currentReceptId = Integer.parseInt(t.getText(detailEventRowIndex, 0));
			currentReceptNavn = t.getText(detailEventRowIndex, 1);

			buildReceptKomponentView(v, currentReceptId, currentReceptNavn);
		}
	}

	private void buildReceptKomponentView(FlexTable ft, int receptId, String receptNavn) {
		final FlexTable sft = ft;

		sft.removeAllRows();

		String selectedRecept = "Recept ID: " + String.valueOf(receptId) + " Recept: " + receptNavn;
		inf.setText(selectedRecept);

		status.setText("Henter receptkomponenter");

		serv.getReceptKomponenter(receptId, new AsyncCallback<ArrayList<ReceptKomponenterDTO>>() {

			@Override
			public void onFailure(Throwable arg0) {
				status.setText("error");
			}

			@Override
			public void onSuccess(ArrayList<ReceptKomponenterDTO> arg0) {
				status.setText("Recept komponenter hentet");
				CurrentKomponenterDTO = arg0;

				ArrayList<ReceptKomponenterDTO> list;
				list = CurrentKomponenterDTO;
				int curRow = 2;

				sft.getRowFormatter().setStyleName(0, "FlexTable-Header");
				sft.setText(1, 1, "#");
				sft.getFlexCellFormatter().setWidth(2, 1, "30px");
				sft.setText(1, 2, "Raavare navn");
				sft.setText(1, 3, "Raavare Id");
				sft.setText(1, 4, "nom_netto");
				sft.setText(1, 5, "tolerance");

				for (int i = 0; i < list.size(); i++) {

					sft.setText(curRow + i, 1, String.valueOf(list.get(i).getSekvens()));
					sft.setText(curRow + i, 2, list.get(i).getNavn());
					sft.setText(curRow + i, 3, String.valueOf(list.get(i).getRaavare_id()));
					sft.setText(curRow + i, 4, String.valueOf(list.get(i).getNom_Netto()));
					sft.setText(curRow + i, 5, String.valueOf(list.get(i).getTolerance()));
					setRowStyle(curRow + i, v);
				}

			}
		});

	}

	private void setRowStyle(int row, FlexTable t) {
		if (row % 2 != 0) t.getRowFormatter().setStyleName(row, "detail-cell1");
		else t.getRowFormatter().setStyleName(row, "detail-cell2");

		t.getCellFormatter().addStyleName(row, 0, "empty");
	}

	private void buildChangeMenu(FlexTable ft) {
		int dataRows = ft.getRowCount() - 1;
		sequenceBoxes = new HashMap<>();
		tolrBoxes = new HashMap<>();
		nomNetBoxes = new HashMap<>();

		for (int i = 2; i <= dataRows; i++) {

			int Sekvens = Integer.parseInt(ft.getText(i, 1));

			@SuppressWarnings("unused")
			String navn = ft.getText(i, 2);

			String rv_id = ft.getText(i, 3);
			@SuppressWarnings("unused")
			int rv_idInt = Integer.parseInt(rv_id);

			double nomNet = Double.parseDouble(ft.getText(i, 4));

			double tolr = Double.parseDouble(ft.getText(i, 5));

			sequenceBox = new IntegerBox();
			sequenceBox.setWidth("30px");
			sequenceBox.setValue(Sekvens);
			sequenceBox.addKeyUpHandler(new SequenceBoxKeyUpHandler(sequenceBox));
			sequenceBoxes.put(i, sequenceBox);
			ft.setWidget(i, 1, sequenceBox);

			nomNetBox = new DoubleBox();
			nomNetBox.setWidth("30px");
			nomNetBox.setValue(nomNet);
			nomNetBox.addKeyUpHandler(new NomNetBoxKeyUpHandler(nomNetBox));
			nomNetBoxes.put(i, nomNetBox);
			ft.setWidget(i, 4, nomNetBox);

			tolrBox = new DoubleBox();
			tolrBox.setWidth("30px");
			tolrBox.setValue(tolr);
			tolrBox.addKeyUpHandler(new ToleranceBoxKeyUpHandler(tolrBox));
			tolrBoxes.put(i, tolrBox);
			ft.setWidget(i, 5, tolrBox);

			setChangeButtons();
		}

	}

	// -----------------------------------gazzillion keyUpHandlers

	private class SequenceBoxKeyUpHandler implements KeyUpHandler {

		private IntegerBox	box;

		public SequenceBoxKeyUpHandler(IntegerBox box) {
			this.box = box;
		}

		@Override
		public void onKeyUp(KeyUpEvent arg0) {
			if (FieldVerifier.isValidSequence(sequenceBoxes, box) && FieldVerifier.isValidSequenceNumber(sequenceBoxes, box)) {
				sekvOk = true;
				this.box.removeStyleName("gwt-TextBox-invalidEntry");
				allGood();
			} else {
				sekvOk = false;
				allGood();
			}
		}
	}

	private class ToleranceBoxKeyUpHandler implements KeyUpHandler {

		private DoubleBox	box;

		public ToleranceBoxKeyUpHandler(DoubleBox box) {
			this.box = box;
		}

		@Override
		public void onKeyUp(KeyUpEvent arg0) {

			if (FieldVerifier.isValidTolerance(this.box.getValue())) {
				this.box.removeStyleName("gwt-TextBox-invalidEntry");
				tolrOk = true;
				for (int i = 2; i <= tolrBoxes.size(); i++) {
					if (!FieldVerifier.isValidTolerance(tolrBoxes.get(i).getValue())) tolrOk = false;
				}
				allGood();
			} else {
				this.box.setStyleName("gwt-TextBox-invalidEntry");
				tolrOk = false;
				allGood();
			}
		}
	}

	private class NomNetBoxKeyUpHandler implements KeyUpHandler {

		private DoubleBox	box;

		public NomNetBoxKeyUpHandler(DoubleBox box) {
			this.box = box;
		}

		@Override
		public void onKeyUp(KeyUpEvent arg0) {

			if (FieldVerifier.isValidNomNetto(box.getValue())) {
				this.box.removeStyleName("gwt-TextBox-invalidEntry");
				nomNetOk = true;
				for (int i = 2; i <= nomNetBoxes.size(); i++) {
					if (!FieldVerifier.isValidNomNetto(nomNetBoxes.get(i).getValue())) nomNetOk = false;
				}
				allGood();
			} else {
				this.box.setStyleName("gwt-TextBox-invalidEntry");
				nomNetOk = false;
				allGood();
			}

		}
	}

	// ---------------------------------Ui Handlers
	// -------------------------------------

	@UiHandler("changeBtn")
	void onChangeBtnClick(@SuppressWarnings("unused") ClickEvent event) {
		buildChangeMenu(v);
	}

	@UiHandler("cancelBtn")
	void onCancelBtnClick(@SuppressWarnings("unused") ClickEvent event) {
		v.removeAllRows();
		buildReceptKomponentView(v, currentReceptId, currentReceptNavn);
		resetButtons();
	}

	@UiHandler("okBtn")
	void onOkBtnClick(@SuppressWarnings("unused") ClickEvent event) {

		ArrayList<ReceptKompDTO> list = new ArrayList<>();
		int dataRows = v.getRowCount() - 1;

		if (dataRows == 2) {

			list.add(new ReceptKompDTO(currentReceptId, Integer.parseInt(v.getText(2, 3)), nomNetBoxes.get(2).getValue(),
					tolrBoxes.get(2).getValue(), sequenceBoxes.get(2).getValue()));
		} else {
			for (int i = 2; i <= dataRows; i++) {

				list.add(new ReceptKompDTO(currentReceptId, Integer.parseInt(v.getText(i, 3)), nomNetBoxes.get(i).getValue(), tolrBoxes.get(i)
						.getValue(), sequenceBoxes.get(i).getValue()));

			}
		}

		serv.updateReceptKomponenter(list, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable arg0) {
				status.setText(arg0.getMessage());
			}

			@Override
			public void onSuccess(Void arg0) {
				status.setText("Opskrift modificeret");
				v.removeAllRows();
				buildReceptKomponentView(v, currentReceptId, currentReceptNavn);
				resetButtons();

			}
		});

	}

	private void resetButtons() {
		cancelBtn.setVisible(false);
		cancelBtn.setEnabled(false);
		okBtn.setVisible(false);
		okBtn.setEnabled(false);
		changeBtn.setVisible(true);
		changeBtn.setEnabled(true);
	}
	private void setChangeButtons() {
		changeBtn.setVisible(false);
		changeBtn.setEnabled(false);

		okBtn.setVisible(true);
		okBtn.setEnabled(false);

		cancelBtn.setVisible(true);
		cancelBtn.setEnabled(true);
	}

	private void allGood() {
		if (sekvOk && tolrOk && nomNetOk) {
			okBtn.setEnabled(true);
		} else okBtn.setEnabled(false);
	}
}
