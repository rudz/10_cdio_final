
package dtu.client.ui.recept;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.misc.DK;


/** General abstraction of recept UI widgets.<br>
 * Contains helper functionality and generic classes.
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class ReceptAbstract extends Composite {

	/* the total list of 'raavare' */
	protected ArrayList<RaavareDTO>			raavareList	= new ArrayList<>();

	/* 'raavare' map */
	protected HashMap<Integer, RaavareDTO>	raavareMap	= new HashMap<>();

	/* the total 'recept' list */
	protected ArrayList<ReceptDTO>			receptList	= new ArrayList<>();

	/* mirror of 'recept' list to find the lowest ID not used */
	protected HashMap<Integer, ReceptDTO>	recepts		= new HashMap<>(100, 1.0f);

	/* the current used 'raavare' in recept to be added */
	protected HashSet<Integer>				usedRaavare	= new HashSet<>();

	/* raavare complete list in the recept */
	// protected ArrayList<RaavareRecept> raavarer = new ArrayList<>();

	/* how many is currently added to current recept? */
	protected int							raavareCount;

	/** data transfer boolean, true if data transfer is in progress, otherwise
	 * false. */
	protected static volatile boolean		dataTransfer;

	/** Initialises the FlexTable.
	 * 
	 * @param f
	 *            : The flextable to initialise. */
	protected static void initFlexTable(FlexTable ft) {
		ft.clear();
		ft.getFlexCellFormatter().setWidth(0, 0, "70px");
		ft.getFlexCellFormatter().setWidth(0, 1, "100px");
		ft.getFlexCellFormatter().setWidth(0, 2, "100px");
		ft.getRowFormatter().addStyleName(0, "FlexTable-Header");
		ft.setText(0, 0, DK.toDk("Råvare ID"));
		ft.setText(0, 1, "Netto (Kg)");
		ft.setText(0, 2, "Tolerance (%)");
	}
	/** Simple function to set any given img widget to visible or hidden.
	 * 
	 * @param img
	 *            : The image to set
	 * @param val
	 *            : The visibility the img should have */
	protected static void setImgVis(Image img, boolean val) {
		if (val) img.getElement().getStyle().setVisibility(Visibility.VISIBLE);
		else img.getElement().getStyle().setVisibility(Visibility.HIDDEN);
	}
	/** Verify that parsed boolean values are all true.
	 * 
	 * @param values
	 *            : The value(s)
	 * @return true if all are true, else false. */
	protected static boolean isValid(boolean... values) {
		for (boolean b : values)
			if (!b) return false;
		return true;
	}
	/** Organises the recepts and finds lowest and highes ID in the process. */
	protected void organiseRecepts() {
		int idNow = 0;
		recepts.clear();
		for (ReceptDTO r : receptList) {
			idNow = r.getReceptId();
			recepts.put(idNow, r);
		}
	}
	/** Adds the given 'raavare' to current map */
	protected void addRaavareToMap(RaavareDTO raavare) {
		raavareMap.put(raavare.getRaavareId(), raavare);
	}
	/** Returns the RaavareDTO object contained in the map with a given key.
	 * 
	 * @param id
	 *            : The key that identifies the RaavareDTO.
	 * @return The RaavareDTO if it exists, otherwise null. */
	protected RaavareDTO getRaavare(int id) {
		if (raavareMap.containsKey(id)) return raavareMap.get(id);
		return null;
	}
	/** Locates the lowest ID from the retrieved Recept list.<br>
	 * 
	 * @return the lowest number which is not used. */
	protected int getLowestFreeReceptID() {
		int listSize = receptList.size(), currentFree, pos;
		int low, high;
		if (listSize < receptList.get(listSize - 1).getReceptId()) {
			low = 0;
			high = listSize;
			while (true) {
				pos = (low + high) / 2;
				if (low + 1 == high) {
					currentFree = receptList.get(pos).getReceptId() + 1;
					break;
				}
				if (pos + 1 < receptList.get(pos).getReceptId()) {
					high = pos;
					continue;
				} else if (pos + 1 == receptList.get(pos).getReceptId()) {
					low = pos;
					continue;
				} else {
					currentFree = receptList.get(pos).getReceptId() + 1;
					break;
				}
			}
		} else {
			currentFree = receptList.get(listSize - 1).getReceptId() + 1;
		}
		return currentFree;
	}
	/** Removes a 'raavare' from current recept set.
	 * 
	 * @param raavareID
	 *            : The 'raavare' ID to remove.
	 * @return true if removed, else false. */
	protected boolean removeRaavareInRecept(int raavareID) {
		return usedRaavare.remove(raavareID);
	}
	/** Adds a 'raavare' to current recept set.
	 * 
	 * @param raavareID
	 *            : The 'raavare' ID to be added.
	 * @return true if added, else false. */
	protected boolean addRaavareInRecept(int raavareID, double netto, double tolerance, FlexTable ft) {
		boolean added = usedRaavare.add(raavareID);
		if (added) {
			ft.setText(raavareCount + 1, 0, Integer.toString(raavareID));
			ft.setText(raavareCount + 1, 1, Double.toString(netto));
			ft.setText(raavareCount + 1, 2, Double.toString(tolerance));
			UIAbstract.setRowStyle(raavareCount++ + 1, ft, true);
		}
		return added;
	}
	/** set label text with danish letter correction */
	protected synchronized static void setLabelText(Label l, String text) {
		l.setText(DK.toDk(text));
	}
}
