
package dtu.client.ui.recept;

import java.util.ArrayList;
import java.util.Iterator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.farmaceut.IReceptService;
import dtu.client.ui.generic.MsgPopup;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.misc.DK;
import dtu.shared.validate.FieldVerifier;
import com.google.gwt.user.client.ui.CaptionPanel;


/** Widget class for creating new 'recept'.<br>
 * 
 * @author Rudy Alex Kohn [s133235] */
public class ReceptAdd extends ReceptAbstract {

	private static ReceptAddUiBinder	uiBinder	= GWT.create(ReceptAddUiBinder.class);
	@UiField
	TextBox								boxName;
	@UiField
	Button								btnAddGood;
	@UiField
	IntegerBox							boxId;
	@UiField
	ListBox								cmbRaavare;
	@UiField
	DoubleBox							boxNomNetto;
	@UiField
	DoubleBox							boxTolerance;
	@UiField
	FlexTable							t;
	@UiField
	Button								btnOk;
	@UiField
	Image								imgProgress;
	@UiField
	Label								lblInfo;
	@UiField
	Label								lblTitle;
	@UiField
	CaptionPanel						capRecept;
	@UiField
	CaptionPanel						capRaavare;
	@UiField
	Label								lblRaavareNavn;

	private boolean						validTolerance;
	private boolean						validNetto;
	private boolean						validName;

	/* local valid ID */
	private boolean						validID;

	/* remote ok receptID */
	private volatile boolean			validIDremote;

	/* local receptID */
	private int							receptID;

	/* data layer reference */
	private IReceptService				serv;

	interface ReceptAddUiBinder extends UiBinder<Widget, ReceptAdd> { /* */}

	public ReceptAdd(IReceptService service) {
		initWidget(uiBinder.createAndBindUi(this));
		dataTransfer = true;
		setImgVis(imgProgress, dataTransfer);
		setLabelText(lblInfo, "Indhenter data.");
		serv = service;
		raavareCount = 0;

		btnAddGood.setText("Tilf" + DK.oe + "j r" + DK.aa + "vare");
		btnAddGood.setEnabled(false);
		capRaavare.setCaptionText(DK.toDk(capRaavare.getCaptionText()));
		lblTitle.setText("Tilf" + DK.oe + "j Recept");
		lblRaavareNavn.setText("R" + DK.aa + "vare");

		btnOk.setEnabled(false);

		/* init flex table dimensions */
		initFlexTable(t);

		serv.getReceptList(new getReceptList());
		serv.getRaavareList(new getRaavareList());
	}
	@UiHandler("boxId")
	void onBoxIdKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		// validID = (boxId.getText().trim().length() > 0 &&
		// boxId.getText().matches("^[0-9]+$"));
		validID = boxId.getValue() > 0;
		receptID = validID ? boxId.getValue() : -1;
		UIAbstract.setValidity(boxId, validID);
		serv.isRecept(boxId.getValue(), new isRecept());
	}
	@UiHandler("boxName")
	void onBoxNameClick(@SuppressWarnings("unused") ClickEvent event) {
		MsgPopup pop = new MsgPopup("Navnet på recepten med ID " + boxId.getText());
		pop.setPopupPosition(boxName.getAbsoluteLeft() + 100, boxName.getAbsoluteTop() + 100);
		pop.show();
	}
	@UiHandler("boxName")
	void onBoxNameKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		validName = (boxName.getText().trim().length() > 0);
		UIAbstract.setValidity(boxName, validName);
		serv.isRecept(boxId.getValue(), new isRecept());
	}
	@UiHandler("boxName")
	void onBoxNameKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) enterPressed();
	}
	@UiHandler("boxNomNetto")
	void onBoxNomNettoClick(@SuppressWarnings("unused") ClickEvent event) {
		MsgPopup pop = new MsgPopup("Nominal netto angivet i Kg (0.1 - 20.0).");
		pop.setPopupPosition(boxNomNetto.getAbsoluteLeft() + 100, boxNomNetto.getAbsoluteTop() + 100);
		pop.show();
	}
	@UiHandler("boxNomNetto")
	void onBoxNomNettoKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) enterPressed();
	}
	@UiHandler("boxNomNetto")
	void onBoxNomNettoKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		validNetto = FieldVerifier.isValidReceptNetto(boxNomNetto.getValue());
		UIAbstract.setValidity(boxNomNetto, validNetto);
		serv.isRecept(boxId.getValue(), new isRecept());
	}
	@UiHandler("boxTolerance")
	void onBoxToleranceClick(@SuppressWarnings("unused") ClickEvent event) {
		MsgPopup pop = new MsgPopup("Tolerance i % (0.1%-10.0%).");
		pop.setPopupPosition(boxTolerance.getAbsoluteLeft() + 100, boxTolerance.getAbsoluteTop() + 100);
		pop.show();
	}
	@UiHandler("boxTolerance")
	void onBoxToleranceKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) enterPressed();
	}
	@UiHandler("boxTolerance")
	void onBoxToleranceKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		validTolerance = FieldVerifier.isValidTolerance(boxTolerance.getValue());
		UIAbstract.setValidity(boxTolerance, validTolerance);
		serv.isRecept(boxId.getValue(), new isRecept());
	}
	@UiHandler("btnAddGood")
	void onBtnAddGoodClick(@SuppressWarnings("unused") ClickEvent event) {
		// addRaavareInRecept(cmbRaavare.getSelectedIndex() + 1,
		// boxNomNetto.getValue(), boxTolerance.getValue(), t);
		addRaavareInRecept(raavareList.get(cmbRaavare.getSelectedIndex()).getRaavareId(), boxNomNetto.getValue(), boxTolerance.getValue(), t);
		addRaavareToMap(raavareList.get(cmbRaavare.getSelectedIndex()));
		isValidInput();
	}
	@UiHandler("btnOk")
	void onBtnOkClick(@SuppressWarnings("unused") ClickEvent event) {
		receptID = boxId.getValue();
		setLabelText(lblInfo, "Opretter recept.");
		dataTransfer = true;
		setImgVis(imgProgress, dataTransfer);
		// create recept entry first.
		serv.createRecept(new ReceptDTO(receptID, boxName.getText()), new createReceptClass());
	}
	@UiHandler("cmbRaavare")
	void onCmbRaavareKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) enterPressed();
	}

	/** Does 'KeyUpEvent' on input boxes. */
	private void fireKeyUps() {
		boxId.fireEvent(new KeyUpEvent() { /**/});
		boxName.fireEvent(new KeyUpEvent() { /**/});
		boxNomNetto.fireEvent(new KeyUpEvent() { /**/});
		boxTolerance.fireEvent(new KeyUpEvent() { /**/});
	}

	/** General functionality for when pressing enter key on a textbox. */
	private void enterPressed() {
		fireKeyUps();
		if (isValidInput()) btnAddGood.click();
	}

	private synchronized boolean isValidInput() {
		boolean isVal = isValid(validID, validNetto, validTolerance, validName);
		btnAddGood.setEnabled(isVal);
		btnOk.setEnabled(t.getCellCount(1) > 0);
		return isVal;
	}

	/* ******************************************************
	 * ************** CALLBACK CLASSES **********************
	 * ****************************************************
	 */

	/** General abstraction of onFailure().
	 * 
	 * @author Rudy Alex Kohn [s133235]
	 * @param <T> */
	abstract class receptCallbackAbstract<T> implements AsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			dataTransfer = false;
			setImgVis(imgProgress, dataTransfer);
			setLabelText(lblInfo, "Fejl ved overførsel.");
			Window.alert(caught.toString());
		}
	}

	class createReceptClass extends receptCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean arg0) {
			dataTransfer = true;
			setImgVis(imgProgress, dataTransfer);
			setLabelText(lblInfo, "Opretter ReceptKomponenter.");
			Iterator<Integer> raaIte = usedRaavare.iterator();			
			double netto = boxNomNetto.getValue();
			double tolerance = boxTolerance.getValue();
			ArrayList<ReceptKompDTO> list = new ArrayList<>();
			int i = 1;
			while (raaIte.hasNext()) {
				netto = Double.valueOf(t.getText(i, 1));
				tolerance = Double.valueOf(t.getText(i, 2));
				list.add(new ReceptKompDTO(receptID, getRaavare(raaIte.next()).getRaavareId(), netto, tolerance, i++));
			}
			serv.createReceptKomponentList(list, new createReceptKomponent());
		}
	}

	class createReceptKomponent extends receptCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean res) {
			setLabelText(lblInfo, res ? "Overførsel ok." : "Overførsel fejlede.");

			if (res) clearInput();
			dataTransfer = false;
			setImgVis(imgProgress, dataTransfer);

			// TESTING
			serv.getReceptList(new getReceptList());
		}

		private void clearInput() {
			boxId.setText(null);
			boxName.setText(null);
			boxNomNetto.setText(null);
			boxTolerance.setText(null);
			initFlexTable(t);
		}
	}

	/** Callback class to get all 'recept' as list.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	class getReceptList extends receptCallbackAbstract<ArrayList<ReceptDTO>> {

		@Override
		public void onSuccess(ArrayList<ReceptDTO> arg0) {
			receptList = arg0;
			organiseRecepts();
			boxId.setText(Integer.toString(getLowestFreeReceptID()));
			fireKeyUps();
			dataTransfer = false;
			setImgVis(imgProgress, dataTransfer);
			setLabelText(lblInfo, "Recepter hentet ok.");
		}
	}

	/** Callback class to get all 'raavare' as list.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	class getRaavareList extends receptCallbackAbstract<ArrayList<RaavareDTO>> {

		@Override
		public void onSuccess(ArrayList<RaavareDTO> result) {
			raavareList = result;
			for (RaavareDTO rv : result)
				cmbRaavare.addItem("[" + Integer.toString(rv.getRaavareId()) + "] " + rv.getRaavareNavn());
			dataTransfer = false;
			setImgVis(imgProgress, dataTransfer);
			setLabelText(lblInfo, "Råvarer hentet ok.");
		}
	}

	class isRecept extends receptCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean arg0) {
			validIDremote = !arg0;
			UIAbstract.setValidity(boxId, validIDremote);
			isValidInput();
		}
	}
}
