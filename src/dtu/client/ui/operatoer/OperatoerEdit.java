
package dtu.client.ui.operatoer;

import java.util.ArrayList;
import java.util.HashMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.admin.IEditService;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.OperatorLevel;
import dtu.shared.validate.FieldVerifier;
import com.google.gwt.user.client.ui.Label;


public class OperatoerEdit extends OperatoerAbstract {

	private static OperatoerEditUiBinder	uiBinder	= GWT.create(OperatoerEditUiBinder.class);
	@UiField
	FlexTable								t;
	@UiField
	Label									lblTitle;

	interface OperatoerEditUiBinder extends UiBinder<Widget, OperatoerEdit> { /**/}

	/* row index for current action being performed */
	private int							eventRowIndex;

	/* id currently being worked on */
	private int							currentID;

	private final int					LEVEL_INDEX		= 4;
	private final int					EDIT_INDEX		= 5;
	private final int					RESET_INDEX		= 7;
	private final int					OK_INDEX		= 5;
	private final int					CANCEL_INDEX	= 6;

	/* maps for editing. */
	private HashMap<Integer, TextBox>	txtBoxes		= new HashMap<>(4, 1.0f);
	private HashMap<Integer, String>	tmpStrings		= new HashMap<>(5, 1.0f);

	/* combo box for operator level edit */
	private ListBox						lvlListBox		= new ListBox();
	private int							levelTemp;

	/* validation fields to keep track of in-/valid inputs */
	private boolean						cprValid		= true;
	private boolean						nameValid		= true;
	private boolean						lvlValid		= true;

	/* map key values */
	private int							name			= 2;
	private int							ini				= 3;
	private int							cpr_nr			= 4;
	private int							lvl				= 5;

	/* Anchors */
	private Anchor						ok;
	private Anchor						cancel;
	private Anchor						edit;
	private Anchor						reset;
	private Anchor						previousCancel	= null;

	/* temporary strings while editing a operator */
	private String						pw;
	// private String tmpName, tmpINI, tmpCPR, tmpLVL;

	/* remote procedure call service interface reference */
	private IEditService				serv;

	/* not-visible operator ids */
	private int[]						ID;

	public OperatoerEdit(IEditService service) {
		this.serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Ret Brugere");
		UIAbstract.setFlexTableHeaders(t, "ID", "Navn", "Initialer", "CPR", "Brugerniveau");
		setFlexTableHeaderSizes(t);
		String s;
		for (int i = 0; i <= LoginStatus.level; i++) {
			s = OperatorLevel.olText(i);
			if (!"".equals(s) && s != null) lvlListBox.addItem(s);
		}
		service.getOperatoerList(new OperatorGetListCallback());
	}

	/* helper methods */
	private void validateForm() {
		if (cprValid & nameValid & lvlValid) t.setWidget(eventRowIndex, OK_INDEX, ok);
		else t.setText(eventRowIndex, OK_INDEX, "ok");
	}

	/* *****************************************************
	 * /* ************** HANDLER CLASSES ********************** /*
	 * ****************************************************
	 */

	/** <p>
	 * Click handler class for Reset Password anchor.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class ResetPasswordHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			eventRowIndex = t.getCellForEvent(event).getRowIndex();
			currentID = ID[eventRowIndex - 1];
			serv.genPassword(currentID, new OperatorGeneratePasswordCallback());
		}
	}

	/** <p>
	 * ClickHandler class for 'edit' anchor.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class EditHandler extends UIAbstract implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			/*
			 * detect if a previous edit action is in progress, if so, force
			 * cancel click handler to fire
			 */
			if (previousCancel != null) previousCancel.fireEvent(new ClickEvent() { /**/});

			/* mark current row of pressed anchor */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			levelTemp = OperatorLevel.olInt(t.getText(eventRowIndex, LEVEL_INDEX));

			/* configure text boxes */
			for (int i = 1; i < 4; i++) {
				txtBoxes.get(i + 1).setText(t.getText(eventRowIndex, i));
				t.setWidget(eventRowIndex, i, txtBoxes.get(i + 1));
			}
			t.setWidget(eventRowIndex, LEVEL_INDEX, lvlListBox);

			/* ****************** */
			/* start editing here */
			/* ****************** */

			/* set name box focus */
			txtBoxes.get(name).setFocus(true);

			/* get edit anchor reference for cancel operation */
			edit = (Anchor) event.getSource();

			/* store the values in the case cancel will be pressed */
			for (Integer i : txtBoxes.keySet())
				tmpStrings.put(i, txtBoxes.get(i).getText());

			/* set the index for list box */
			lvlListBox.setSelectedIndex(levelTemp);

			/* set up anchors and their respective click handlers */
			ok = UIAbstract.generateAnchor("ok", "Foretag ændringer for denne bruger.", new OkClickHandler());
			reset = UIAbstract.generateAnchor("reset pw", "Opret nyt kodeord til denne bruger.", new ResetPasswordHandler());
			cancel = UIAbstract.generateAnchor("fortryd", "Fortryd eventuelle ændringer foretager.", new CancelClickHandler());
			previousCancel = cancel;

			/* assign event handler classes to the text boxes */
			txtBoxes.get(cpr_nr).addKeyUpHandler(new CPRKeyUpHandler());
			txtBoxes.get(name).addKeyUpHandler(new NameKeyUpHandler());

			/* force KeyUpEvent to make sure they are validated */
			for (Integer i : txtBoxes.keySet())
				txtBoxes.get(i).fireEvent(new KeyUpEvent() { /**/});

			/* drop those anchors onto the table */
			t.setWidget(eventRowIndex, OK_INDEX, ok);
			t.setWidget(eventRowIndex, CANCEL_INDEX, cancel);
			t.setWidget(eventRowIndex, RESET_INDEX, reset);
			setFlexTableHeaderSizes(t);
		}

		/** <p>
		 * ClickHandler for 'ok' Anchor.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class OkClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				// TODO : FIX ALIGNMENT!

				currentID = ID[eventRowIndex - 1];
				/* since ok was clicked, modify the table to show the new values */
				for (Integer i : txtBoxes.keySet())
					t.setText(eventRowIndex, i - 1, txtBoxes.get(i).getText());

				/* for the user level */
				t.setText(eventRowIndex, LEVEL_INDEX, lvlListBox.getItemText(lvlListBox.getSelectedIndex()));
				/* update the new user information in database */
				serv.updateOperatoer(new OperatoerDTO(currentID, txtBoxes.get(name).getText(), txtBoxes.get(ini).getText(), txtBoxes.get(cpr_nr)
						.getText(), OperatorLevel.olInt(t.getText(eventRowIndex, LEVEL_INDEX))), new OperatorUpdateCallback());

				// restore new state
				t.setWidget(eventRowIndex, EDIT_INDEX, edit);
				t.clearCell(eventRowIndex, 6);
				t.clearCell(eventRowIndex, 7);
				previousCancel = null;
				cancel = null;
				setFlexTableHeaderSizes(t);
			}
		}

		/** <p>
		 * ClickHandler for 'cancel' anchor.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class CancelClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				// restore original content of textboxes and rerun input
				// validation
				txtBoxes.get(name).fireEvent(new KeyUpEvent() { /**/});
				// TODO : FIX ALIGNMENT!
				for (Integer i : tmpStrings.keySet()) {
					t.setText(eventRowIndex, i - 1, tmpStrings.get(i));
				}
				t.setText(eventRowIndex, LEVEL_INDEX, OperatorLevel.olText(levelTemp));
				// restore edit link
				t.setWidget(eventRowIndex, 5, edit);
				t.clearCell(eventRowIndex, 6);
				t.clearCell(eventRowIndex, 7);
				previousCancel = null;
				setFlexTableHeaderSizes(t);
			}
		}

		/** <p>
		 * KeyUpHandler class for 'CPR' input field.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class CPRKeyUpHandler implements KeyUpHandler {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				cprValid = FieldVerifier.isValidCpr(txtBoxes.get(cpr_nr).getText());
				setValidity(txtBoxes.get(cpr_nr), cprValid);
				validateForm();
			}
		}

		/** <p>
		 * KeyUpHandler class for 'name' input field.
		 * 
		 * @author Rudy Alex Kohn [s133235] */
		private class NameKeyUpHandler implements KeyUpHandler {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				nameValid = FieldVerifier.isValidName(txtBoxes.get(name).getText());
				setValidity(txtBoxes.get(name), nameValid);
				validateForm();
			}
		}
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	/** General abstraction of onFailure(), resets pw to null also.
	 * 
	 * @author Rudy Alex Kohn [s133235]
	 * @param <T> */
	private abstract class AbstractEditCallBack<T> implements AsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			pw = null;
			Window.alert("Server fejl : " + caught.getMessage());
		}
	}

	private class OperatorGeneratePasswordCallback extends AbstractEditCallBack<String> {

		@Override
		public void onSuccess(String result) {
			pw = result;
			serv.updatePW(new OperatoerDTO(currentID, pw), new OperatorUpdatePassword());
		}
	}

	private class OperatorUpdatePassword extends AbstractEditCallBack<Boolean> {

		@Override
		public void onSuccess(Boolean result) {
			if (result) {
				Window.alert("Password opdateret : " + pw);
				pw = null;
			}
		}
	}

	private class OperatorUpdateCallback extends AbstractEditCallBack<Boolean> {

		@Override
		public void onSuccess(Boolean bool) {
			if (bool) {
				// TODO :FIX ALIGNMENT!
				for (Integer i : txtBoxes.keySet())
					tmpStrings.put(i, txtBoxes.get(i).getText());
				Window.alert("Operat" + DK.oe + "r med ID : " + Integer.toString(currentID) + " opdateret i databasen.");
			} else Window.alert("Fejl ved opdatering af operat" + DK.oe + "r, kontakt sysadmin.");
		}
	}

	/** Fetch the operators to show on the table.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class OperatorGetListCallback extends AbstractEditCallBack<ArrayList<OperatoerDTO>> {

		@Override
		public void onSuccess(ArrayList<OperatoerDTO> result) {
			if (!result.isEmpty()) {
				int rowIndex = 0;
				ID = new int[result.size()];
				for (int i = 0; i < result.size(); i++) {
					ID[rowIndex] = result.get(i).getId();
					if (LoginStatus.level == 9 || LoginStatus.level >= result.get(i).getLevel()) {
						UIAbstract.addFlexTableRow(t, rowIndex + 1, String.valueOf(result.get(i).getId()), result.get(i).getName(), result.get(i)
								.getIni(), result.get(i).getCpr(), OperatorLevel.olText(result.get(i).getLevel()), " ", " ", " ");
						/* only allow to edit non-administrators */
						if (result.get(i).getLevel() < 9) {
							edit = UIAbstract.generateAnchor("ret", "Rediger information for \n[" + Integer.toString(result.get(i).getId()) + "] "
									+ result.get(i).getName(), new EditHandler());
							t.setWidget(rowIndex + 1, EDIT_INDEX, edit);
						} else t.setText(rowIndex + 1, EDIT_INDEX, "ret");
						UIAbstract.setRowStyle(rowIndex++ + 1, t, true);
					}
				}
				setFlexTableHeaderSizes(t);
				// editPanel.add(t);

				/*
				 * text boxes, doing these through map allows instant access
				 * based on string
				 */
				for (int i = 2; i < 6; i++)
					txtBoxes.put(i, new TextBox());

				txtBoxes.get(name).setWidth("100px");
				txtBoxes.get(cpr_nr).setWidth("100px");
				txtBoxes.get(ini).setWidth("20px");
				txtBoxes.get(lvl).setWidth("20px");
			}
		}
	}
}
