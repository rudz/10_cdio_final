
package dtu.client.ui.operatoer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.base64.Base64Coder;
import dtu.client.service.admin.IAddViewService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.MsgPopup;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.OperatorLevel;
import dtu.shared.validate.FieldVerifier;
import dtu.shared.validate.PasswordValidator;


public class OperatoerAdd extends Composite {

	/* data layer reference */
	private IAddViewService				serv;

	/* UI binding and fields */
	private static OperatoerAddUiBinder	uiBinder	= GWT.create(OperatoerAddUiBinder.class);
	@UiField
	VerticalPanel						vp;
	@UiField
	TextBox								boxName;
	@UiField
	TextBox								boxIni;
	@UiField
	TextBox								boxCPR;
	@UiField
	ListBox								lstLevel;
	@UiField
	PasswordTextBox						boxPw1;
	@UiField
	PasswordTextBox						boxPw2;
	@UiField
	Button								btnAdd;
	@UiField
	Label								lblTitle;
	@UiField
	IntegerBox							boxID;

	/* valid fields */
	private boolean						cprValid	= false;
	private boolean						nameValid	= false;
	private boolean						pwValid		= false;
	private boolean						iniValid	= false;

	interface OperatoerAddUiBinder extends UiBinder<Widget, OperatoerAdd> { /* */}

	public OperatoerAdd(final IAddViewService service) {
		this.serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		serv.getSize(new GetUserCountCallBack());
		configureLayout();
	}

	/* *********************************
	 * ******** HELPER METHODS *******************************************
	 */

	/** Generic keyPress handler. */
	private void onKeyPress(@SuppressWarnings("unused") KeyPressEvent event) {
		checkFormValid();
		if (btnAdd.isEnabled()) btnAdd.click();
	}

	private void configureLayout() {
		/* populate level combo and set widgets and their information */
		String s;
		for (int i = 1; i <= LoginStatus.level; i++) {
			s = OperatorLevel.olText(i);
			if (!"".equals(s) && s != null) lstLevel.addItem(s);
		}

		/* disable button and add text with danish characters. */
		btnAdd.setEnabled(false);
		lblTitle.setText("Tilf" + DK.oe + "j Bruger");
		btnAdd.setText("Tilf" + DK.oe + "j");

		/* set tool tips and index */
		boxID.setTitle("Bruger ID, kan ikke vælges.");
		boxName.setTitle("Navn.");
		boxName.setTabIndex(1);
		boxIni.setTitle("Initialer.");
		boxIni.setTabIndex(2);
		boxCPR.setTitle("Gyldigt dansk CPR nummer (######-####).");
		boxCPR.setTabIndex(3);
		lstLevel.setTitle("Den nye brugers rolle.");
		lstLevel.setTabIndex(4);
		boxPw1.setTitle("Kodeord.");
		boxPw1.setTabIndex(5);
		boxPw2.setTitle("Gentag kodeord.");
		boxPw2.setTabIndex(6);
	}

	private void clearBoxes() {
		boxName.setText(null);
		boxIni.setText(null);
		boxCPR.setText(null);
		boxPw1.setText(null);
		boxPw2.setText(null);

		/* skip keyup event and set boxes to invalid */
		boxName.fireEvent(new KeyUpEvent() { /**/});
		boxIni.fireEvent(new KeyUpEvent() { /**/});
		boxCPR.fireEvent(new KeyUpEvent() { /**/});
		boxPw1.fireEvent(new KeyUpEvent() { /**/});
		boxPw2.fireEvent(new KeyUpEvent() { /**/});
	}

	private void checkFormValid() {
		UIAbstract.setValidity(boxID, (boxID.getValue() > 0));
		btnAdd.setEnabled(cprValid & nameValid & pwValid & iniValid);
	}

	@SuppressWarnings("unused")
	@UiHandler("boxName")
	void onBoxNameKeyUp(KeyUpEvent event) {
		nameValid = FieldVerifier.isValidName(boxName.getText());
		UIAbstract.setValidity(boxName, nameValid);
		checkFormValid();
	}
	@UiHandler("boxName")
	void onBoxNameKeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}

	@UiHandler("boxIni")
	void onBoxIniKeyUp(KeyUpEvent event) {
		onBoxNameKeyUp(event);
		iniValid = FieldVerifier.isValidINI(boxIni.getText(), boxIni.getText());
		UIAbstract.setValidity(boxIni, iniValid);
		checkFormValid();
	}
	@UiHandler("boxIni")
	void onBoxIniKeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}
	@UiHandler("boxCPR")
	void onBoxCPRKeyUp(@SuppressWarnings("unused") KeyUpEvent event) {
		cprValid = FieldVerifier.isValidCpr(boxCPR.getText());
		UIAbstract.setValidity(boxCPR, cprValid);
		checkFormValid();
	}
	@UiHandler("boxCPR")
	void onBoxCPRKeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}

	@SuppressWarnings("unused")
	@UiHandler("boxPw1")
	void onBoxPw1KeyUp(KeyUpEvent event) {
		pwValid = FieldVerifier.isValidPW(LoginStatus.userCount + 1, boxPw1.getText(), boxPw2.getText());
		UIAbstract.setValidity(boxPw1, pwValid);
		UIAbstract.setValidity(boxPw2, pwValid);
		checkFormValid();
	}
	@UiHandler("boxPw1")
	void onBoxPw1KeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}
	@SuppressWarnings("static-method")
	@UiHandler("boxPw1")
	void onBoxPw1Click(ClickEvent event) {
		MsgPopup pop = new MsgPopup(PasswordValidator.getRequirements());
		pop.setPopupPosition(event.getScreenX() + 100, event.getScreenY() - 200);
		pop.show();
	}

	@UiHandler("boxPw2")
	void onTbpw2KeyUp(KeyUpEvent event) {
		onBoxPw1KeyUp(event);
	}
	@UiHandler("boxPw2")
	void onTbpw2KeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}

	@UiHandler("btnAdd")
	void onBtnAddClick(@SuppressWarnings("unused") ClickEvent event) {
		serv.createOperatoer(
				new OperatoerDTO(boxID.getValue(), boxName.getText(), boxIni.getText(), boxCPR.getText(), Base64Coder.encodeString(boxPw1.getText()),
						null, OperatorLevel.olInt(lstLevel.getItemText(lstLevel.getSelectedIndex()))), new AddOperator());
	}

	@UiHandler("lstLevel")
	void onLstLevelKeyPress(KeyPressEvent event) {
		onKeyPress(event);
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	// /**
	// * General abstraction of onFailure().
	// * @author Rudy Alex Kohn [s133235]
	// * @param <T>
	// */
	// private abstract class AbstractAddCallBack<T> implements AsyncCallback<T>
	// {
	// @Override
	// public void onFailure(Throwable caught) {
	// Window.alert(DK.toDk("Serverfejl ved opretning af operatør i databasen."));
	// }
	// }
	private class AddOperator extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean result) {
			if (result) {
				Window.alert(DK.toDk("Bruger med ID " + Integer.toString(++LoginStatus.userCount) + " gemt i databasen."));
				clearBoxes();
			} else {
				onFailure(new Exception("Fejl ved oprettelse af bruger med ID = " + Integer.toString(LoginStatus.userCount + 1)
						+ ", prøv venligst igen. Bruger ID vil nu blive opdateret igen."));
				serv.getSize(new GetUserCountCallBack());
			}
		}
	}

	/** Callback for retrieving user count from server
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class GetUserCountCallBack extends AsyncCallbackAbstract<Integer> {

		@Override
		public void onSuccess(Integer result) {
			if (result > 0) {
				LoginStatus.userCount = result.intValue();
				boxID.setValue(LoginStatus.userCount + 1);
				UIAbstract.setValidity(boxID, boxID.getValue() > 0);
			} else onFailure(new Exception("0 antal brugere fundet, prøv igen."));
		}
	}
}
