
package dtu.client.ui.operatoer;

import java.util.HashMap;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;


public class OperatoerAbstract extends Composite {

	private int[]	headerSize	= new int[] {
			30, 100, 50, 100, 60, 50
								};

	private String getHeaderSize(int i) {
		return Integer.toString(headerSize[i]) + "px";
	}
	protected void setFlexTableHeaderSizes(FlexTable t) {
		for (int i = 0; i < headerSize.length; i++)
			t.getFlexCellFormatter().setWidth(0, i, Integer.toString(headerSize[i]) + "px");
	}
	protected void setTextBoxSize(HashMap<Integer, TextBox> tb) {
		for (Integer i : tb.keySet()) {
			tb.get(i).setWidth(getHeaderSize(i));
		}
	}
}
