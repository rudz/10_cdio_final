
package dtu.client.ui.operatoer;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.admin.IDeleteService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;


public class OperatoerDelete extends OperatoerAbstract {

	private static OperatoerDeleteUiBinder	uiBinder		= GWT.create(OperatoerDeleteUiBinder.class);

	/* data layer reference */
	private IDeleteService					service;

	/* widgets */
	@UiField
	FlexTable								t;
	@UiField
	Label									lblTitle;

	/* anchors */
	private Anchor							previousCancel	= null;
	private Anchor							deleteAnchor	= null;
	private Anchor							okAnchor		= null;
	private Anchor							cancelAnchor	= null;

	/* variables */
	private int								eventRowIndex;
	private int								rowIndex;

	interface OperatoerDeleteUiBinder extends UiBinder<Widget, OperatoerDelete> { /**/}

	public OperatoerDelete(IDeleteService service) {
		this.service = service;
		initWidget(uiBinder.createAndBindUi(this));
		resetTable();
		lblTitle.setText("Slet Brugere");
	}

	/* ******************************************************
	 * /* ************** HELPER METHODS ************************ /*
	 * ****************************************************
	 */

	private void setFlexTableFormatSize() {
		UIAbstract.setFlexTableHeaders(t, "ID", "Navn", "Initialer", "CPR", "Brugerniveau");
		setFlexTableHeaderSizes(t);
	}

	private void resetTable() {
		setFlexTableFormatSize();
		service.getOperatoerList(new GetOps());
	}

	/* *****************************************************
	 * /* ************** HANDLER CLASSES ********************** /*
	 * ****************************************************
	 */

	private class DeleteHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {

			/* if previous cancel open - force cancel operation */
			if (previousCancel != null) previousCancel.fireEvent(new ClickEvent() { /**/});

			/* get rowindex where event happened */
			eventRowIndex = t.getCellForEvent(event).getRowIndex();

			/* get delete anchor ref for cancel operation */
			deleteAnchor = (Anchor) event.getSource();

			/* configure "ok" / "fortryd" anchors */
			okAnchor = UIAbstract.generateAnchor("ok", "Godkend ændringer foretaget.", new OkClickHandler());
			cancelAnchor = UIAbstract.generateAnchor("fortryd", "Fortryd eventuelle ændringer.", new CancelClickHandler());
			previousCancel = cancelAnchor;

			/* show ok and cancel widgets */
			t.setWidget(eventRowIndex, 4, okAnchor);
			t.setWidget(eventRowIndex, 5, cancelAnchor);
		}
	}

	private class CancelClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			t.clearCell(eventRowIndex, 4);
			t.clearCell(eventRowIndex, 5);
			t.setWidget(eventRowIndex, 4, deleteAnchor);
		}
	}

	private class OkClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			service.sackOperatoer(Integer.parseInt(t.getText(eventRowIndex, 0)), new SackOpr());
			previousCancel = null;
		}
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	/** Sack operator callback class
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class SackOpr extends AsyncCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean result) {
			Window.alert("Operat" + DK.oe + "r deaktiveret.");
			t.removeAllRows();
			resetTable();
		}
	}

	/** Get operator list callback class.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class GetOps extends AsyncCallbackAbstract<ArrayList<OperatoerDTO>> {

		@Override
		public void onSuccess(ArrayList<OperatoerDTO> result) {
			rowIndex = 0;
			for (OperatoerDTO o : result) {
				if (o.getLevel() > 0 && o.getLevel() < LoginStatus.level || LoginStatus.level == 9) {
					UIAbstract.addFlexTableRow(t, rowIndex + 1, Integer.toString(o.getId()), DK.toDk(o.getName()), o.getIni(), o.getCpr(), " ", " ");
					if (o.getLevel() < 9) {
						Anchor delete = UIAbstract.generateAnchor("slet",
								"Slet bruger \n[" + Integer.toString(o.getId()) + "] " + DK.toDk(o.getName()), new DeleteHandler());
						t.setWidget(rowIndex + 1, 4, delete);
					} else t.setText(rowIndex + 1, 4, "slet");
					UIAbstract.setRowStyle(rowIndex++ + 1, t, true);
				}
			}
		}
	}
}
