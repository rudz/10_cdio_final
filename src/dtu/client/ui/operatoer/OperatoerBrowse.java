
package dtu.client.ui.operatoer;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.admin.IBrowseViewService;
import dtu.client.ui.generic.AsyncCallbackAbstract;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.OperatorLevel;


public class OperatoerBrowse extends OperatoerAbstract {

	private IBrowseViewService				serv;
	private static OperatoerBrowseUiBinder	uiBinder	= GWT.create(OperatoerBrowseUiBinder.class);
	@UiField
	FlexTable								t;
	@UiField
	Label									lblTitle;

	interface OperatoerBrowseUiBinder extends UiBinder<Widget, OperatoerBrowse> { /**/}

	public OperatoerBrowse(IBrowseViewService service) {
		serv = service;
		initWidget(uiBinder.createAndBindUi(this));
		lblTitle.setText("Vis Brugere");
		serv.getOperatoerList(new browseOpr());
	}

	/* ******************************************************
	 * /* ************** CALLBACK CLASSES ********************** /*
	 * ****************************************************
	 */

	/** Browse operator callback class
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	private class browseOpr extends AsyncCallbackAbstract<ArrayList<OperatoerDTO>> {

		@Override
		public void onSuccess(ArrayList<OperatoerDTO> result) {
			setFlexTableHeaderSizes(t);
			UIAbstract.setFlexTableHeaders(t, "ID", "Navn", "Initialer", "CPR", "Brugerniveau");
			int i = 0;
			for (OperatoerDTO o : result) {
				if ((o.getLevel() != 0 && LoginStatus.level < 9) || LoginStatus.level == 9) {
					t.setText(i + 1, 0, Integer.toString(o.getId()));
					t.setText(i + 1, 1, DK.toDk(o.getName()));
					t.setText(i + 1, 2, o.getIni());
					t.setText(i + 1, 3, o.getCpr());
					t.setText(i + 1, 4, OperatorLevel.olText(o.getLevel()));
					UIAbstract.setRowStyle(i++ + 1, t, true);
				}
			}
		}
	}
}
