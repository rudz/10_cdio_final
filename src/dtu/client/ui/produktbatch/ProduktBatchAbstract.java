
package dtu.client.ui.produktbatch;

import java.util.ArrayList;
import java.util.HashMap;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import dtu.client.ui.generic.UIAbstract;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.misc.DK;


/** General abstraction of recept UI widgets.<br>
 * Contains helper functionality and generic classes.
 * 
 * @author Rudy Alex Kohn [s133235] */
public abstract class ProduktBatchAbstract extends Composite {

	/* the total list of 'recept' */
	protected ArrayList<ReceptDTO>			receptList			= new ArrayList<>();

	/* 'recept' map */
	protected HashMap<Integer, ReceptDTO>	receptMap			= new HashMap<>();

	/* the total 'produkt' list */
	protected ArrayList<ProduktBatchDTO>	produktbatchList	= new ArrayList<>();

	/* the value boxes for each element */
	private HashMap<Integer, IntegerBox>	boxValue			= new HashMap<>();

	/* how many is currently added to current recept? */
	protected int							receptCount;

	protected IntegerBox					numberOfBatches;

	protected Button						cancel;

	protected static boolean				transferInProgress;

	/** Initialises the FlexTable.
	 * 
	 * @param f
	 *            : The flextable to initialise. */
	protected static void initFlexTable(FlexTable ft) {
		ft.clear();
		ft.getFlexCellFormatter().setWidth(0, 0, "70px");
		ft.getFlexCellFormatter().setWidth(0, 1, "100px");
		ft.getFlexCellFormatter().setWidth(0, 2, "50px");
		ft.getFlexCellFormatter().setWidth(0, 3, "50");
		ft.getRowFormatter().addStyleName(0, "FlexTable-Header");
		ft.setText(0, 0, DK.toDk("Recept ID"));
		ft.setText(0, 1, "Recept navn");
		ft.setText(0, 2, "Antal");
	}

	/** Simple function to set any given img widget to visible or hidden.
	 * 
	 * @param img
	 *            : The image to set
	 * @param val
	 *            : The visibility the img should have */
	protected static void setImgVis(Image img, boolean value) {
		if (value) img.getElement().getStyle().setVisibility(Visibility.VISIBLE);
		else img.getElement().getStyle().setVisibility(Visibility.HIDDEN);
	}

	/** Verify that parsed boolean values are all true.
	 * 
	 * @param values
	 *            : The value(s)
	 * @return true if all are true, else false. */
	protected static boolean isValid(boolean... values) {
		for (boolean b : values)
			if (!b) return false;
		return true;
	}

	/** Adds the given 'recept' to current map */
	protected void addReceptToMap(ReceptDTO recept) {
		receptMap.put(recept.getReceptId(), recept);
	}

	protected ReceptDTO getRecept(int id) {
		if (receptMap.containsKey(id)) return receptMap.get(id);
		return null;
	}

	protected void addIntegerBox(int key) {
		boxValue.put(key, new IntegerBox());
		boxValue.get(key).setValue(1);
		boxValue.get(key).setWidth("50px");
		boxValue.get(key).addKeyUpHandler(new AmountBoxKeyUpHandler());
	}

	protected void removeIntegerBox(int key) {
		boxValue.remove(key);
	}

	/** Adds a 'raavare' to current recept set.
	 * 
	 * @param raavareID
	 *            : The 'raavare' ID to be added.
	 * @return true if added, else false. */
	protected boolean addReceptInProduktBatch(int receptID, String receptName, FlexTable ft, ClickHandler CancelHandler) {
		ft.setText(receptCount + 1, 0, Integer.toString(receptID));
		ft.getFlexCellFormatter().setWidth(receptCount, 0, "70px");
		ft.setText(receptCount + 1, 1, receptName);
		ft.getFlexCellFormatter().setWidth(receptCount + 1, 1, "100px");
		addIntegerBox(receptCount);
		ft.getFlexCellFormatter().setWidth(receptCount + 1, 1, "50px");
		ft.setWidget(receptCount + 1, 2, boxValue.get(receptCount));
		ft.getFlexCellFormatter().setWidth(receptCount + 1, 3, "50px");
		cancel = new Button("slet");
		cancel.addClickHandler(CancelHandler);
		ft.setWidget(receptCount + 1, 3, cancel);
		UIAbstract.setRowStyle(receptCount++, ft, true);
		// Window.alert("ADD RECEPT");
		return true;
	}

	protected static void setLabelText(Label l, String text) {
		l.setText(DK.toDk(text));
	}

	class AmountBoxKeyUpHandler implements KeyUpHandler {

		@Override
		public void onKeyUp(KeyUpEvent arg0) {
			if (arg0.getSource() instanceof IntegerBox) {
				IntegerBox iBox = (IntegerBox) arg0.getSource();
				UIAbstract.setValidity(iBox, (iBox.getValue() > 0));
			}
		}
	}
}
