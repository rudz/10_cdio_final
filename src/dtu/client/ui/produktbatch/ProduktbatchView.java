
package dtu.client.ui.produktbatch;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IProduktBatchService;
import dtu.client.ui.generic.UIAbstract;
import dtu.client.ui.produktbatch.operatortask.TaskStringGenerator;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.StringToolLib;


public class ProduktbatchView extends Composite {

	@UiField
	FlexTable								t;

	@UiField
	IntegerBox								startBox;
	@UiField
	IntegerBox								endBox;
	@UiField
	Label									startLabel;
	@UiField
	InlineLabel								endLabel;
	@UiField
	Button									getInterval;
	@UiField
	CheckBox								notStarted;
	@UiField
	CheckBox								underProduction;
	@UiField
	CheckBox								doneProduction;
	@UiField
	Label									lblTitle;
	@UiField
	CaptionPanel							capFilter;

	private static ProduktbatchViewUiBinder	uiBinder	= GWT.create(ProduktbatchViewUiBinder.class);

	private IProduktBatchService			serv;
	private ArrayList<ProduktBatchDTO>		BatchList;
	@SuppressWarnings("unused")
	private Anchor							details, detailsEnd, previousDetailsEnd;
	private int								eventRowIndex;
	private int								startInterval, endInterval;
	/** To be changed to a single status variable at some point.
	 * 
	 * @deprecated */
	private int								status0, status1, status2;

	interface ProduktbatchViewUiBinder extends UiBinder<Widget, ProduktbatchView> { /**/}

	public ProduktbatchView(IProduktBatchService service) {
		initWidget(uiBinder.createAndBindUi(this));
		serv = service;
		lblTitle.setText("Vis ProduktBatch");
		notStarted.setText(DK.toDk(notStarted.getText()));
		capFilter.setCaptionText("S" + DK.oe + "ge filter");
		startBox.setTabIndex(1);
		endBox.setTabIndex(2);
		notStarted.setTabIndex(3);
		underProduction.setTabIndex(4);
		doneProduction.setTabIndex(5);
		getInterval.setTabIndex(6);
		startBox.setFocus(true);
	}

	@UiHandler("endBox")
	void checkStartBoxForEnter(KeyDownEvent e) {
		if (e.getNativeKeyCode() == 13) getInterval.click();
	}
	@UiHandler("startBox")
	void checkEndBoxForEnter(KeyDownEvent e) {
		if (e.getNativeKeyCode() == 13) getInterval.click();
	}

	@UiHandler("getInterval")
	void onBtnAddGoodClick(@SuppressWarnings("unused") ClickEvent event) {

		startInterval = startBox.getValue();
		endInterval = endBox.getValue();
		if (notStarted.getValue()) status0 = 0;
		else status0 = -1;
		if (underProduction.getValue()) status1 = 1;
		else status1 = -1;
		if (doneProduction.getValue()) status2 = 2;
		else status2 = -1;

		serv.getProduktBatchListWithStatus(startInterval, endInterval, status0, status1, status2, new AsyncCallback<ArrayList<ProduktBatchDTO>>() {

			@Override
			public void onSuccess(ArrayList<ProduktBatchDTO> result) {
				t.removeAllRows();
				BatchList = result;

				UIAbstract.setFlexTableHeaders(t, "Recept Navn", "Batch ID", "Recept ID", "Status", "Oprettet", "Startet", "Sluttet");
				t.getFlexCellFormatter().setWidth(0, 0, "200px");
				t.getFlexCellFormatter().setWidth(0, 1, "170px");
				t.getFlexCellFormatter().setWidth(0, 2, "170px");
				t.getFlexCellFormatter().setWidth(0, 3, "150px");
				t.getFlexCellFormatter().setWidth(0, 4, "220px");
				t.getFlexCellFormatter().setWidth(0, 5, "200px");
				t.getFlexCellFormatter().setWidth(0, 6, "200px");
				t.setWidth("550px");

				for (int i = 0; i < BatchList.size(); i++) {
					t.setWidget(i + 1, 0, UIAbstract.generateAnchor(BatchList.get(i).getReceptName(), "Vis extra detaljer / udskriv "
							+ BatchList.get(i).getReceptName(), new ShowTaskInPopupClickHandler(BatchList.get(i).getPbId(), BatchList.get(i))));
					t.setText(i + 1, 1, String.valueOf((BatchList.get(i).getPbId())));
					t.setText(i + 1, 2, String.valueOf((BatchList.get(i).getReceptId())));
					t.setText(i + 1, 3, String.valueOf((BatchList.get(i).getStatus())));
					t.setText(i + 1, 4, (StringToolLib.left(BatchList.get(i).getOprettet(), 16)));
					if (BatchList.get(i).getStart() != null) t.setText(i + 1, 5, (StringToolLib.left(BatchList.get(i).getStart(), 16)));
					if (BatchList.get(i).getSlut() != null) t.setText(i + 1, 6, (StringToolLib.left(BatchList.get(i).getSlut(), 16)));

					details = UIAbstract.generateAnchor("detaljer", "Vis detaljer for " + BatchList.get(i).getReceptName(), new DetailClickHandler());
					t.setWidget(i + 1, 7, details);
					t.getFlexCellFormatter().setWidth(i + 1, 7, "70px");
					UIAbstract.setRowStyle(i + 1, t, true);
				}

			}
			@Override
			public void onFailure(Throwable arg0) {
				Window.alert(arg0.getMessage());
			}
		});
	}

	private class DetailClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {

			eventRowIndex = t.getCellForEvent(event).getRowIndex();
			detailsEnd = UIAbstract.generateAnchor("skjul detaljer", "Skjul detaljer for denne ProduktBatch", new DetailEndClickHandler());

			previousDetailsEnd = detailsEnd;

			t.setWidget(eventRowIndex, 7, detailsEnd);
			int produktBatchId = Integer.parseInt(t.getText(eventRowIndex, 1));
			serv.getProduktBatchKompList(produktBatchId, new AsyncCallback<ArrayList<ProduktBatchKompDTO>>() {

				@Override
				public void onFailure(Throwable arg0) {
					Window.alert(arg0.getMessage());

				}
				@Override
				public void onSuccess(ArrayList<ProduktBatchKompDTO> arg0) {
					buildProduktBatchKomponentView(arg0);
				}
			});
		}
	}

	private class DetailEndClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent arg0) {
			int removeRowIndex = t.getCellForEvent(arg0).getRowIndex() + 1;
			details = UIAbstract.generateAnchor("detaljer", "Vis detaljer", new DetailClickHandler());
			t.setWidget(t.getCellForEvent(arg0).getRowIndex(), 7, details);
			while (t.getText(removeRowIndex, 0).isEmpty() && !t.getText(removeRowIndex, 1).isEmpty()) {
				// removes the information rows until it finds a recept name
				t.removeRow(removeRowIndex);
			}

		}

	}

	private void buildProduktBatchKomponentView(ArrayList<ProduktBatchKompDTO> list) {
		int curRow = eventRowIndex + 2; // current row, for inserting text and
		t.insertRow(eventRowIndex + 1);
		t.getRowFormatter().setStyleName(eventRowIndex + 1, "FlexTable-Header");
		t.getFlexCellFormatter().setWidth(eventRowIndex + 1, 0, "100px");
		t.getFlexCellFormatter().setWidth(eventRowIndex + 1, 1, "100px");
		t.getFlexCellFormatter().setWidth(eventRowIndex + 1, 2, "100px");
		t.getFlexCellFormatter().setWidth(eventRowIndex + 1, 3, "100px");
		t.getFlexCellFormatter().setWidth(eventRowIndex + 1, 4, "100px");
		t.setText(eventRowIndex + 1, 1, "ProduktBatch ID");
		t.setText(eventRowIndex + 1, 2, "R" + DK.aa + "vareBatch ID");
		t.setText(eventRowIndex + 1, 3, "Tara");
		t.setText(eventRowIndex + 1, 4, "Netto");
		t.setText(eventRowIndex + 1, 5, "Operat" + DK.oe + "r ID");

		for (int i = 0; i < list.size(); i++) {
			t.insertRow(curRow + i);
			t.setText(curRow + i, 1, String.valueOf(list.get(i).getPbId()));
			t.setText(curRow + i, 2, String.valueOf(list.get(i).getRbId()));
			t.setText(curRow + i, 3, String.valueOf(list.get(i).getTara()));
			t.setText(curRow + i, 4, String.valueOf(list.get(i).getNetto()));
			t.setText(curRow + i, 5, String.valueOf(list.get(i).getOprId()));
			UIAbstract.setRowStyle(curRow + i, t, true);
		}
	}

	class ShowTaskInPopupClickHandler implements ClickHandler {

		private ArrayList<ReceptKomponenterDTO>	rkomplist			= new ArrayList<>();
		// private ArrayList<ProduktBatchDTO> prodbatchlist = new ArrayList<>();
		private ProduktBatchDTO					prodbatchDTO;
		private ArrayList<ProduktBatchKompDTO>	prodbatchkomplist	= new ArrayList<>();
		private int								pb_id;
		private TextArea						ta;

		public ShowTaskInPopupClickHandler(int pb_id, ProduktBatchDTO prodbatchDTO) {
			this.prodbatchDTO = prodbatchDTO;
			this.pb_id = pb_id;
		}
		@Override
		public void onClick(ClickEvent event) {
			serv.getReceptKomponenter(prodbatchDTO.getReceptId(), new AsyncCallback<ArrayList<ReceptKomponenterDTO>>() {

				@Override
				public void onSuccess(ArrayList<ReceptKomponenterDTO> result) {
					rkomplist = result;
					serv.getProduktBatchKompList(pb_id, new AsyncCallback<ArrayList<ProduktBatchKompDTO>>() {

						@Override
						public void onFailure(Throwable arg0) {
							rkomplist = null;
							Window.alert(arg0.getMessage());
						}
						@Override
						public void onSuccess(ArrayList<ProduktBatchKompDTO> arg0) {
							// Window.alert("Fået PBKOMP liste");
							prodbatchkomplist = arg0;
							final DialogBox dialogBox = createDialogBox();
							dialogBox.setGlassEnabled(true);
							dialogBox.setAnimationEnabled(true);
							dialogBox.center();
							dialogBox.show();
						}
					});
				}
				@Override
				public void onFailure(Throwable arg0) {
					Window.alert("Fejl ved overførsel af ReceptKomponent listen.");
				}
			});
		}
		native void openPrintWindow(String contents) /*-{
			var printWindow = window.open("", "Operatør Job");
			if (printWindow && printWindow.top) {
				printWindow.document.write(contents);
				printWindow.print();
				printWindow.close();
			} else {
				alert("Udprintnings popupvindue er blokeret af din browser.");
			}
		}-*/;

		private DialogBox createDialogBox() {
			// Create a dialog box and set the caption text
			final DialogBox dialogBox = new DialogBox();
			final String style = "<!DOCTYPE html><html><head><style>p.mono { font-family: \"Lucida Console\", Monaco, monospace; font-size: 9pt; }</style><p class=\"mono\">%s</p></body></html>";
			dialogBox.ensureDebugId("cwDialogBox");
			dialogBox.setText(DK.toDk("ProduktBatch : " + Integer.toString(pb_id)));
			// Create a table to layout the content
			VerticalPanel dialogContents = new VerticalPanel();
			dialogContents.setSpacing(4);
			dialogBox.setWidget(dialogContents);

			// Add some text to the top of the dialog
			HTML details1 = new HTML("Udskriver : " + LoginStatus.userName);
			dialogContents.add(details1);
			dialogContents.setCellHorizontalAlignment(details1, HasHorizontalAlignment.ALIGN_CENTER);

			// Add the actual information to the dialog.
			ta = new TextArea();
			ScrollPanel sp = new ScrollPanel();
			sp.setSize("830px", "500px");
			sp.add(ta);
			ta.setSize("820px", "490px");
			ta.setStyleName("TaskText");
			ta.setReadOnly(true);
			TaskStringGenerator tsg = new TaskStringGenerator(prodbatchDTO, prodbatchkomplist, rkomplist);
			ta.setText(tsg.generateTask());
			tsg = null;
			dialogContents.add(sp);
			dialogContents.setCellHorizontalAlignment(sp, HasHorizontalAlignment.ALIGN_CENTER);

			// Add container for the buttons
			HorizontalPanel hp = new HorizontalPanel();
			hp.setWidth("830px");

			// Add a close button at the bottom of the dialog
			final Button closeButton = new Button("Luk", new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					dialogBox.hide(true);
					dialogBox.removeFromParent();
				}
			});
			closeButton.setTitle("Luk vindue");
			hp.add(closeButton);

			// Add a print button too!
			Button printButton = new Button("Udskriv", new ClickHandler() {

				@Override
				public void onClick(ClickEvent arg0) {
					/*
					 * convert simple things to HTML to make sure viewing the
					 * page is optimal to a point
					 */
					HTML html = new HTML(style.replaceFirst(
							"%s",
							ta.getText().replaceAll("\u00B7", "&middot;").replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;")
									.replaceAll("\u00B1", "&plusmn;")));
					openPrintWindow(html.getHTML());
				}
			});
			printButton.setTitle(DK.toDk("Udskriv viste dokument (åbner i ny tab)"));
			hp.add(printButton);
			dialogContents.add(hp);

			if (LocaleInfo.getCurrentLocale().isRTL()) {
				dialogContents.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_LEFT);
				dialogContents.setCellHorizontalAlignment(printButton, HasHorizontalAlignment.ALIGN_LEFT);
			} else {
				dialogContents.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_RIGHT);
				dialogContents.setCellHorizontalAlignment(printButton, HasHorizontalAlignment.ALIGN_RIGHT);
			}
			// Return the dialog box
			return dialogBox;
		}
	}

}
