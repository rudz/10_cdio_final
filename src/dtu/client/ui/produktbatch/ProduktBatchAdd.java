
package dtu.client.ui.produktbatch;

import java.util.ArrayList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import dtu.client.service.foreman.IProduktBatchService;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.misc.DK;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;


public class ProduktBatchAdd extends ProduktBatchAbstract {

	private static ProduktBatchAddUiBinder	uiBinder	= GWT.create(ProduktBatchAddUiBinder.class);

	interface ProduktBatchAddUiBinder extends UiBinder<Widget, ProduktBatchAdd> { /* */}
	@UiField
	Button							btnAddGood;
	@UiField
	ListBox							cmbRecept;
	@UiField
	FlexTable						t;
	@UiField
	Button							btnOk;
	@UiField
	Image							imgInProgress;
	@UiField
	Label							lblProgress;
	@UiField
	Label							lblTitle;

	private boolean					validTolerance;
	private boolean					validNetto;
	private boolean					validName;
	private boolean					validID;

	private int						receptID;

	/* data layer reference */
	private IProduktBatchService	serv;

	public ProduktBatchAdd(IProduktBatchService service) {
		initWidget(uiBinder.createAndBindUi(this));
		setLabelText(lblProgress, "Indhenter recepter i databasen.");

		serv = service;
		receptCount = 0;

		lblTitle.setText("Tilf" + DK.oe + "j ProduktBatch");
		btnAddGood.setText(lblTitle.getText());
		btnAddGood.setEnabled(true);

		btnOk.setEnabled(false);

		/* init flex table dimensions */
		initFlexTable(t);

		serv.getReceptList(new getReceptList());
	}
	@UiHandler("btnAddGood")
	void onBtnAddGoodClick(@SuppressWarnings("unused") ClickEvent event) {
		// addRaavareInRecept(cmbRaavare.getSelectedIndex() + 1,
		// boxNomNetto.getValue(), boxTolerance.getValue(), t);
		addReceptInProduktBatch(receptList.get(cmbRecept.getSelectedIndex()).getReceptId(), receptList.get(cmbRecept.getSelectedIndex())
				.getReceptNavn(), t, new CancelAddClickHandler());
		addReceptToMap(receptList.get(cmbRecept.getSelectedIndex()));
		btnOk.setEnabled(true);
	}
	@UiHandler("btnOk")
	void onBtnOkClick(@SuppressWarnings("unused") ClickEvent event) {
		btnOk.setEnabled(false);
		setLabelText(lblProgress, "Genererer liste..");
		setImgVis(imgInProgress, true);
		ArrayList<ProduktBatchDTO> DTOList = new ArrayList<>();
		int receptAmount; /* 'antal' counter */
		for (int i = 0; i < receptCount; i++) {
			receptID = Integer.parseInt(t.getText(i + 1, 0));
			receptAmount = ((IntegerBox) t.getWidget(i + 1, 2)).getValue();
			// Window.alert(receptAmount + " ANTAL");
			for (int j = 0; j < receptAmount; j++)
				DTOList.add(new ProduktBatchDTO(0, 0, receptID));
		}
		setLabelText(lblProgress, "Sender ordrer...");
		serv.createProduktBatchList(DTOList, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable arg0) {
				setLabelText(lblProgress, "Fejl ved overførsel.");
				setImgVis(imgInProgress, false);
				// Window.alert(arg0.toString());
				btnOk.setEnabled(true);
			}
			@Override
			public void onSuccess(Boolean arg0) {
				setLabelText(lblProgress, "Overførsel ok.");
				setImgVis(imgInProgress, false);
				btnOk.setEnabled(true);
			}
		});
	}
	@UiHandler("cmbRecept")
	void onCmbReceptKeyDown(KeyDownEvent event) {
		if (event.getNativeKeyCode() == 13) enterPressed();
	}

	/** General functionality for when pressing enter key on a textbox. */
	private void enterPressed() {
		if (isValidInput()) btnAddGood.click();
	}

	private boolean isValidInput() {
		boolean isVal = isValid(validID, validNetto, validTolerance, validName);
		btnAddGood.setEnabled(isVal);
		btnOk.setEnabled(t.getCellCount(1) > 0);
		return isVal;
	}

	/* ******************************************************
	 * ************** CALLBACK CLASSES **********************
	 * ****************************************************
	 */

	/** General abstraction of onFailure().
	 * 
	 * @author Rudy Alex Kohn [s133235]
	 * @param <T> */
	abstract class produktbatchCallbackAbstract<T> implements AsyncCallback<T> {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.toString());
			setImgVis(imgInProgress, false);
		}
	}

	class createProduktBatch extends produktbatchCallbackAbstract<Boolean> {

		@Override
		public void onSuccess(Boolean arg0) {
			setLabelText(lblProgress, "Handling udført.");
			setImgVis(imgInProgress, false);
		}
	}

	/** Callback class to get all 'recept' as list.
	 * 
	 * @author Rudy Alex Kohn [s133235] */
	class getReceptList extends produktbatchCallbackAbstract<ArrayList<ReceptDTO>> {

		@Override
		public void onSuccess(ArrayList<ReceptDTO> result) {
			receptList = result;
			for (ReceptDTO rv : result)
				cmbRecept.addItem("[" + Integer.toString(rv.getReceptId()) + "] " + rv.getReceptNavn());
			setImgVis(imgInProgress, false);
			setLabelText(lblProgress, "Recepter indhentet.");
		}
	}

	private class CancelAddClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent arg0) {
			int row = t.getCellForEvent(arg0).getRowIndex();
			t.removeRow(row);
			receptMap.remove(row);
			removeIntegerBox(row);
			if (--receptCount == 0) btnOk.setEnabled(false);
		}
	}

}
