
package dtu.client.ui.produktbatch.operatortask;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import dtu.shared.LoginStatus;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.ReceptKomponenterDTO;
import dtu.shared.misc.DK;
import dtu.shared.misc.OperatorLevel;
import dtu.shared.misc.StringToolLib;


/** Generates the strings for showing the task ready for printing.<br>
 * Uses generic java features which GWT can convert into js. ** THIS CLASS IS
 * NOT 100% FINISHED YET AS INTENDED, BUT WORKS!!!! **
 * 
 * @author Rudy Alex Kohn [s133235]
 * @author Thor Adrian Dahlstrøm [s144867] */
public class TaskStringGenerator {

	private final int								MAX_LEN					= 100;
	private final String							dot						= "\u00B7";																		// middle
																																								// dot
	private final String							SEP						= StringToolLib.replicate(dot, MAX_LEN);

	private final String							DATO					= "Udskrevet d.      : ";
	private final String							WRITER					= "Udskrevet af      : ";
	private final String							PBNFO					= "Produkt Batch nr. : ";
	private final String							RECNFO					= "Recept nr.        : ";
	private final String							RAANFO					= DK.toDk("Råvare nr.        : ");
	private final String							RAANAVN					= DK.toDk("Råvare navn       : ");

	// private final String TAB = "\u0009"; // tabulator
	// private final String PLUSMIN = "±"; //"\u00B1";
	private final String							PLUSMIN					= "\u00B1";

	private final int[]								COLW					= new int[] {
			7, 10, 8, 8, 14, 11, 12, 13
																			};

	private final String							DETAILS					= "Del    Mængde    Tolerance   Tara    Netto (Kg)    Batch      Opr.    Terminal";

	private ProduktBatchDTO							prodbatch;
	private ArrayList<ProduktBatchKompDTO>			prodbatchkomplist;
	private ArrayList<ReceptKomponenterDTO>			rkomplist;

	private HashMap<Integer, String>				menu					= new HashMap<>(41);
	private HashMap<Integer, ProduktBatchKompDTO>	sortedProdbatchkomplist	= new HashMap<>();

	public TaskStringGenerator(ProduktBatchDTO prodbatch, ArrayList<ProduktBatchKompDTO> prodbatchkomplist, ArrayList<ReceptKomponenterDTO> rkomplist2) {
		this.prodbatch = prodbatch;
		this.prodbatchkomplist = prodbatchkomplist;

		for (int i = 0; i < prodbatchkomplist.size(); i++) { // dirty sorting of
																// the
																// prodbatchkomps,
																// so that they
																// can be
																// accessed by
																// RbId
			sortedProdbatchkomplist.put(prodbatchkomplist.get(i).getRaavareId(), prodbatchkomplist.get(i));
		}

		this.rkomplist = rkomplist2;
	}

	public String generateTask() {
		int pos = getTop(1);
		pos = getStatus(pos);
		pos = getButtom(pos);
		StringBuilder sb = new StringBuilder((pos * MAX_LEN) + 1); // reserve
																	// buffer
		for (Integer i : menu.keySet())
			sb.append(menu.get(i)).append("\n");
		return sb.toString();
	}

	/** Generate the top part of all menus
	 * 
	 * @param pos
	 *            : the current pos in the menu generating process */
	protected int getTop(int pos) {
		int p = pos;
		menu.put(++p, SEP);
		menu.put(++p, makeSingleBordered(WRITER + LoginStatus.userName + " [" + OperatorLevel.olText(LoginStatus.level) + "]"));
		menu.put(++p, makeSingleBordered(DATO + new Date().toString()));
		menu.put(++p, makeSingleBordered(PBNFO + StringToolLib.rSet(Integer.toString(prodbatch.getPbId()), 8, '0')));// produkt
																														// batch
																														// id
		// Window.alert("GET TOP 1");
		menu.put(++p, makeSingleBordered(RECNFO + StringToolLib.rSet(Integer.toString(prodbatch.getReceptId()), 8, '0'))); // add
																															// recept
																															// nr
		// Window.alert("GET TOP 2");
		menu.put(++p, makeSingleBordered(""));
		return p;
	}

	/** Generates the status part of the menu screen.
	 * 
	 * @param pos
	 *            : The current row position.
	 * @return the row position when done. */
	protected int getStatus(int pos) {
		int p = pos;
		// loop through all the raavare in the list!
		for (int i = 0; i < rkomplist.size(); i++) {
			menu.put(++p, makeSingleBordered(""));
			menu.put(++p, makeSingleBordered(RAANFO + StringToolLib.rSet(Integer.toString(rkomplist.get(i).getRaavare_id()), 8, '0'))); // raavare
																																		// nr
			menu.put(++p, makeSingleBordered(RAANAVN + DK.toDk(rkomplist.get(i).getNavn()))); // raavare
																								// navn
			menu.put(++p, SEP);
			menu.put(++p, makeSingleBordered(DETAILS));
			menu.put(
					++p,
					makeSingleBordered(StringToolLib.lSet("1", COLW[0], ' ') + doString(1, rkomplist.get(i).getNom_Netto())
							+ StringToolLib.lSet(PLUSMIN + Double.toString(rkomplist.get(i).getTolerance()) + "%", COLW[2], ' ')
							+ insertProduktbatchkompData(p, rkomplist.get(i).getRaavare_id())));
			menu.put(++p, makeSingleBordered(""));
			menu.put(++p, makeSingleBordered(""));
		}
		return p;
	}
	/** Makes the last bit of the menu screen.
	 * 
	 * @param pos
	 *            : current row pos
	 * @return The current row pos when done. */
	protected int getButtom(int pos) {
		int p = pos;
		menu.put(++p, makeSingleBordered("Produktion Status  : " + getStatusString(prodbatch.getStatus())));
		menu.put(++p, makeSingleBordered("Produktion Oprettet: "
				+ (prodbatch.getOprettet() != null ? StringToolLib.left(prodbatch.getOprettet(), 16).replaceAll("-", "/") : "")));// prodbatch.getStart().trim()));
		menu.put(++p, makeSingleBordered("Produktion Startet : "
				+ (prodbatch.getSlut() != null ? StringToolLib.left(prodbatch.getStart(), 16).replaceAll("-", "/") : "")));// prodbatch.getStart().trim()));
		menu.put(++p, makeSingleBordered("Produktion Slut    : "
				+ (prodbatch.getSlut() != null ? StringToolLib.left(prodbatch.getSlut(), 16).replaceAll("-", "/") : "")));
		menu.put(++p, makeSingleBordered(""));
		menu.put(++p, StringToolLib.overwrite(SEP, " < G10 Pharma BETA > ", 5));
		return p;
	}

	private String getStatusString(int status) {
		if (status == 0) return "Startet";
		else if (status == 1) return "Under produktion";
		return "Afsluttet";
	}

	/** Generates a line containing a single 'dot' at the beginning and end.<b>
	 * The string will follow after a space from the left, filled with spaces up
	 * until the last 'dot'.
	 * 
	 * @param s
	 *            : the string to encapsulate.
	 * @return : the resulting string */
	private String makeSingleBordered(String s) {
		final int sLen = s.length();
		if (sLen > MAX_LEN - 2) return s;
		StringBuilder sb = new StringBuilder(80);
		sb.append(dot).append(' ').append(s);
		sb.append(StringToolLib.space(MAX_LEN - 3 - sLen)).append(dot);
		return sb.toString();
	}
	/** Generates a line containing a single 'dot' at the beginning and end.<b>
	 * The rest is filled with spaces and the parsed string is centred.
	 * 
	 * @param s
	 *            : the string to centre
	 * @return : the resulting string */
	@SuppressWarnings("unused")
	private String makeSingleBorderCentered(String s) {
		final int sLen = s.length();
		if (sLen > MAX_LEN - 2) return s;
		StringBuilder sb = new StringBuilder(80);
		if (sLen == MAX_LEN - 4) {
			sb.append(dot).append(' ').append(s).append(' ').append(dot);
		} else if (sLen < MAX_LEN - 4) {
			sb.append(dot);
			sb.append(StringToolLib.space(((MAX_LEN - (sLen % 2)) - 4 - sLen) >> 1));
			sb.append(' ').append(s).append(' ');
			if (sLen % 2 != 0) sb.append(StringToolLib.space(sLen % 2));
			sb.append(StringToolLib.space(((MAX_LEN - (sLen % 2)) - 4 - sLen) >> 1));
			sb.append(dot);
		} else sb.append(s);
		return sb.toString();
	}
	/** Encapsulates a specified string with filled "border" of current 'dot'.<br>
	 * The parsed string will be centred.
	 * 
	 * @param s
	 *            : the string to encapsulate
	 * @return the completed string */
	@SuppressWarnings("unused")
	private String makeFilledBorder(String s) {
		final int sLen = s.length();
		StringBuilder sb = new StringBuilder(80);
		if (sLen < MAX_LEN - 2) {
			if (sLen == MAX_LEN - 4) {
				sb.append(dot).append(' ').append(s).append(' ').append(dot);
			} else {
				sb.append(StringToolLib.replicate(dot, ((MAX_LEN - (sLen % 2)) - 2 - sLen) >> 1));
				sb.append(' ').append(s).append(' ');
				sb.append(StringToolLib.replicate(dot, ((MAX_LEN - (sLen % 2)) - 2 - sLen) >> 1));
				if (sLen % 2 != 0) sb.append(StringToolLib.replicate(dot, sLen % 2));
			}
		} else sb.append(s);
		return sb.toString();
	}

	@SuppressWarnings("unused")
	private String insertProduktbatchkompData(int p, int raavareId) {
		String toAdd = "";
		int rbId, oprId;

		double Netto, Tara;

		ProduktBatchKompDTO DTO;
		if (!(prodbatchkomplist.size() == 0) && sortedProdbatchkomplist.get(raavareId) != null) {
			DTO = sortedProdbatchkomplist.get(raavareId);

			rbId = DTO.getRbId();
			int rbIdl = String.valueOf(rbId).length();
			oprId = DTO.getOprId();
			int oprIdl = String.valueOf(oprId).length();
			Tara = DTO.getTara();
			int Taral = String.valueOf(Tara).length();
			Netto = DTO.getNetto();
			int Nettol = String.valueOf(Netto).length();

			return doString(3, Tara) + doString(4, Netto) + doString(5, rbId) + doString(6, oprId);
			// return StringToolLib.lSet(String.valueOf(Tara), COLW[3] - Taral,
			// ' ' )
			// + StringToolLib.lSet(String.valueOf(Netto), COLW[4] - Nettol,
			// ' ')
			// + StringToolLib.lSet(String.valueOf(rbId), COLW[5] - rbIdl, ' ')
			// + StringToolLib.lSet(String.valueOf(oprId), COLW[6] - oprIdl,
			// ' ');

		}
		return toAdd;
	}

	private String doString(int col, double val) {
		String space = StringToolLib.replicate(' ', COLW[col]);
		return StringToolLib.overwrite(space, Double.toString(val), 0);
	}

}
