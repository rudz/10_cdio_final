package dtu.shared;

import dtu.shared.dto.db.OperatoerDTO;

/**
 * Login wrapper class<br>
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class LoginStatus {
	public static boolean loggedIn;
	public static int level;
	public static int userID;
	public static int userCount;
	public static OperatoerDTO attemptingLogin;
	public static String userName;
	public static String token;
	
	static {
		clear();
	}

	public static void clear() {
		token = null;
		loggedIn = false;
		level = -1;
		userID = -1;
		attemptingLogin = null;
		userName = null;
		userCount = -1;
	}
}

