package dtu.shared.misc;

import java.util.HashMap;


/**
 * Constants for danish letters using Unicode number.<br>
 * If you can do this smarter, please do so! :)<br>
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class DK {
	private static HashMap<String, String> dkIdx = new HashMap<>(7, 1.0f);
	private static HashMap<String, String> dkASEIdx = new HashMap<>(7, 1.0f);
	
	public static final String ae = "\u00E6";
	public static final String AE = "\u00C6";
	public static final String oe = "\u00F8";
	public static final String OE = "\u00D8";
	public static final String aa = "\u00E5";
	public static final String AA = "\u00C5";
	
	
	/* for the WCU/ASE */
	public static final String ASE_ae = "ae";
	public static final String ASE_AE = "AE";
	public static final String ASE_oe = "oe";
	public static final String ASE_OE = "OE";
	public static final String ASE_aa = "aa";
	public static final String ASE_AA = "AA";
	
	
	static {
		dkIdx.put("ø", oe);
		dkIdx.put("æ", ae);
		dkIdx.put("å", aa);
		dkIdx.put("Ø", OE);
		dkIdx.put("Æ", AE);
		dkIdx.put("Å", AA);
		
		dkASEIdx.put("ø", ASE_oe);
		dkASEIdx.put("æ", ASE_ae);
		dkASEIdx.put("å", ASE_aa);
		dkASEIdx.put("Ø", ASE_OE);
		dkASEIdx.put("Æ", ASE_AE);
		dkASEIdx.put("Å", ASE_AA);
	}
	
	public final static String toDk(String s) {
		String ret = s;
		for (String key : dkIdx.keySet())
			ret = ret.replaceAll(key, dkIdx.get(key));
		return ret;
	}
	
	public final static String toDkASE(String s) {
		String ret = s;
		for (String key : dkASEIdx.keySet())
			ret = ret.replaceAll(key, dkASEIdx.get(key));
		return ret;
	}
	
}
