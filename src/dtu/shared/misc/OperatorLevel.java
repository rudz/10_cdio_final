package dtu.shared.misc;

import java.util.HashMap;

/**
 * Simple convertion of operator level to title.<br>
 * <p>
 * 0 <-> "Inaktiv"<br>
 * 1 <-> "Operatoer"<br>
 * 2 <-> "Værkfører"<br>
 * 3 <-> "Farmaceut"<br>
 * 9 <-> "Admin"<br>
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class OperatorLevel {
	private static HashMap<Integer, String> olIdx = new HashMap<>(6, 1.0f);
	public static final String _0 = "Inaktiv";
	public static final String _1 = "Operat" + DK.oe + "r";
	public static final String _2 = "V" + DK.ae + "rkf" + DK.oe + "rer";
	public static final String _3 = "Famaceut";
	public static final String _9 = "Admin";
	
	static {
		olIdx.put(0, _0);
		olIdx.put(1, _1);
		olIdx.put(2, _2);
		olIdx.put(3, _3);
		olIdx.put(9, _9);
	}

	/**
	 * Convert integer value from "table" to actual string name.
	 * @param n : The integer value for role.
	 * @return The actual name as String.
	 */
	public final static String olText(int n) {
		if (olIdx.containsKey(n)) return olIdx.get(n);
		return null;
	}
	/**
	 * Convert a actual name into table integer value.
	 * @param s : The actual name as String
	 * @return The integer value for the "table"
	 */
	public final static int olInt(String s) {
		for (Integer i : olIdx.keySet()) if (olIdx.get(i).equals(s)) return i;
		return -1;
	}
}
