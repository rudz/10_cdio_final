package dtu.shared.misc;


/**
 * General purpose string managing class.<br>
 * Contains functions to handle different tasks on strings.<br>
 * <br>
 * v1.0		[rudz]	- first<br>
 * v1.01	[rudz]	- speedup for toCamelCase()<br>
 * v1.1		[rudz]	- re-wrote countString (wuhu!)<br>
 * 					- added joinCharArray() & joinStringArray()<br>
 * 					\ both with custom delimiter between elements.<br>
 * v1.11	[rudz]	- fixed asc()<br>
 * v1.12	[rudz]  - fixed joinStringArray & joinCharArray<br>
 * v1.2		[rudz]	- added hex2Dec() & dec2Hex():<br>
 *					- added binStringToInt()<br>
 * 					- added toReverseCamelCase()<br>
 * 					- added byteSizeToString()<br>
 * v1.3		[rudz]	- added msToHMS()<br>
 *					- added identicalStrings()<br>
 *					- added containingString()<br>
 * v1.31	[rudz]	- added lTrim & rTrim with custom trim.<br>
 * @author Rudy Alex Kohn [s133235]
 */
public class StringToolLib {
	/**
	 * Creates a string containing spaces
	 * @param amount : how many spaces to make
	 * @return the string containing amount spaces
	 */
	public static String space(int amount) {
		return replicate(' ', amount);
	}
	/**
	 * Replicate a char
	 * @param c : the char to replicate
	 * @param amount : how many time to replicate
	 * @return the string containing amount char
	 */
	public static String replicate(char c, int amount) {
		StringBuilder sb = new StringBuilder(amount);
		for (int i = 1; i <= amount; i++) sb.append(c);
		return sb.toString();
	}
	public static String replicate(String c, int amount) {
		StringBuilder sb = new StringBuilder(amount);
		for (int i = 1; i <= amount; i++) sb.append(c);
		return sb.toString();
	}
	/**
	 * Inject a string into another string
	 * @param into : the string to inject into
	 * @param toInsert : the string to inject
	 * @param startPos : where to inject it
	 * @return into string with the new string injected string at startpos
	 */
	public static String inject(String into, String toInsert, int startPos) {
		int len = into.length();
		if (len == 0) return toInsert;
		int lenInsert = toInsert.length();
		if (lenInsert == 0) return into;
		StringBuilder sb = new StringBuilder(len+lenInsert);
		if (startPos == len || startPos > len) sb.append(into).append(toInsert);
		else if (startPos == 1) sb.append(toInsert).append(into);
		else sb.append(into.substring(0, startPos)).append(toInsert).append(into.substring(startPos, len));
		return sb.toString();
	}
	
	/**
	 * Replaces a piece of an existing string with another string.<br>
	 * @param into : The string to overwrite in
	 * @param toInsert : The string which is put in the into string.
	 * @param startPos : Start position where it should be inserted at.
	 * @return The resulting string.
	 */
	public static String overwrite(String into, String toInsert, int startPos) {
		int len = into.length();
		if (len == 0) return toInsert;
		int lenInsert = toInsert.length();
		if (lenInsert == 0) return into;
		StringBuilder sb = new StringBuilder(len);
		// no fault check from here!
		sb.append(into.substring(0, startPos-1));
		sb.append(toInsert);
		if (startPos - 1 + lenInsert <= len - 1) {
			sb.append(into.substring(sb.length(), into.length()));
		}
		return sb.toString();
	}
	
	/**
	 * Simple trimming of spaces from the right
	 * @param in : the string to trim spaces from the right
	 * @return : the string trimmed
	 */
	public static String rTrim(String in) {
		return rTrim(in, ' ');
	}
	
	/**
	 * Simple right trim function
	 * @param in : the string to trim 'toTrim' char from the right
	 * @param toTrim : the char to trim
	 * @return : the string trimmed
	 */
	public static String rTrim(String in, char toTrim) {
        int len = in.length();
		if (len == 0 || !in.endsWith(Character.toString(toTrim))) return in;
		if (toTrim == 0) toTrim = ' ';
        int st = 0, l = len;
        char[] c = in.toCharArray();
        while (c[--l] == toTrim) st++;
		return (st > 0) ? in.substring(0, len-st) : in;
    }
	/**
	 * Simple left trim function
	 * @param in : the string to trim spaces from the left
	 * @return the string trimmed
	 */
	public static String lTrim(String in) {
		return lTrim(in, ' ');
    }
	/**
	 * Simple left trim function with custom trim char
	 * @param in : the string to trim from the left
	 * @return the string trimmed
	 */
	public static String lTrim(String in, char toTrim) {
		int len = in.length();
		if (len == 0 || !in.startsWith(Character.toString(toTrim))) return in;
        int st = 0;
        char[] c = in.toCharArray();
        while (c[st] == toTrim) st++;
		return (st > 0) ? in.substring(st, len) : in;
    }
	/**
	 * Grabs a string lenght from the right of a string and returns it.
	 * @param in : the string.
	 * @param amount : how many chars to grab?
	 * @return : the grabbed piece from the string.
	 */
	public static String right(String in, int amount) {
		int len = in.length();
		if (amount > len || amount == len || amount == 0) return in;
		return (len > 0) ? in.substring(len-amount, len) : in;
	}
	/**
	 * Grabs a piece of a string from the left of a string and returns it.
	 * @param in : the string
	 * @param amount : the amount to grab
	 * @return the grabbed piece
	 */
	public static String left(String in, int amount) {
		int len = in.length();
		if (amount > len || amount == len || amount == 0) return in;
		return (len > 0) ? in.substring(0, amount) : in;
	}
	/**
	 * Grabs a piece from a string (safely)
	 * @param in : the string to grab something from
	 * @param startPos : the start position (not index)
	 * @param endPos : the end position (not index)
	 * @return the grabbed piece of string
	 */
	public static String mid(String in, int startPos, int endPos) {
		int len = in.length();
		if (endPos > len) endPos = len;
		if (startPos < 1) startPos = 1;
		if (endPos - startPos > len || endPos < startPos) return in;
		return in.substring(startPos-1, endPos);
	}
	/**
	 * Set a string right based..<br>
	 * rSet("R", 8) -> "       R"<br>
 	 * @param in : the input string
 	 * @param len : the length
 	 * @return the newly formed padded string
	 */
	public static String rSet(String in, int len) {
		return rSet(in, len, ' ');
	}
 	/**
 	 * rSet("R", 8, "-")	 -> "-------R"<br>
 	 * rSet("LongString", 4) -> "Long"
 	 * @param in : the input string
 	 * @param len : the length
 	 * @param c : the char to pad with
 	 * @return the newly formed padded string
 	 */
	public static String rSet(String in, int len, char c) {
		int l = in.length();
		if (len < 1 || len == l) return in;
		if (l == 0) return replicate(c, len);
		if (len < l) return in.substring(0, len);
		StringBuilder ret = new StringBuilder(len+l);
		if (len > 1) ret.append(replicate(c, len-1));
		else ret.append(c);
		return ret.append(in).toString();
	}
	/**
	 * lSet("L", 8) -> "L       "
	 * @param in : the input string
	 * @param len : the length to pad
	 * @return the newly space (' ') padded string
	 */
	public static String lSet(String in, int len) {
		return lSet(in, len, ' ');
	}
	/**
	 * lSet("L", 8, '-')	 -> "L-------"<br>
	 * lSet("LongString", 4) -> "Long"
	 * @param in : input string
	 * @param len : length of setting
	 * @param c : the char to set with
	 * @return the resulting new string
	 */
	public static String lSet(String in, int len, char c) {
		int l = in.length();
		if (len < 1 || len == l) return in;
		if (l == 0) return replicate(c, len);
		if (len < l) return in.substring(0, len);
		StringBuilder ret = new StringBuilder(len+l);
		ret.append(in);
		if (len > 1) ret.append(replicate(c, len-1));
		else ret.append(c);
		return ret.toString();
	}
	/**
	 * Returns the string field (1 based) from a string based on a delimiter.<br>
	 * @param s : the input string
	 * @param index : which field to get (1 = 0 array index)
	 * @param delimiter : what to split with, can be longer than 1
	 * @return the result, if delimiter can't be found, the whole string is returned.
	 */
	public static String stringField(String s, int index, String delimiter) {
		String[] a = s.split(delimiter);
		if (index > a.length) return null;
		return a[index-1];
	}
	/**
	 * Joins a char array to a string.
	 * @param a : the char array to join.
	 * @return the joined array as string.
	 */
	public static String joinCharArray(char a[]) {
		return joinCharArray(a, "");
	}
	/**
	 * Joins a char array with custom delimiter.
	 * @param a : the char array to join
	 * @param delimiter : the delimiter to put in between the elements.
	 * @return the array joined with each element separated by delimiter argument.
	 */
	public static String joinCharArray(char a[], String delimiter) {
		int l = a.length;
		if (l == 0) return "";
		if (l == 1) return Character.toString(a[0]);
		int dl = delimiter.length();
		StringBuilder sb;
		if (dl > 0) {
			sb = new StringBuilder(l+((l-1)*dl));
			for (char c : a) sb.append(c).append(delimiter);
			return sb.toString().substring(0, (sb.length()-dl));
		}
		sb = new StringBuilder(l);
		for (char c : a) sb.append(c);
		return sb.toString();
	}
	/**
	 * Joins a string array to a string.
	 * @param a : the string array to join.
	 * @return the joined array as string.
	 */
	public static String joinStringArray(String s[]) {
		return joinStringArray(s, "");
	}
	/**
	 * Joins a string array to a string with custom delimiter.
	 * @param s : the string array
	 * @param delimiter : what to put between each element.
	 * @return the joined array as string with delimiter between each element.
	 */
	public static String joinStringArray(String s[], String delimiter) {
		int l = s.length;
		if (l == 0) return "";
		if (l == 1) return s[0];
		int dl = delimiter.length();
		StringBuilder sb;
		if (dl > 0) {
			sb = new StringBuilder(l<<1+((l-1)*dl));
			for (String c : s) sb.append(c).append(delimiter);
			return sb.toString().substring(0, (sb.length()-dl));
		}
		sb = new StringBuilder(l<<1);
		for (String c : s) sb.append(c);
		return sb.toString();
	}
	/**
	 * Counts a specific string inside another string<br>
	 * @param in : the input string
	 * @param toCount : what string to count?
	 * @return the count as an integer
	 */
	public static int countString(String in, String toCount) {
		int cLen = toCount.length();
		int len = in.length();
		if (cLen == 0 || len == 0) return 0;
		int count = len - in.replaceAll(toCount, "").length();
		return (count != len) ? count / cLen : 0;
	}
	/**
	 * Simple function to get the ASCII value of the first letter in a string.
	 * @param in : the input string (only first letter is used)
	 * @return the value
	 */
	public static int asc(String in) {
		if (in.length() == 0) return -1;
		return (in.charAt(0) - '0');
	}
	/**
	 * To camel case "THIS IS A TEST" -> "this Is A Test"
	 * @param in : the input string
	 * @return the camel string
	 */
	public static String toCamelCase(String in) {
		int len = in.length();
		if (len == 0) return "";
		if (len == 1) return in.toLowerCase();
		StringBuilder sb = new StringBuilder(len);
		int pos = 0;
		char prec, c = Character.toLowerCase(in.charAt(pos++));
		sb.append(c);
		while (pos < len) { // should be <= ?
			prec = in.charAt(pos-1); // could there be some work-around for this?
			c = in.charAt(pos++);
			if (prec == ' ' && c != ' ') sb.append(Character.toUpperCase(c));
			else sb.append(Character.toLowerCase(c));
		}
		return sb.toString();
	}
	public static String toLowerUnderscore(String input) {
		int len = input.length();
		if (len == 0) return "";
		String ret = input.toLowerCase();
		len = countString(ret, " ");
		if (len == 0) return ret;
		return ret.replaceAll(" ", "_");
	}

	
	
	/**
	 * To reverse camel case "THIS IS A TEST" -> "tHIS iS a tEST"
	 * @param in : the input string
	 * @return the reverse camel string
	 */
	public static String toReverseCamelCase(String in) {
		int len = in.length();
		if (len == 0) return "";
		if (len == 1) return in.toLowerCase();
		StringBuilder sb = new StringBuilder(len);
		int pos = 0;
		char prec, c = Character.toLowerCase(in.charAt(pos++));
		sb.append(c);
		while (pos < len) { // should be <= ?
			prec = in.charAt(pos-1); // could there be some work-around for this?
			c = in.charAt(pos++);
			if (prec == ' ' && c != ' ') sb.append(Character.toLowerCase(c));
			else sb.append(Character.toUpperCase(c));
		}
		return sb.toString();
	}
	/**
	 * Convert any hex decimal to decimal
	 * @param hex : what to convert "c8a3"
	 * @return the number
	 */
	public static int hex2Dec(String hex) {
		String he = hex.toUpperCase();
		int l = he.length();
		if (l == 0 || countString(hex, "0") == l) return 0;
		char a;
		int d = 0;
		for (int i = 0; i < l; i++) {
			a = he.charAt(i);
			d <<= 4;
			d += ((a > 60) ? a - 55 : a - 48);
		}
		return d;
	}
	/**
	 * Convert any decimal value (integer) to a hex decimal String
	 * @param dec : the number
	 * @return the hex string in upper case
	 */
	public static String dec2Hex(int dec) {
		StringBuilder hb = new StringBuilder(8);
		try {
			// validate the argument
			if (dec < 1 || dec > Integer.MAX_VALUE)
				throw new Exception("dec2Hex -> Invalid number parsed.");
			final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
			hb.setLength(8);
			for (int i = 8 - 1; i >= 0; --i) {
				hb.setCharAt(i, hex[dec & 0x0F]);
				dec >>= 4;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hb.toString();
	}
	/**
	 * Decimal to multiplier -> Convert a number to fx bin, oct or hex
	 * @param N : the number to convert
	 * @param m : to what? ex: 2 = bin, 8 = oct, 16 = hex
	 * @return the string of the converted number
	 */
    public static String dec2m(int N, int m) {
    	if (m == 0) return "";
    	if (N == 0) return ((m == 16) ? "00" : "0");
    	StringBuilder sb = new StringBuilder(m>>1);
        int r, n = N;
        do {
        	r = n % m;
        	sb.insert(0, (r < 10 ? r : Character.toString((char) ('A' - 10 + r))));
        	n /= m;
        } while (n > 0);
        // check for uneven length and pad with a zero
        return (m == 16 && (sb.length() & 1) == 1) ? sb.insert(0, '0').toString() : sb.toString();
    }
    /**
     * Converts a string of binary numbers to a value<br>
     * Only 1's and 0's are used.
     * @param s : the input string containing the binary string..
     */
    public static long binStringToInt(String s) {
    	int l = s.length();
    	if (l == 0) return 0;
    	try {
    		Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return 0;
		}
    	char c;
    	long val = 0;
    	for (int i = 0; i < l - 1; i++) {
    		c = s.charAt(i);
    		if (c == '1') val = val++ << 1;
    		else if (c == '0') val <<= 1;
    	}
    	return val;
    }
    /**
     * Converts a number, primarily used for file sizes to readable.<br>
     * Fx if the filesize is 19922944 -> "19.0 MB"
     * @param l
     * @param decimals
     * @return
     * 
     */
    
    // DOES NOT WORK IN GWT!
    
//	public static String byteSizeToString(long bytes, boolean si) {
//		int unit = si ? 1000 : 1024;
//		if (bytes < unit) return bytes + " B";
//		int exp = (int) (Math.log(bytes) / Math.log(unit));
//		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
//		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre).replace(',', '.');
//	}
	/**
	 * Convert an number to formatted string "xx:xx:xx" for any given time.
	 * @param milliseconds : the time in milliseconds.
	 * @return the time in HR:MI:SE format
	 */
	public final static String msToHMS(long milliseconds) {
		int hr = (int)((milliseconds / 3600000.0 > 0) ? Math.floor(milliseconds / 3600000.0) : Math.ceil(milliseconds / 3600000.0));
		int min0 = (int)(((double)(milliseconds % 3600000) > 0) ? Math.floor(milliseconds % 3600000) : Math.ceil(milliseconds % 3600000));
		int min = (int)((min0 / 60000.0 > 0) ? Math.floor(min0 / 60000.0) : Math.ceil(min0 / 60000.0));
		int sec = (int)(((double)(min0 % 60000) > 0) ? Math.floor(min0 % 60000) : Math.ceil(min0 % 60000));
		sec = (int)((sec / 1000.0 > 0) ? Math.floor(sec / 1000.0) : Math.ceil(sec / 1000.0));
		StringBuilder sb = new StringBuilder(10);
		if (hr < 10)  sb.append('0');
		sb.append(hr).append(':');
		if (min < 10) sb.append('0');
		sb.append(min).append(':');
		if (sec < 10) sb.append('0');
		sb.append(sec);
		return sb.toString();
	}
	/**
	 * Checks if parsed strings are identical
	 * @param s : the strings to check.. can be , seperated or an array
	 * @return true if all are identical
	 */
	public static boolean identicalStrings(String ... s) {
		int l = s.length;
		if (l < 2) return false;
		for (int i = 0; i < l - 2; i++) if (s[i].equals(s[i+1])) return true;
		if (s[l-2].equals(s[l-1])) return true;
		return false;
	}
	/**
	 * Checks if one string contains another string from parsed strings.
	 * @param s : the strings to check.. can be , seperated or an array
	 * @return true if one string contains another.
	 */
	public static boolean containingStrings(String ... s) {
		int l = s.length;
		if (l < 2) return false;
		for (int i = 0; i < s.length - 1; i++) {
			for (int j = i; j < s.length - 1; j++) if (s[i].contains(s[j]) || s[j].contains(s[i])) return true;
		}
		return false;
	}
	
	/**
	 * Blazing fast byte array to hexadecimal converter.<br>
	 * @param data : the input array
	 * @return the hexstring
	 */
	public static String byteArrToHexString(byte[] data) {
		StringBuilder sb = new StringBuilder((data.length << 1) + 1);
		int hb;
		for (byte b : data) {
			hb = (b >>> 4) & 0x0F;
			for (int i = 0; i < 2; i++) {
				if (hb >= 0 && hb <= 9) sb.append((char) ('0' + hb));
				else sb.append((char) ('a' + (hb - 10)));
				hb = b & 0x0F;
			}
		}
		return sb.toString();
	}
}
