package dtu.shared.validate;

import dtu.shared.misc.DK;

/**
 * Updated and FIXED password checker!<br>
 * @author Rudy Alex Kohn [s133235]
 */
public class PasswordValidator {

	private static char[] charsSpecial		= new char[] {'.', '-', '_', '+', '!', '?', '='};
	private static char[] upperCharBounds	= new char[] {'9', 'z', 'Z'};
	private static char[] lowerCharBounds	= new char[] {'0', 'a', 'A'};

	private final static int GROUP_COUNT = 3;
	private final static int GROUPS_REQ = 3;
	public final static int PW_LENGTH_REQ = 6;

	/* regex to find if pw contains all four groups */
	private final static String PW_REGEX1 = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.-_+!?=]).{" + PW_LENGTH_REQ + ",40})";
	
	/**
	 * Determine if the characters in the password are legal.<br>
	 * Look at the current password rules for more information.
	 * @param pw : The password to check
	 * @return The amount of characters that are invalid in the parsed password.
	 */
	private static int isLegalChars(String pw) {
		char c;
		int len = pw.length();
		boolean isOkay;
		for (int i = 0; i < len; i++) {
			c = pw.charAt(i);
			isOkay = ((c >= lowerCharBounds[0] & c <= upperCharBounds[0]) 
					| (c >= lowerCharBounds[1] & c <= upperCharBounds[1])
					| (c >= lowerCharBounds[2] & c <= upperCharBounds[2]));
			if (!isOkay) {
				for (char kk : charsSpecial) if (c == kk) return 0;
				return i+1;
			}
		}
		return 0;
	}
	
	/**
	 * Checks if String contains any special char's
	 * @param pw
	 * @return true if String contains special char's
	 */
	private static boolean chkStringSpecialChars(String pw) {
		for (int i = 0; i < pw.length(); i++) {
			char c = pw.charAt(i);
			for (char jj : charsSpecial)
				if (c == jj)
					return true;
		}
		return false;
	}
	
	/**
	 * Return number of char groups present in a string.
	 * @param String to be checked
	 * @return integer with number of char groups present
	 */
	private static int rtnNumberOfCases(String pw) {
		int charGroup[] = { 0, 0, 0 };
		for (int i = 0; i < pw.length(); i++) {
			char c = pw.charAt(i);
			// CharBounds[0] represents numbers, CharBounds[1] represents
			// lowercase etc
			// This for loop iterates through each case group, checks if they
			// are present in the string
			for (int j = 0; j < GROUP_COUNT; j++) {
				if (c >= lowerCharBounds[j] && c <= upperCharBounds[j]) {
					if (charGroup[j] == 0)
						charGroup[j++] = 1;
				}
			}
		}
		//Return value of the 3 chargroups(a,A,0) plus specialcase
		return (charGroup[0] + charGroup[1] + charGroup[2] + ((chkStringSpecialChars(pw)) ? 1 : 0));
	}

	/**
	 * Verify that the parsed password adheres to the rules.<br>
	 * This function is 'strict', meaning that the user id must not be located<br>
	 * in the password either.
	 * @param id : The user ID.
	 * @param pw : The password to check.
	 * @return true if the password is valid, otherwise false.
	 */
	public static boolean isPasswordValidStrict(int id, String pw) {
		if (pw.contains(Integer.toString(id))) return false;
		if (pw.matches(PW_REGEX1)) return true;
		return isPasswordValid(pw);
	}

	/**
	 * Verify parsed password if it adheres to the rules.
	 * @param pw : The password
	 * @return true if the password is valud, otherwise false
	 */
	public static boolean isPasswordValid(String pw) {
		if ("".equals(pw.trim())) return false;
		if (pw.length() < PW_LENGTH_REQ) return false;
		if (isLegalChars(pw) > 0) return false;
		if (rtnNumberOfCases(pw) < GROUPS_REQ) return false;
		return true;
	}

	/**
	 * Returns the requirements for the password as string.
	 * @return : String containing descriptive information about the rules.
	 */
	public static String getRequirements() {
		StringBuilder sb = new StringBuilder(100);
		sb.append(DK.toDk("1) Kodeordet må ikke være blankt\n"));
		sb.append(DK.toDk("2) Kodeordet skal min. længde af ")).append(PW_LENGTH_REQ).append("\n");
		sb.append(DK.toDk("3) Kodeordet må ikke indeholde brugerID\n"));
		sb.append(DK.toDk("4) Kodeordet skal indeholde 3 ud af 4 af disse karaktergrupper :\n"));
		sb.append(DK.toDk(" - 'a-z'\n - 'A-Z'\n - '0-9'\n - Specielle karakterer : '")).append(new String(charsSpecial)).append("'\n");
		return sb.toString();
	}
	
}
