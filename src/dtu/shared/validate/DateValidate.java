package dtu.shared.validate;

/**
 * Validate a given date.
 * @author Rudy Alex Kohn [s133235]
 */
public abstract class DateValidate {
	public static int validDate(int day, int month, int year) {
		if (day < 1 || day > 31) return -1;
		else if (month < 1 || month > 12) return -2;
		else if (day == 31 && (month == 4 || month == 6 || month == 11)) return -1;
		else if (month == 2 && day > 28)
			if ((day > 29) || (!(year % 4 == 0 && (year % 100 != 0 || year % 400 == 0) && year != 3600))) return -1;
		else if (year < -50000 || year > 50000) return -3;
		return 0;
	}
}
