
package dtu.shared.validate;

import java.util.HashMap;
import com.google.gwt.user.client.ui.IntegerBox;
import dtu.shared.LoginStatus;

/** <p>
 * FieldVerifier validates that the name the user enters is valid.
 * </p> */
public class FieldVerifier {

	/** Verifies that the specified name is valid for our service.
	 * 
	 * @param name
	 *            the name to validate
	 * @return true if valid, false if invalid */
	public static boolean isValidName(String name) {
		return isValidName(name, 1, 30);
//		/* check for invalid names! */
//		if (name == null || name.length() == 0 || name.trim().length() != name.length() || name.contains("'")) return false;
//		return name.length() <= 30;
	}
	
	public static boolean isValidName(String name, int minLen, int maxLen) {
		if (name == null || name.length() == 0 || name.trim().length() != name.length() || name.contains("'")) return false;
		if (name.length() < minLen || name.length() > maxLen) return false;
		return true;
	}

	public static boolean isValidINI(String name, String ini) {
		if (ini.length() > 3 || ini.length() > name.length()) return false;
		return isValidName(ini);
	}

	public static boolean isValidLvl(String text) {
		if (text.matches("[0-9]")) {
			int newLvl = Integer.parseInt(text);
			if (LoginStatus.level == 9) return true;
			if (newLvl > LoginStatus.level) return false;
			if (LoginStatus.level < 9 && newLvl == 0) return false;
			return true;
		}
		return false;
	}
	/** Call the appropiate validation function.
	 * 
	 * @param cpr
	 *            : the CPR as string
	 * @return true if valid, or false if invalid */
	public static boolean isValidCpr(String cpr) {
		if (cpr.length() < 11) return false;
		return CPRValidering.validateCPR(cpr) > -1;
	}

	/** Validate password for correctness according to current rules.
	 * 
	 * @param userID
	 *            : The user ID
	 * @param pw
	 *            : The first password
	 * @param pw2
	 *            : The second password (re-typed first password)
	 * @return true if both are identical and follow the rules, else false. */
	public static boolean isValidPW(int userID, String pw, String pw2) {
		if (!pw.trim().equals(pw2) || !pw2.trim().equals(pw) || (pw.trim().length() == 0)) return false;
		return PasswordValidator.isPasswordValidStrict(userID, pw);
	}

	/* RECEPT and RECECPTKOMP INPUT VALIDATION */
	
	public static boolean isValidTolerance(double tolerance) {
		return tolerance >= 0.1d && tolerance <= 10.0d;
	}
	
	public static boolean isValidNomNetto(double tolerance){
		return tolerance > 0.05d && tolerance <= 5d;
	}
	
	public static boolean isValidReceptNetto(double netto) {
		return netto >= 0.05d && netto <= 20.0d;
	}
	
	public static boolean isValidSequenceNumber(HashMap<Integer, IntegerBox> map, IntegerBox box){
		boolean returnBool = true;
		
		
		IntegerBox curBox;
		for(int i = 2; i <= map.size()+1; i++){
			
			curBox = map.get(i); 			
			if (curBox.getValue() == box.getValue() && curBox != box ){				
				returnBool = false;
			}			
		} return returnBool;
	}
	
	public static boolean isValidSequence(HashMap<Integer, IntegerBox> map, IntegerBox box){
		
		int checkSum = 0;
		
		for(int i = 1; i <= map.size(); i++){			
			checkSum = checkSum + i;
		}	
		
		for(int i = 2; i <= map.size()+1; i++){
			checkSum = checkSum - map.get(i).getValue();			
		}	
		
		if(checkSum == 0) return true;
		box.setStyleName("gwt-TextBox-invalidEntry");
		return false;
	}
	
	/**
	 * checks if ip contains numbers and . or : for ipv6 addresses
	 * @param ip
	 * @return
	 */
	public static boolean isValidIp(String ip){
		String[] splitted = new String[7];
		splitted = ip.split("\\.");
		if(splitted.length != 4) {
			return false;
		}
		for (int i = 0; i < splitted.length; i++) {
			try {
			int ipInt =Integer.parseInt(splitted[i]);
			if (ipInt > 255 || ipInt < 0) {
				System.out.println(ipInt);
				return false;
			}
			} catch (NumberFormatException e) {
				return false;
			}			
		}	
		return true;
	}
	
	/**
	 * checks if port is below 65535 and not in the standard range of other processes
	 * @param port
	 * @return
	 */
	public static boolean isValidPort(int port){
		return port > 1024 && port < 65535;
	}
}


