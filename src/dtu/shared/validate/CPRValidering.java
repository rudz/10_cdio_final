package dtu.shared.validate;

import java.io.Serializable;
import java.util.Date;


/**
 * Validate a "real" full danish cpr number.<br>
 * <p>Format of danish CPR number:<br>
 * DDMMYY-NNNC :<br>
 * DD = Day<br>
 * MM = Month<br>
 * YY = Year<br>
 * NNN = Sequence (includes century)<br>
 * C = Checksum (includes sex)<br>
 * @author Rudy Alex Kohn [s133235]
 */
public class CPRValidering extends DateValidate implements Serializable {
	private static final long serialVersionUID = -948208371921265596L;
	private final String cpr;
	
	public CPRValidering(String cpr) {
		this.cpr = cpr;
	}
	/**
	 * Validate CPR number.
	 * @param cpr : the cpr number in format ######-####
	 * @return Age if valid, else -1
	 */
	public static int validateCPR(String cpr) {
		CPRValidering cprVal = new CPRValidering(cpr);
		int age = -1;
		if (cprVal.isValid()) {
			age = cprVal.getAge();
			if (age > 100) age -= 100;
		}
		return age;
	}
	/**
	 * Just to get the age quickly if the cpr already has passed validation!
	 * @param cpr : the cpr ######-####
	 * @return age as integer.
	 */
	public static int getPersonAge(String cpr) {
		return new CPRValidering(cpr).getAge();
	}
	/**
	 * Validation entry-point
	 * @return true if valid, else false.
	 */
	public boolean isValid() {
		if (cpr.length() != 11 || cpr.charAt(6) != '-' ) return false;
		if (dd() > 31 || mm() > 12 || century() < 1) return false;
		if (validDate(dd(), mm(), Integer.parseInt(Integer.toString(century()).substring(0, 1) + Integer.toString(yy()))) < 0) return false;
		return isValidNumbers();
	}

	
	/* age calculation */
	
	/**
	 * Calculate the age of a person from their cpr number.
	 * @return age in years as integer.
	 */
	@SuppressWarnings("deprecation")
	public int getAge() {
		Date now	= new Date();
		int bYear	= century() + yy();
		int nYear	= now.getYear()+1900;
		int nMM		= now.getMonth()+1;
		int bMM		= mm();
		int bDay	= dd();
		int result = nYear - bYear;
		if (bMM > nMM) result--;
		else if (bMM == nMM) {
			int nowDay = now.getDate();
			if (bDay > nowDay) result--;
		}
		return result;
	}
	/**
	 * Calculate century of which the person was born.
	 * @return the century as integer.
	 */
	private int century(int value) {
		switch (value) {
			case 0:case 1:case 2:case 3:
				return 1900;
			case 4:case 9:
				if (yy() < 37) return 2000;
				return 1900;
			case 5:case 6:case 7:case 8:
				if (yy() < 37) return 2000;
				else if (yy() > 57) return 1800;
		}
		return 0;
	}
	private int century() {
		return century(numericValue(cpr.charAt(7)));
	}
	
	/* helper methods */
	private boolean isValidNumbers() {
		final int[] ix =  {0, 1, 2, 3, 4, 5, 7, 8, 9, 10};
		final int[] ch =  {4, 3, 2, 7, 6, 5, 4, 3, 2, 1};
		int temp = 0;
		for (int i = 0; i < 10; i++) {
			if (!Character.isDigit(cpr.charAt(ix[i]))) return false;
			temp += ch[i] * numericValue(cpr.charAt(ix[i]));
		}
		return ((temp % 11) == 0);
	}
	@SuppressWarnings("unused")
	private boolean isMale() {
		return odd(numericValue(cpr.charAt(10)));
	}
	@SuppressWarnings("unused")
	private boolean isFemale() {
		return even(numericValue(cpr.charAt(10)));
	}
	private int dd() {
		return Integer.parseInt(cpr.substring(0, 2));
	}
	private int mm() {
		return Integer.parseInt(cpr.substring(2, 4));
	}
	private int yy() {
		return Integer.parseInt(cpr.substring(4, 6));
	}
	private boolean odd(int v) {
		return ((v & 1) == 1);
	}
	private boolean even(int v) {
		return ((v & 1) == 0);
	}
	private int numericValue(char c) {
		return (c - '0');
	}
	
//	public static void main(String[] args) {
//		// generates 10 fake cpr's for internal use :
////		long start = System.nanoTime();
//		int i;
////
////		int nowMonth = now.getMonth()+1;
////		int nowYear = now.getYear()+1900;
////		int nowDay = now.getDate();
//		int nowMonth = 11;
//		int nowYear = 2002;
//		int nowDay = 24;
//		int count = 0;
//		String base = (nowDay < 10 ? "0" + Integer.toString(nowDay) : Integer.toString(nowDay)) + (nowMonth < 10 ? "0" + Integer.toString(nowMonth) : Integer.toString(nowMonth)) + Integer.toString(nowYear).substring(2, 4) + "-";
////		System.out.println(base);
//		for (i = 1000; i < 10000; i++) {
//			if (CPRValidering.validateCPR(base + Integer.toString(i)) > 0) {
//				System.out.println("Valid CPR : " + base + Integer.toString(i) + " alder : " + CPRValidering.getPersonAge(base + Integer.toString(i)) + " år");
//				count++;
//			}
//		}
////		System.out.println(System.nanoTime() - start + " ns");
////		System.out.println(count);
//	}
	
	
	
}