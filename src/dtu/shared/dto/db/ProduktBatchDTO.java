
package dtu.shared.dto.db;

import java.io.Serializable;


public class ProduktBatchDTO implements Serializable {

	private static final long	serialVersionUID	= 7179292618672678140L;

	private int					pbId;

	// 0: ikke paabegyndt, 1: under produktion, 2: afsluttet
	private int					status;

	private int					receptId;

	private String				receptName;
				
	private String oprettet, start, slut;
	
	public ProduktBatchDTO() {
	}

	public ProduktBatchDTO(int pbId, int status, int receptId, String receptName, String oprettet, String start, String slut) {
		this.pbId = pbId;
		this.status = status;
		this.receptId = receptId;
		this.receptName = receptName;
		this.oprettet = oprettet;
		this.start = start;
		this.slut = slut;
	}
	/**
	 * Overloaded constructor for extracting data
	 * @param pbId
	 * @param status
	 * @param receptId
	 * @param receptName
	 */
	public ProduktBatchDTO(int pbId, int status, int receptId, String receptName){
		this.pbId = pbId;
		this.status = status;
		this.receptId = receptId;
		this.receptName = receptName;
	}
	
	public ProduktBatchDTO(int pbId, int status, int receptId) {
		this.pbId = pbId;
		this.status = status;
		this.receptId = receptId;
	}

	public int getPbId() {
		return pbId;
	}
	public void setPbId(int pbId) {
		this.pbId = pbId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getReceptId() {
		return receptId;
	}
	public void setReceptId(int receptId) {
		this.receptId = receptId;
	}
	public String getReceptName() {
		return receptName;
	}
	public void setReceptName(String receptName) {
		this.receptName = receptName;
	}
	@Override
	public String toString() {
		return pbId + "\t" + status + "\t" + receptId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pbId;
		result = prime * result + receptId;
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ProduktBatchDTO other = (ProduktBatchDTO) obj;
		if (pbId != other.pbId) return false;
		if (receptId != other.receptId) return false;
		if (status != other.status) return false;
		return true;
	}

	public String getOprettet() {
		return oprettet;
	}

	public void setOprettet(String oprettet) {
		this.oprettet = oprettet;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getSlut() {
		return slut;
	}

	public void setSlut(String slut) {
		this.slut = slut;
	}

}
