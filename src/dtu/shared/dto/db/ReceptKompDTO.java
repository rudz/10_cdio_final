
package dtu.shared.dto.db;

import java.io.Serializable;

public class ReceptKompDTO implements Serializable {

	private static final long	serialVersionUID	= 4543586387862943208L;
	private int		receptId;	// auto genereres fra 1..n
	private int		raavareId;	// i omraadet 1-99999999
	private double	nomNetto;	// skal vaere positiv og passende stor
	private double	tolerance;	// skal vaere positiv og passende stor
	private int		sequence;
	
	public ReceptKompDTO() { }
	public ReceptKompDTO(int receptId, int raavareId, double nomNetto, double tolerance, int sequence) {
		this.receptId = receptId;
		this.raavareId = raavareId;
		this.nomNetto = nomNetto;
		this.tolerance = tolerance;
		this.sequence = sequence;
	}

	public double getNomNetto() {
		return nomNetto;
	}
	public int getRaavareId() {
		return raavareId;
	}
	public int getReceptId() {
		return receptId;
	}
	public double getTolerance() {
		return tolerance;
	}
	public void setNomNetto(double nomNetto) {
		this.nomNetto = nomNetto;
	}
	public void setRaavareId(int raavareId) {
		this.raavareId = raavareId;
	}
	public void setReceptId(int receptId) {
		this.receptId = receptId;
	}
	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	@Override
	public String toString() {
		return receptId + "\t" + raavareId + "\t" + nomNetto + "\t" + tolerance;
	}
}
