package dtu.shared.dto.db;

import java.io.Serializable;
import dtu.client.base64.Base64Coder;

/**
 * Operatoer Data Access Object
 * @author mn/tb
 * 
 * @author Rudy Alex Kohn [s133235]<br>
 * - Added default Base64 de-/encoding for values.<br>
 * @version 1.3
 */
public class OperatoerDTO implements Serializable {
	private static final long serialVersionUID = 63012841212371004L;
	private int id;
	private String name;
	private String ini;
	private String cpr;
	private String password;
	private String salt;
	private int level;

	public OperatoerDTO(int id, String name, String ini, String cpr, String password, String salt, int level) {	
		setName(name);
		setIni(ini);
		setCpr(cpr);
		setPassword(password);
		setSalt(salt);
		setLevel(level);
		setId(id);
	}
	
	/**
	 * Constructor without Password, used for transporting information.
	 * @param name
	 * @param ini
	 * @param cpr
	 * @param lvl
	 * @param id
	 */
	public OperatoerDTO(int id, String name, String ini, String cpr, int level) {
		this(id, name, ini, cpr, "", "", level);
	}
	
	public OperatoerDTO(){ }

	public OperatoerDTO(int id, String pw) {
		setPassword(pw);
		setId(id);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int ID) {
		this.id = ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String oprNavn) {
		this.name = oprNavn;
	}

	public String getIni() {
		return ini;
	}

	public void setIni(String ini) {
		this.ini = ini;
	}

	public String getCpr() {
		return cpr;
	}

	public void setCpr(String cpr) {
		this.cpr = cpr;
	}

	public String getPassword() {
		return Base64Coder.decodeString(password);
	}

	public void setPassword(String password) {
		if (!"".equals(password) && password != null)
			this.password = Base64Coder.encodeString(password);
	}
	
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return getId()	+ "\t"
			 + getName()	+ "\t"
			 + getIni()		+ "\t"
			 + getCpr()		+ "\t"
			 + getLevel();
	}
}
