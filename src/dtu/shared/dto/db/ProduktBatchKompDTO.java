package dtu.shared.dto.db;

import java.io.Serializable;
import java.sql.Date;

public class ProduktBatchKompDTO implements Serializable {
	private static final long	serialVersionUID	= 3164214550149079412L;
	/** produkt batch id i området 1-99999999. Vælges af brugerne */   
	private int pbId;    
	/** raavare batch id i området 1-99999999. Vælges af brugerne */ 
	private int rbId;       
	/** tara i kg */
	private double tara;
	/** afvejet nettomængde i kg */
	private double netto;
	/** Operatør-identifikationsnummer */
	private int oprId;
	/**raavare-id*/
	private int raavareId;	

	private Date createDate, startDate, doneDate;
	
	public ProduktBatchKompDTO() { }
	
	public ProduktBatchKompDTO(int pbId, int rbId, double tara, double netto, int oprId) {
		this.pbId = pbId;
		this.rbId = rbId;
		this.tara = tara;
		this.netto = netto;
		this.oprId = oprId;
	}
	public ProduktBatchKompDTO(int pbId, int rbId, double tara, double netto, int oprId, Date createDate, Date startDate, Date doneDate, int raavareId ) {
		this.pbId = pbId;
		this.rbId = rbId;
		this.tara = tara;
		this.netto = netto;
		this.oprId = oprId;
		this.createDate = createDate;
		this.startDate = startDate;
		this.doneDate = doneDate;
		this.raavareId = raavareId;
	}
	
	public ProduktBatchKompDTO(int pbId, int rbId, double tara, double netto, int oprId, Date createDate, Date startDate, Date doneDate ) {
		this.pbId = pbId;
		this.rbId = rbId;
		this.tara = tara;
		this.netto = netto;
		this.oprId = oprId;
		this.createDate = createDate;
		this.startDate = startDate;
		this.doneDate = doneDate;
	}
	
	public final int getPbId() {
		return pbId;
	}
	public final void setPbId(int pbId) {
		this.pbId = pbId;
	}
	public final int getRbId() {
		return rbId;
	}
	public final void setRbId(int rbId) {
		this.rbId = rbId;
	}
	public final double getTara() {
		return tara;
	}
	public final void setTara(double tara) {
		this.tara = tara;
	}
	public final double getNetto() {
		return netto;
	}
	public final void setNetto(double netto) {
		this.netto = netto;
	}
	public final int getOprId() {
		return oprId;
	}
	public final void setOprId(int oprId) {
		this.oprId = oprId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(Date doneDate) {
		this.doneDate = doneDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public int getRaavareId() {
		return raavareId;
	}
}
