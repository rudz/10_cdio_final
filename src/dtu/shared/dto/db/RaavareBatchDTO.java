
package dtu.shared.dto.db;

import java.io.Serializable;


public class RaavareBatchDTO implements Serializable {

	private static final long	serialVersionUID	= -2425389727905546745L;
	private int							rbId;
	private int							raavareId;
	private double						maengde;

	public RaavareBatchDTO(int rbId, int raavareId, double maengde) {
		this.rbId = rbId;
		this.raavareId = raavareId;
		this.maengde = maengde;
	}

	public RaavareBatchDTO() {	}
	public int getRbId() {
		return rbId;
	}
	public void setRbId(int rbId) {
		this.rbId = rbId;
	}
	public int getRaavareId() {
		return raavareId;
	}
	public void setRaavareId(int raavareId) {
		this.raavareId = raavareId;
	}
	public double getMaengde() {
		return maengde;
	}
	public void setMaengde(double maengde) {
		this.maengde = maengde;
	}
	@Override
	public String toString() {
		return rbId + "\t" + raavareId + "\t" + maengde;
	}
}
