package dtu.shared.dto.db;

import java.io.Serializable;


public class VaegtForbindelseDTO implements Serializable{
	
	
	 
	private static final long	serialVersionUID	= -4903596552471598822L;
		
	private String ip;
	private int port;
	private int id;
	
	public VaegtForbindelseDTO(){
		//cus we need an empty constructor
	}
	/**
	 * constructor used for inserting data, as id is auto incremented
	 * @param ip
	 * @param port
	 */
	public VaegtForbindelseDTO(String ip, int port) {		
		
		this.ip = ip;
		this.port = port;
		
	}
	/**
	 * constructor used for extracting data
	 * @param id
	 * @param ip
	 * @param port
	 */
	public VaegtForbindelseDTO(int id, String ip, int port){
		this.id = id;
		this.ip = ip;
		this.port = port;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	

}
