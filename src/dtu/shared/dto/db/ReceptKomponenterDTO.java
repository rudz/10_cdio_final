package dtu.shared.dto.db;

import java.io.Serializable;


public class ReceptKomponenterDTO implements Serializable{
	private static final long	serialVersionUID	= -1223898105525477876L;
	private int raavare_id;
	private int recept_id;
	private String navn;
	private double nom_Netto;
	private double tolerance;
	private int sekvens;
	
	public ReceptKomponenterDTO() {
	}
	
	public ReceptKomponenterDTO(int raavare_id, int recept_id, String navn, double nom_Netto, double tolerance, int sekvens){
		this.raavare_id = raavare_id;
		this.recept_id = recept_id;
		this.navn = navn;
		this.nom_Netto = nom_Netto;
		this.tolerance = tolerance;
		this.sekvens = sekvens;    
	}
	
	
	public int getRaavare_id() {
		return raavare_id;
	}
	
	public void setRaavare_id(int raavare_id) {
		this.raavare_id = raavare_id;
	}
	
	public int getRecept_id() {
		return recept_id;
	}
	
	public void setRecept_id(int recept_id) {
		this.recept_id = recept_id;
	}
	
	public String getNavn() {
		return navn;
	}
	
	public void setNavn(String navn) {
		this.navn = navn;
	}
	
	public double getNom_Netto() {
		return nom_Netto;
	}
	
	public void setNom_Netto(double nom_Netto) {
		this.nom_Netto = nom_Netto;
	}
	
	public double getTolerance() {
		return tolerance;
	}
	
	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}

	public int getSekvens() {
		return sekvens;
	}

	public void setSekvens(int sekvens) {
		this.sekvens = sekvens;
	}

}
