package functionality.response;
import data.Weightdata;
import dtu.shared.misc.StringToolLib;
/***
 * This class is responsible for checking RM20 command parameters from a
 * connected client is correct
 */
public class RM20Functions {

	private final int MAX_QUOTES = 6;
	Weightdata data;
	String dataFromClient;
	private int text1Start, text1End, text2End, text2Start, text3Start, text3End;
	private String text1, text2, text3;
	private final int MINIMUM_LENGTH = 15;
	private final int TEXT1_MAXIMUM = 24;
	private final int TEXT2_MAXIMUM = 7;
	private final int TEXT3_MAXIMUM = 7;
	private String inputType;
//	private String[] allowedInputTypes = {"1","2","3","4","5","6","7","8","9","10","11"}; // for when expanded

	public RM20Functions(Weightdata data, String dataFromClient) {
		this(data, dataFromClient, "8");
	}
	public RM20Functions(Weightdata data, String dataFromClient, String inputType) {
		this.data = data;
		this.dataFromClient = dataFromClient;
		this.inputType = inputType;
	}
	/**
	 * Extracts the text field strings from dataFromClient string
	 */
	private void createTextFieldStrings() {
		text1 = dataFromClient.substring(text1Start, text1End);
		text2 = dataFromClient.substring(text2Start, text2End);
		text3 = dataFromClient.substring(text3Start, text3End);
	}
	/**
	 * Generate index of each text fields beginning for dataFromClient String.
	 */
	private void generateTextFieldIndexes() {
		int startIndex = 0;
		text1Start = dataFromClient.indexOf("\"", startIndex + 1) + 1;
		text1End = dataFromClient.indexOf("\"", text1Start);
		text2Start = dataFromClient.indexOf("\"", text1End + 1) + 1;
		text2End = dataFromClient.indexOf("\"", text2Start);
		text3Start = dataFromClient.indexOf("\"", text2End + 1) + 1;
		text3End = dataFromClient.indexOf("\"", text3Start);
	}

	/**
	 * Sets displays on UI according to input from RM20.
	 */
	public void RM20SetDisplays() {
//		data.setInstructionDisplay(text1);
		data.setPrompt(text1);
		data.setDefaultText(text2);
//		data.setInline(text3);
		data.setUnit(text3);
		synchronized (data) {
			data.notify();
		}
	}
	/***
	 * This method is combining various checks on the input for RM20 from client.
	 * @param dataFromClient : raw input from client
	 * @return true if the command is a valid RM20 command
	 */
	public boolean checkRM20() {
		if (!dataFromClient.startsWith("RM20 ")) return false;
		if (StringToolLib.countString(dataFromClient, "\"") != MAX_QUOTES) return false;
		generateTextFieldIndexes();
		createTextFieldStrings();
		if (!has8AfterRM20()) return false;
		if (!textFieldsIsCorrectLength()) return false;
		if (dataFromClient.trim().length() < MINIMUM_LENGTH) return false;
		if (text3End + 1 != dataFromClient.trim().length()) return false;
		if (hasCharactersOutsideQuotes()) return false;
		return true;
	}
	/***
	 * Checks if there is a allowed input type after RM20<br>
	 * and unspecfied number of spaces
	 * @return true if a rm20 8 command
	 */
	private boolean has8AfterRM20() {
		// locate the required input type which comes after "RM20" and before
		// first " in RM20 command
		return (inputType.equals(dataFromClient.substring(5, text1Start - 1).trim()));
//		String inputType = dataFromClient.substring(5, text1Start - 1).trim();
//		for (int i = 0; i < allowedInputTypes.length; i++) {
//			if(allowedInputTypes[i].equals(inputType))  return true;
//		}
//		return false;
	}	
	/**
	 * Verify that there doesn't exists any chars between text strings
	 * @return true if there exists characters between strings
	 */
	private boolean hasCharactersOutsideQuotes() {
		String[] tmp = dataFromClient.split("\"");
		return (tmp[2].trim().length() != 0 || tmp[4].trim().length() != 0);
	}
	/***
	 * Checks each text field is not longer than maximum
	 * @return true if length of input text strings are in required range
	 */
	private boolean textFieldsIsCorrectLength() {
		if (text1.length() > TEXT1_MAXIMUM)
			return false;
		if (text2.length() > TEXT2_MAXIMUM)
			return false;
		if (text3.length() > TEXT3_MAXIMUM)
			return false;
		return true;
	}
}
