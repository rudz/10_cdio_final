package functionality.response;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import data.Weightdata;
import functionality.statics.Cons;

/**
 * RM20Thread makes calls to check if input from user is correct to RM20 command format.<br>
 * It waits on a notify() from WeightData, sent after server console
 * user has entered RM20 response, then sends RM20 A answer to the client
 */
public class RM20Thread implements Runnable {

	Socket socket;
	String dataFromClient;
	RM20Functions RM20;
	Weightdata weightData;
	OutputStreamWriter osw;

	public RM20Thread(Weightdata data, Socket socket, String dataFromClient) {
		this.socket = socket;
		this.dataFromClient = dataFromClient;
		this.weightData = data;
	}
	/**
	 * Checks if RM20 input is correct, then sends out responses to client after
	 * server has entered RM20 response
	 */
	@Override
	public void run() {
		try {
			osw = new OutputStreamWriter(new BufferedOutputStream(socket.getOutputStream()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			RM20 = new RM20Functions(weightData, dataFromClient);
			if (RM20.checkRM20()) {
				osw.write("RM20 B" + Cons.CRLF);
				osw.flush();
				weightData.setRM20IsWaiting(true);
				RM20.RM20SetDisplays();
				try {
					synchronized (weightData.getRM20Lock()) {
						weightData.getRM20Lock().wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (weightData.getRM20Response().equals("CANCEL")) {
					osw.write("RM20 C");
				} else {
					osw.write("RM20 A \"");
					osw.write(("".equals(weightData.getRM20Response()) && !""
							.equals(weightData.getDefaultText())) ? weightData
							.getDefaultText() : weightData.getRM20Response());
					osw.write("\"" + Cons.CRLF);
				}
			} else {
				osw.write("RM20 L" + Cons.CRLF);
			}
			weightData.setDefaultText("");
			osw.flush();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
