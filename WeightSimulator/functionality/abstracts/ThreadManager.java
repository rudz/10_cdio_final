package functionality.abstracts;

import data.Weightdata;
import functionality.interfaces.IThreadManager;
import ui.IUImenu;

/**
 * To manage top-level thread classes with some common stuff in them.
 * @author rudz
 */
public abstract class ThreadManager implements IThreadManager {

	protected Weightdata weightData;
	protected IUImenu tUImenu;

	public ThreadManager(IUImenu tUImenu, Weightdata weightData) {
		this.tUImenu = tUImenu;
		this.weightData = weightData;
	}
}
