package functionality.abstracts;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import dtu.shared.misc.StringToolLib;
import functionality.interfaces.IWeightFunction;

/**
 * Abstract factory for all the commands being used in the WeightSimulator
 * Command's can be added one at a time by creating a new class extending Command.
 */
public abstract class Command  {
	
	protected Weightdata weightData;
	protected OutputStreamWriter osw;
	protected IWeightFunction wfunc;
	protected boolean isSimple;
	
	public Command(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		this.osw = osw;
		this.weightData = weightData;
		this.wfunc = wfunc;
		this.isSimple = isSimple;
	}
	/***
	 * General execute method used to execute any of the commands to clients
	 * @param dataFromClient
	 * @throws IOException
	 */
	public abstract void execute(String dataFromClient) throws IOException;
	
	protected abstract boolean isValid(String dataFromClient) throws IOException;

	/***
	 * Makes sure S and T responds to the client is in accordance to SISC protocol
	 * @param cmd
	 * @param input <double weight>
	 * @return Output according to SISC standards
	 */
	protected static String Fix(String cmd, double input) {
		StringBuilder sb = new StringBuilder();
		String parse =  Double.toString(input);
		int l = parse.length(), counter, count;
		char[] D = { '0','.','0','0','0','0','0'};	    
		
		counter = 0; 
		for (int i = parse.indexOf('.') -1 ; i < l && i < 4+parse.indexOf('.'); i++) {
			D[counter++] = parse.charAt(i);
		}
		
		sb.insert(0, cmd);
		sb.append(' ').append('S').append(StringToolLib.space(5));
		if (parse.contains("-")) sb.append('-'); //  (" S     -");
		else sb.append(' '); //   (" S      ");
		
		count = 9;
		for (int i = 0; i < 5; i++) sb.insert(count++, D[i]);
		
		return sb.append(" Kg").toString();
	}

}
