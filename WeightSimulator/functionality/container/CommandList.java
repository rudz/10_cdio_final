
package functionality.container;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import data.Weightdata;
import functionality.abstracts.Command;
import functionality.commands.level0.Q;
import functionality.commands.level0.S;
import functionality.commands.level1.D;
import functionality.commands.level1.DW;
import functionality.commands.level1.T;
import functionality.commands.remote.P110;
import functionality.commands.remote.P111;
import functionality.commands.remote.P121;
import functionality.commands.remote.RM20;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


/** Iterates through the list of commands and executes the one corresponding to
 * input from user on client
 * 
 * @author hylle */
public class CommandList {

	public List<Command>	commandList	= new ArrayList<>();
	// private OutputStreamWriter osw;

	// For weight fixing
	OutputStreamWriter		osw;
	Weightdata				data;
	IWeightFunction			func;
	RM20 rm20ninja;

	public CommandList(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, Socket sock, BufferedReader instream) {

		// this.osw = osw;
		this.osw = osw;
		this.data = weightData;
		this.func = wfunc;

		D d = new D(osw, weightData, wfunc, false);
		commandList.add(d);

		DW dw = new DW(osw, weightData, wfunc, true);
		commandList.add(dw);

		P111 p111 = new P111(osw, weightData, wfunc, false);
		commandList.add(p111);

		S s = new S(osw, weightData, wfunc, true);
		commandList.add(s);

		T t = new T(osw, weightData, wfunc, true);
		commandList.add(t);

		RM20 rm20 = new RM20(osw, weightData, wfunc, sock, false);
		rm20ninja = rm20;
		commandList.add(rm20);

		Q q = new Q(osw, weightData, wfunc, true, sock, instream);
		commandList.add(q);

		P110 p110 = new P110(osw, weightData, wfunc, true);
		commandList.add(p110);

		P121 p121 = new P121(osw, weightData, wfunc, true);
		commandList.add(p121);

	}
	/** Runs a specified command.<br>
	 * Iterates through commandList until it matches command given.<br>
	 * Compares input String with a list of class names in a List.<br>
	 * 
	 * @param dataFromClient
	 *            input from telnet client
	 * @throws IOException
	 *             if there is a problem executing command */
	public void executeCommand(String dataFromClient) throws IOException {
		System.out.println(dataFromClient);
		if (dataFromClient.startsWith("RM49")) {
			data.setInstructionDisplay(dataFromClient);
			osw.write("RM49 B\r\n");
			synchronized (data) {
				data.notify();
			}
			osw.flush();
			
			rm20ninja.execute("RM20 8 \"Bekræft besked\" \"ok\" \"RM49\"");
			
			
		}

		if (dataFromClient.startsWith("P110")) {
			osw.write("P110 A");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
		}

		if (dataFromClient.startsWith("RM30 ")) {
			data.setInstructionDisplay(dataFromClient);
			osw.write("RM30 B");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
		}

		if (dataFromClient.startsWith("RM39 1")) {
			osw.write("RM39 A");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
			
			rm20ninja.execute("RM20 8 \"Afvej på vaegt\" \"ok\" \"RM30\"");
		}
		if (dataFromClient.startsWith("P111 \"Fjern")) {
			osw.write("P111 A");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
			
			rm20ninja.execute("RM20 8 \"Fjern beholder\" \"ok\" \"Text3\"");
		}
		

		if (dataFromClient.startsWith("P120")) {
			osw.write("P120 A");
			synchronized (data) {
				data.notify();
			}
			osw.write(Cons.CRLF);
			osw.flush();
		}

		if (dataFromClient.startsWith("P121")) {
			data.setInstructionDisplay(dataFromClient);
			osw.write("P121 A");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
		}

		if (dataFromClient.startsWith("RM39 0")) {
			osw.write("RM39 A");
			synchronized (data) {
				data.notify();
			}

			osw.write(Cons.CRLF);
			osw.flush();
		}
		

		String cmd[] = dataFromClient.split(" ");
		for (int i = 0; i < commandList.size(); i++) {
			Command command = commandList.get(i);

			if (cmd[0].equals(command.getClass().getSimpleName())) {
				command.execute(dataFromClient);
				break;
			}
		}
	}
	/** Returns true if a command matches the given input. Check's a String input
	 * against a list of class names.
	 * 
	 * @param dataFromClient
	 *            input from telnet client
	 * @return true if command exists */
	public boolean isValidCommand(String dataFromClient) {
		if (dataFromClient.startsWith("RM49"))
			return true;
		if (dataFromClient.startsWith("P110"))
			return true;
		if (dataFromClient.startsWith("RM30"))
			return true;
		if (dataFromClient.startsWith("RM39 1") )
			return true;
		if (dataFromClient.startsWith("P120") )
			return true;
		if (dataFromClient.startsWith("P121") )
			return true;
		if (dataFromClient.startsWith("RM39 0"))
			return true;

		
		String cmd[] = dataFromClient.split(" ");
		for (Command command : commandList) {
			if (cmd[0].equals(command.getClass().getSimpleName())) { return true; }
		}
		return false;
	}
}
