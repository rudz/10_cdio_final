package functionality;

// TODO : To be re-designed!!

import data.WeightConnectionData;
/***
 * Sets port in ConnectionData variable according to first index of a String array
 */
public class ArgumentReader {

	private String[] args;
	private WeightConnectionData cData;
	
	public ArgumentReader(String[] args, WeightConnectionData cData) {
		this.args = args;
		this.cData = cData;
	}
	private static String getArg(String arg) throws NumberFormatException {
		if (arg.contains("/port=")) return arg.replaceAll("/port=", "");
		if (arg.contains("port="))  return arg.replaceAll("port=", "");
		return arg;
	}
	/**
	 * Set the port received from the command line argument, if it is valid.
	 */
	public void setPort() {
		if (args.length == 0) {
			System.out.println("Using default port : " + Integer.toString(cData .getListeningPort()));
			System.out.println("Run with argument /port=<port> to specify custom port.");
		}
		else { // just read the first parameter and go with it!.. ignore the rest
			int port = cData.getListeningPort();
			try {
				port = Integer.parseInt(getArg(args[0]));
			} catch (NumberFormatException e) {
				System.out.println("Invalid argument for port number recieved, using default port " + Integer.toString(cData .getListeningPort()));
				e.printStackTrace();
			}
			if (port <= 1024) System.out.println("Warning, you are using a reserved port number between 0 and 1024.");
			cData.setListeningPort(port);
		}
	}

}
