package functionality;

import data.Weightdata;
import exceptions.UpperWeightLimitException;
import functionality.interfaces.IWeightFunction;

/**
 * Responsible for getting and handling data from Weightdata DTO, throw's exceptions if UpperWeightLimit is reached.
 */
public class WeightFunctions implements IWeightFunction {

	Weightdata data;
	
	public WeightFunctions(Weightdata data) {
		this.data = data;
	}
	
	@Override
	public void setToTara() throws UpperWeightLimitException {
		if (!validWeight(data.getBrutto())) throw new UpperWeightLimitException("T " +
				(data.getBrutto() <= Weightdata.WEIGHT_LOWER_LIMIT ? "-" : "+"));
		data.setTara(data.getBrutto());
	}

	@Override
	public void setTara(double amount) throws UpperWeightLimitException{
		if (!validWeight(amount)) throw new UpperWeightLimitException("T +");
		data.setTara((int)(amount*1000));
	}

	@Override
	public double getTara(){
		return (double)(data.getTara())/1000;
	}

	@Override
	public void setBrutto(double amount){
		data.setBrutto((int)(amount*1000));
	}
	
	@Override
	public double getBrutto(){
		return (double)(data.getBrutto())/1000;
	}

	/**
	 * Validates the weight based on the physical limitation of the original weight.
	 * @param amount : the weight to validate.
	 * @return true if it is within range.
	 */
	private boolean validWeight(double amount) {
		return (amount >= Weightdata.WEIGHT_LOWER_LIMIT && amount <= Weightdata.WEIGHT_UPPER_LIMIT);
	}
	/**
	 * Special getNetto for RequestHandler. Exception msg for RequestHandler ("S +")
	 * @return netto weight
	 * @throws UpperWeightLimitException
	 */
	@Override
	public double rhGetNetto() throws UpperWeightLimitException {
		if (!validWeight(data.getBrutto())) throw new UpperWeightLimitException("S " +
				((double)(data.getBrutto())/1000 <= Weightdata.WEIGHT_LOWER_LIMIT ? "-" : "+"));
		return (double)(data.getBrutto() - data.getTara())/1000;
	}
	/***
	 * The physical weight in our studies has a maximum capacity of about 6 kg,
	 *  mass > 6kg placed on the weight will result in |__| on display on physical weight
	 *  This is used on weightserver operators UI display
	 */
	@Override
	public double getNetto() throws UpperWeightLimitException{ 
		if (!validWeight(data.getBrutto())) throw new UpperWeightLimitException("|__|"); 
		return (double)(data.getBrutto() - data.getTara())/1000;
	}

	@Override
	public String getP111() {
		if (data.getUnit().equals(""))
			return data.getP111();
		else 
			return data.getUnit();
	}

	@Override
	public String getInline() {
		if (data.getPrompt().equals(""))
			return data.getInline();
		else
			return data.getPrompt();
	}
	
	public String getDisplay() {
		if (data.isRM20IsWaiting()) return "RM20: " + data.getPrompt() +" "+ data.getDefaultText() +" "+ data.getUnit();	
		if (!data.getInline().equals(""))
			return (data.getP111() + " " + data.getInline());
		try {
			return (data.getP111() + " " + getNetto());
		} catch (UpperWeightLimitException e) {
			return (data.getP111() +" " + e.getMessage());
		}
	}

	/** should use the one with double instead */
	public void addBrutto(int amount) {
		data.setBrutto(data.getBrutto()+(amount));
	}
	public void addBrutto(double amount) {
		data.setBrutto(data.getBrutto()+(int)(amount*1000));
	}

	public String getDefaultText() {
		return data.getDefaultText();
	}

	public void setRM20Respond(String text) {
		data.setRM20Response(text);
	}
	
}
