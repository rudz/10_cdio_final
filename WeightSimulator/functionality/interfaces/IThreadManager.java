package functionality.interfaces;

import java.io.IOException;

/**
 * Here is currently plenty of room to make a ton of custom exceptions :>
 * @author rudz
 *
 */
public interface IThreadManager {
	void quit() throws IOException;
}
