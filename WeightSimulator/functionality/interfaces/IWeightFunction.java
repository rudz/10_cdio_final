package functionality.interfaces;

import exceptions.UpperWeightLimitException;

public interface IWeightFunction {

	public abstract void setToTara() throws UpperWeightLimitException;

	public abstract void setTara(double amount) throws UpperWeightLimitException;

	public abstract double getTara();

	public abstract void setBrutto(double amount);

	public abstract double getBrutto();

	public abstract double getNetto() throws UpperWeightLimitException;	
	
	public abstract String getP111();
	
	public abstract String getInline();
	
	public abstract double rhGetNetto() throws UpperWeightLimitException;
	

}