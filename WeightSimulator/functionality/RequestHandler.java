package functionality;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import ui.IUImenu;
import ui.TUImenu;
import data.WeightConnectionData;
import data.Weightdata;
import functionality.abstracts.ThreadManager;
import functionality.container.CommandList;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;

/**
 * Handles the various commands from from clients(each with own socket connection), connecting to this weight simulator
 */
public class RequestHandler extends ThreadManager implements Runnable {
	private WeightConnectionData cmData;
	private Socket sock;
	private BufferedReader instream;
	private String dataFromClient;
	private OutputStreamWriter osw;
	private IWeightFunction wfunc;
	private boolean firstRun;
	private CommandList cmdList;
	
	public RequestHandler(IUImenu tUImenu, Weightdata weightData, Socket connection) throws IOException {
		super(tUImenu, weightData);
		cmData = new WeightConnectionData();
		this.sock = connection;
		instream = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		osw = new OutputStreamWriter(new BufferedOutputStream(sock.getOutputStream()));
		wfunc = new WeightFunctions(weightData);
		cmdList = new CommandList(osw,weightData,wfunc, sock, instream);
	}
	/**
	 * Parses input from the user's connection and sends it on to CommandList class.
	 */
	@Override
	public void run() {
		firstRun = false;
		try {
			TUImenu.println("Connected to " + sock.getInetAddress().getHostAddress() + ":" + sock.getPort());
			TUImenu.println("Listening on port " + cmData.getListeningPort());	
			while (true) {
				if (instream.ready()) {
					dataFromClient = instream.readLine();
					if (firstRun) {
						dataFromClient = dataFromClient.substring(21, dataFromClient.length());
						firstRun = false;
					}
						if ((dataFromClient.length() == dataFromClient.trim().length()) && cmdList.isValidCommand(dataFromClient)) {
							cmdList.executeCommand(dataFromClient);
						} else {
							osw.write("ES" + Cons.CRLF);
							osw.flush();
						}
				} else {
					try {
						Thread.sleep(50);		//Wait for instream to become ready
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			TUImenu.println("Client disconnect or in error");
		} finally {
			try {
				quit();
			} catch (IOException e) {
				System.out.println("Error shutting down RequestHandler socket.");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Closing all resources in case of error in run()
	 */
	@Override
	public void quit() throws IOException {
		instream.close();
		osw.close();
		sock.close();
	}
}
