
package functionality.commands.remote;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


public class P121 extends Command {

	public P121(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(String dataFromClient) throws IOException {

		weightData.setInstructionDisplay(dataFromClient);
		osw.write("P121 A");
		synchronized (weightData) {
			weightData.notify();
		}

		osw.write(Cons.CRLF);
		osw.flush();

	}

	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
