package functionality.commands.remote;

import java.io.IOException;
import java.io.OutputStreamWriter;

import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;

public class P111 extends Command  {

	public P111(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
	}
	/**
	 * @param dataFromClient : String
	 */
	@Override
	public void execute(String dataFromClient) throws IOException {
		if (isValid(dataFromClient)) {
			if(!weightData.isRM20IsWaiting()){
				weightData.setP111(returnP111(dataFromClient));
				osw.write("P111 A");
				synchronized (weightData) {
					weightData.notify();
				} 
			} else{osw.write("P111 I");}	


		}
		else osw.write("P111 L");
		osw.write(Cons.CRLF);
		osw.flush();
	}
	/**
	 * checks if the P111 is valid, including length and is surrounded by "", 
	 * and does not contain " besides the surrounding ones
	 * @param dataFromClient : String
	 * @return boolean
	 */
	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		int l = dataFromClient.length();
		return ((dataFromClient.charAt(5)== '"' &&dataFromClient.charAt(l-1) == '"') 
				&& l < 37 
				&& !(dataFromClient.substring(6, l-2).contains("\""))
				&& dataFromClient.indexOf('\"', 6) == l-1);
	}
	/**
	 * 
	 * @param input : String, The whole P111 input
	 * @return the message to be printed in the P111 field
	 */
	private String returnP111(String input) {
		return input.substring(6, input.length()-1);
	}
}
