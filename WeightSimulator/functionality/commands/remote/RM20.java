package functionality.commands.remote;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.response.RM20Thread;
import functionality.statics.Cons;

/***
 * Executes the RM20Thread unless a RM20 is already being processed
 */

public class RM20 extends Command {

	private Socket sock;
	
	public RM20(OutputStreamWriter osw, Weightdata weightData,
			IWeightFunction wfunc, Socket sock, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
		this.sock = sock;
	}

	@Override
	public void execute(String dataFromClient) throws IOException {
		if(weightData.isRM20IsWaiting()) {
			osw.write("RM20 I" + Cons.CRLF);
			osw.flush();
		} else if(!weightData.isRM20IsWaiting()) {
			Runnable RM20Thread = new RM20Thread(weightData, sock, dataFromClient);
			new Thread(RM20Thread).start();
		}
	}

	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return false;
	}
	
}
