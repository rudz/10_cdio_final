package functionality.commands.remote;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


public class P110 extends Command{

	public P110(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(String dataFromClient) throws IOException {
		if (isValid(dataFromClient)){
			weightData.setP111("");
			synchronized (weightData) {
				weightData.notify();
			}
			osw.write("P111 A");
		} else {osw.write("P111 L");}
		
		osw.write(Cons.CRLF);
		osw.flush();
	}

	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return dataFromClient.equals("P110");
	}

}
