
package functionality.commands.level1;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


/** DW command class */
public class DW extends Command {

	public DW(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
	}

	/** Resets the display for messages received by the D command */
	@Override
	public void execute(String dataFromClient) throws IOException {
		weightData.setInline("");
		synchronized (weightData) {
			weightData.notify();
		}
		osw.write("DW A" + Cons.CRLF);
		osw.flush();
	}

	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return false;
	}

}
