
package functionality.commands.level1;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


/** MTSC D command class */
public class D extends Command {

	public D(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
	}

	/** Executes D commands from the Client
	 * 
	 * @param dataFromClient
	 *            String from client, including "" */
	@Override
	public void execute(String dataFromClient) throws IOException {
		int dLength = verifyD(dataFromClient);
		if (dLength <= 7 && dLength != -1 && (dataFromClient.indexOf('\"', 3) == (dataFromClient.length() - 1))) {
			String[] cmd = dataFromClient.split("\"");
			weightData.setInline(cmd[1]);
			synchronized (weightData) {
				weightData.notify();
			}
			osw.write("D A");
		} else osw.write("D L");
		osw.write(Cons.CRLF);
		osw.flush();
	}

	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		// not used for D command atm
		return false;
	}
	/** Checks if the length input from the D command is valid, and that is is
	 * encased by ""
	 * 
	 * @param input
	 *            the whole D input, fx D "message"
	 * @return the length of the message between " ", or -1 if input is invalid */
	private int verifyD(String input) {
		int l = input.length();
		if (input.charAt(2) == '"' && input.charAt(l - 1) == '"') return input.substring(3, l - 1).length();
		return -1;
	}
}
