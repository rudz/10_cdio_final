
package functionality.commands.level1;

import java.io.IOException;
import java.io.OutputStreamWriter;
import data.Weightdata;
import exceptions.UpperWeightLimitException;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;


/** T command class */
public class T extends Command {

	public T(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
	}

	/** Writes T S + tara to the client, and tares the weight, and updates the
	 * weight display. */
	@Override
	public void execute(String dataFromClient) throws IOException {
		try {
			if (isValid(dataFromClient)) {
				wfunc.setToTara();
				osw.write(Fix("T", wfunc.getTara()));
			} else osw.write("ES");

		} catch (UpperWeightLimitException e) {
			osw.write("T+");
		}
		osw.write(Cons.CRLF);
		osw.flush();
		synchronized (weightData) {
			weightData.notify();
		}
	}
	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return (isSimple && dataFromClient.equals("T"));
	}
}
