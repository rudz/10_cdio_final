package functionality.commands.level0;

import java.io.IOException;
import java.io.OutputStreamWriter;

import data.Weightdata;
import exceptions.UpperWeightLimitException;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;

/**
 * MTSC S command class
 */
public class S extends Command {
	
	public S(OutputStreamWriter osw, Weightdata weightData, IWeightFunction wfunc, boolean isSimple) {
		super(osw, weightData, wfunc, isSimple);
	}
	
	/**
	 * writes S S + the Netto weight to the client. The S command is checked for validity
	 * @param dataFromClient String : the D command 
	 */
	@Override
	public void execute(String dataFromClient) throws IOException {
		try {
			if(isValid(dataFromClient)){
//				wfunc.rhGetNetto(); //rhGetNetto() has "S +" exception message
				osw.write(Fix("S", wfunc.rhGetNetto()));
			} else osw.write("ES");
		} catch (UpperWeightLimitException e) {
			osw.write(e.getMessage());
		}
		osw.write(Cons.CRLF);
		osw.flush();
	}
	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return (isSimple && dataFromClient.equals("S"));
	}

}
