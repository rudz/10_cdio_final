package functionality.commands.level0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import data.Weightdata;
import functionality.abstracts.Command;
import functionality.interfaces.IWeightFunction;

/**
 * Q command class
 */
public class Q extends Command {
	BufferedReader instream;
	Socket sock;

	public Q(OutputStreamWriter osw, Weightdata weightData,
			IWeightFunction wfunc, boolean isSimple, Socket sock, BufferedReader instream) {
		super(osw, weightData, wfunc, isSimple);
		this.instream = instream;
		this.sock = sock;
	}
	
	/**
	 * closes the TCP connection
	 */
	@Override
	public void execute(String dataFromClient) throws IOException {
		instream.close();
		osw.close();
		sock.close();
	}
	@Override
	protected boolean isValid(String dataFromClient) throws IOException {
		return (isSimple && dataFromClient.equals("Q"));
	}

}
