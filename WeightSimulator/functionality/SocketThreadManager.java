package functionality;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import ui.IUImenu;
import data.WeightConnectionData;
import data.Weightdata;
import functionality.abstracts.ThreadManager;

/**
 * Waits for incoming connections, when connection occours a new thread is spawned to handle the request.
 */
public class SocketThreadManager extends ThreadManager implements Runnable {
	private ServerSocket serversocket;
	ArrayList<RequestHandler> cons = new ArrayList<>();
	int i;
	
	public SocketThreadManager(IUImenu tUImenu, Weightdata tuiData, WeightConnectionData cmData) throws IOException  {
		super(tUImenu, tuiData);
		serversocket = new ServerSocket(cmData.getListeningPort());
		i = 0;
	}
	@Override
	public void run() {
		while (true) {
			Socket connection;
			try {
				connection = serversocket.accept();
				cons.add(new RequestHandler(tUImenu, weightData, connection));
				new Thread(cons.get(i++)).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void quit() throws IOException{
		for (RequestHandler x: cons) x.quit();
		try {serversocket.close();} catch (IOException e) { System.out.println("Failure on exit"); e.printStackTrace();}
	}
}
