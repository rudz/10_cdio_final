package functionality;

import ui.IUImenu;
import ui.TUImenu;
import data.Weightdata;

/**
 * Updates menu when weightData has changed (via wait/notify)
 */
public class DataWatcher implements Runnable {

	private Weightdata Weightdata;
	private IUImenu tUImenu;

	public DataWatcher(Weightdata weightData, IUImenu tUImenu) {
		this.Weightdata = weightData;
		this.tUImenu = tUImenu;
	}
	/**
	 * Sits and awaits changes in weightData, if a notify happens then UI menu is updated.
	 */
	@Override
	public void run() {
		while (true) {
			synchronized (Weightdata) {
				try {
					Weightdata.wait();
				} catch (InterruptedException e) {
					TUImenu.println("Datawatcher stopped");
				}
			}
			tUImenu.update();
		}
	}
}
