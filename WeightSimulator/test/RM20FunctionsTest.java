package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import functionality.response.RM20Functions;

/**
 * This test's various RM20 8 command input strings, and sees if the class checking RM20 input is working
 * 
 */
public class RM20FunctionsTest {
	
	private RM20Functions RM20;
	private String[] testStringsNotApproved = {			
			"RM20       8      \"11111\"         \"2222\"        \"33333\" ddddd      ",
			"RM208      \"11111\"         \"2222\"        \"33333\" ddddd      ",
			"RM20    8      \"\"\"         \"2222\"        \"33333\"      ",
			"RM20         \"\"\"         \"2222\"       \"33333\"",
			"RM20       8  \"\"\"      a   \"2222\"        \"33333\"",
			"RM27 8 \"11111\"     \"2222\"      \"3333  3\" ",
			"RM20 8 \"11111\"aaa   aaa      \"2222\" \"3333  3\"",
			"RM20 8\\\"1\\1\\1\\1\\1\"\\\"22\\2\\2\"\\\"3\\3\\3\\3\\3\"",
		   " RM20 8 \"a\"\"b\"\"c\"",
			"RM20 70 \"\" \"\" \"\"",
			"RM20 11\"hej\"\"default\"\"test2\"",
			"RM20 4 \"\" \"\" \"\" ",
			"RM20 8 \"\"\" \"2222\" \"33333\"      a",
			"RM20 \"\""
	};
	private String[] testStringsApproved = {
			"RM20 8 \"11111\"         \"2222\"        \"3333  3\" ",
			"RM20     8     \" oooo\"\"jjjjj\"\"!!!!!\"    ",
			"RM20 8 \"hej\" \"default\" \"test\"",
			"RM20 8 \"\" \"\" \"\" ",
			"RM20 8\"hej\"\"default\"\"test2\"",
			"RM20 8 \"hej\" \"hejd\" \"hej\"",
			"RM20 8 \"test \" \"test  \" \"test   \""
	};

	@After
	public void after() {
		RM20 = null;
	}
	
	
	// Begin *NEGATIVE* tests

	@Test
	public void testAppendingChars() {
		RM20 = new RM20Functions(null,testStringsNotApproved[0]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testMissingFirstSpace() {
		RM20 = new RM20Functions(null,testStringsNotApproved[1]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testDoubleQuoteCount() {
		RM20 = new RM20Functions(null,testStringsNotApproved[2]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testMissingEight() {
		RM20 = new RM20Functions(null,testStringsNotApproved[3]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testInvalidCharBetween() {
		RM20 = new RM20Functions(null,testStringsNotApproved[4]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testInvalidIdentifier() {
		RM20 = new RM20Functions(null,testStringsNotApproved[5]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testInvalidCharInString() {
		RM20 = new RM20Functions(null,testStringsNotApproved[6]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testInvalidCharInString2() {
		RM20 = new RM20Functions(null,testStringsNotApproved[7]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testInvalidIndex() {
		RM20 = new RM20Functions(null,testStringsNotApproved[8]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void noSpaceBetweenQuotes() {
		RM20 = new RM20Functions(null,testStringsNotApproved[9]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testInvalidIndex2() {
		RM20 = new RM20Functions(null,testStringsNotApproved[10]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testInvalidRM20Command() {
		RM20 = new RM20Functions(null,testStringsNotApproved[11]);
		assertFalse(RM20.checkRM20());
	}

	@Test
	public void testInvalidCharactersAfterStrings() {
		RM20 = new RM20Functions(null,testStringsNotApproved[12]);
		assertFalse(RM20.checkRM20());
	}
	
	@Test
	public void testForDoubleQuotesOnly() {
		RM20 = new RM20Functions(null,testStringsNotApproved[13]);
		assertFalse(RM20.checkRM20());
	}

	// END NEGATIVE TESTS
	
	
	// BEGIN *POSITIVE* TESTS
	@Test
	public void testSpace0() {
		RM20 = new RM20Functions(null,testStringsApproved[0]);
		assertTrue(RM20.checkRM20());
	}

	@Test
	public void testSpace1() {
		RM20 = new RM20Functions(null,testStringsApproved[1]);
		assertTrue(RM20.checkRM20());
	}
	
	@Test
	public void testRegular() {
		RM20 = new RM20Functions(null,testStringsApproved[2]);
		assertTrue(RM20.checkRM20());
	}

	@Test
	public void testMinLen() {
		RM20 = new RM20Functions(null,testStringsApproved[3]);
		assertTrue(RM20.checkRM20());
	}

	@Test
	public void testConcised() {
		RM20 = new RM20Functions(null,testStringsApproved[4]);
		assertTrue(RM20.checkRM20());
	}
	
	@Test
	public void testValidIndex() {
		RM20 = new RM20Functions(null,testStringsApproved[5]);
		assertTrue(RM20.checkRM20());
	}
	
	@Test
	public void testContainingStrings() {
		RM20 = new RM20Functions(null,testStringsApproved[6]);
		assertTrue(RM20.checkRM20());
	}
	


	// END *POSITIVE* TESTS
}
