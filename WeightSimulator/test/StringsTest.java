package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import dtu.shared.misc.StringToolLib;

public class StringsTest {

	@Test
	public void testSpace() {
		String spaces = "     ";
		assertEquals(spaces, StringToolLib.space(spaces.length()));
	}

	@Test
	public void testReplicate() {
		String s = "kkkkkkkkkk";
		assertEquals(s, StringToolLib.replicate('k', s.length()));
	}

	@Test
	public void testInject() {
		String a = "jjjjiiiioooo";
		String b = "uuuuiooooaaa";
		String c = "y7y7";
		assertEquals(a + b, StringToolLib.inject(a, b, a.length()));
		assertEquals(a + c + b, StringToolLib.inject(a + b, c, a.length()));
		
	}

	@Test
	public void testRtrim() {
		String q = "not trimmed";
		String s = "not trimmed    ";
		String d = "not trimmed     ";
		assertEquals(q, StringToolLib.rTrim(s));
		assertEquals(q, StringToolLib.rTrim(d));
		assertEquals(q, StringToolLib.rTrim(q));
		assertEquals(q + q, StringToolLib.rTrim(d) + StringToolLib.rTrim(s));
	}

	@Test
	public void testLtrim() {
		String q = "not trimmed";
		String s = "    not trimmed";
		String d = "     not trimmed";
		assertEquals(q, StringToolLib.lTrim(s));
		assertEquals(q, StringToolLib.lTrim(d));
		assertEquals(q, StringToolLib.lTrim(q));
		assertEquals(q + q, StringToolLib.lTrim(d) + StringToolLib.lTrim(s));
	}

	@Test
	public void testRight() {
		String q = "not trimmed";
		String s = "mmed";
		String d = "trimmed";
		assertEquals(s, StringToolLib.right(q, 4));
		assertEquals(d, StringToolLib.right(q, 7));
		assertEquals(q, StringToolLib.right(q, 0));
	}

	@Test
	public void testLeft() {
		String q = "not trimmed";
		String s = "not ";
		String d = "not trim";
		assertEquals(s, StringToolLib.left(q, 4));
		assertEquals(d, StringToolLib.left(q, 8));
		assertEquals(q, StringToolLib.left(q, 0));
	}

	@Test
	public void testMid() {
		String q = "not trimmed";
		String d = "not";
		assertEquals("ot ", StringToolLib.mid(q, 2, 4));
		assertEquals(d, StringToolLib.mid(q, 1, 3));
		assertEquals(q, StringToolLib.mid(q, q.length()+1, 0));
	}
	
	@Test
	public void testToCamelCase() {
		String q = "THIS IS A TEST";
		String w = "This Is A Test";
		String e = "this Is A Test";
		assertEquals(e, StringToolLib.toCamelCase(q));
		assertEquals(e, StringToolLib.toCamelCase(w));
		assertEquals(e, StringToolLib.toCamelCase(e));
	}
	
	@Test
	public void testJoinCharArray() {
		char[] c = new char[] {'a','b','c','d','4',' ','m'};
		String s = "abcd4 m";
		assertEquals(s, StringToolLib.joinCharArray(c, ""));
	}
	
	@Test
	public void testJoinStringArray() {
		String[] s = new String[] {"a","b","c","d","4"," ","m"};
		String ss = "abcd4 m";
		assertEquals(ss, StringToolLib.joinStringArray(s, ""));
		String deli = "|";
		ss = s[0] + deli + s[1] + deli + s[2] + deli + s[3] + deli + s[4] + deli + s[5] + deli + s[6];
		assertEquals(ss, StringToolLib.joinStringArray(s, deli));
		deli = "Morten Sad På en Gren og Pep";
		ss = s[0] + deli + s[1] + deli + s[2] + deli + s[3] + deli + s[4] + deli + s[5] + deli + s[6];
		assertEquals(ss, StringToolLib.joinStringArray(s, deli));
	}
	
	@Test
	public void testCountString() {
		String a = "jjjkkkjjjkkkjjjkkkjjjlllkkiiijjiijjjjjjuuopowe";
		String s = "jjj";
		int expected = 6;
		assertEquals(expected, StringToolLib.countString(a, s));
	}
	
	@Test
	public void testHex2Dec() {
		String hex = "ff";
		int expected = 255;
		assertEquals(expected, StringToolLib.hex2Dec(hex));
		hex = "03";
		expected = 3;
		assertEquals(expected, StringToolLib.hex2Dec(hex));
		hex = "0101";
		expected = 257;
		assertEquals(expected, StringToolLib.hex2Dec(hex));
		hex = "01010F";
		expected = 65807;
		assertEquals(expected, StringToolLib.hex2Dec(hex));
		hex = "c8a3";
		expected = 51363;
		assertEquals(expected, StringToolLib.hex2Dec(hex));
	}

}
