package ui;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import com.google.gwt.thirdparty.guava.common.base.Strings;
import data.WeightConnectionData;
import data.Weightdata;
import dtu.shared.misc.StringToolLib;
import functionality.WeightFunctions;
import functionality.interfaces.IWeightFunction;
import functionality.statics.Cons;

public class TUImenu implements IUImenu {
	private Weightdata weightData;	
	private WeightConnectionData cData;
	private IWeightFunction weight;
	
	public TUImenu(Weightdata tuiData, WeightConnectionData cData) {
		this.weightData = tuiData;
		this.cData = cData;
		weight = new WeightFunctions(weightData);
	}

	@SuppressWarnings("static-access")
	@Override
	public void update() {
		for (int i=0;i<25;i++) System.out.println();
		int lineLen = 80;
		InetAddress localIP = null;
		try {
			// priority for IPv4 addresses, if it fails, try get a IPv6
			localIP = Inet4Address.getLocalHost();
			if (localIP.toString().length() == 0) localIP = Inet6Address.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		String line = StringToolLib.replicate('*', lineLen);
		System.out.println(StringToolLib.replicate('*', (lineLen - 8) >> 1) + " Data : " + StringToolLib.replicate('*', (lineLen - 8) >> 1));
		try {
		System.out.println("Netto         : " + (weight.getNetto())+ " kg" );
		} catch (Exception e) {
			System.out.println("Netto         : " + e.getMessage());
		}
		System.out.println("Instr. display: " + weightData.getInstructionDisplay());
		System.out.println("Brutto        : " + Double.toString(weightData.getBrutto())+ " kg" );
		System.out.println("Text received : " + weightData.getInline());
		System.out.println("P111          : " + weightData.getP111());
		System.out.println(StringToolLib.replicate('*', (lineLen - 36) >> 1) + " Accepted commands from client(s) : " + StringToolLib.replicate('*', (lineLen - 36) >> 1));
		System.out.println("'D \"text\"', 'DW', 'S', 'T', 'Q', 'RM20 8 \"text\" \"text\" \"text\"', 'P111 \"text\"'");
		System.out.println(line);
		System.out.println("Enter T for tara (reset weight with current weight).");
		System.out.println("Enter B for brutto (place mass on weight).");
		System.out.println("Enter Q for shutdown.");
		if (localIP != null) {
			System.out.println(StringToolLib.replicate('*', (lineLen - 18) >> 1) + " Connection info: " + StringToolLib.replicate('*', (lineLen - 18) >> 1));
			System.out.println("Host name : " + localIP.getHostName());
			System.out.println("IP:Port   : " + localIP.getHostAddress() + ":" + cData.getListeningPort());
		}
		if (!"".equals(weightData.getDefaultText()))
			System.out.println("Default Text: " + weightData.getDefaultText());	
		System.out.println(line);
		
		if(weightData.isRM20IsWaiting()){
		System.out.println("Waiting for RM20 response");
		System.out.println("Enter CANCEL to abort RM20 command");	
		}
		System.out.print ("Enter command: ");
	}

	public static void println(String s) {
		print(s + Cons.CRLF);
	}
	
	public static void print(String s) {
		System.out.print(s);
	}
}
