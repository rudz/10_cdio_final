package ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import data.WeightConnectionData;
import data.Weightdata;
import exceptions.UpperWeightLimitException;
import functionality.ArgumentReader;
import functionality.DataWatcher;
import functionality.SocketThreadManager;
import functionality.WeightFunctions;


public class GUIMenu implements IUImenu{

	private JFrame	frmWeightClientG;
	private JTextField bruttoInput;
	private JTextField MessageField_D;
	private JTextField MessageField_p111;
	private JTextField InstructionDisplay;
	private JTextField IpAddressField;
	private JTextField PortAddressField;

	static Weightdata weightdata;
	static WeightConnectionData connectionData;
	static WeightFunctions function;
	static DataWatcher w;
	private JTextField nettoDataField;
	private JTextField bruttoDataField;
	private JTextField taraDataField;
	private JTextPane InputTextPane;
	
	private JButton btnSend;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		weightdata = new Weightdata();
		connectionData = new WeightConnectionData();
		ArgumentReader argumentReader = new ArgumentReader(args, connectionData);
		argumentReader.setPort();
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					
					GUIMenu window = new GUIMenu();
					window.frmWeightClientG.setVisible(true);
					SocketThreadManager s = new SocketThreadManager(window, weightdata, connectionData);
					new Thread(s).start();
					w = new DataWatcher(weightdata, window);
					new Thread(w).start();
					
					function = new WeightFunctions(weightdata);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws UnknownHostException 
	 */
	public GUIMenu() throws UnknownHostException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws UnknownHostException 
	 */
	@SuppressWarnings("static-access")
	private void initialize() throws UnknownHostException {
		frmWeightClientG = new JFrame();
		frmWeightClientG.getContentPane().setBackground(SystemColor.menu);
		frmWeightClientG.setTitle("Weight Simulator G10");
		frmWeightClientG.setBounds(100, 100, 549, 503);
		frmWeightClientG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmWeightClientG.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("Button.background"));
		panel.setBounds(10, 11, 511, 352);
		frmWeightClientG.getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_7 = new JPanel();
		panel_7.setForeground(Color.WHITE);
		panel_7.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Weight", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_7.setBounds(10, 0, 283, 99);
		panel.add(panel_7);
		panel_7.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.controlHighlight);
		panel_1.setBounds(10, 22, 263, 66);
		panel_7.add(panel_1);
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setLayout(null);
		
		bruttoInput = new JTextField();
		bruttoInput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.setBrutto(Double.parseDouble(bruttoInput.getText()));
				update();
			}
		});
		bruttoInput.setText("0.000");
		bruttoInput.setBounds(10, 36, 65, 20);
		panel_1.add(bruttoInput);
		bruttoInput.setColumns(10);
		
		JButton buttonTara = new JButton("Tara");
		buttonTara.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					function.setToTara();
				} catch (UpperWeightLimitException e1) {
					error(e1);
				} 
				update();
			}
		});
		buttonTara.setBounds(107, 5, 65, 23);
		panel_1.add(buttonTara);
		
		JLabel lblkg_2 = new JLabel("kg");
		lblkg_2.setBounds(85, 39, 26, 14);
		panel_1.add(lblkg_2);
		
		JButton buttonReset = new JButton("Reset");
		buttonReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.setBrutto(0);
				try {
					function.setTara(0);
				} catch (UpperWeightLimitException e1) {
					e1.printStackTrace(); // shouldn't happen
				}
				update();
			}
		});
		buttonReset.setBounds(182, 5, 72, 23);
		panel_1.add(buttonReset);
		
		JLabel lblKg_1 = new JLabel("kg");
		lblKg_1.setBounds(85, 5, 26, 14);
		panel_1.add(lblKg_1);
		
		JButton buttonOK = new JButton("OK!");
		buttonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.setBrutto(Double.parseDouble(bruttoInput.getText()));
				update();
			}
		});
		buttonOK.setBounds(107, 35, 65, 23);
		panel_1.add(buttonOK);
		
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});
		btnAnnuler.setBounds(182, 35, 72, 23);
		panel_1.add(btnAnnuler);
		
		nettoDataField = new JTextField();
		nettoDataField.setEditable(false);
		nettoDataField.setText("0.000");
		nettoDataField.setColumns(10);
		nettoDataField.setBounds(10, 6, 65, 20);
		panel_1.add(nettoDataField);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Weight Buttons", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_9.setBounds(10, 101, 493, 110);
		panel.add(panel_9);
		panel_9.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.controlHighlight);
		panel_2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 21, 473, 78);
		panel_9.add(panel_2);
		panel_2.setLayout(null);
		
		JButton plus2kg = new JButton("+2kg");
		plus2kg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				function.addBrutto(2000);
				update();
			}
		});
		plus2kg.setBounds(10, 11, 65, 23);
		panel_2.add(plus2kg);
		
		JButton plus500g = new JButton("+500g");
		plus500g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				function.addBrutto(500);
				update();
			}
		});
		plus500g.setBounds(87, 11, 73, 23);
		panel_2.add(plus500g);
		
		JButton plus100g = new JButton("+100g");
		plus100g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(100);
				update();
			}
		});
		plus100g.setBounds(172, 11, 73, 23);
		panel_2.add(plus100g);
		
		JButton plus10g = new JButton("+10g");
		plus10g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(10);
				update();
			}
		});
		plus10g.setBounds(255, 11, 65, 23);
		panel_2.add(plus10g);
		
		JButton plus5g = new JButton("+5g");
		plus5g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(5);
				update();
			}
		});
		plus5g.setBounds(330, 11, 57, 23);
		panel_2.add(plus5g);
		
		JButton plus1g = new JButton("+1g");
		plus1g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(1);
				update();
			}
		});
		plus1g.setBounds(397, 11, 59, 23);
		panel_2.add(plus1g);
		
		JButton minus2kg = new JButton("-2kg");
		minus2kg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-2000);
				update();
			}
		});
		minus2kg.setBounds(10, 45, 65, 23);
		panel_2.add(minus2kg);
		
		JButton minus500g = new JButton("-500g");
		minus500g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-500);
				update();
			}
		});
		minus500g.setBounds(87, 45, 73, 23);
		panel_2.add(minus500g);
		
		JButton minus100g = new JButton("-100g");
		minus100g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-100);
				update();
			}
		});
		minus100g.setBounds(172, 45, 73, 23);
		panel_2.add(minus100g);
		
		JButton minus10g = new JButton("-10g");
		minus10g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-10);
				update();
			}
		});
		minus10g.setBounds(255, 45, 65, 23);
		panel_2.add(minus10g);
		
		JButton minus1g = new JButton("-1g");
		minus1g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-1);
				update();
			}
		});
		minus1g.setBounds(397, 45, 59, 23);
		panel_2.add(minus1g);
		
		JButton minus5g = new JButton("-5g");
		minus5g.setBounds(330, 45, 57, 23);
		panel_2.add(minus5g);
		minus5g.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				function.addBrutto(-5);
				update();
			}
		});
		
		JPanel panel_8 = new JPanel();
		panel_8.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Intern Data", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_8.setBounds(300, 0, 203, 99);
		panel.add(panel_8);
		panel_8.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(SystemColor.controlHighlight);
		panel_3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 21, 183, 67);
		panel_8.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblKg_2 = new JLabel("kg");
		lblKg_2.setBounds(150, 11, 46, 14);
		panel_3.add(lblKg_2);
		
		JLabel lblKg_3 = new JLabel("kg");
		lblKg_3.setBounds(150, 42, 46, 14);
		panel_3.add(lblKg_3);
		
		JLabel lblBrutto = new JLabel("Brutto:");
		lblBrutto.setBounds(15, 11, 46, 14);
		panel_3.add(lblBrutto);
		
		JLabel lblTara = new JLabel("Tara:");
		lblTara.setBounds(15, 42, 46, 14);
		panel_3.add(lblTara);
		
		bruttoDataField = new JTextField();
		bruttoDataField.setEditable(false);
		bruttoDataField.setText("0.000");
		bruttoDataField.setColumns(10);
		bruttoDataField.setBounds(71, 8, 65, 20);
		panel_3.add(bruttoDataField);
		
		taraDataField = new JTextField();
		taraDataField.setEditable(false);
		taraDataField.setText("0.000");
		taraDataField.setColumns(10);
		taraDataField.setBounds(71, 39, 65, 20);
		panel_3.add(taraDataField);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Display", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_10.setBounds(10, 215, 495, 137);
		panel.add(panel_10);
		panel_10.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(SystemColor.controlHighlight);
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(10, 23, 475, 103);
		panel_10.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("P111");
		lblNewLabel.setBounds(10, 73, 46, 14);
		panel_4.add(lblNewLabel);
		
		MessageField_D = new JTextField();
		MessageField_D.setEditable(false);
		MessageField_D.setBounds(66, 39, 167, 20);
		panel_4.add(MessageField_D);
		MessageField_D.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Display");
		lblNewLabel_1.setBounds(10, 14, 46, 14);
		panel_4.add(lblNewLabel_1);
		
		MessageField_p111 = new JTextField();
		MessageField_p111.setEditable(false);
		MessageField_p111.setBounds(66, 70, 167, 20);
		panel_4.add(MessageField_p111);
		MessageField_p111.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Message");
		lblNewLabel_2.setBounds(10, 39, 46, 23);
		panel_4.add(lblNewLabel_2);
		
		InstructionDisplay = new JTextField();
		InstructionDisplay.setEditable(false);
		InstructionDisplay.setBounds(66, 11, 399, 20);
		panel_4.add(InstructionDisplay);
		InstructionDisplay.setColumns(10);
		
		btnSend = new JButton("Send");
		btnSend.setEnabled(false);
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				function.setRM20Respond(InputTextPane.getText());
				GUIMenu.weightdata.setRM20IsWaiting(false);
				synchronized (GUIMenu.weightdata.getRM20Lock()){
					GUIMenu.weightdata.getRM20Lock().notify();
				}
				GUIMenu.weightdata.setPrompt("");
				GUIMenu.weightdata.setUnit("");
				update();
			}
		});
		btnSend.setBounds(379, 72, 86, 20);
		panel_4.add(btnSend);
		
		InputTextPane = new JTextPane();
		InputTextPane.setEnabled(false);
		InputTextPane.setBounds(243, 39, 222, 20);
		panel_4.add(InputTextPane);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBounds(10, 362, 511, 92);
		frmWeightClientG.getContentPane().add(panel_5);
		panel_5.setLayout(null);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Server", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_11.setBounds(10, 11, 494, 81);
		panel_5.add(panel_11);
		panel_11.setLayout(null);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(SystemColor.controlHighlight);
		panel_6.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_6.setBounds(10, 23, 474, 47);
		panel_11.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("IP:");
		lblNewLabel_3.setBounds(10, 11, 29, 14);
		panel_6.add(lblNewLabel_3);
		
		IpAddressField = new JTextField();
		IpAddressField.setEditable(false);
		IpAddressField.setBounds(30, 8, 162, 20);
		panel_6.add(IpAddressField);
		IpAddressField.setColumns(10);
		IpAddressField.setText(Inet4Address.getLocalHost().toString());
		
		JLabel lblNewLabel_4 = new JLabel("Port:");
		lblNewLabel_4.setBounds(202, 11, 29, 14);
		panel_6.add(lblNewLabel_4);
		
		PortAddressField = new JTextField();
		PortAddressField.setEditable(false);
		PortAddressField.setBounds(241, 8, 86, 20);
		panel_6.add(PortAddressField);
		PortAddressField.setColumns(10);
		PortAddressField.setText(Integer.toString(connectionData.getListeningPort()));
	}

	@Override
	public void update() {
		try {
			nettoDataField.setText(Double.toString(function.getNetto()));
		} catch (UpperWeightLimitException e) {
			nettoDataField.setText(e.getMessage());
			error(e);
		}
		bruttoInput.setText(Double.toString(function.getBrutto()));
		bruttoDataField.setText(Double.toString(function.getBrutto()));
		taraDataField.setText(Double.toString(function.getTara()));
		
		MessageField_D.setText(function.getInline());
		MessageField_p111.setText(function.getP111());
		InputTextPane.setText(function.getDefaultText());
		InstructionDisplay.setText(function.getDisplay());
		
		if (GUIMenu.weightdata.isRM20IsWaiting()){
			InputTextPane.setEnabled(true);
			btnSend.setEnabled(true);
		} else {
			InputTextPane.setEnabled(false);
			btnSend.setEnabled(false);
		}
			
	}
	
	@SuppressWarnings("static-method")
	public void error(Exception e){
		JFrame er = new JFrame();
		er.getContentPane().add(new Label(e.toString()));
		er.setBounds(300, 100, 300, 100);
		er.setVisible(true);
	}
}
