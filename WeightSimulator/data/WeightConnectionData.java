package data;
/**
 * Connection data holds the current port which the server is listening on.
 */
public class WeightConnectionData {
	private int listeningPort =8000;

	public int getListeningPort() {
		return listeningPort;
	}

	public void setListeningPort(int listeningPort) {
		this.listeningPort = listeningPort;
	}
}