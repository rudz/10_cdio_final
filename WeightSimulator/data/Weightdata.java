package data;
/**
 * Weight data DTO
 * Contains various variables used by the Weight Simulator, such as display test, brutto, tara etc.
 */
public class Weightdata {
	
	private int brutto, tara;
	private  String instructionDisplay, inline, rm20response, P111disp, defaultText, prompt, unit;
	private boolean RM20IsWaiting;
	private Object RM20Lock;
	public final static int WEIGHT_UPPER_LIMIT = 6200;
	public final static int WEIGHT_LOWER_LIMIT = 0;
	
	
	public Weightdata() {
		instructionDisplay = inline = rm20response = P111disp = defaultText = prompt = unit = "";
		tara = brutto = 0;
		RM20IsWaiting = false;
		RM20Lock = new Object();
	}
	public String getInline() {
		return inline;
	}
	public void setInline(String inline) {
		this.inline = inline;
	}
	public int getBrutto() {
		return brutto;
	}
	public void setBrutto(int brutto) {
		this.brutto = brutto;
	}
	public int getTara() {
		return tara;
	}
	public void setTara(int tara) {
		this.tara = tara;
	}
	public void setRM20Response(String response){
		this.rm20response = response;
	}
	public String getRM20Response(){
		return rm20response;
	}
	public synchronized String getInstructionDisplay() {
		return instructionDisplay;
	}
	public synchronized void setInstructionDisplay(String instructionDisplay) {
		this.instructionDisplay = instructionDisplay;
	}
	public boolean isRM20IsWaiting() {
		return RM20IsWaiting;
	}
	public void setRM20IsWaiting(boolean rM20IsWaiting) {
		RM20IsWaiting = rM20IsWaiting;
	}
	public void setP111(String input){
		P111disp = input;
	}
	public String getP111(){
		return P111disp;
	}
	public String getDefaultText() {
		return defaultText;
	}
	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}
	public Object getRM20Lock() {
		return RM20Lock;
	}
	public void setRM20Lock(Object rM20Lock) {
		RM20Lock = rM20Lock;
	}
	public void setPrompt(String text1) {
		prompt = text1;
	}
	public String getPrompt(){
		return prompt;
	}
	public void setUnit(String text3) {
		unit = text3;
	}
	public String getUnit(){
		return unit;
	}
}
