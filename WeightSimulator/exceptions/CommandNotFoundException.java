package exceptions;

/**
 * Exception class for unknown commands
 */
@SuppressWarnings("serial")
public class CommandNotFoundException extends Exception {

	public CommandNotFoundException(String msg) {
		super(msg);
	}
	
}
