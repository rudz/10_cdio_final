package exceptions;
/**
 * Exception class in case the mass goes over upper limit 
 * @author hylle
 *
 */
@SuppressWarnings("serial")
public class UpperWeightLimitException extends Exception {
	
	public UpperWeightLimitException(String msg) {
		super(msg);
	}

}
