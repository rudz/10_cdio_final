package main;

import java.io.IOException;
import java.net.BindException;
import java.util.Scanner;
import ui.IUImenu;
import ui.TUImenu;
import data.WeightConnectionData;
import data.Weightdata;
import dtu.shared.misc.StringToolLib;
import exceptions.UpperWeightLimitException;
import functionality.ArgumentReader;
import functionality.DataWatcher;
import functionality.SocketThreadManager;
import functionality.WeightFunctions;
import functionality.interfaces.IWeightFunction;

// TODO : Re-write thread handling to better be able to handle errors

public class TuiMain {

	static Weightdata Weightdata;
	static SocketThreadManager socketManager;
	static WeightConnectionData connectionData;
	static String command;
	static double brutto;
	static Scanner scan;
	static IWeightFunction function;
	static IUImenu tUImenu;
	static DataWatcher dataWatcher;
	static ArgumentReader argumentReader;

	public static void main(String[] args) {

		argumentReader = new ArgumentReader(args, connectionData);
		argumentReader.setPort();
		try {
			createSocketThreadManagerThread();
			createDataWatcherThread();
		} catch (BindException e) {
			TUImenu.println("Error while binding, the address and port is already in use.");
			quit(1);
		} catch (IOException e) {
			TUImenu.println("Unable to initialize socket interface.");
			quit(2);
		} catch (Exception e) {
			quit(0);
		}
		
		do {
			tUImenu.update();
			command = scan.nextLine();
			if (command.equals("T")) {
				try {
					function.setToTara();
				} catch (UpperWeightLimitException e) {
					TUImenu.println(e.getMessage());
				}
				TUImenu.println(Double.toString(function.getTara()));
			} else if (command.equals("B")) 		enterBrutto();
			else if (command.equals("Q"))			break;
			else if (Weightdata.isRM20IsWaiting()) 	setRM20Response(command);
		} while (!command.equals("Q"));
		quit(0);
	}
	
	/**
	 * Keeps track of changes in weightData and updates menu when changes happen.
	 */
	private static void createDataWatcherThread() {
		dataWatcher = new DataWatcher(Weightdata, tUImenu);
		new Thread(dataWatcher).start();
	}

	/***
	 * Listen's for incoming socket connections and creates new RequestHandler threads to handle each socket
	 * @throws IOException
	 */
	private static void createSocketThreadManagerThread() throws IOException {
		socketManager = new SocketThreadManager(tUImenu, Weightdata, connectionData);
		new Thread(socketManager).start();
	}

	/**
	 * Runs until a valid double has been entered.<br>
	 * Will automaticly convert , into . as some localisations use , on keypads.
	 */
	private static void enterBrutto() {
		String rawInput;
		int len;
		final String err = "Error, only numbers are accepted. Use only one '.' or ',' as decimal separator.";
		do {
			TUImenu.print("Enter brutto (max 6.2 Kg, Q to exit) > ");
			rawInput = scan.nextLine();
			if (rawInput.toUpperCase().equals("Q")) break;
			if (rawInput.contains(",")) rawInput = rawInput.replace(',', '.');
			len = rawInput.length() - StringToolLib.countString(rawInput, ".");
			if (len == rawInput.length() - 1) {
				TUImenu.println(err);
				continue;
			}
			if (!rawInput.contains(".")) rawInput += ".0";
			try {
				brutto = Double.valueOf(rawInput);
				function.setBrutto(brutto);
			} catch (NumberFormatException e) {
				TUImenu.println(err);
			}
		} while (!rawInput.toString().equals(Double.toString(brutto))); 
	}
	/***
	 * Executed if RM20 is received from telnet client, and the user of server has entered input in server console.
	 * Notifies RM20Thread, which sends RM20Response to telnet clients. 
	 * @param inputFromServerUser is the input from the user operating the server 
	 */
	private static void setRM20Response(String inputFromServerUser) {
		Weightdata.setRM20Response(inputFromServerUser);
		Weightdata.setRM20IsWaiting(false);
		synchronized (Weightdata.getRM20Lock()) {
			Weightdata.getRM20Lock().notify();
		}
	}
	/***
	 * Quits whole program, closes scanner and any sockets connected
	 */
	public static void quit(int exitCode) {
		TUImenu.println("System shutting down.");
		if (socketManager != null) {
			try {
				socketManager.quit();
			} catch (NullPointerException | IOException e) {
				TUImenu.println("Error shutting down socketManager.");
			}
		}
		if (scan != null) scan.close();
		System.exit(exitCode);
	}
	static {
		Weightdata = new Weightdata();
		connectionData = new WeightConnectionData();
		tUImenu = new TUImenu(Weightdata, connectionData);
		scan = new Scanner(System.in);
		function = new WeightFunctions(Weightdata);
	}
}
