package dtu;


public class DAOTest {
	
	//Fejlhåndtering - out of bounds - hvs ikke er et tal

	public static void main (String args[]){
	
	String ip = "192.168.1.1";	
	System.out.println(isValidIp(ip));
	}

	private static boolean isValidIp(String ip) {
		String[] splitted = new String[7];
		splitted = ip.split("\\.");
		if(splitted.length != 4) {
			return false;
		}
		for (int i = 0; i < splitted.length; i++) {
			try {
			int ipInt =Integer.parseInt(splitted[i]);
			if (ipInt > 255 || ipInt < 0) {
				System.out.println(ipInt);
				return false;
			}
			} catch (NumberFormatException e) {
				return false;
			}			
		}	
		return true;		
	}
}
