package data;

import java.util.ArrayList;
import dtu.client.service.IAdminService;
import dtu.server.dal.dao.impl.PharmaServiceImpl;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;



public class RemoteDataDAO implements IDataDAO {
	
	private IAdminService service;
	
	public RemoteDataDAO (){
		service = new PharmaServiceImpl();
	}

	@Override
	public RaavareDTO getRaavare(int id) throws DALException {
		return service.getRaavare(id);
	}

	@Override
	public RaavareBatchDTO getRaavareBatch(int id) throws DALException {
		return service.getRaavareBatch(id);
	}

	@Override
	public OperatoerDTO getOperatoerDTO(int id) throws DALException {
		return service.getOperatoer(id);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProduktBatchDTO getProduktBatchDTO(int rbid) throws DALException {
		return service.getProduktBatch(rbid);
	}

	@Override
	public ArrayList<ReceptKompDTO> getReceptKompList(int receptId) throws DALException {
		return service.getReceptKomp(receptId);
	}
	@Deprecated
	@Override
	public void updateRaavareBatchAmount(int rBatchId, double amount) throws DALException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RaavareBatchDTO getRaavareBatchDTO(int rbid) throws DALException {
		return service.getRaavareBatch(rbid);
	}

	@Override
	public void updateRaavareBatch(RaavareBatchDTO raavareBatchDTO) throws DALException {
		service.updateRaavareBatch(raavareBatchDTO);
	}

	@Override
	public void createProduktBatchKompDTO(ProduktBatchKompDTO dto) throws DALException {
		service.createProduktBatchKomp(dto);
	}

	@Override
	public void updateProduktBatch(ProduktBatchDTO produktBatchDTO) throws DALException {
		service.updateProduktBatch(produktBatchDTO);
	}

	@Override
	public ArrayList<VaegtForbindelseDTO> getForbindelseList() throws DALException {
		return service.getForbindelseList();
	}

	@Override
	public ArrayList<ReceptKompDTO> getReceptKompListWCU(int receptId, int produktBatchId) throws DALException {
		// TODO Auto-generated method stub
		return service.getReceptKompListWCU(receptId,produktBatchId);
	}
	
	
	
	
	
	
}
