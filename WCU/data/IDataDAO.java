package data;

import java.util.ArrayList;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;

public interface IDataDAO {

	/**	 
	 * Creates an IngredientDTO from data in the database, corresponding to the given Key value	 
	 * @param Ingrdnt Unique identifier for the Ingredient ex. vareNr
	 */
	public abstract RaavareDTO getRaavare(int id) throws DALException;
	
	public abstract RaavareBatchDTO getRaavareBatch(int id) throws DALException;	

	/**	 
	 * Creates an OperatorDTO from data in the database, corresponding to the given Key value	 
	 * @param Ingrdnt Unique identifier for the Ingredient ex. Operator ID
	 */
	public abstract OperatoerDTO getOperatoerDTO(int id) throws DALException;
	
	/**
	 * This method actually stores the new data in the local datafiles
	 */
	public abstract void updateRaavareBatchAmount(int rBatchId, double amount) throws DALException;
	/**
	 * Closes any connection to remote databases 	
	 */
	public abstract void close();
	
	public abstract ProduktBatchDTO getProduktBatchDTO(int rbid) throws DALException;

	public abstract ArrayList<ReceptKompDTO> getReceptKompList(int receptId) throws DALException;

	public abstract dtu.shared.dto.db.RaavareBatchDTO getRaavareBatchDTO(int rbid) throws DALException;

	public abstract void updateRaavareBatch(dtu.shared.dto.db.RaavareBatchDTO raavareBatchDTO) throws DALException;
	
	public abstract void createProduktBatchKompDTO(ProduktBatchKompDTO dto) throws DALException;

	public abstract void updateProduktBatch(dtu.shared.dto.db.ProduktBatchDTO produktBatchDTO) throws DALException;
	
	public abstract ArrayList<VaegtForbindelseDTO> getForbindelseList() throws DALException;

	public abstract ArrayList<ReceptKompDTO> getReceptKompListWCU(int receptId, int produktBatchId) throws DALException;
	
}