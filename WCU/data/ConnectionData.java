package data;

/**
 * class for holding connection data. Will be created with at startup based on main args
 * 
 */
public class ConnectionData {
	
	private String ipAddr;	
	private int port;	
	private String hostName;
	
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	

}
