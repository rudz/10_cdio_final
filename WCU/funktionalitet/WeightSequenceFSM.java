package funktionalitet;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import data.IDataDAO;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.exceptions.DALException;
import dtu.shared.misc.StringToolLib;
import funktionalitet.produktionsKommandoer.PromptConfirmation;
import funktionalitet.produktionsKommandoer.PromptIngredientAdd;
import funktionalitet.produktionsKommandoer.PromptNettoWeight;
import funktionalitet.produktionsKommandoer.PromptOprID;
import funktionalitet.produktionsKommandoer.PromptProduktBatchId;
import funktionalitet.produktionsKommandoer.PromptRaavareBatch;
import funktionalitet.produktionsKommandoer.PromptTarer;
import funktionalitet.weightManager.WeightManager;

public class WeightSequenceFSM {	

	private Step step;
	private boolean isEND;
	private static Step previousStep; // used by the INVALID state to return to the previous state, 
	// all changeState(), must set this at codestart
	private static WeightManager wM;

	private static IDataDAO DAO;
	private static String errMsg; //used for passing different error messages to the INVALID state
	private static double tare;
	private static String ingrNetto;
	private static String msg;
	private static double controlBrutto;
	private static OperatoerDTO OprDTO;
	private static RaavareBatchDTO RaavareBatchDTO;
	private static ProduktBatchDTO ProduktBatchDTO;
	private static ArrayList<ReceptKompDTO> receptKompList;
	private static ReceptKompDTO receptKomp;
	private static int i;
	private static int rbid;
	private static double amount;
	private static RaavareDTO RaavareDTO;
	private static String raavareNavn;
	private static double dListNetto;
	private static double lstNetto;



	public WeightSequenceFSM(WeightManager wM, IDataDAO DAO){

		WeightSequenceFSM.DAO = DAO;
		WeightSequenceFSM.wM = wM;
		this.step = step.START;	
	}

	private enum Step{

		/**
		 * Uses the PromptOprID to get a OperatorDTO from the database
		 */
		START{
			@Override
			public Step changeState(){
				previousStep = START;
				String msg; 
				PromptTarer.execute(wM);
				msg = PromptOprID.execute(wM);	
				try {
					OprDTO = DAO.getOperatoerDTO(Integer.parseInt(msg));
					if(OprDTO == null){
						PromptConfirmation.execute(wM, "Opr_id findes ikke");
						return START;
					}
					if (OprDTO.getLevel()==0){
						PromptConfirmation.execute(wM,"Du er fyret!");
						return START;
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return START;
				} catch (DALException e) {
					e.printStackTrace();
				}
				return STEP1;
			}
		},
		/**
		 * Prompts operator to confirms name. Upon negative response START is repeated.
		 */
		STEP1{
			@Override
			public Step changeState(){
				previousStep = STEP1;

				msg = PromptConfirmation.execute(wM, OprDTO.getName() + "? ok/nej(n)");
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return EARLYSTOP;

				case "ok":
					return STEP2;
				case "n":
				case "nej":
					return START;
				case "connection lost":
					return RECONNECT;
				default: 
					return INVALIDINPUT;
				}					
			}	
		},
		/**
		 * Prompts the operator for Product batch. And creates receptkomplist
		 */
		STEP2{
			@Override
			public Step changeState(){
				previousStep = STEP2;		
				msg = PromptProduktBatchId.execute(wM);
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				default:					
					try {
						ProduktBatchDTO = DAO.getProduktBatchDTO(Integer.parseInt(msg));
						if (ProduktBatchDTO==null){
							PromptConfirmation.execute(wM,"Pb findes ikk");
							return STEP2;
						}
						int produktBatchId = Integer.parseInt(msg);
						int receptId = ProduktBatchDTO.getReceptId();
						receptKompList = DAO.getReceptKompListWCU(receptId,produktBatchId);
					} catch (DALException e) {
						errMsg = e.getMessage();
						System.out.println("db fejl");
						System.out.println(e.toString());
						return INVALIDINPUT;
					} catch (NumberFormatException e2)	{
						System.out.println("numberfejl");
						errMsg = e2.getMessage();
						return INVALIDINPUT;
					}
					return STEP22;

				}
			}
		},
		/**
		 * checks if product batch is already used in database
		 */
		STEP22{
			@Override
			public Step changeState(){
				previousStep = STEP22;
				if (ProduktBatchDTO.getStatus()==2){
					msg = PromptConfirmation.execute(wM, "Produktbatch brugt" );
					switch(msg.toLowerCase().trim()){
					
					case "cancel":
					case "c":
						return EARLYSTOP;
					
					case "connection lost":
						return RECONNECT;

					case "ok":
						return STEP2;
					case "nej":
					case "n":
						i = 0;
						return START;
					default:			
						return INVALIDINPUT;
					}
				}
				return STEP3;
			}
		},
		/**
		 * Confirms the productbatch id from operator
		 */
		STEP3{
			@Override
			public Step changeState(){
				previousStep = STEP3;			
				msg = PromptConfirmation.execute(wM, ProduktBatchDTO.getPbId() + " pb_id ok / nej(n)" );
				switch(msg.toLowerCase().trim()){
				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				case "ok":
					return STEP33;
				case "nej":
				case "n":
					i = 0;
					return STEP2;
				default:			
					return STEP3;

				}
			}
		},
		/**
		 * Gives recept id for operator to confirm
		 */
		STEP33{
			@Override
			public Step changeState(){
				previousStep=STEP33;

				msg = PromptConfirmation.execute(wM, ProduktBatchDTO.getReceptId()+ " " + ProduktBatchDTO.getReceptName()+" ok");
				switch(msg.toLowerCase().trim()){
				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				case "ok":
					return STEP4;
				case "nej":
				case "n":
					i = 0;
					return STEP2;
				default:			
					return INVALIDINPUT;
				}			
			}

		},
		/**
		 * creates loop for all ingredients in a recept 
		 */
		STEP4{
			@Override
			public Step changeState(){
				previousStep = STEP4;	
				if (i < receptKompList.size()){
					receptKomp = receptKompList.get(i++);
					return STEP5;
				} else
					i=0;
					return DONE;
			}
		},
		/**
		 * Prompts the operator to add tare to the weight and confirm. 
		 * Upon confirmation weight will be taraed by WCU, and tare is stored.
		 */
		STEP5{
			@Override
			public Step changeState(){
				previousStep = STEP5;
				PromptTarer.execute(wM);
				msg = PromptConfirmation.execute(wM, "Placer afvejningbeholder");
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					String tmp = PromptTarer.execute(wM);
					tare = Double.parseDouble(tmp.toLowerCase().replace(" kg", "").trim()) + tare;
					
					return STEP6;
				case "connection lost":
					return RECONNECT;
				default:			
					return INVALIDINPUT;
				}
			}
		},
		/**
		 * Ask the operator to confirm the ingredient id and name
		 */
			STEP6{

			@Override
			public Step changeState(){
				previousStep= STEP6;				
				try {
					RaavareDTO = DAO.getRaavare(receptKomp.getRaavareId());
				} catch (DALException e) {
					e.printStackTrace();
				}
				raavareNavn = RaavareDTO.getRaavareNavn();

				//Max msg length 24 chars
				if(raavareNavn.length() >= 10) {
					raavareNavn = raavareNavn.substring(0, 9);
				}
				msg = PromptConfirmation.execute(wM, raavareNavn +" "+receptKomp.getRaavareId()+ "? ok"); 

				switch(msg.toLowerCase().trim()){
				case "cancel":
				case "c":					
					return DONE;

				case "ok":
					return STEP7;
				case "connection lost":
					return RECONNECT;
				case "n":
				case "nej":
					return STEP2;

				default:			
					return INVALIDINPUT;
				}


			}
		},

		/**
		 * Prompts the operator to add material to the weight and confirm. 
		 */
		STEP7{
			@Override
			public Step changeState(){
				previousStep= STEP7;				
				msg = PromptRaavareBatch.execute(wM);
				System.out.println("Indtast raavarebatch id sendt");
				
				try {
					rbid = Integer.valueOf(msg);
				} catch (Exception e) {
					System.out.println("Going back to previous step");
					return previousStep;
				}
				
				
				try {
					System.out.println("Henter råvare batch");
					RaavareBatchDTO = DAO.getRaavareBatch(rbid);
					System.out.println("Råvare batch hentet");
				} catch (DALException e) {
					msg = PromptConfirmation.execute(wM, rbid + " ej fundet");
					return STEP7;
				}
				
				System.out.println("Tester om der er nok i raavarebatch");
				if (RaavareBatchDTO.getMaengde() < receptKomp.getNomNetto()){
					msg = PromptConfirmation.execute(wM, "Ikke nok i Rb");
					return STEP7;
				}
				
				System.out.println("Tester om raavare id er ens i råvaredto og råvarebatch");
				if(RaavareDTO.getRaavareId() != RaavareBatchDTO.getRaavareId()) {
					msg = PromptConfirmation.execute(wM, "Forkert raavarebatch");
					return STEP7;
				}
				
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":					
					return DONE;
				case "connection lost":
					return RECONNECT;

				default:			
					return STEP8;
				}
			}
		},

		/**
		 * Prompts the operator to add material to the weight and confirm. 
		 */
		STEP8{
			@Override
			public Step changeState() {
				previousStep= STEP8;
				double vaegt = receptKomp.getNomNetto()-amount;
				vaegt *= 1000;
				int i = (int)(vaegt);
				vaegt = i;
				vaegt = vaegt /1000;
				
				String besked = StringToolLib.lSet(raavareNavn, 6, ' ') +" " + Double.toString(vaegt)+" Kg";
				System.out.println(besked);
				msg = PromptIngredientAdd.execute(wM, besked);
				System.out.println("MESSAGE -> " + msg);

				switch(msg.toLowerCase().trim()){
				case "cancel":
				case "c":
					return DONE;
				case "ok":
					return STEP9;
				case "connection lost":
					return RECONNECT;
				default:
					return INVALIDINPUT;
				}
			}
		},
		
		/**
		 * Calculates tolerance and makes sure operator has measured the right amount
		 * and logs the used amount in raavareBatch
		 */
		STEP9 {
			@Override
			public Step changeState(){
				previousStep= STEP9;				
				msg = PromptNettoWeight.execute(wM, null);
				System.out.println(msg);
				
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":					
					return DONE;
				case "connection lost":
					return RECONNECT;

				default:
					ingrNetto = msg;
					amount = Double.parseDouble(ingrNetto.toLowerCase().replace(" kg", "").trim()) + amount;
					double nomNetto = receptKomp.getNomNetto();
					double tolerance = receptKomp.getTolerance();					

					double highTol = nomNetto + nomNetto * (tolerance/100);
					double lowTol = nomNetto - nomNetto * (tolerance/100);
					
					if(lowTol > amount) {
						PromptTarer.execute(wM);
						return STEP99;
					}
					
					if(highTol <= amount) {
						return TOLERANCEERROR;
					}

					try {
						RaavareBatchDTO = DAO.getRaavareBatchDTO(rbid);
						double newamount = RaavareBatchDTO.getMaengde()-amount;
						RaavareBatchDTO.setMaengde(newamount);
						DAO.updateRaavareBatch(RaavareBatchDTO);
					} catch (DALException e) {
						e.printStackTrace();
					}
					PromptTarer.execute(wM);
					return STEP10;
				}
			}
		},
		
		STEP99{
			@Override
			public Step changeState(){
				previousStep= STEP99;
					
				msg = PromptConfirmation.execute(wM, "Fjern materiel fra vaegt");

				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					msg = PromptNettoWeight.execute(wM, null);					
					dListNetto = Double.parseDouble(msg.toLowerCase().replace(" kg", "").trim()) + dListNetto;
					PromptTarer.execute(wM);
					msg = PromptNettoWeight.execute(wM, null);
					if (Double.parseDouble(msg.toLowerCase().replace(" kg", "").trim())!=0)
						return STEP99;
					
					return STEP5;
				case "connection lost":
					return RECONNECT;


				default:			
					return INVALIDINPUT;
				}

			}
		},
		/**
		 * Prompts the operator to remove all material from the weight and confirm.
		 */
		STEP10{
			@Override
			public Step changeState(){
				previousStep = STEP10;
				msg = PromptConfirmation.execute(wM, "Fjern materiel fra vaegt");

				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					return STEP11;
				case "connection lost":
					return RECONNECT;


				default:			
					return INVALIDINPUT;
				}


			}
		},
		/**
		 * This step assumes that the process has been executed succesfully from the operator's point of view. 
		 * Logs tara, netto, and performs Brutto control. 
		 * If Brutto control detects a deviation of more than 0.005 Kg FSM will proceed to BRUTTOERROR. 
		 */
		STEP11{

			@Override
			public Step changeState(){
				previousStep = STEP11;	

				msg = PromptNettoWeight.execute(wM, null);
				dListNetto = Double.parseDouble(msg.toLowerCase().replace(" kg", "").trim()) + dListNetto;
				System.out.println(dListNetto+"dL");
				controlBrutto = BruttoCalc(tare, amount, dListNetto);		
				System.out.println(controlBrutto+"cb");

				int remainder = BruttoControl(controlBrutto);		

				
				if (remainder <= 5 && remainder >= -5){
					ProduktBatchKompDTO produktBatchKomp = new ProduktBatchKompDTO(ProduktBatchDTO.getPbId(),
						rbid,
						tare,
						amount,
						OprDTO.getId());			
					try {
						DAO.createProduktBatchKompDTO(produktBatchKomp);
						ProduktBatchDTO.setStatus(1);
						DAO.updateProduktBatch(ProduktBatchDTO);
					} catch (DALException e) {
						System.out.println("Fejl ved oprettelse af ProduktBatchKomp i db");
						e.printStackTrace();
					}
					amount = 0; //Reset  amount
					tare = 0;
					dListNetto = 0;
	
					return STEP4;
					
				} else return BRUTTOERROR;

			}

		},
		/**
		 * Instructs the Operator to destroy material and confirm. Process is logged
		 */
		BRUTTOERROR{
			@Override
			public Step changeState(){
				previousStep= BRUTTOERROR;				
				msg = PromptConfirmation.execute(wM, "Fjern "+controlBrutto+ " kg");
				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					dListNetto=Double.parseDouble(PromptNettoWeight.execute(wM, null).toLowerCase().replace(" kg", "").trim());
					return STEP11;
					
				case "connection lost":
					return RECONNECT;

				default:			
					return INVALIDINPUT;
				}				
			}
		},
		/**
		 * Instructs the operator to get the tolerance right compared to the nomnetto
		 */
		TOLERANCEERROR{
			@Override
			Step changeState() {
				previousStep= TOLERANCEERROR;				

				msg = PromptConfirmation.execute(wM, "Uden for tolerance");

				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					return STEP8;
				case "connection lost":
					return RECONNECT;

				default:			
					return INVALIDINPUT;
				}				
			}

		},
		/**
		 * State that handles non critical unknown input from operator.
		 */
		INVALIDINPUT{
			@Override
			public Step changeState(){
				try {
					wM.sendD(errMsg);
				} catch (IOException e) {

					e.printStackTrace();
				}

				return previousStep;
			}
		},
		/**
		 * Step that prompts if the operator will continue weighing material. If so it repeats STEP1, 
		 * for the convenience of the operator.
		 */
		DONE{
			@Override
			public Step changeState(){
				previousStep = DONE;

				try {
					ProduktBatchDTO.setStatus(2);
					DAO.updateProduktBatch(ProduktBatchDTO);
				} catch (DALException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				msg = PromptConfirmation.execute(wM, "Fortsæt produktion?");

				switch(msg.toLowerCase().trim()){

				case "cancel":
				case "c":
				case "n":
				case "nej":
					return START;

				case "ok":					
					return STEP1;

				case "connection lost":
					return RECONNECT;


				default:			
					return INVALIDINPUT;
				}				
			}
		},
		
		/**
		 * Close everything in case of fatal error
		 */
		FATALERROR{
			@Override
			public Step changeState(){
				DAO.close();
				try {
					wM.close();
				} catch (IOException e) {
				}
				return FATALERROR;
			}
		},
		/**
		 * Step used for Stopping the WCU externally
		 */
		END{
			@Override
			public Step changeState(){
				//				log.close();
				return END;
			}
		},
		/**
		 * Reconnects the WCU to the weight when disconnected.
		 */
		RECONNECT{
			@Override
			public Step changeState(){
				try {					
					wM.reConnect();
				} catch (UnknownHostException e) {
					System.out.println("Ingen forbindelse fundet");					
					return previousStep;
				} catch (IOException e) {
					System.out.println("Ingen forbindelse fundet");				
					return previousStep;
				}
				return previousStep;
			}
		},
		/**
		 * If the process is stop early returns to start and sets status=0
		 */
		EARLYSTOP{
			@Override
			public Step changeState(){
				ProduktBatchDTO.setStatus(0);
				return START;
			}

		};
		abstract Step changeState();
		/**
		 * experimental extracted switch
		 * @param msg
		 * @param nextStep
		 * @return
		 */
		@Deprecated
		private static Step startSwitch(String msg, Step nextStep){

			OperatoerDTO OprDTO;
			switch(msg.toLowerCase().trim()){

			case "cancel":
			case "c":
				return START;
			case "connection lost":
				return RECONNECT;
			case "stop":
				return END;

			default:					
				try{
					OprDTO = DAO.getOperatoerDTO(Integer.parseInt(msg));

				}catch (DALException e){
					errMsg = e.getMessage();
					return INVALIDINPUT;
				}
				try {
					wM.sendD(OprDTO.getName() + " er aktiv");
				} catch (IOException e) {						
					e.printStackTrace();
					return INVALIDINPUT;
				}
				return nextStep;
			}
		}

	}

	public void run(){

		System.out.println("running changestate on: " + step.toString());
		this.step = this.step.changeState(); 

	}

	public boolean isEND() {
		return isEND;
	}

	public boolean isDone(){
		if(this.step.equals(Step.FATALERROR) || this.step.equals(Step.END)) return true;
		else return false;		
	}
	/**
	 * Performs simple addition
	 * @param tare
	 * @param ingrNetto
	 * @param lstNetto
	 * @return
	 */
	private static double BruttoCalc(double tare, double ingrNetto, double lstNetto){
		System.out.println(tare + " tare");
		System.out.println(ingrNetto + " ingrNetto");
		System.out.println(lstNetto + " lstNetto");
		return (tare+ingrNetto)+lstNetto;
	}
	/**
	 * 
	 * @param brutto calculated difference in brutto
	 * @return int representation of input converted to gram. Returns zero for values less than one gram.
	 * However the first miligram is checked to see if it is 5 or greater. 
	 */
	private static int BruttoControl(double brutto){
		String conv = String.valueOf(brutto).replace("E-", "");
		if(conv.length() <= 5){				
			conv = conv.concat("000"); // adds zeroes to "stabilize" the double, so it can be converted safely without index out of bounds errors
		}
		String convd = conv.replace(".", "").substring(0, 4);
		int remainder = Integer.parseInt(convd);
		String roundUpOrDown = conv.substring(5, 6);


		if(Integer.parseInt(roundUpOrDown) >= 5) remainder += 1;
		return remainder; 

	}
}
