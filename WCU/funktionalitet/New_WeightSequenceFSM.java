
package funktionalitet;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import com.ibm.icu.text.DecimalFormat;
import data.IDataDAO;
import dtu.shared.dto.db.OperatoerDTO;
import dtu.shared.dto.db.ProduktBatchDTO;
import dtu.shared.dto.db.ProduktBatchKompDTO;
import dtu.shared.dto.db.RaavareBatchDTO;
import dtu.shared.dto.db.RaavareDTO;
import dtu.shared.dto.db.ReceptKompDTO;
import dtu.shared.exceptions.DALException;
import dtu.shared.misc.DK;
import functionality.WeightFunctions;
import funktionalitet.produktionsKommandoer.PromptConfirmation;
import funktionalitet.produktionsKommandoer.PromptNettoWeight;
import funktionalitet.produktionsKommandoer.PromptOprID;
import funktionalitet.produktionsKommandoer.PromptProduktBatchId;
import funktionalitet.produktionsKommandoer.PromptRM49;
import funktionalitet.produktionsKommandoer.PromptRaavareBatch;
import funktionalitet.produktionsKommandoer.PromptTarer;
import funktionalitet.produktionsKommandoer.Weight;
import funktionalitet.weightManager.WeightManager;


public class New_WeightSequenceFSM {

	private Step							step;
	private boolean							isEND;
	private static Step						previousStep;		// used by the
																// INVALID state
																// to return to
																// the previous
																// state,
	// all changeState(), must set this at codestart
	private static WeightManager			wM;

	private static IDataDAO					DAO;
	private static String					errMsg;			// used for
																// passing
																// different
																// error
																// messages to
																// the INVALID
																// state
	private static double					tare;
	private static String					ingrNetto;
	private static String					msg;
	private static double					controlBrutto;
	private static OperatoerDTO				OprDTO;
	private static RaavareBatchDTO			RaavareBatchDTO;
	private static ProduktBatchDTO			ProduktBatchDTO;
	private static ArrayList<ReceptKompDTO>	receptKompList;
	private static ReceptKompDTO			receptKomp;
	private static int						i;
	private static int						rbid;
	private static double					amount;
	private static RaavareDTO				RaavareDTO;
	private static String					raavareNavn;
	private static double					dListNetto;
	private static double					lstNetto;
	private static double					newtargetNetto;
	private static double					actualNetto;
	private static int						amountToWeigh;

	public New_WeightSequenceFSM(WeightManager wM, IDataDAO DAO) {

		New_WeightSequenceFSM.DAO = DAO;
		New_WeightSequenceFSM.wM = wM;
		this.step = step.START;
	}

	private enum Step {

		/** Uses the PromptOprID to get a OperatorDTO from the database */
		START {

			@Override
			public Step changeState() {
				previousStep = START;
				String msg;
				// TILFØJ VED BRUG AF DEN RIGTIGE VÆGT!
				try {
					wM.sendS();

					wM.sendS();
					System.out.println(wM.readLine());

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return OPRIDFIRST;
			}

		},
		OPRIDFIRST {

			@Override
			public Step changeState() {
				PromptTarer.execute(wM);

				msg = PromptOprID.execute(wM);
				System.out.println("Besked ved oprID: " + msg);
				System.out.println("i     =     " + i);
				try {
					OprDTO = DAO.getOperatoerDTO(Integer.parseInt(msg));
					if (OprDTO == null) {
						PromptRM49.execute(wM, "Operatoer id findes ikke!");
						return OPRIDFIRST;
					}
					if (OprDTO.getLevel() == 0) {
						PromptRM49.execute(wM, "Operatoer id er inaktivt!");
						return OPRIDFIRST;
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return OPRIDFIRST;
				} catch (DALException e) {
					e.printStackTrace();
				}
				return STEP1;
			}
		},
		/** Prompts operator to confirms name. Upon negative response START is
		 * repeated. */
		STEP1 {

			@Override
			public Step changeState() {
				previousStep = STEP1;

				msg = PromptRM49.execute(wM, "Operatoer navn " + DK.toDkASE(OprDTO.getName()) + " tryk OK hvis det er rigigt");
				System.out.println(msg);
				System.out.println("Operatør bekræftelse sendt");

				switch (msg) {

				case "cancel":
				case "c":
					return EARLYSTOP;

				case "RM49 A 1":
				case "ok":
					return STEP2;
				case "RM49 A 2":
				case "nej":
				case "n":
					return OPRIDFIRST;
				case "connection lost":
					return RECONNECT;
				default:
					return INVALIDINPUT;
				}
			}
		},
		/** Prompts the operator for Product batch. And creates receptkomplist */
		STEP2 {

			@Override
			public Step changeState() {
				previousStep = STEP2;
				msg = PromptProduktBatchId.execute(wM);
				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				default:
					try {
						ProduktBatchDTO = DAO.getProduktBatchDTO(Integer.parseInt(msg));
						if (ProduktBatchDTO == null) {
							PromptRM49.execute(wM, "Produktbatch findes ikke!");
							return STEP2;
						}
						int produktBatchId = Integer.parseInt(msg);
						int receptId = ProduktBatchDTO.getReceptId();
						receptKompList = DAO.getReceptKompListWCU(receptId, produktBatchId);
//						System.out.println("____________________________RECEPTLIST_______________________");
//						try {
//							wM.sendRM52AndRM53(receptKompList);
//							System.out.println("____________________________RECEPTLIST_____DONE__________________");
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//							return previousStep;
//						}
					} catch (DALException e) {
						errMsg = e.getMessage();
						System.out.println("db fejl");
						System.out.println(e.toString());
						return previousStep;
					} catch (NumberFormatException e2) {
						System.out.println("numberfejl");
						errMsg = e2.getMessage();
						return INVALIDINPUT;
					}
					return STEP22;

				}
			}
		},
		/** checks if product batch is already used in database */
		STEP22 {

			@Override
			public Step changeState() {
				previousStep = STEP22;
				if (ProduktBatchDTO.getStatus() == 2) {
					msg = PromptRM49.execute(wM, "Produktbatchen er alleredet afsluttet");
					switch (msg) {

					case "cancel":
					case "c":
						return EARLYSTOP;

					case "connection lost":
						return RECONNECT;

					case "RM49 A 1":
					case "ok":
						return STEP2;
					case "RM49 A 2":
					case "n":
						i = 0;
						return START;
					default:
						return INVALIDINPUT;
					}
				}
				return STEP3;
			}
		},
		/** Confirms the productbatch id from operator */
		STEP3 {

			@Override
			public Step changeState() {
				previousStep = STEP3;

				msg = PromptRM49.execute(wM, "Kontroller dette produktbatch id er " + ProduktBatchDTO.getPbId());

				switch (msg) {
				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				case "ok":
				case "RM49 A 1":
					return STEP33;
				case "RM49 A 2":
				case "nej":
				case "n":
					i = 0;
					return STEP2;
				default:
					return STEP3;

				}
			}
		},
		/** Gives recept id for operator to confirm */
		STEP33 {

			@Override
			public Step changeState() {
				previousStep = STEP33;

				msg = PromptRM49.execute(wM, "Produktion af " + DK.toDkASE(ProduktBatchDTO.getReceptName()) + " recept id " + ProduktBatchDTO.getReceptId());

				switch (msg) {
				case "cancel":
				case "c":
					return EARLYSTOP;
				case "connection lost":
					return RECONNECT;

				case "ok":
				case "RM49 A 1":
					return STEP4;
				case "RM49 A 2":
				case "nej":
				case "n":
					i = 0;
					return STEP2;
				default:
					return INVALIDINPUT;
				}
			}

		},
		/** creates loop for all ingredients in a recept */
		STEP4 {

			@Override
			public Step changeState() {
				previousStep = STEP4;
				if (i < receptKompList.size()) {
					receptKomp = receptKompList.get(i++);
					
					return STEP5;
				}
				i = 0;
				return DONE;
			}
		},
		/** Prompts the operator to add tare to the weight and confirm. Upon
		 * confirmation weight will be taraed by WCU, and tare is stored. */
		STEP5 {

			@Override
			public Step changeState() {
				previousStep = STEP5;
				PromptTarer.execute(wM);
				msg = PromptRM49.execute(wM, "Placer afvejningsbeholder");

				switch (msg) {

				case "cancel":
				case "c":
					return DONE;

				case "ok":
				case "RM49 A 1":
					String tmp = PromptTarer.execute(wM);
					tare = Double.parseDouble(tmp.toLowerCase().replace(" kg", "").trim());
					if (tare <= 0.0) {
						String confirmationQuestion = PromptRM49.execute(wM, "Du har ikke placeret en beholder tryk ok!");
						if (confirmationQuestion.equals("RM49 A 1") || confirmationQuestion.equals("ok")) return STEP5;
						tare = 0;
						return STEP5;
					}
					if (Weight.isFirst) return STEP6;
					return STEP8;
				case "RM49 A 2":
				case "n":
					//Resets displays
					try {
						Weight.abortSequence = true;
						wM.RM30("OK");
						wM.RM39_1();
						Weight.emptyWeight(wM, "Sekvens afbrudt, fjern alt!", tare, receptKomp.getNomNetto());
						Weight.abortSequence = false;
						Weight.clearLocals();
						wM.RM39_0();
						wM.resetP();
						i = 0;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return OPRIDFIRST;
				case "connection lost":
					return RECONNECT;
				default:
					return INVALIDINPUT;
				}
			}
		},
		/** Ask the operator to confirm the ingredient id and name */
		STEP6 {

			@Override
			public Step changeState() {
				previousStep = STEP6;

				try {
					RaavareDTO = DAO.getRaavare(receptKomp.getRaavareId());
				} catch (DALException e) {
					e.printStackTrace();
				}
				raavareNavn = DK.toDkASE(RaavareDTO.getRaavareNavn());

				// Max msg length 24 chars
				if (raavareNavn.length() >= 10) {
					raavareNavn = raavareNavn.substring(0, 9);
				}

				msg = PromptRM49.execute(wM, "Raavare id " + receptKomp.getRaavareId() + " raavare navn " + raavareNavn);

				switch (msg) {
				case "cancel":
				case "c":
					return DONE;

				case "ok":
				case "RM49 A 1":
					return STEP7;
				case "connection lost":
					return RECONNECT;
				case "n":
				case "nej":
				case "RM49 A 2":
					return STEP2;

				default:
					return INVALIDINPUT;
				}

			}
		},

		/** Prompts the operator to add material to the weight and confirm. */
		STEP7 {

			@Override
			public Step changeState() {
				previousStep = STEP7;
				msg = PromptRaavareBatch.execute(wM);
				System.out.println("Indtast raavarebatch id sendt");

				try {
					rbid = Integer.valueOf(msg);
				} catch (Exception e) {
					System.out.println("Exception ved raavarebatch id");
					return STEP5;
				}

				try {
					System.out.println("Henter råvare batch");
					RaavareBatchDTO = DAO.getRaavareBatch(rbid);
					System.out.println("Råvare batch hentet");
				} catch (DALException e) {
					msg = PromptRM49.execute(wM, "rb id " + rbid + " findes ikke");
					return STEP7;
				}

				System.out.println("Tester om der er nok i raavarebatch");
				if (RaavareBatchDTO.getMaengde() < receptKomp.getNomNetto()) {
					msg = PromptRM49.execute(wM, "Der er ikke nok i RB");
					return STEP7;
				}

				System.out.println("Tester om raavare id er ens i råvaredto og råvarebatch");
				if (RaavareDTO.getRaavareId() != RaavareBatchDTO.getRaavareId()) {
					msg = PromptRM49.execute(wM, "Forkert raavarebatch");
					return STEP7;
				}

				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return DONE;
				case "connection lost":
					return RECONNECT;

				default:
					return STEP8;
				}
			}
		},

		/** Prompts the operator to add material to the weight and confirm. */
		STEP8 {

			@Override
			public Step changeState() {
				previousStep = STEP8;
				newtargetNetto = receptKomp.getNomNetto();
				int amountToWeighInKg = Weight.toGram(newtargetNetto, 0.001)[0];
				amountToWeigh = (5000 % amountToWeighInKg);
				System.out.println("Mængden som skal afvejes efter modullus: " + amountToWeigh);
				System.out.println("Nom netto: " + receptKomp.getNomNetto() + " " + receptKomp.getTolerance());
				DecimalFormat df = new DecimalFormat("#.###");
				try {
					String amountToAfterFormat = df.format( (receptKomp.getNomNetto() - Weight.weightProgress));
					msg = Weight.execute(wM, "Afvej " + amountToAfterFormat + " kg " + raavareNavn, receptKomp.getNomNetto(),
							receptKomp.getTolerance(), tare, amountToWeigh, true);
//					if (msg.startsWith("F") || msg.startsWith("C") || msg.startsWith("c")) {
//						// If the weighing fails still needs to update database.
//						System.out.println("Fejl fra Weight execute: " + msg);
//						String remove = msg.substring(1);
//						double removeamount = Double.parseDouble(msg);
//						try {
//							RaavareBatchDTO = DAO.getRaavareBatchDTO(rbid);
//							double newamount = RaavareBatchDTO.getMaengde() - removeamount;
//							RaavareBatchDTO.setMaengde(newamount);
//							DAO.updateRaavareBatch(RaavareBatchDTO);
//						} catch (DALException e) {
//							e.printStackTrace();
//						}
//						msg = msg.substring(0, 1);
//
//					}

					 if (Weight.isOkay) {
						System.out.println("-------------------------Vægten er ok -----------------------------");
						amount = Weight.weightProgress;
						tare = Weight.taraInProgress;
						System.out.println("Weight variabler");
						System.out.println("Weight in progress "+Weight.weightProgress);
						System.out.println("TaraInProgress " + Weight.taraInProgress);
						System.out.println("isFirst: " + Weight.isFirst);
						System.out.println("isOkay: " + Weight.isOkay);
						System.out.println("-----------------------Clearing locals---------------------");
						Weight.clearLocals();
						System.out.println("Weight variabler");
						System.out.println("Weight in progress "+Weight.weightProgress);
						System.out.println("TaraInProgress " + Weight.taraInProgress);
						System.out.println("isFirst: " + Weight.isFirst);
						System.out.println("isOkay: " + Weight.isOkay);
						return STEP9;
					} else if(!Weight.isOkay && (Weight.weightProgress - (receptKomp.getNomNetto()*(receptKomp.getTolerance()/100))) < receptKomp.getNomNetto()) {
						System.out.println("====================DER SKAL VEJES MERE==================");
						System.out.println("Weight in progress:      " + Weight.weightProgress);
						System.out.println("Tara in progress:       " + Weight.taraInProgress );
						System.out.println("Recept Nom Netto:  " + receptKomp.getNomNetto());
						System.out.println("isFirst:   " + Weight.isFirst);
						System.out.println("isOkay:  " + Weight.isOkay);
						return STEP5;
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return RECONNECT;
				}
				System.out.println("MESSAGE -> " + msg);

				switch (msg.toLowerCase().trim()) {
				case "cancel":
				case "c":
					// FAIL
					return START;
				case "connection lost":
					return RECONNECT;
				default:
					// SETTER NOM NETTO DETTE SKAL GEMMES I DB
					amount = Double.parseDouble(msg);
					return STEP9;
				}
			}
		},

		/** Calculates tolerance and makes sure operator has measured the right
		 * amount and logs the used amount in raavareBatch */
		STEP9 {

			@Override
			public Step changeState() {
				previousStep = STEP9;

				try {
					System.out.println("RaavareBatch GEMMES!");
					RaavareBatchDTO = DAO.getRaavareBatchDTO(rbid);
					double newamount = RaavareBatchDTO.getMaengde() - amount;
					RaavareBatchDTO.setMaengde(newamount);
					DAO.updateRaavareBatch(RaavareBatchDTO);
					System.out.println("RaavareBatch Gemt");
				} catch (DALException e) {
					e.printStackTrace();
				}
				System.out.println("Tarare vægten");
				PromptTarer.execute(wM);
				System.out.println("Gem Produktbatch");
				return GEMPRODUKTBATCHKOMP;

			}
		},

		STEP99 {

			@Override
			public Step changeState() {
				previousStep = STEP99;

				msg = PromptConfirmation.execute(wM, "Fjern materiel fra vaegt");

				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					msg = PromptNettoWeight.execute(wM, null);
					dListNetto = Double.parseDouble(msg.toLowerCase().replace(" kg", "").trim()) + dListNetto;
					PromptTarer.execute(wM);
					msg = PromptNettoWeight.execute(wM, null);
					if (Double.parseDouble(msg.toLowerCase().replace(" kg", "").trim()) != 0) return STEP99;

					return STEP5;
				case "connection lost":
					return RECONNECT;

				default:
					return INVALIDINPUT;
				}

			}
		},
		/** Prompts the operator to remove all material from the weight and
		 * confirm. */
		STEP10 {

			@Override
			public Step changeState() {
				previousStep = STEP10;
				msg = PromptConfirmation.execute(wM, "Fjern materiel fra vaegt");

				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					return GEMPRODUKTBATCHKOMP;
				case "connection lost":
					return RECONNECT;

				default:
					return INVALIDINPUT;
				}

			}
		},
		/** This step assumes that the process has been executed succesfully from
		 * the operator's point of view. Logs tara, netto, and performs Brutto
		 * control. If Brutto control detects a deviation of more than 0.005 Kg
		 * FSM will proceed to BRUTTOERROR. */
		STEP11 {

			@Override
			public Step changeState() {
				previousStep = STEP11;

				msg = PromptNettoWeight.execute(wM, null);
				dListNetto = Double.parseDouble(msg.toLowerCase().replace(" Kg", "").trim()) + dListNetto;
				System.out.println(dListNetto + "dL");
				controlBrutto = BruttoCalc(tare, amount, dListNetto);
				System.out.println(controlBrutto + "cb");

				int remainder = BruttoControl(controlBrutto);

				if (remainder <= 5 && remainder >= -5) {
					ProduktBatchKompDTO produktBatchKomp = new ProduktBatchKompDTO(ProduktBatchDTO.getPbId(), rbid, tare, amount, OprDTO.getId());
					try {
						DAO.createProduktBatchKompDTO(produktBatchKomp);
						ProduktBatchDTO.setStatus(1);
						DAO.updateProduktBatch(ProduktBatchDTO);
					} catch (DALException e) {
						System.out.println("Fejl ved oprettelse af ProduktBatchKomp i db");
						e.printStackTrace();
					}
					amount = 0; // Reset amount
					tare = 0;
					dListNetto = 0;

					return STEP4;

				} else return BRUTTOERROR;

			}

		},
		GEMPRODUKTBATCHKOMP {

			@Override
			Step changeState() {
				previousStep = GEMPRODUKTBATCHKOMP;
				System.out.println("Instancieres produktbatchkompDTO");
				System.out.println("Råvarebatch id: " + rbid);
				System.out.println("Tara: " + tare);
				System.out.println("NomNetto/amount: " + amount);
				System.out.println("Opr_id: " + OprDTO.getId());
				ProduktBatchKompDTO produktBatchKomp = new ProduktBatchKompDTO(ProduktBatchDTO.getPbId(), rbid, tare, amount, OprDTO.getId());
				try {
					System.out.println("Gemmer DTO til DB");
					DAO.createProduktBatchKompDTO(produktBatchKomp);
					System.out.println("Ændre status til 1");
					ProduktBatchDTO.setStatus(1);
					DAO.updateProduktBatch(ProduktBatchDTO);
					System.out.println("Status update gemt");
				} catch (DALException e) {
					System.out.println("Fejl ved oprettelse af ProduktBatchKomp i db");
					e.printStackTrace();
					return previousStep;
				}

				System.out.println("Sætter amount og tare til 0");
				amount = 0;
				tare = 0;

				return STEP4;
			}

		},
		/** Instructs the Operator to destroy material and confirm. Process is
		 * logged */
		BRUTTOERROR {

			@Override
			public Step changeState() {
				previousStep = BRUTTOERROR;
				msg = PromptConfirmation.execute(wM, "Fjern " + controlBrutto + " Kg");
				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					dListNetto = Double.parseDouble(PromptNettoWeight.execute(wM, null).toLowerCase().replace(" Kg", "").trim());
					return STEP11;

				case "connection lost":
					return RECONNECT;

				default:
					return INVALIDINPUT;
				}
			}
		},
		/** Instructs the operator to get the tolerance right compared to the
		 * nomnetto */
		TOLERANCEERROR {

			@Override
			Step changeState() {
				previousStep = TOLERANCEERROR;

				msg = PromptConfirmation.execute(wM, "Uden for tolerance");

				switch (msg.toLowerCase().trim()) {

				case "cancel":
				case "c":
					return DONE;

				case "ok":
					return STEP8;
				case "connection lost":
					return RECONNECT;

				default:
					return INVALIDINPUT;
				}
			}

		},
		/** State that handles non critical unknown input from operator. */
		INVALIDINPUT {

			@Override
			public Step changeState() {
				try {
					wM.sendP111("INPUTFEJL");
				} catch (IOException e) {

					e.printStackTrace();
				}

				return previousStep;
			}
		},
		/** Step that prompts if the operator will continue weighing material. If
		 * so it repeats STEP1, for the convenience of the operator. */
		DONE {

			@Override
			public Step changeState() {
				previousStep = DONE;

				try {
					ProduktBatchDTO.setStatus(2);
					DAO.updateProduktBatch(ProduktBatchDTO);
				} catch (DALException e) {

					e.printStackTrace();
				}

				msg = PromptRM49.execute(wM, "Fortsaet produktion?");

				switch (msg) {

				case "cancel":
				case "c":
				case "n":
				case "nej":
				case "RM49 A 1":
					return STEP1;

				case "RM49 A 2":
				case "ok":
					return OPRIDFIRST;

				case "connection lost":
					return RECONNECT;

				default:
					return START;
					// return INVALIDINPUT;
				}
			}
		},

		/** Close everything in case of fatal error */
		FATALERROR {

			@Override
			public Step changeState() {
				DAO.close();
				try {
					wM.close();
				} catch (IOException e) {
					System.out.println("Fatal fejl!!! IO Fejl ved lukning af WeightManager..");
				}
				return FATALERROR;
			}
		},
		/** Step used for Stopping the WCU externally */
		END {

			@Override
			public Step changeState() {
				// log.close();
				return END;
			}
		},
		/** Reconnects the WCU to the weight when disconnected. */
		RECONNECT {

			@Override
			public Step changeState() {
				try {
					wM.reConnect();
				} catch (IOException e) {
					System.out.println("Ingen forbindelse fundet");
					return previousStep;
				}
				return previousStep;
			}
		},
		/** If the process is stop early returns to start and sets status=0 */
		EARLYSTOP {

			@Override
			public Step changeState() {
//				ProduktBatchDTO.setStatus(0);
				i =  0;
				return OPRIDFIRST;
			}

		};

		abstract Step changeState();
		/** experimental extracted switch
		 * 
		 * @param msg
		 * @param nextStep
		 * @return */
		private static Step startSwitch(String msg, Step nextStep) {

			OperatoerDTO OprDTO1;
			switch (msg.toLowerCase().trim()) {

			case "cancel":
			case "c":
				return START;
			case "connection lost":
				return RECONNECT;
			case "stop":
				return END;

			default:
				try {
					OprDTO1 = DAO.getOperatoerDTO(Integer.parseInt(msg));
				} catch (DALException e) {
					errMsg = e.getMessage();
					return INVALIDINPUT;
				}
				try {
					wM.sendD(DK.toDkASE(OprDTO1.getName()) + " er aktiv");
				} catch (IOException e) {
					e.printStackTrace();
					return INVALIDINPUT;
				}
				return nextStep;
			}
		}

	}

	public void run() {
		System.out.println("running changestate on: " + step.toString());
		this.step = this.step.changeState();
	}

	public boolean isEND() {
		return isEND;
	}

	public boolean isDone() {
		if (this.step.equals(Step.FATALERROR) || this.step.equals(Step.END)) return true;
		return false;
	}
	/** Performs simple addition
	 * 
	 * @param tare1
	 * @param ingrNetto1
	 * @param lstNetto1
	 * @return */
	private static double BruttoCalc(double tare1, double ingrNetto1, double lstNetto1) {
		System.out.println(tare1 + " tare");
		System.out.println(ingrNetto1 + " ingrNetto");
		System.out.println(lstNetto1 + " lstNetto");
		return (tare1 + ingrNetto1) + lstNetto1;
	}
	/** @param brutto
	 *            calculated difference in brutto
	 * @return int representation of input converted to gram. Returns zero for
	 *         values less than one gram. However the first miligram is checked
	 *         to see if it is 5 or greater. */
	private static int BruttoControl(double brutto) {
		String conv = String.valueOf(brutto).replace("E-", "");
		if (conv.length() <= 5) {
			conv = conv.concat("000"); // adds zeroes to "stabilize" the double,
										// so it can be converted safely without
										// index out of bounds errors
		}
		String convd = conv.replace(".", "").substring(0, 4);
		int remainder = Integer.parseInt(convd);
		String roundUpOrDown = conv.substring(5, 6);

		if (Integer.parseInt(roundUpOrDown) >= 5) remainder += 1;
		return remainder;

	}
}
