package funktionalitet.weightManager;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import dtu.shared.dto.db.ReceptKompDTO;
import funktionalitet.TimeOut;

public class WeightManager {

	private WeightConnection con;
	private String lastIP;
	private int lastPort;

	/**

	 * Standard constructor
	 * @param con unconnected weightConnection
	 * @throws NullPointerException 
	 * @throws UnknownHostException 
	 */
	public WeightManager(WeightConnection con){
		this.con = con;		
	}	

	/**
	 * Used for setting last known Ip and port
	 */
	private void backUpCon(){
		try {
			lastIP = con.getIp();
			lastPort = con.getPort();
		} catch (NullPointerException e) {			
			e.printStackTrace();
		}
	}

	/**
	 * Sends RM20 8 command with 3 strings as input.
	 * Method waits for the operator response, and only prints the response input
	 * @param first message to operator
	 * @param second default message
	 * @param third unit ex. &3 if operator is to reply with numbers
	 * @return Operator response
	 * @throws IOException
	 */
	public String sendRM20(String first, String second, String third) throws IOException, NullPointerException{
		String f = "\"" + first + "\"";
		String s = "\"" + second + "\"";
		String t = "\"" + third + "\"";		
		con.write("RM20 8 " + 
				" " + f + 
				" " + s + 
				" " + t);
		String out = con.read().replace("RM20 A ", "").replaceAll("\"", "");
		out = out.replace("RM20 C", "C").replaceAll("\"", "");
		return out;	
	}

	public String sendS() throws IOException{
		return con.write("S");
	}

	public String sendT() throws IOException{
		return con.write("T");

	}

	public String sendD(String command) throws IOException{
		return con.write("D " + "\""+ command +"\"");
	}

	public String sendDW() throws IOException{
		return con.write("DW");
	}

	public void sendQ() throws IOException{
		con.write("Q");
	}

	public void sendP111(String command) throws IOException{
		con.write("P111 "+ "\""+ command +"\"");
	}

	/**
	 * Method that tries to reconnect to last know connection
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void reConnect() throws UnknownHostException, IOException{
		con = new WeightConnection().connect(lastIP, lastPort);
	}
	/**
	 * 
	 * @param IpAddr target Ip address
	 * @param port target process port
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void connect(String IpAddr, int port) throws UnknownHostException, IOException {
		con.connect(IpAddr, port);
		backUpCon();
	}

	public boolean isConnected(){
		return con.isConnected();		
	}

	public String getIp(){
		return lastIP;
	}
	public int getPort(){
		return lastPort;
	}

	public void close() throws IOException{
		con.close();
	}

	public String sendP121(int target, int h, int h2) throws IOException {
		return con.write("P121 " + target + " g " + h + " g "+ h2 + " g");
	}

	public String RM30(String string, String string2) throws IOException {
		return con.write("RM30 \""+string + "\" \"" +string2 +"\"");
	}
	public String RM30(String string) throws IOException {
		return con.write("RM30 \""+string + "\"");
	}
	

	public String RM39_1() throws IOException{
		return con.write("RM39 1");
	}
	public String RM39_0() throws IOException{
		// Should be RM39 A 
		return con.write("RM39 0");
	}
	
	public String sendRM49(String command) throws IOException{
		//Answer from weight can be 
		//RM49 A 1 <--- OK
		//RM49 A 2 <--- Cancel
		con.write("RM49 4 \"" + command + "\"");
		return readLine();
	}
	
	public void resetP() throws IOException {
		con.write("P110");
		con.write("P120");
	}
	
	public String sendP120() throws IOException{
		return con.write("P120");
	}

	public String readLine() throws IOException {
		return con.read();
	}	
	
	public String sendRM52AndRM53(ArrayList<ReceptKompDTO> list) throws IOException{
		con.write("RM54 1");
		String command = null;
		for(ReceptKompDTO komp : list){
			
		}
			
//		con.write("RM53 1 \"Abort\" \"" + komp.getRaavareId() + " \\x09 " + komp.getNomNetto());
		con.write("RM53 1 \"\"");
		return null;
	}
	
	public String sendI2() throws IOException{
		return con.write("I2");
	}

}
