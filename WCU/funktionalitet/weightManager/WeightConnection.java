package funktionalitet.weightManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import funktionalitet.TimeOut;


public class WeightConnection {
	
	private Socket weightSock;
	private BufferedReader in;
	private BufferedWriter out;
	private String CLFR = "\r\n";
	private String DEFAULTIP = "127.0.0.1";
	private int DEFAULTPORT = 8000;	
	
	/**
	 * Standard connecter
	 * @param ipAddr
	 * @param port
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public WeightConnection connect(String ipAddr, int port) throws UnknownHostException, IOException{
		weightSock = new Socket(ipAddr, port);
		in = new BufferedReader(new InputStreamReader(weightSock.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(weightSock.getOutputStream()));
		return this;
	}	
	/**
	 * Overloaded connecter, that connects using DEFAULTIP and DEFAULTPORT
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void connect() throws UnknownHostException, IOException{
		connect(DEFAULTIP, DEFAULTPORT);
	}
	
	/**
	 * used for writing commands to the weight
	 * @param input Command input to the weight, \n\r will be added by this method
	 * @return the response code from the weight
	 * @throws IOException
	 */
	public String write(String input) throws IOException{
		out.write(input+CLFR);
		out.flush();
		return in.readLine();
	}
	/**
	 * timed write
	 * @param input
	 * @param time
	 * @return
	 * @throws IOException
	 */
	public String write(String input, int time) throws IOException,TimeOut{
		out.write(input+CLFR);
		out.flush();
		int i = 0;
		do{
			try {Thread.sleep(time);} catch (InterruptedException e) {e.printStackTrace();}
			time *= 2;
		} while (!in.ready() || i++ < 4);
		if (in.ready()) return in.readLine();
		throw new TimeOut();
	}
	
	
	/**
	 * used for reading the return RM20 weight "answer"
	 * @return
	 * @throws IOException
	 */
	public String read() throws IOException{
		return in.readLine();
	}
	
	public int getPort() throws NullPointerException{
		return weightSock.getPort();		
		
	}
	
	public String getIp() throws NullPointerException{
		return weightSock.getInetAddress().getHostAddress();		
	}
	
	public void close() throws IOException{
		in.close();
		out.close();
		weightSock.close();
	}
	
	public boolean isConnected(){
		if (weightSock != null)	
			return weightSock.isConnected();
		return false;
	}

}