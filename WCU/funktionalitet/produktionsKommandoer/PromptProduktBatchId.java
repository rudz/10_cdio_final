package funktionalitet.produktionsKommandoer;

import funktionalitet.weightManager.WeightManager;

public class PromptProduktBatchId extends Command{

	public static String execute(WeightManager wM) {
		try
		{
			return wM.sendRM20("Indtast produktbatch id", "", "&3pb_id");
		}
		catch(Exception e){
			return "connection lost";
		}				
	}
}
