package funktionalitet.produktionsKommandoer;

import java.io.IOException;
import dtu.shared.misc.StringToolLib;
import funktionalitet.weightManager.WeightManager;

public class PromptConfirmation{
	
	public static String execute(WeightManager wM, String command) {
		
		try {
			System.out.println(command);
			return wM.sendRM20(command, "Ok", "?");
		} catch (IOException e) {
			return "connection lost";
		}

	}
}
