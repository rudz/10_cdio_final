package funktionalitet.produktionsKommandoer;

import dtu.shared.misc.StringToolLib;
import funktionalitet.weightManager.WeightManager;

public class PromptIngredientAdd {
	
	public static String execute(WeightManager wM, String Command){
		try {
			
			return wM.sendRM20("Afvej " + Command, "Ok", "");
		} catch ( Exception e ) {
			return "connection lost";
		} 
	}
}