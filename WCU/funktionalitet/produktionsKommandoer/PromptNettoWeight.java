package funktionalitet.produktionsKommandoer;

import java.io.IOException;
import dtu.shared.misc.StringToolLib;
import funktionalitet.TimeOut;
import funktionalitet.weightManager.WeightManager;

public class PromptNettoWeight{
	public static String execute(WeightManager wM,String command) {
			try {				
				String msg = wM.sendS().replace("S S", "").trim();
				if (msg.equals("S +")) return error(wM, command);
				return msg;
			} catch (IOException e) {
				return "connection lost";
			} 
	}
	
//	public static String exe(WeightManager wM, String command){
//		wM.sendP111(command);
//	}

	private static String error(WeightManager wM,String command) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return execute(wM,command);
	}
}
