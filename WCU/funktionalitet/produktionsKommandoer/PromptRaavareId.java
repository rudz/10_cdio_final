package funktionalitet.produktionsKommandoer;

import java.io.IOException;
import dtu.shared.misc.StringToolLib;
import funktionalitet.weightManager.WeightManager;

public class PromptRaavareId {

	public static String execute(WeightManager wM,String command) {
		try
		{
			return wM.sendRM20("Raavare id: " + command, "Ok", "?");			
		}
		catch(IOException e){
			return "connection lost";
		}				
	}
}