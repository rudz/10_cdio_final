package funktionalitet.produktionsKommandoer;

import funktionalitet.weightManager.WeightManager;

public class PromptOprID {
	
	public static String execute(WeightManager wM) {
		try
		{
			return wM.sendRM20("Indtast Operatoer id: ", "", "&3 ID");
		}
		catch(Exception e){
			return "connection lost";
		}				
	}
}