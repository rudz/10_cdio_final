
package funktionalitet.produktionsKommandoer;

import java.io.IOException;
import org.apache.tools.tar.TarEntry;
import com.ibm.icu.text.DecimalFormat;
import funktionalitet.weightManager.WeightManager;


public class Weight {

	public static double	lowerLimit;
	public static double	upperLimit;
	public static double	weightProgress;
	public static double	taraInProgress;
	public static boolean	isOkay;
	public static boolean	isFirst;
	public static boolean	abortSequence;

	static {
		clearLocals();
	}

	public static boolean sqeduele(double target) {
		return false;
	}

	public static void clearLocals() {
		lowerLimit = 0.0d;
		upperLimit = 0.0d;
		weightProgress = 0.0d;
		taraInProgress = 0.0d;
		isOkay = false;
		isFirst = true;
		abortSequence = false;
	}

	public static String execute(WeightManager wM, String command, double target, double tol, Double tara, int amountToWeigh, boolean lasttime)
			throws IOException {
		if (isFirst) isFirst = false;
		wM.sendP111(command);
		System.out.println(command + "Beskeden som sendes");
		double start = target * (tol / 100);
		lowerLimit = target - start;
		upperLimit = target + start;
		int[] t = toGram(target - weightProgress, tol);
		wM.sendP121(t[0], t[1], t[1]);
		System.out.println(t[0] + " Target vægt for receptkomponenten ");
		System.out.println(t[1] + " Tolerance for receptkomponenten");
		String msg = wM.RM30("Tjek", "Abort");
		System.out.println("Besked fra vægt: " + msg);
		if (msg.equals("RM30 B")) {
			msg = wM.RM39_1();

			System.out.println("Besked efter RM39_1 skulle være = RM39 A: " + msg);
			msg = wM.readLine();

			// //REMOVE WHEN USEING WEIGHT
			// msg = wM.readLine();
			// System.out.println("NOT ON WEIGHT: "+msg);
			// if(msg.startsWith("RM20 ")){
			// msg = msg.substring(8,16);
			// }
			// //REMOVE WHEN USING WEIGHT

			System.out.println("OPR indtaster: " + msg);

		} else return "C";
		if ("RM30 A 1".equals(msg) || "RM30 C 1".equals(msg)) {
			System.out.println("Starter tol Control");
			// AMOUNT TO WEIGH CHANGED
			msg = tolControl(wM.sendS(), target, tol, tara, wM);
			System.out.println("Besked for tolerance control tjek: " + msg);

			if (msg.equals("F")) {
				String failmsg = "F";
				while (failmsg.equals("F")) {
					failmsg = fail(wM, "Uden for tolerance", target, tol, tara);
				}
				// Fail method should be good.
				msg = failmsg;
			}
			System.out.println("Fjern beholder fra vægten sendes");
			String emptyControl = emptyWeight(wM, "Fjern beholder", tara, target);

			System.out.println("Beskeden for fjern vægt: " + emptyControl);

			if (emptyControl.equals("C")) return "C";

			System.out.println("Sender P110 og P120 til vægten");
			wM.resetP();
			System.out.println("Færdig med at send P110 og P120 ");
			System.out.println("Sender RM39 0 for at slukke til vægten");
			wM.RM39_0();
			System.out.println("Weight.execute besked: " + msg);
			return msg;
		}
		return "C";
	}

	public static int[] toGram(double target, double tol) {

		System.out.println("To gram konvertering");
		System.out.println("Target før: " + target + " kg");
		System.out.println("Tolerance før: " + tol + " %");
		target = 1000 * target;

		int[] t = new int[2];
		t[0] = (int) target;

		tol = target * tol / 100;
		t[1] = (int) tol;
		if (t[1] < 1) t[1] = 1;
		System.out.println("Nu returneres target og tolerance");
		System.out.println("Target = " + t[0] + " g");
		System.out.println("Tolerance i " + t[1] + " g");
		return t;
	}

	private static String fail(WeightManager wM, String command, double target, double tol, double tara) throws IOException {
		wM.sendP111(command);
		System.out.println("FEJL besked er sendt: " + command);
		String msg = wM.readLine();
		System.out.println("Besked efter fail: " + msg + "");
		if ("RM30 A 1".equals(msg)) {
			msg = tolControl(wM.sendS(), target, tol, tara, wM);
			return msg;
		}
		System.out.println("Nu returneres fra fail(): F");
		return "C";
	}

	private static String tolControl(String msg, double target, double tol, double tara, WeightManager wM) throws IOException {
		// TODO OPS -------- Kg eller kg på vægt -------------
		System.out.println("Tolerance control message  " + msg);
		msg = msg.replace("S S", "").replace(" kg", "");
		msg = msg.trim();
		System.out.println("tolControl af" + msg);
		double amount = Double.parseDouble(msg);
		System.out.println("Weight in progress: " + weightProgress);
		System.out.println("Amount: ");
		if ((amount + weightProgress) < (target - (target * (tol / 100)))) {
			System.out.println("Tolerance tjek er stadig langt under!");
			weightProgress += amount;
			System.out.println("Weight in progress før convertering " + weightProgress);
			DecimalFormat df = new DecimalFormat("#.###");
			weightProgress = Double.parseDouble(df.format(weightProgress));
			System.out.println("Weight in progress after " + weightProgress);
			int newtarget[] = toGram(target - weightProgress, tol);
			
			System.out.println("SVAR FRA P120: ---------------"+wM.sendP120() + "----------------");
			System.out.println("NEWTARGET ------------------------------->" + newtarget[0]);
			System.out.println("SVAR fra P121 " + wM.sendP121(newtarget[0], newtarget[1], newtarget[1]));
		
			taraInProgress += tara;
			

			return msg;
		}
		isOkay = (weightProgress + amount) < upperLimit && (weightProgress + amount) > lowerLimit;
		System.out.println("tolerance kontrol >> : so far = " + weightProgress + " | Er målet nået? = " + isOkay);
		if (isOkay) {
			weightProgress += amount;
			taraInProgress += tara;
			// if (amount < target + target * tol && amount > target - target *
			// tol) {
			// System.out.println("If tjek: " + amount + "<" + target + " + " +
			// target + " * " + tol + " && " + amount + " " + " > " + target +
			// " - "
			// + target + " * " + tol);
			System.out.println("Nu returneres msg efter tolerance tjek alt godt");
			return msg;
		}

		System.out.println("tolControl fejlet returner: F");
		return "F";
	}

	public static String emptyWeight(WeightManager wM, String command, double tara, double target) throws IOException {
		wM.sendP111(command);
		String msg = wM.readLine();
		System.out.println(msg);

		// DecimalFormat df = new DecimalFormat("#.###");
		// if(!isOkay){
		// wM.sendP111("Der mangler " + df.format(target - weightProgress) +
		// " kg");
		// System.out.println(wM.readLine());
		// }
		// //REMOVE WHEN USEING WEIGHT
		// msg = wM.readLine();
		// System.out.println(msg);
		// msg = wM.readLine();
		// System.out.println(msg);
		// if(msg.startsWith("RM20 ")){
		// msg = msg.substring(8,16);
		// }
		// //REMOVE WHEN USEING WEIGHT
		System.out.println("Besked ved empty tjek: " + msg);
		if ("RM30 A 1".equals(msg) || "RM30 C 1".equals(msg) || "RM30 A 2".equals(msg) || "RM30 C 2".equals(msg)) {
			System.out.println("Sender S ");
			msg = wM.sendS();
			System.out.println("S er Sendt");
			// TODO OPS -------- Kg eller kg på vægt -------------
			msg = msg.replace("S S", "").replace(" kg", "");
			msg = msg.trim();
			System.out.println("Beskeden efter trimning: " + msg);
			double taracontrol = Double.parseDouble(msg);
			System.out.println("Nu tjekkes tara er den samme som før");
			System.out.println(taracontrol + " == " + tara);
			// TODO OPS ------- CHANGING == to !=
			if (abortSequence) {
				msg = PromptTarer.execute(wM);
				msg = msg.replace("T S", "").replace(" kg", "");
				msg = msg.trim();
				if (Double.parseDouble(msg) != 0) {
					System.out.println("VED ABORT Fejl ved fjerning");
					emptyWeight(wM, "Fjern alt.", 0, 0);
				}
			} else if (taracontrol + tara != 0) {
				emptyWeight(wM, command, tara, target);
			}
			System.out.println("Nu tarares vægten");
			PromptTarer.execute(wM);
			System.out.println("Der burde ikke være noget på vægten nu");
			return "OK";
		}
		System.out.println("EmptyWeight afbryd!");
		return "C";

	}

}
