package funktionalitet.produktionsKommandoer;

import funktionalitet.weightManager.WeightManager;

public class PromptRaavareBatch extends Command{

	public static String execute(WeightManager wM) {
		try
		{
			return wM.sendRM20("Indtast Raavare Batch id", "", "&3rb_id");
		}
		catch(Exception e){
			return "connection lost";
		}				
	}
}
