package funktionalitet.produktionsKommandoer;

import funktionalitet.weightManager.WeightManager;


public class PromptWeightMaxLimit {
	
	public static String execute(WeightManager wM) {
		try
		{
			String msg = wM.sendI2();
			msg = msg.replace("S S", "").replace(" kg", "");
			msg = msg.trim();
			return msg;
		}
		catch(Exception e){
			return "connection lost";
		}				
	}

}
