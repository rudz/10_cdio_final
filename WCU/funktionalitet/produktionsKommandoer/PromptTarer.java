package funktionalitet.produktionsKommandoer;

import java.io.IOException;
import funktionalitet.weightManager.WeightManager;

public class PromptTarer {
	
	public static String execute( WeightManager wM) {
		String msg;
		try {
			msg = wM.sendT();
			msg = msg.replace("T S", "").trim();
			return msg;
		} catch (IOException e) {
			return "connection lost";
		}
	}
}
