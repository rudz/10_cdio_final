package main;

import java.util.Scanner;
/**
 * Main menu, that is displayed to the System admin when WCU is in test/setup mode
 *
 */
public class WCUMenu {	
	
	Scanner scan;
	
	public WCUMenu() {
		// TODO Auto-generated constructor stub
	}

	public void menu(){
		scan = new Scanner(System.in);
		String choice = null;
		do{
			printMenu();
			choice = scan.next();
			
			switch(choice.toUpperCase()){
			case "Q":				
				scan.close();
				break;
			case "C":
				connect();
				break;			
			default:
				System.out.println("Ugyldigt input");
			}

		}while(!(choice.equals("Q")));
		System.out.println("WCU stoppet");
		
	}
	
	private void printMenu(){
		System.out.println(" ****  Welcome plz choose\t\t  ****"
							+ "\n ****  Q: Afslut WCU\t\t\t  ****"
							+ "\n ****  C: Forbind vægt og start WCU\t  ****"
							);
	}
	private void connect(){
		System.out.println(" ****  Indtast IP Adresse  ****");
		String IP = scan.next();
		System.out.println(" ****  Indtast Portnummer  ****");
		int port = scan.nextInt();
		ProductionController mC = new ProductionController(IP, port);
		new Thread(mC).start();
	}
}
