package main;

public class ArgumentReader {

	String arg;
	String Ip;
	String port;

	boolean IpArg = false;
	boolean portArg = false;
/**
 * Parses given arguments using any valid inputs to set booleans
 * @param args
 * @return index:Data <br>0: IpAddress <br>1: Port <br>2: RemoteUserName<br> 3: RemotePassword<br> 4: remoteDTBIpaddres 
 */
	public String[] parseArguments(String[] args){
		String[] list = new String[5];
		for (int i = 0; i < args.length; i++) {
			arg = args[i];

			switch(arg.toLowerCase()){
				case "-ip":
					list[0] = args[i+1];				
					IpArg = true;
					break;
				case "-p":
					list[1] = args[i+1];				
					portArg = true;
					break;			
			}			
		}		
		return list;
	}	
	
	public boolean isIpArg() {
		return IpArg;
	}

	public boolean isPortArg() {
		return portArg;
	}
}