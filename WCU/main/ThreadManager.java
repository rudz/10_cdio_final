package main;

import java.util.ArrayList;
import java.util.Iterator;
import data.RemoteDataDAO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;

/***
 * ThreadManager for starting new WCU threads from ip addresses in MySQL db, or a single thread from ip/port argument
 * 
 * Can also close all connections
 */
public class ThreadManager {

	ArrayList<VaegtForbindelseDTO> vaegtForbindelser = new ArrayList<>();
	RemoteDataDAO remoteDataDAO = new RemoteDataDAO();
	ArrayList<ProductionController> pcList = new ArrayList<>();
	
	/**
	 * Connects to MySQL and starts threads according to the amount of connections
	 */
	public void createThreadsFromDb() {
		try {
			vaegtForbindelser = remoteDataDAO.getForbindelseList();
		} catch (DALException e) {
			e.printStackTrace();
		}
		
		for (Iterator<VaegtForbindelseDTO> vf = vaegtForbindelser.iterator(); vf.hasNext();) {
			VaegtForbindelseDTO vaegtForbindelseDTO = vf.next();
			String ip = vaegtForbindelseDTO.getIp();
			int port = vaegtForbindelseDTO.getPort();
			
			ProductionController mC = new ProductionController(new RemoteDataDAO(), ip, port);
			new Thread(mC).start();	
			pcList.add(mC);
		}
			
	}

	/**
	 * Starts a new single thread
	 * @param ip
	 * @param port
	 */
	public void startThread(String ip, int port) {
		ProductionController mC = new ProductionController(new RemoteDataDAO(), ip, port);
		new Thread(mC).start();		
		pcList.add(mC);
	}
	
	
	/**
	 * Closes connections
	 */
	public void closeConnection() {
		for (Iterator<ProductionController> iterator = pcList.iterator(); iterator.hasNext();) {
			ProductionController productionController = iterator.next();
			productionController.quit();
		}
	}

		
}
