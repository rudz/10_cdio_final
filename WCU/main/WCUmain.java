package main;

import java.util.ArrayList;
import java.util.Iterator;
import data.RemoteDataDAO;
import dtu.shared.dto.db.VaegtForbindelseDTO;
import dtu.shared.exceptions.DALException;

public class WCUmain {

	public static void main(String[] args){
		ArgumentReader argParser = new ArgumentReader();
		String[] startData = argParser.parseArguments(args);
		ArrayList<VaegtForbindelseDTO> vaegtForbindelser = new ArrayList<>();
		RemoteDataDAO remoteDataDAO = new RemoteDataDAO();
		ThreadManager threadManager = new ThreadManager();
		
		//Laver tråd ud fra argument - kun 1
		if (argParser.IpArg && argParser.portArg) {	
			threadManager.startThread(startData[0], Integer.parseInt(startData[1]));
		}
		
		//Indhent liste af ip'er fra MySQL og opret tråde derefter
		if(!(argParser.IpArg && argParser.portArg)) {
			
			threadManager.createThreadsFromDb();			
		}
		WCUMenu mm = new WCUMenu();
		mm.menu();
		
	}	
}