package main;

import java.io.IOException;
import java.net.UnknownHostException;
import data.ConnectionData;
import data.IDataDAO;
import data.RemoteDataDAO;
import funktionalitet.New_WeightSequenceFSM;
import funktionalitet.weightManager.WeightConnection;
import funktionalitet.weightManager.WeightManager;

public class ProductionController implements Runnable{
	
	private WeightManager wM = new WeightManager(new WeightConnection());
	private New_WeightSequenceFSM wSFSM;
	private IDataDAO DAO;
	private ConnectionData conData;
	
	public ProductionController(IDataDAO DAO, String IpAddr, int port){
		this(IpAddr, port);
		this.DAO = DAO;		
	}
	
	public ProductionController(String ipAddr, int port){
		this.DAO = new RemoteDataDAO();
		conData = new ConnectionData();
		conData.setIpAddr(ipAddr);
		conData.setPort(port);
	}
	
	
	public ProductionController(RemoteDataDAO remoteDataDAO) {
		this.DAO = new RemoteDataDAO();
	}

	/**
	 * Checks if connection to weIght has been established. If so a weightManager is instantiates, 
	 * and the production sequence is initiated 
	 */
	public void runProduction(){		
		if(wM != null && wM.isConnected()){
			wSFSM = new New_WeightSequenceFSM(wM, DAO);
			if(!runSequence()) // runSequence returns false if WCU is closed externally fx at the weight
				try {
					wM.close();
					DAO.close();
				} catch (IOException e) {
					System.out.println("WCU er blevet lukket externt. Resourcer ikke lukket korrekt");
				}			
		} else System.out.println("Ingen vægt forbundet");		
	}
	
public boolean runSequence() {
		
		while(!wSFSM.isDone()){
			wSFSM.run();
		}	
		if(wSFSM.isEND()) return false;
		return true;
	}
	
	
	/**
	 * closes all system resources
	 */
	public void quit(){
		try {
			System.out.println("lukker forbindelse til " + wM.getIp() + ":" + wM.getPort());
			wM.close();
			DAO.close();
		} catch (NullPointerException e) {
			System.out.println("Ingen forbindelse fundet");
		} catch (IOException e){
			System.out.println("Forsøg på at lukke forbindelse fejlet");
		}
	}

	@Override
	public void run() {
		try {
			wM.connect(conData.getIpAddr(), conData.getPort());			
			System.out.println("Forbundet til vægt på port: " + wM.getPort() + ", Ip: " + wM.getIp());
			runProduction();
		} catch (UnknownHostException e) {
			System.out.println("Ingen forbindelse til vægt opnået");
		} catch (IOException e) {
			System.out.println("Ingen forbindelse til vægt opnået");
		}
	};
}